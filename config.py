import os

# --- FLASK BASIC SETTINGS --- #
basedir = os.path.abspath(os.path.dirname(__file__))
SECRET_KEY = 'lkjsfkjsdflkiwhsnsdsdsdkjsdfhhsldndksdkjhsdfjkhsfnnf'
DEBUG = False

# --- POSTGRES USER AND PASSWORD --- #

PG_PASSWORD = os.environ.get('WGRAS_WRITER_PWD')
PG_USER = 'wgraswriter'

# --- FLASK SQLALCHEMY EXTENSION --- #
SQLALCHEMY_DATABASE_URI = 'postgresql://{}:{}@wgras-postgres/wgrasusers'.format(PG_USER, PG_PASSWORD)
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = False

# --- FLASK MONGOALCHEMY EXTENSION --- #
MONGOALCHEMY_DATABASE = 'temp_layers'
MONGOALCHEMY_SERVER = 'wgras-mongo'

# --- FLASK UPLOAD EXTENSION --- #
ALLOWED_EXTENSIONS = {'zip', 'tif', 'csv'}
UPLOAD_FOLDER = os.path.join('uploads')
UPLOAD_GEOTIFF_FOLDER = os.path.join(UPLOAD_FOLDER, 'geotiff')
UPLOAD_CSV_FOLDER = os.path.join(UPLOAD_FOLDER, 'csv')
UPLOAD_ZIPPEDSHP_FOLDER = os.path.join(UPLOAD_FOLDER, 'zippedshp')
UPLOAD_UNZIPPEDSHP_FOLDER = os.path.join(UPLOAD_FOLDER, 'unzippedshp')

# --- DOWNLOAD (used for downloading files from GeoServer)--- #
DOWNLOAD_FOLDER = os.path.join('downloads')
DOWNLOAD_TMP_RASTER = os.path.join(DOWNLOAD_FOLDER, 'tempraster')
DOWNLOADS_RESULT_FOLDER = os.path.join(DOWNLOAD_FOLDER, 'results')

# --- CELERY (background tasks)
CELERY_BROKER_URL = 'redis://wgras-redis:6379/0'
CELERY_RESULT_BACKEND = 'db+postgresql://{}:{}@wgras-postgres/wgrasusers'.format(PG_USER, PG_PASSWORD)

# --- FLASK MAIL EXTENSION --- #
MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 465
MAIL_USERNAME = 'wgras.app@gmail.com'
MAIL_PASSWORD = 'ppa.sargw'
MAIL_USE_TLS = False
MAIL_USE_SSL = True

# ----- SPATIAL DATABASE (used by GDAL) ----- #
SPATIAL_DATABASE_URI = 'postgresql://{}:{}@wgras-postgres/wgrasspatial'.format(PG_USER, PG_PASSWORD)
SPATIAL_DB_USER = PG_USER
SPATIAL_DB_PASSWORD = PG_PASSWORD
SPATIAL_DB_NAME = 'wgrasspatial'
SPATIAL_DB_SERVER = 'wgras-postgres'
SPATIAL_DB_PORT = "5432"
SPATIAL_DB_TYPE = "postgis"
SPATIAL_DB_SCHEMA = "public"

# --- GEOSERVER --- #
#GEOSERVER_URI = 'http://wgras-geoserver:8080/geoserver'
#GEOSERVER_USER = "admin"
#GEOSERVER_PW = "wgrasproject"

GEOSERVER_URI = 'http://wgras-geoserver2:8080/geoserver'
GEOSERVER_USER = "admin"
GEOSERVER_PW = "wgrasproject"
