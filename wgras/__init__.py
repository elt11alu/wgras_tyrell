from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_uploads import UploadSet, configure_uploads, ARCHIVES, patch_request_class
from flask_mail import Mail
from flask_mongoalchemy import MongoAlchemy
import os
from celery import Celery
import redis
import logging

import jinja2

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)
mail = Mail(app)

mongo_db = MongoAlchemy(app)
# celery = Celery('wgras',
#                 broker='redis://localhost:6379/0',
#                 backend='db+postgresql://postgres:postgres@localhost/wgrasusers',
#                include=['wgras.rasterCalculation.evacPlanning', 'wgras.sendEmail.SendEmail'])

celery = Celery('wgras',
                broker=app.config['CELERY_BROKER_URL'],
                backend=app.config['CELERY_RESULT_BACKEND'],
                include=['wgras.rasterCalculation.evacPlanning', 'wgras.sendEmail.SendEmail'])

redisconn = redis.StrictRedis(host='localhost', port=6379, db=0)

# create log directory


# Logger
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
app.logger.addHandler(handler)

# Configuration for uploading spatial data
ZIP_SHP_UPLOAD_SET = UploadSet(name='zippedShp', extensions=ARCHIVES)
GEOTIFF_UPLOAD_SET = UploadSet(name='geotiffs', extensions='.tif')
CSV_UPLOAD_SET = UploadSet(name='csv', extensions='.csv')
app.config['UPLOADS_DEFAULT_DEST'] = 'uploads'
configure_uploads(app, (ZIP_SHP_UPLOAD_SET, GEOTIFF_UPLOAD_SET, CSV_UPLOAD_SET))
patch_request_class(app, 32 * 1024 * 1024)

# Configuration for downloading spatial data
DOWNLOAD_FOLDER = os.path.join('downloads')
TEMP_RASTER_FOLDER = os.path.join(DOWNLOAD_FOLDER, 'coverages')

# Configuration for process result storage
TEMP_RASTER_RESULT_FOLDER = os.path.join(DOWNLOAD_FOLDER, 'results')

# # Tells the application
# # to load files from root folder
app.jinja_loader = jinja2.ChoiceLoader([
    app.jinja_loader,
    jinja2.FileSystemLoader(['']),
])

from wgras import urls, models, mongodbmodels
