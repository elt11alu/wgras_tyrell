from . import db
from sqlalchemy import event
import bcrypt
import uuid


# ------------ WORKSPACE ------------ #


class Workspace(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    name = db.Column(db.String(100), unique=True)
    type = db.Column(db.String(50))
    wsid = db.Column(db.String(40))
    layers = db.relationship('Layer', backref='workspace',
                             lazy='dynamic')
    readpermissions = db.relationship(
        'Readpermission', backref='workspace', lazy='dynamic', cascade="all")
    writepermissions = db.relationship(
        'Writepermission', backref='workspace', lazy='dynamic', cascade="all")

    __mapper_args__ = {
        'polymorphic_identity': 'workspace',
        'polymorphic_on': type
    }

    def getObj(self):
        return {'name': self.name, 'type': self.type, 'workspaceId': self.wsid}

    def getUsersWithWritePermission(self):
        perm = self.writepermissions.first()
        users = perm.users.all()
        list = []
        for u in users:
            list.append(u.getObj())
        return list

    def getUsersWithReadPermission(self):
        perm = self.readpermissions.first()
        users = perm.users.all()
        list = []
        for u in users:
            list.append(u.getObj())
        return list

    def canBeReadBy(self, user):
        if isinstance(user, basestring):
            u = User.query.filter_by(username=user).first()
        else:
            u = user
        perm = self.readpermissions.first().users.filter_by(username=u.username).first()
        return perm is not None

    def canBeWrittenBy(self, user):
        if isinstance(user, basestring):
            u = User.query.filter_by(username=user).first()
        else:
            u = user
        perm = self.writepermissions.first().users.filter_by(username=u.username).first()
        return perm is not None


class Publicworkspace(Workspace):
    id = db.Column(db.Integer, db.ForeignKey('workspace.id'), primary_key=True)
    __mapper_args__ = {
        'polymorphic_identity': 'publicworkspace',
    }

    def getObj(self):
        return {'name': self.name, 'type': self.type, 'workspaceId': self.wsid}


class Adminworkspace(Workspace):
    id = db.Column(db.Integer, db.ForeignKey('workspace.id'), primary_key=True)
    __mapper_args__ = {
        'polymorphic_identity': 'adminworkspace',
    }

    def getObj(self):
        return {'name': self.name, 'type': self.type, 'workspaceId': self.wsid}


class Countryworkspace(Workspace):
    id = db.Column(db.Integer, db.ForeignKey('workspace.id'), primary_key=True)
    country_id = db.Column(db.Integer, db.ForeignKey(
        'country.id'), nullable=False)
    __mapper_args__ = {
        'polymorphic_identity': 'countryworkspace',
    }

    def getObj(self):
        return {'name': self.name, 'type': self.type, 'workspaceId': self.wsid, 'country': self.country.isocode}


class Orgworkspace(Workspace):
    id = db.Column(db.Integer, db.ForeignKey('workspace.id'), primary_key=True)
    organization_id = db.Column(db.Integer, db.ForeignKey('organization.id'))
    __mapper_args__ = {
        'polymorphic_identity': 'orgworkspace',
    }

    def getObj(self):
        return {'name': self.name, 'type': self.type, 'workspaceId': self.wsid,
                'country': self.organization.country.isocode, 'organization': self.organization.fullname}


class Privateworkspace(Workspace):
    id = db.Column(db.Integer, db.ForeignKey('workspace.id'), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    __mapper_args__ = {
        'polymorphic_identity': 'privateworkspace',
    }

    def getObj(self):
        return {'name': self.name, 'type': self.type, 'workspaceId': self.wsid,
                'user': self.user.getObj()}


def create_workspace_id_and_permission(mapper, conn, target):
    target.wsid = str(uuid.uuid4())
    Readpermission(workspace=target)
    Writepermission(workspace=target)


event.listen(Publicworkspace, 'before_insert',
             create_workspace_id_and_permission)

event.listen(Adminworkspace, 'before_insert',
             create_workspace_id_and_permission)

event.listen(Countryworkspace, 'before_insert',
             create_workspace_id_and_permission)

event.listen(Orgworkspace, 'before_insert', create_workspace_id_and_permission)

event.listen(Privateworkspace, 'before_insert',
             create_workspace_id_and_permission)


# ------------ LAYER CATEGORY ------------ #
class Layercategory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    categoryid = db.Column(db.String(), unique=True)
    name = db.Column(db.String(), nullable=False, unique=True)
    layers = db.relationship('Layer', backref='category',
                             lazy='dynamic')

    def getObj(self):
        return {'name': self.name, 'categoryId': self.categoryid, 'layers': [l.getObj() for l in self.layers]}

    def getSimpleObj(self):
        return {'name': self.name, 'categoryId': self.categoryid}


@event.listens_for(Layercategory, 'before_insert')
def create_layercategory_id(mapper, conn, target):
    target.categoryid = str(uuid.uuid4())


# ------------ LAYER ------------ #

class Layerbase(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    layers = db.relationship("Layer",
                    backref='layerbase',
                    cascade='all, delete-orphan')

class Layer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    company_id = db.Column(db.Integer, db.ForeignKey('layerbase.id'))
    type = db.Column(db.String(50))
    name = db.Column(db.String(), nullable=False)
    title = db.Column(db.String(), nullable=False)
    layerid = db.Column(db.String(), nullable=False)
    workspace_id = db.Column(db.Integer, db.ForeignKey(
        'workspace.id'), nullable=False)
    store = db.Column(db.String(), nullable=False)
    published = db.Column(db.Boolean(), nullable=False)
    minx = db.Column(db.Float, nullable=False)
    miny = db.Column(db.Float, nullable=False)
    maxx = db.Column(db.Float, nullable=False)
    maxy = db.Column(db.Float, nullable=False)
    description = db.Column(db.String())
    category_id = db.Column(db.Integer, db.ForeignKey('layercategory.id'))

    __mapper_args__ = {
        'polymorphic_identity': 'layer',
        'polymorphic_on': type
    }

    def getObj(self):
        return {
            'layerName': self.name, 'type': self.type,
            'layerTitle': self.title, 'layerId': self.layerid,
            'workspaceName': self.workspace.name, 'store': self.store,
            'minx': self.minx, 'maxx': self.maxx, 'miny': self.miny,
            'maxy': self.maxy, 'description': self.description, 'category': self.category.name
        }


class Vectorlayer(Layer):
    id = db.Column(db.Integer, db.ForeignKey('layer.id'), primary_key=True)
    type = db.Column(db.String())
    __mapper_args__ = {
        'polymorphic_identity': 'Vector',
        'polymorphic_on': type
    }

    def getObj(self):
        return {
            'layerName': self.name, 'type': self.type,
            'layerTitle': self.title, 'layerId': self.layerid,
            'workspaceName': self.workspace.name, 'store': self.store,
            'minx': self.minx, 'maxx': self.maxx, 'miny': self.miny,
            'maxy': self.maxy,'description': self.description, 'category': self.category.name
        }


class Pointlayer(Vectorlayer):
    id = db.Column(db.Integer, db.ForeignKey(
        'vectorlayer.id'), primary_key=True)

    __mapper_args__ = {
        'polymorphic_identity': 'Point',
    }


class Multipointlayer(Vectorlayer):
    id = db.Column(db.Integer, db.ForeignKey(
        'vectorlayer.id'), primary_key=True)

    __mapper_args__ = {
        'polymorphic_identity': 'MultiPoint',
    }


class Linelayer(Vectorlayer):
    id = db.Column(db.Integer, db.ForeignKey(
        'vectorlayer.id'), primary_key=True)
    __mapper_args__ = {
        'polymorphic_identity': 'LineString',
    }


class Multilinelayer(Vectorlayer):
    id = db.Column(db.Integer, db.ForeignKey(
        'vectorlayer.id'), primary_key=True)
    __mapper_args__ = {
        'polymorphic_identity': 'MultiLineString',
    }


class Polygonlayer(Vectorlayer):
    id = db.Column(db.Integer, db.ForeignKey(
        'vectorlayer.id'), primary_key=True)
    __mapper_args__ = {
        'polymorphic_identity': 'Polygon',
    }


class Multipolygonlayer(Vectorlayer):
    id = db.Column(db.Integer, db.ForeignKey(
        'vectorlayer.id'), primary_key=True)
    __mapper_args__ = {
        'polymorphic_identity': 'MultiPolygon',
    }


class Rasterlayer(Layer):
    id = db.Column(db.Integer, db.ForeignKey('layer.id'), primary_key=True)
    __mapper_args__ = {
        'polymorphic_identity': 'Raster',
    }

    def getObj(self):
        return {
            'layerName': self.name, 'type': self.type, 'layerTitle': self.title,
            'layerId': self.layerid, 'workspaceName': self.workspace.name,
            'store': self.store, 'minx': self.minx, 'maxx': self.maxx,
            'miny': self.miny, 'maxy': self.maxy, 'description': self.description, 'category': self.category.name
        }


class Elevationlayer(Rasterlayer):
    id = db.Column(db.Integer, db.ForeignKey(
        'rasterlayer.id'), primary_key=True)
    minvalue = db.Column(db.Float)
    maxvalue = db.Column(db.Float)
    meanvalue = db.Column(db.Float)
    stdvvalue = db.Column(db.Float)

    __mapper_args__ = {
        'polymorphic_identity': 'Elevation',
    }

    def getObj(self):
        return {
            'layerName': self.name, 'type': self.type, 'layerTitle': self.title,
            'layerId': self.layerid, 'workspaceName': self.workspace.name,
            'store': self.store, 'minx': self.minx, 'maxx': self.maxx,
            'miny': self.miny, 'maxy': self.maxy, 'minValue': self.minvalue,
            'maxValue': self.maxvalue, 'meanValue': self.meanvalue,
            'stdvValue': self.stdvvalue, 'description': self.description, 'category': self.category.name
        }


def create_layer_id(mapper, conn, target):
    target.layerid = str(uuid.uuid4())


event.listen(Rasterlayer, 'before_insert', create_layer_id)
event.listen(Vectorlayer, 'before_insert', create_layer_id)
event.listen(Pointlayer, 'before_insert', create_layer_id)
event.listen(Linelayer, 'before_insert', create_layer_id)
event.listen(Polygonlayer, 'before_insert', create_layer_id)
event.listen(Multipointlayer, 'before_insert', create_layer_id)
event.listen(Multilinelayer, 'before_insert', create_layer_id)
event.listen(Multipolygonlayer, 'before_insert', create_layer_id)
event.listen(Elevationlayer, 'before_insert', create_layer_id)


# ------------ PERMISSION ------------ #

class Readpermission(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    permtype = db.Column(db.String(5))
    workspaceid = db.Column(db.Integer, db.ForeignKey('workspace.id'))


readaccess = db.Table('readaccess',
                      db.Column('permissionid', db.Integer,
                                db.ForeignKey('readpermission.id')),
                      db.Column('userid', db.Integer, db.ForeignKey('user.id'))
                      )


class Writepermission(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    permtype = db.Column(db.String(5))
    workspaceid = db.Column(db.Integer, db.ForeignKey('workspace.id'))


writeaccess = db.Table('writeacess',
                       db.Column('permissionid', db.Integer,
                                 db.ForeignKey('writepermission.id')),
                       db.Column('userid', db.Integer,
                                 db.ForeignKey('user.id'))
                       )


def create_write_perm_type(mapper, conn, target):
    target.permtype = 'write'


def create_read_perm_type(mapper, conn, target):
    target.permtype = 'read'


event.listen(Writepermission, 'before_insert', create_write_perm_type)
event.listen(Readpermission, 'before_insert', create_read_perm_type)

# ------------ CONVERSATION ------------ #

usersconversations = db.Table('users_conversations',
                              db.Column('conversationid', db.Integer,
                                        db.ForeignKey('conversation.id')),
                              db.Column('userid', db.Integer,
                                        db.ForeignKey('user.id'))
                              )


class Conversation(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String, nullable=False)
    messages = db.relationship(
        'Message', backref='conversation', lazy='dynamic', cascade="all")

    def getObj(self):
        return {'title': self.title, 'conversationId': self.id, 'messages': [m.getObj() for m in self.messages]}


class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    message = db.Column(db.String, nullable=False)
    conversation_id = db.Column(db.Integer, db.ForeignKey('conversation.id'))
    sender_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    created = db.Column(db.DateTime, default=db.func.current_timestamp())

    def getObj(self):
        return {'message': self.message, 'sender': User.query.filter_by(id=self.sender_id).first().getObj(),
                'id': self.id}


# ------------ USER ------------ #

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(80), nullable=False)
    userid = db.Column(db.String(40), unique=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    sentmessages = db.relationship('Message', backref='sender', lazy='dynamic', cascade="all")
    conversations = db.relationship('Conversation', secondary=usersconversations, lazy='dynamic',
                                    backref=db.backref('participants', lazy='dynamic'), cascade="all")
    readpermissions = db.relationship('Readpermission', secondary=readaccess, lazy='dynamic',
                                      backref=db.backref('users', lazy='dynamic'))
    writepermissions = db.relationship('Writepermission', secondary=writeaccess, lazy='dynamic',
                                       backref=db.backref('users', lazy='dynamic'))
    privateworkspace = db.relationship(
        'Privateworkspace', backref='user', lazy='dynamic')
    type = db.Column(db.String(50))

    __mapper_args__ = {
        'polymorphic_identity': 'user',
        'polymorphic_on': type
    }

    def auth(self, username, password):
        if self.username == username and bcrypt.hashpw(password.encode('utf-8'),
                                                       self.password.encode('utf-8')) == self.password:
            return True
        return False

    def getRoles(self):
        return User.__subclasses__()

    def has_readPermission(self, workspace):
        if isinstance(workspace, basestring):
            ws = Workspace.query.filter_by(name=workspace).first()
        else:
            ws = workspace
        permission = self.readpermissions.filter_by(workspace=ws).first()
        if permission is not None:
            return True
        return False

    def has_writePermission(self, workspace):
        if isinstance(workspace, basestring):
            ws = Workspace.query.filter_by(name=workspace).first()
        else:
            ws = workspace
        permission = self.writepermissions.filter_by(workspace=ws).first()
        if permission is not None:
            return True
        return False

    def grant_readPermission(self, workspace):
        if isinstance(workspace, basestring):
            ws = Workspace.query.filter_by(name=workspace).first()
        else:
            ws = workspace
        permission = Readpermission.query.filter_by(workspace=ws).first()
        self.readpermissions.append(permission)
        db.session.commit()

    def grant_fullPermission(self, workspace):
        if isinstance(workspace, basestring):
            ws = Workspace.query.filter_by(name=workspace).first()
        else:
            ws = workspace
        readpermission = Readpermission.query.filter_by(workspace=ws).first()
        self.readpermissions.append(readpermission)
        writepermission = Writepermission.query.filter_by(workspace=ws).first()
        self.writepermissions.append(writepermission)
        db.session.commit()

    def revoke_writePermission(self, workspace):
        if isinstance(workspace, basestring):
            ws = Workspace.query.filter_by(name=workspace).first()
        else:
            ws = workspace
        writepermission = self.writepermissions.filter_by(workspace=ws).first()
        if writepermission is not None:
            self.writepermissions.remove(writepermission)
            db.session.commit()

    def revoke_readPermission(self, workspace):
        if isinstance(workspace, basestring):
            ws = Workspace.query.filter_by(name=workspace).first()
        else:
            ws = workspace
        readpermission = self.readpermissions.filter_by(workspace=ws).first()
        if readpermission is not None:
            self.readpermissions.remove(readpermission)
            db.session.commit()

    def getObj(self):
        return {'username': self.username, 'email': self.email, 'created': self.date_created, 'type': self.type,
                'userId': self.userid}


class Superadmin(User):
    id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)

    __mapper_args__ = {
        'polymorphic_identity': 'superadmin',
    }

    def getObj(self):
        return {'userId': self.userid, 'username': self.username, 'email': self.email, 'created': self.date_created,
                'type': self.type}


class Countryadmin(User):
    id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    countryid = db.Column(db.Integer, db.ForeignKey('country.id'))
    __mapper_args__ = {
        'polymorphic_identity': 'countryadmin',
    }

    def getObj(self):
        return {'userId': self.userid, 'username': self.username, 'email': self.email, 'created': self.date_created,
                'type': self.type,
                'isocode': self.country.isocode, 'countryName': self.country.name}


class Countryuser(User):
    id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    countryid = db.Column(db.Integer, db.ForeignKey('country.id'))
    __mapper_args__ = {
        'polymorphic_identity': 'countryuser',
    }

    def getObj(self):
        return {'userId': self.userid, 'username': self.username, 'email': self.email, 'created': self.date_created,
                'type': self.type,
                'isocode': self.country.isocode, 'countryName': self.country.name}


class Orgadmin(User):
    id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    organizationid = db.Column(db.Integer, db.ForeignKey('organization.id'))
    __mapper_args__ = {
        'polymorphic_identity': 'orgadmin',
    }

    def getObj(self):
        return {'userId': self.userid, 'username': self.username, 'email': self.email, 'created': self.date_created,
                'type': self.type,
                'isocode': self.organization.country.isocode, 'countryName': self.organization.country.name,
                'organization': self.organization.fullname, 'orgid': self.organization.orgid}


def create_user_id(mapper, conn, target):
    target.userid = str(uuid.uuid4())


event.listen(Superadmin, 'before_insert', create_user_id)
event.listen(Countryadmin, 'before_insert', create_user_id)
event.listen(Countryuser, 'before_insert', create_user_id)
event.listen(Orgadmin, 'before_insert', create_user_id)


# ------------ COUNTRY ------------ #

class Country(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    isocode = db.Column(db.String(10), unique=True, nullable=False)
    name = db.Column(db.String(100), unique=True, nullable=False)
    countryusers = db.relationship(
        'Countryuser', backref='country', lazy='dynamic', cascade="all")
    countryadmins = db.relationship(
        'Countryadmin', backref='country', lazy='dynamic', cascade="all")
    organizations = db.relationship(
        'Organization', backref='country', lazy='dynamic', cascade="all")
    workspaces = db.relationship(
        'Countryworkspace', backref='country', lazy='dynamic', cascade="all")

    def getObj(self):
        users = self.countryusers.all()
        admins = self.countryadmins.all()
        organizations = self.organizations.all()
        workspaces = self.workspaces.all()
        u_list = [u.getObj() for u in users]
        a_list = [a.getObj() for a in admins]
        org_list = [o.getObj() for o in organizations]
        ws_list = [w.getObj() for w in workspaces]

        return {'name': self.name, 'isocode': self.isocode, 'users': u_list, 'admins': a_list,
                'organizations': org_list, 'workspaces': ws_list}

    def getSimpleObj(self):
        return {'name': self.name, 'isocode': self.isocode}

    def getObjWithUsers(self):
        u_list = [u.getObj() for u in self.countryusers.all()]
        return {'name': self.name, 'isocode': self.isocode, 'users': u_list}


class Availablecountry(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    isocode = db.Column(db.String(10), unique=True, nullable=False)
    name = db.Column(db.String(100), unique=True, nullable=False)
    published = db.Column(db.Boolean, default=False)

    def publish(self):
        country = Country(name=self.name, isocode=self.isocode)
        self.published = True
        db.session.add(country)
        db.session.commit()

    def unPublish(self):
        country = Country.query.filter_by(isocode=self.isocode).first()
        self.published = False
        db.session.delete(country)
        db.session.commit()

    def isPublished(self):
        return self.published

    def getObj(self):
        return {'name': self.name, 'isocode': self.isocode, 'published': self.published}


@event.listens_for(Country, 'before_delete')
def unpublish(mapper, connection, target):
    c = Availablecountry.query.filter_by(isocode=target.isocode).first()
    c.published = False


# ------------ ORGANIZATION ------------ #

class Organization(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    fullname = db.Column(db.String(150), nullable=False)
    shortname = db.Column(db.String(30), nullable=False)
    orgid = db.Column(db.String(40))
    countryname = db.Column(db.String(10))
    countryid = db.Column(db.Integer, db.ForeignKey('country.id'))
    orgadmins = db.relationship(
        'Orgadmin', backref='organization', lazy='dynamic', cascade="all")
    workspaces = db.relationship(
        'Orgworkspace', backref='organization', lazy='dynamic', cascade="all")

    def getObj(self):
        return {'fullname': self.fullname, 'shortname': self.shortname, 'isocode': self.country.isocode,
                'countryName': self.country.name, 'orgid': self.orgid}


@event.listens_for(Organization, 'before_insert')
def modify_organization(mapper, conn, target):
    target.countryname = target.country.isocode
    target.orgid = str(uuid.uuid4())


class Geomtype(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    geomtype = db.Column(db.Enum('Point', 'LineString', 'LinearRing', 'Polygon', 'MultiPoint', 'MultiLineString',
                                 'MultiPolygon', 'GeometryCollection', 'Circle', name='geometry_type'), nullable=False,
                         unique=True)
    common_name = db.Column(db.String(30))
    geomtypeid = db.Column(db.String(40))

    def getObj(self):
        return {'geomType': self.geomtype, 'commonName': self.common_name, 'geomTypeId': self.geomtypeid}


@event.listens_for(Geomtype, 'before_insert')
def set_geom_type_id(mapper, conn, target):
    target.geomtypeid = uuid.uuid4()
    if target.geomtype == 'Point':
        target.common_name = 'Point'
    elif target.geomtype == 'MultiPoint':
        target.common_name = 'MultiPoint'
    elif target.geomtype == 'LineString':
        target.common_name = 'Line'
    elif target.geomtype == 'MultiLineString':
        target.common_name = 'MultiLineString'
    elif target.geomtype == 'Polygon':
        target.common_name = 'Polygon'
    elif target.geomtype == 'MultiPolygon':
        target.common_name = 'MultiPolygon'


class Attributetype(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    attributetype = db.Column(
        db.Enum('String', 'Integer', 'Real', name='attributes'), nullable=False, unique=True)
    openlayersname = db.Column(db.String, unique=True)
    attributeid = db.Column(db.String(40))

    def getObj(self):
        return {'attributeType': self.attributetype, 'openLayersName': self.openlayersname,
                'attributeId': self.attributeid}


@event.listens_for(Attributetype, 'before_insert')
def set_geom_type_id(mapper, conn, target):
    target.attributeid = uuid.uuid4()
    if target.attributetype == 'String':
        target.openlayersname = 'string'
    elif target.attributetype == 'Integer':
        target.openlayersname = 'number'
    elif target.attributetype == 'Real':
        target.openlayersname = 'number'


# --- EXTERNAL WMS URLS --- #

class Externalows(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(), nullable=False)
    name = db.Column(db.String(), nullable=False)
    description = db.Column(db.String())
    category = db.Column(db.String())
    owsid = db.Column(db.String())

    def getObj(self):
        return {'url': self.url, 'name': self.name, 'description': self.description, 'category': self.category, 'owsId': self.owsid}


@event.listens_for(Externalows, 'before_insert')
def set_externalows_id(mapper, conn, target):
    target.owsid = uuid.uuid4()

