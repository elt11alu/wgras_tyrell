from ..models import *



class AccessCreator(object):
    def createCountryWorkspaceAccess(self, wsid, isocode):
        country = Country.query.filter_by(isocode=isocode).first()
        workspace = Countryworkspace.query.filter_by(wsid=wsid).first()
        superadmins = Superadmin.query.all()
        countryadmins = Countryadmin.query.filter_by(country=country).all()
        countryusers = Countryuser.query.filter_by(country=country).all()

        for s in superadmins:
            s.grant_fullPermission(workspace)
        for c_admin in countryadmins:
            c_admin.grant_fullPermission(workspace)
        for c_user in countryusers:
            c_user.grant_readPermission(workspace)

    def createOrganizationWorkspaceAccess(self, wsid, orgid):
        workspace = Workspace.query.filter_by(wsid=wsid).first()
        organization = Organization.query.filter_by(orgid=orgid).first()
        country = organization.country
        superadmins = Superadmin.query.all()
        countryadmins = Countryadmin.query.filter_by(country=country).all()
        orgadmins = Orgadmin.query.all()

        for s in superadmins:
            s.grant_fullPermission(workspace=workspace)
        for c_admin in countryadmins:
            c_admin.grant_fullPermission(workspace=workspace)
        for o_admin in orgadmins:
            if o_admin.organization.country.isocode == country.isocode:
                if o_admin.organization.orgid == orgid:
                    o_admin.grant_fullPermission(workspace)
                else:
                    o_admin.grant_readPermission(workspace)

    def create_access_for_country_admin(self, userid):
        user = Countryadmin.query.filter_by(userid=userid).first()
        adminws = Adminworkspace.query.all()
        countryws = Countryworkspace.query.all()
        orgws = Orgworkspace.query.all()
        public_ws = Publicworkspace.query.all()
        for aws in adminws:
            user.grant_readPermission(workspace=aws)
        for cws in countryws:
            if user.country.isocode == cws.country.isocode:
                user.grant_fullPermission(workspace=cws)
        for ows in orgws:
            country = ows.organization.country
            if user.country.isocode == country.isocode:
                user.grant_fullPermission(workspace=ows)
        for pws in public_ws:
            user.grant_fullPermission(workspace=pws)

        private_ws = Privateworkspace.query.filter_by(user=user).first()
        if private_ws is not None:
            user.grant_fullPermission(private_ws)

    def create_access_for_country_user(self, userid):
        user = Countryuser.query.filter_by(userid=userid).first()
        workspaces = Countryworkspace.query.filter_by(country=user.country)
        public_ws = Publicworkspace.query.all()
        for ws in workspaces:
            user.grant_readPermission(workspace=ws)

        for pws in public_ws:
            user.grant_fullPermission(workspace=pws)

        private_ws = Privateworkspace.query.filter_by(user=user).first()
        if private_ws is not None:
            user.grant_fullPermission(private_ws)

    def create_access_for_org_admin(self, userid):
        orgws = Orgworkspace.query.all()
        user = Orgadmin.query.filter_by(userid=userid).first()
        public_ws = Publicworkspace.query.all()
        for ows in orgws:
            if user.organization.country.isocode == ows.organization.country.isocode:
                user.grant_fullPermission(workspace=ows)
            else:
                user.grant_readPermission(workspace=ows)

        for pws in public_ws:
            user.grant_fullPermission(workspace=pws)

        private_ws = Privateworkspace.query.filter_by(user=user).first()
        if private_ws is not None:
            user.grant_fullPermission(private_ws)

    def create_access_for_superadmin(self, userid):
        workspaces = Workspace.query.all()
        user = Superadmin.query.all()
        for ws in workspaces:
            user.grant_fullPermission(workspace=ws)
