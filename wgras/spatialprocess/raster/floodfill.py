import os
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
from osgeo import ogr
from osgeo import osr
from .. import TEMP_RASTER_RESULT_FOLDER
import json

class FloodFill(object):
    def floodFillLocal(self, filePath, incoords, water_increase):
        self._createTempDirectory()
        self._emptyTempDirectory()

        gtif = gdal.Open(filePath, GA_ReadOnly)
        coord = incoords
        gt = gtif.GetGeoTransform()
        rb = gtif.GetRasterBand(1)
        elev_data = rb.ReadAsArray()


        filled = set()
        fill = set()
        width = elev_data.shape[1] - 1
        height = elev_data.shape[0] - 1

        spatialRef = osr.SpatialReference()
        spatialRef.ImportFromEPSG(4326)
        lat, lng = coord
        wkt = "POINT (" + str(lat) + " " + str(lng) + ")"
        point = ogr.CreateGeometryFromWkt(wkt, spatialRef)
        mx = point.GetX()
        my = point.GetY()


        # Find pixels at coordinate
        px = int((mx - gt[0]) / gt[1])
        py = int((my - gt[3]) / gt[5])

        seed = elev_data[py][px] # elevation at point

        fill.add((px, py))
        flood = numpy.zeros_like(elev_data, dtype=numpy.int8)

        while fill:
            x, y = fill.pop()
            if y == height - 1 or x == width - 1 or x < 0 or y < 0:
                continue

            if abs(seed - elev_data[y][x]) < water_increase: #Math.abs(seedR - cr) < delta
                flood[y][x] = 1 # the mask that we use to create the layer from (all cells with value one are inncluded in poly)
                filled.add((x, y))

                west = (x - 1, y)
                east = (x + 1, y)
                north = (x, y - 1)
                south = (x, y + 1)

                if west not in filled:
                    fill.add(west)
                if east not in filled:
                    fill.add(east)
                if north not in filled:
                    fill.add(north)
                if south not in filled:
                    fill.add(south)

        geotiffLayername = os.path.join(TEMP_RASTER_RESULT_FOLDER,'result.tiff')
        geotiffdriver = gdal.GetDriverByName("GTiff")
        ds_out = geotiffdriver.Create(geotiffLayername, gtif.RasterXSize, gtif.RasterYSize, 1, gdal.GDT_Byte)
        CopyDatasetInfo(gtif, ds_out)
        band_out = ds_out.GetRasterBand(1)
        band_out.SetNoDataValue(0)
        BandWriteArray(band_out, flood)
        outDriver = ogr.GetDriverByName('GeoJSON')
        outDataSource = outDriver.CreateDataSource(os.path.join(TEMP_RASTER_RESULT_FOLDER, 'flood.geojson'))
        outLayer = outDataSource.CreateLayer(os.path.join(TEMP_RASTER_RESULT_FOLDER, 'flood.geojson'), geom_type=ogr.wkbPolygon)
        gdal.Polygonize(band_out, band_out, outLayer, -1, [], callback=None)
        outDataSource.Destroy()

        gsfile = os.path.join(TEMP_RASTER_RESULT_FOLDER, 'flood.geojson')
        dataSource = outDriver.Open(gsfile, 0)
        layer = dataSource.GetLayer()

        geometries = []
        feature_collection = {"type": "FeatureCollection",
                              "features": []
                              }
        for feature in layer:
            feature_collection['features'].append(feature.ExportToJson(as_object=True))
            #geom = feature.GetGeometryRef()
            #geometries.append(geom.ExportToWkt())

        dataSource.Destroy()
        return str(json.dumps(feature_collection))

    def _createTempDirectory(self):
        directory = TEMP_RASTER_RESULT_FOLDER
        if not os.path.exists(directory):
            os.makedirs(directory)

    def _emptyTempDirectory(self):
        directory = TEMP_RASTER_RESULT_FOLDER
        fileList = os.listdir(directory)
        for fileName in fileList:
            os.remove(os.path.join(directory, fileName))