import os
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
from osgeo import ogr
from osgeo import osr

class RasterStatistics(object):

    def getRasterStatisticsFromFile(self, filePath):
        raster = gdal.Open(filePath, GA_ReadOnly)
        band = raster.GetRasterBand(1)
        stats = band.GetStatistics(True, True)
        minValue = stats[0]
        maxValue = stats[1]
        meanValue = stats[2]
        stdvValue = stats[3]

        return {
            'minValue': minValue,
            'maxValue': maxValue,
            'meanValue': meanValue,
            'stdvValue': stdvValue
        }