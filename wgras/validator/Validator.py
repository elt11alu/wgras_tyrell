import re
from uuid import UUID


def validateUsername(username):
    regex = r"^[a-zA-Z0-9_]{3,30}$"
    if re.match(regex, username):
        return True
    return False


def validatePassword(password):
    regex = r"^[a-zA-Z0-9_]{5,30}$"
    if re.match(regex, password):
        return True
    return False


def validateRole(role):
    regex = r"(ADMIN|USER)"
    if re.match(regex, role):
        return True
    return False


def validateRoleCode(role):
    return True


def validateTableName(tableName):
    regex = r"^[a-zA-Z0-9_]{3,40}$"
    if re.match(regex, tableName):
        return True
    return False


def validateWorkspaceName(workspaceName):
    regex = r"^[a-zA-Z0-9_]{3,40}$"
    if re.match(regex, workspaceName):
        return True
    return False


def validateLayerName(layerName):
    regex = r"^[a-zA-Z0-9:_]{3,50}$"
    if re.match(regex, layerName):
        return True
    return False


def validateStoreName(storeName):
    regex = r"^[a-zA-Z0-9_-]{3,50}$"
    if re.match(regex, storeName):
        return True
    return False


def validateLayerTitle(layerTitle):
    regex = r"^[a-zA-Z0-9_ ]{3,50}$"
    if re.match(regex, layerTitle):
        return True
    return False


def validateLayerKeywords(keywords):
    regex = r"^[a-zA-Z0-9_ ]{1,50}$"
    for keyword in keywords:
        if re.match(regex, keyword):
            return True
        return False


def validateLayerInfo(layerInfo):
    regex = r"^([a-zA-Z0-9_ .,]{0,300})$"
    if re.match(regex, layerInfo):
        return True
    return False


def validateEmail(email):
    regex = r"([^@]+@[^@]+\.[^@]+)"
    if re.match(regex, email):
        return True
    return False


def validateId(id):
    regex = r"(^[0-9]+$)"
    if re.match(regex, id):
        return True
    return False


def validatePublish(publish):
    return isinstance(publish, bool)


def validateFileName(fileName):
    regex = r"^[a-zA-Z0-9_ .-]{0,100}$"
    if re.match(regex, fileName):
        return True
    return False


def validateInteger(inputNumber):
    return isinstance(inputNumber, (int, long))


def validateFloat(inputNumber):
    return isinstance(inputNumber, float)


def validateUUID4(uuid_string):
    try:
        UUID(uuid_string, version=4)
        return True
    except ValueError:
        return False


def validateManagerCeleryId(celeryId):
    cid = celeryId.split('manager-')[1]
    return validateUUID4(cid)


def validateIsocode(isocode):
    regex = r"^[a-zA-Z]{3}$"
    if re.match(regex, isocode):
        return True
    return False


def validateEmailText(message):
    regex = r"^[a-zA-Z0-9 ,.!?()\"\'\r\n]+$"
    if re.match(regex, message):
        return True
    return False


def validateOrganizationName(orgname):
    regex = r"^[a-zA-Z0-9 .-_]+$"
    if re.match(regex, orgname):
        return True
    return False


def validateOrganizationShortName(orgname_short_name):
    regex = r"^[a-zA-Z0-9_]{3,30}$"
    if re.match(regex, orgname_short_name):
        return True
    return False


def validateUrl(url):
    regex = r"^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$"
    if re.match(regex, url):
        return True
    return False
