from owslib.wms import WebMapService
import logging


class WMSProxy:
    def __init__(self):
        self.wms = None
        self.logger = logging.getLogger()

    def connect(self, url):
        try:
            self.wms = WebMapService(url=url)
        except Exception as e:
            self.logger.warn(e.message)
            return None

    def get_layers(self):
        if self.wms:
            layer_list = []
            for k,v in self.wms.contents.iteritems():
                layer = self.wms[k]

                layer_list.append({
                    'layerName': layer.name,
                    'layerTitle': layer.title,
                    'abstract': layer.abstract
                })
            return layer_list
        raise Exception('You must establish a connection first!')


