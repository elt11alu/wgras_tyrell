class MetadataService:
    # --- Metadata functions: returns metadata about the tables (layers) available at the geoServer --- #
    # --- The metadata is stored in user db, and is created at layer creation time

    def get_all_vector_metadata(self):
        pass

    def get_vector_metadata_by_name(self, name):
        pass

    def get_vector_metadata_by_id(self, layer_id):
        pass

    def get_all_point_metadata(self):
        pass

    def get_point_metadata_by_name(self, name):
        pass

    def get_point_metadata_by_id(self, layer_id):
        pass

    def get_all_line_metadata(self):
        pass

    def get_line_metadata_by_name(self):
        pass

    def get_line_metadata_by_id(self, layer_id):
        pass

    def get_all_polygon_metadata(self):
        pass

    def get_polygon_metadata_by_name(self, name):
        pass

    def get_polygon_metadata_by_id(self, layer_id):
        pass