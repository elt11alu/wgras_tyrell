from .base import Service
from .geoserverservice import GeoServerService
from .postgisservice import PostgisService
from ..models import *


class PostgisGeoServerLink(Service):
    def __init__(self):
        super(PostgisGeoServerLink, self).__init__()

    def get_workspace_diff(self):
        gs_service = GeoServerService()

        gs_workspaces = gs_service.getWorkspaceNames()
        diff = []
        for gs_workspace in gs_workspaces:
            gs_workspace_name = gs_workspace.name
            db_workspace = Workspace.query.filter_by(name=gs_workspace_name)
            if db_workspace is None:
                diff.append({'workspaceName': gs_workspace_name})
        return diff

    def get_vector_layer_diff(self):
        gs_service = GeoServerService()

        gs_wfs_list = gs_service.getAllWFSMetadata()
        diff = []
        for gs_wfs in gs_wfs_list:
            gs_wfs_name = gs_wfs['layerName'].split(':')[1]
            db_workspace = Vectorlayer.query.filter_by(name=gs_wfs_name)
            if db_workspace is None:
                diff.append({'layerName': gs_wfs_name})
        return diff

    def get_raster_layer_diff(self):

        # Should this search wms or wcs???
        gs_service = GeoServerService()

        gs_wms_list = gs_service.getAllWMSMetadata()
        diff = []
        for gs_wms in gs_wms_list:
            gs_wms_name = gs_wms['layerName'].split(':')[1]
            db_workspace = Rasterlayer.query.filter_by(name=gs_wms_name)
            if db_workspace is None:
                diff.append({'layerName': gs_wms_name})
        return diff

    def sync_db_to_geoserver(self):
        pass
