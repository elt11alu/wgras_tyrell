from .base import Service
from .. import app
from .processservice import ProcessService
from ..models import *
from ..postgisconnection.geojson_to_postgis import GeojsonToPostgis
from ..postgisconnection.table import Table
from ..postgisconnection.geometryfunctions import GeometryFunctions
from ..postgisconnection.base import PostGisConnection
from ..geoserverconnection.bbox import BboxFactory


class PostgisService(Service):
    def __init__(self):
        super(PostgisService, self).__init__()

    def create_db_layer(self, geom_type, name, title, workspaceId, store_name, published, description, category,
                        file_path=None):

        workspace = Workspace.query.filter_by(wsid=workspaceId).first()
        if workspace is not None:
            category_for_layer = Layercategory.query.filter_by(name=category).first()
            if category_for_layer is None:
                category_for_layer = Layercategory(name=category)
            latlon_bbox = self._get_bbox(layer_name=name, workspace_name=workspace.name)
            if latlon_bbox is not None:
                minx = float(latlon_bbox[0])
                maxx = float(latlon_bbox[2])
                miny = float(latlon_bbox[1])
                maxy = float(latlon_bbox[3])
            else:
                minx = -180.0
                maxx = 180.0
                miny = -90.0
                maxy = 90.0
            if geom_type == 'Point':
                vector_metadata = Pointlayer(name=name, title=title, workspace=workspace, store=store_name,
                                             published=published, minx=minx, maxx=maxx, miny=miny, maxy=maxy,
                                             description=description, category=category_for_layer)
                raster_metadata = Rasterlayer(name=name, title=title, workspace=workspace, store=store_name,
                                              published=published, minx=minx, maxx=maxx, miny=miny, maxy=maxy,
                                              description=description, category=category_for_layer)
                db.session.add(vector_metadata)
                db.session.add(raster_metadata)
                db.session.commit()
                app.logger.info(
                    'Inserted raster and Point metadata for {} into db'.format(name))
                return {'vector_metadata': vector_metadata.getObj(), 'raster_metadata': raster_metadata.getObj()}
            elif geom_type == 'MultiPoint':
                vector_metadata = Multipointlayer(name=name, title=title, workspace=workspace, store=store_name,
                                                  published=published, minx=minx, maxx=maxx, miny=miny, maxy=maxy,
                                                  description=description, category=category_for_layer)
                raster_metadata = Rasterlayer(name=name, title=title, workspace=workspace, store=store_name,
                                              published=published, minx=minx, maxx=maxx, miny=miny, maxy=maxy,
                                              description=description, category=category_for_layer)
                db.session.add(vector_metadata)
                db.session.add(raster_metadata)
                db.session.commit()
                app.logger.info(
                    'Inserted raster and MultiPoint metadata for {} into db'.format(name))
                return {'vector_metadata': vector_metadata.getObj(), 'raster_metadata': raster_metadata.getObj()}
            elif geom_type == 'LineString':
                vector_metadata = Linelayer(name=name, title=title, workspace=workspace, store=store_name,
                                            published=published, minx=minx, maxx=maxx, miny=miny, maxy=maxy,
                                            description=description, category=category_for_layer)
                raster_metadata = Rasterlayer(name=name, title=title, workspace=workspace, store=store_name,
                                              published=published, minx=minx, maxx=maxx, miny=miny, maxy=maxy,
                                              description=description, category=category_for_layer)
                db.session.add(vector_metadata)
                db.session.add(raster_metadata)
                db.session.commit()
                app.logger.info(
                    'Inserted raster and LineString metadata for {} into db'.format(name))
                return {'vector_metadata': vector_metadata.getObj(), 'raster_metadata': raster_metadata.getObj()}
            elif geom_type == 'MultiLineString':
                vector_metadata = Multilinelayer(name=name, title=title, workspace=workspace, store=store_name,
                                            published=published, minx=minx, maxx=maxx, miny=miny, maxy=maxy,
                                            description=description, category=category_for_layer)
                raster_metadata = Rasterlayer(name=name, title=title, workspace=workspace, store=store_name,
                                              published=published, minx=minx, maxx=maxx, miny=miny, maxy=maxy,
                                              description=description, category=category_for_layer)
                db.session.add(vector_metadata)
                db.session.add(raster_metadata)
                db.session.commit()
                app.logger.info(
                    'Inserted raster and MultiLineString metadata for {} into db'.format(name))
                return {'vector_metadata': vector_metadata.getObj(), 'raster_metadata': raster_metadata.getObj()}
            elif geom_type == 'Polygon':
                vector_metadata = Polygonlayer(name=name, title=title, workspace=workspace, store=store_name,
                                               published=published, minx=minx, maxx=maxx, miny=miny, maxy=maxy,
                                               description=description, category=category_for_layer)
                raster_metadata = Rasterlayer(name=name, title=title, workspace=workspace, store=store_name,
                                              published=published, minx=minx, maxx=maxx, miny=miny, maxy=maxy,
                                              description=description, category=category_for_layer)
                db.session.add(vector_metadata)
                db.session.add(raster_metadata)
                db.session.commit()
                app.logger.info(
                    'Inserted raster and polygon metadata for {} into db'.format(name))
                return {'vector_metadata': vector_metadata.getObj(), 'raster_metadata': raster_metadata.getObj()}
            elif geom_type == 'MultiPolygon':
                vector_metadata = Multipolygonlayer(name=name, title=title, workspace=workspace, store=store_name,
                                               published=published, minx=minx, maxx=maxx, miny=miny, maxy=maxy,
                                               description=description, category=category_for_layer)
                raster_metadata = Rasterlayer(name=name, title=title, workspace=workspace, store=store_name,
                                              published=published, minx=minx, maxx=maxx, miny=miny, maxy=maxy,
                                              description=description, category=category_for_layer)
                db.session.add(vector_metadata)
                db.session.add(raster_metadata)
                db.session.commit()
                app.logger.info(
                    'Inserted raster and MultiPolygon metadata for {} into db'.format(name))
                return {'vector_metadata': vector_metadata.getObj(), 'raster_metadata': raster_metadata.getObj()}
            elif geom_type == 'Raster':
                raster_metadata = Rasterlayer(name=name, title=title, workspace=workspace, store=store_name,
                                              published=published, minx=minx, maxx=maxx, miny=miny, maxy=maxy,
                                              description=description, category=category_for_layer)
                db.session.add(raster_metadata)
                db.session.commit()
                app.logger.info(
                    'Inserted raster metadata for {} into db'.format(name))
                return {'raster_metadata': raster_metadata.getObj()}
            elif geom_type == 'Elevation':
                if file_path is not None:
                    pr_service = ProcessService()
                    stats = pr_service.getRasterStatistics(file_path)
                    if stats is not None:
                        "If stats were calculated, we create elevation metadata"
                        elevation_metadata = Elevationlayer(name=name, title=title, workspace=workspace,
                                                            store=store_name,
                                                            published=published, minvalue=stats['minValue'],
                                                            maxvalue=stats['maxValue'], meanvalue=stats['meanValue'],
                                                            stdvvalue=stats['stdvValue'], minx=minx, maxx=maxx,
                                                            miny=miny,
                                                            maxy=maxy, description=description,
                                                            category=category_for_layer)
                        db.session.add(elevation_metadata)
                        db.session.commit()
                        app.logger.info(
                            'Statistics were successfully calculated, inserted elevation metadata for {} into db'.format(
                                name))
                        return {'elevation_metadata': elevation_metadata.getObj()}
                    else:
                        "If stats were not calculated, we create an ordinary raster layer"
                        raster_metadata = Rasterlayer(name=name, title=title, workspace=workspace, store=store_name,
                                                      published=published, minx=minx, maxx=maxx, miny=miny, maxy=maxy,
                                                      description=description, category=category_for_layer)
                        db.session.add(raster_metadata)
                        db.session.commit()
                        app.logger.info(
                            'Statistics could not be calculated, inserted raster metadata for {} into db'.format(name))
                        return {'raster_metadata': raster_metadata.getObj()}
        app.logger.warning('Tried to get workspace with id {} but found None'.format(workspaceId))
        return None

    def create_point_table_from_geojson(self, layer_name, schema_name, points, field_definition):
        return GeojsonToPostgis().create_point_table_with_geometry(schema=schema_name, table_name=layer_name,
                                                                   json_points=points,
                                                                   field_definition=field_definition)

    def create_multipoint_table_from_geojson(self, layer_name, schema_name, points, field_definition):
        return GeojsonToPostgis().create_multipoint_table_with_geometry(schema=schema_name, table_name=layer_name,
                                                                        json_points=points,
                                                                        field_definition=field_definition)

    def create_line_table_from_geojson(self, layer_name, schema_name, lines, field_definition):
        return GeojsonToPostgis().create_line_table_with_geometry(schema=schema_name, table_name=layer_name,
                                                                  json_lines=lines,
                                                                  field_definition=field_definition)

    def create_multiline_table_from_geojson(self, layer_name, schema_name, lines, field_definition):
        return GeojsonToPostgis().create_multiline_table_with_geometry(schema=schema_name, table_name=layer_name,
                                                                       json_lines=lines,
                                                                       field_definition=field_definition)

    def create_polygon_table_from_geojson(self, layer_name, schema_name, polygons, field_definition):
        return GeojsonToPostgis().create_polygon_table_with_geometry(schema=schema_name, table_name=layer_name,
                                                                     json_polygons=polygons,
                                                                     field_definition=field_definition)

    def create_multipolygon_table_from_geojson(self, layer_name, schema_name, polygons, field_definition):
        return GeojsonToPostgis().create_multipolygon_table_with_geometry(schema=schema_name, table_name=layer_name,
                                                                          json_polygons=polygons,
                                                                          field_definition=field_definition)

    # --- Point --- #
    def create_empty_point_table(self, layer_name, schema_name, fields):
        return Table().create_empty_point_table(table_name=layer_name, schema_name=schema_name, fields=fields)

    def insert_geojson_point(self, layer_id, geojson_point):
        layer = Pointlayer.query.filter_by(layerid=layer_id).first()
        inserted = Table().insert_json_geom(
            table_name=str(layer.name), json_geom=geojson_point)
        if inserted:
            bbox = self._recalculate_bbox(layer.name)
            if bbox is not None:
                print bbox
                # bbbox = [minx, maxx, miny, maxy]
                layer.minx = bbox[0]
                layer.maxx = bbox[1]
                layer.miny = bbox[2]
                layer.maxy = bbox[3]
                db.session.commit()
        return inserted

    # --- MultiPoint -- #
    def create_empty_multipoint_table(self, layer_name, schema_name, fields):
        return Table().create_empty_multipoint_table(table_name=layer_name, schema_name=schema_name, fields=fields)

    def insert_geojson_multipoint(self, layer_id, geojson_point):
        layer = Multipointlayer.query.filter_by(layerid=layer_id).first()
        inserted = Table().insert_json_geom(
            table_name=str(layer.name), json_geom=geojson_point)
        if inserted:
            bbox = self._recalculate_bbox(layer.name)
            if bbox is not None:
                # bbbox = [minx, maxx, miny, maxy]
                layer.minx = bbox[0]
                layer.maxx = bbox[1]
                layer.miny = bbox[2]
                layer.maxy = bbox[3]
                db.session.commit()
        return inserted

    # --- Line --- #
    def create_empty_line_table(self, layer_name, schema_name, fields):
        return Table().create_empty_line_table(table_name=layer_name, schema_name=schema_name, fields=fields)

    def insert_geojson_line(self, layer_id, geojson_line):
        layer = Linelayer.query.filter_by(layerid=layer_id).first()
        inserted = Table().insert_json_geom(
            table_name=str(layer.name), json_geom=geojson_line)
        if inserted:
            bbox = self._recalculate_bbox(layer.name)
            if bbox is not None:
                # bbbox = [minx, maxx, miny, maxy]
                layer.minx = bbox[0]
                layer.maxx = bbox[1]
                layer.miny = bbox[2]
                layer.maxy = bbox[3]
                db.session.commit()
        return inserted

    # --- MultiLine --- #
    def create_empty_multiline_table(self, layer_name, schema_name, fields):
        return Table().create_empty_multiline_table(table_name=layer_name, schema_name=schema_name, fields=fields)

    def insert_geojson_multiline(self, layer_id, geojson_line):
        layer = Multilinelayer.query.filter_by(layerid=layer_id).first()
        inserted = Table().insert_json_geom(
            table_name=str(layer.name), json_geom=geojson_line)
        if inserted:
            bbox = self._recalculate_bbox(layer.name)
            if bbox is not None:
                # bbbox = [minx, maxx, miny, maxy]
                layer.minx = bbox[0]
                layer.maxx = bbox[1]
                layer.miny = bbox[2]
                layer.maxy = bbox[3]
                db.session.commit()
        return inserted

    # --- Polygon --- #
    def create_empty_polygon_table(self, layer_name, schema_name, fields):
        return Table().create_empty_polygon_table(table_name=layer_name, schema_name=schema_name, fields=fields)

    def insert_geojson_polygon(self, layer_id, geojson_polygon):
        layer = Polygonlayer.query.filter_by(layerid=layer_id).first()
        inserted = Table().insert_json_geom(
            table_name=str(layer.name), json_geom=geojson_polygon)
        if inserted:
            bbox = self._recalculate_bbox(layer.name)
            if bbox is not None:
                # bbbox = [minx, maxx, miny, maxy]
                layer.minx = bbox[0]
                layer.maxx = bbox[1]
                layer.miny = bbox[2]
                layer.maxy = bbox[3]
                db.session.commit()
        return inserted

    # --- MultiPolygon --- #
    def create_empty_multipolygon_table(self, layer_name, schema_name, fields):
        return Table().create_empty_multipolygon_table(table_name=layer_name, schema_name=schema_name, fields=fields)

    def insert_geojson_multipolygon(self, layer_id, geojson_polygon):
        layer = Multipolygonlayer.query.filter_by(layerid=layer_id).first()
        inserted = Table().insert_json_geom(
            table_name=str(layer.name), json_geom=geojson_polygon)
        if inserted:
            bbox = self._recalculate_bbox(layer.name)
            if bbox is not None:
                # bbbox = [minx, maxx, miny, maxy]

                layer.minx = bbox[0]
                layer.maxx = bbox[1]
                layer.miny = bbox[2]
                layer.maxy = bbox[3]
                db.session.commit()
        return inserted

    # --- TYPE METHODS --- #
    def get_geom_types(self):
        return [geom_type.getObj() for geom_type in Geomtype.query.all()]

    def get_attribute_types(self):
        return [attr_type.getObj() for attr_type in Attributetype.query.all()]

    def get_geom_type_for_table(self, table_name):
        return PostGisConnection().get_geom_type_for_table(table_name=table_name)

    # --- ELEVATION STATISTICS METHODS --- #
    def insert_elevation_statistics(self, stat_dict):
        elev_stat = Elevationlayer(minvalue=stat_dict['minValue'], maxvalue=stat_dict[
            'maxValue'], meanvalue=stat_dict['meanvalue'], stdvvalue=stat_dict['stdvvalue'])
        db.session.add(elev_stat)
        db.session.commit()
        return elev_stat.layerid

    # --- BBOX METHODS --- #
    def _recalculate_bbox(self, layer_name):
        layer = Layer.query.filter_by(name=layer_name).first()
        bbox = BboxFactory()
        return bbox.recalculate_bbox(resource_name=layer_name,
                              workspace_name=layer.workspace.name)

    def _set_world_bbox(self, layer_name):
        layer = Layer.query.filter_by(name=layer_name).first()
        bbox = BboxFactory()
        bbox.set_bbox_on_resource_to_max(
            resource_name=layer.name, workspace_name=layer.workspace.name)

    def _get_bbox(self, layer_name, workspace_name):
        return BboxFactory().get_bbox_for_resource(resource_name=layer_name, workspace_name=workspace_name)
