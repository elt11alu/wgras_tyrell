from .base import Service
from ..wmsproxy import wmsproxy
from ..geoserverconnection import WCS
from ..spatialprocess.raster import floodfill, statistics
from ..postgisconnection.dbFunctions import DbFunctions
from ..postgisconnection.geometryfunctions import GeometryFunctions
from ..models import Layer, Elevationlayer, Vectorlayer, Pointlayer, Polygonlayer
from .. import app


class ProcessService(Service):
    def __init__(self):
        super(ProcessService, self).__init__()

    def extractPolygonFromRaster(self, layer_id, coords, increase):
        layer = Elevationlayer.query.filter_by(layerid=layer_id).first()
        workspace_name = layer.workspace.name
        layer_name = '{}{}{}'.format(workspace_name, ':', layer.name)
        wcs = WCS.WCSConnection()
        flood_fill = floodfill.FloodFill()
        filePath = wcs.downloadWCSLayer(layerName=layer_name)
        if filePath is not None:
            return flood_fill.floodFillLocal(filePath=filePath, incoords=coords, water_increase=increase)
        return None

    def buffer_layer(self, layer_id, distance):
        layer = Layer.query.filter_by(layerid=layer_id).first()
        if layer is not None:
            return GeometryFunctions().buffer_layer(layer_name=layer.name, distance=distance)
        app.logger.warning('Could not find layer with id {}'.format(layer_id))
        return None

    def buffer_feature(self, feature_id, layer_id, distance):
        layer = Layer.query.filter_by(layerid=layer_id).first()
        if layer is not None:
            return GeometryFunctions().buffer_feature(layer_name=layer.name, feature_id=feature_id, distance=distance)
        app.logger.warning('Could not find layer with id {}'.format(layer_id))
        return None

    def getRasterStatistics(self, filePath):
        stats = statistics.RasterStatistics()
        return stats.getRasterStatisticsFromFile(filePath)


    def get_external_wms(self, url):
        wms_proxy = wmsproxy.WMSProxy()
        wms_proxy.connect(url)
        return wms_proxy.get_layers()

    def calculate_within(self, point_layer_id, polygon_layer_id):
        point_layer = Pointlayer.query.filter_by(layerid=point_layer_id).first()
        polygon_layer = Polygonlayer.query.filter_by(layerid=polygon_layer_id).first()
        if point_layer is not None:
            if polygon_layer is not None:
                return GeometryFunctions().within(points_table_name=point_layer.name, polygon_table_name=polygon_layer.name)
            app.logger.warning('Could not find layer with id {}'.format(polygon_layer_id))
            return None
        app.logger.warning('Could not find layer with id {}'.format(point_layer_id))
        return None

    def query_by_attribute(self, layer_id, attribute_name, attribute_value, operator):
        layer = Vectorlayer.query.filter_by(layerid=layer_id).first()
        if layer is not None:
            return GeometryFunctions().query_by_attribute(
                layer_name=layer.name,
                attribute_name=attribute_name,
                attribute_value=attribute_value,
                operator=operator
            )
        app.logger.warning('Could not find layer with id {}'.format(layer_id))
        return None
