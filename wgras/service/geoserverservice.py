from base import Service
from ..models import *
from ..geoserverconnection import WCS, WFS, WMS, Workspaces, Stores, Base, Resource, bbox
from ..postgisconnection.geometryfunctions import GeometryFunctions
from ..namefactory.name_factory import NameFactory


class GeoServerService(Service):
    def __init__(self):
        super(GeoServerService, self).__init__()
        self.name_fac = NameFactory()

    def getAllWMSMetadata(self):
        wms = WMS.WMSConnection()
        layers = wms.getAllWMSMetadata()
        wmsList = []
        for layer in layers['layers']:
            db_layer = Rasterlayer.query.filter_by(name=layer['layerName'].split(':')[1]).first()
            if db_layer is not None:
                layer['layerId'] = db_layer.layerid
                wmsList.append(layer)
        return {'layers': wmsList}

    def getWMSMetadata(self, name):
        wms = WMS.WMSConnection()
        return wms.getWMSMetadata(name)

    def getAllWCSMetadata(self):
        wcs = WCS.WCSConnection()
        return wcs.getAllWCSMetadata()

    def getWCSMetadata(self, name):
        wcs = WCS.WCSConnection()
        return wcs.getWMSMetadata(name)

    def get_elevation_metadata(self):
        layers = Elevationlayer.query.all()
        return [lyr.getObj() for lyr in layers]

    def getAllWFSMetadata(self):
        wfs = WFS.WFSConnection()
        layers = wfs.getAllWFSMetadata()
        wfsList = []
        for layer in layers['layers']:
            db_layer = Vectorlayer.query.filter_by(name=layer['layerName'].split(':')[1]).first()
            if db_layer is not None:
                layer['layerId'] = db_layer.layerid
                wfsList.append(layer)
        return {'layers': wfsList}

    def getWFSMetadata(self, name):
        wfs = WFS.WFSConnection()
        return wfs.getWFSMetadata(name)

    def get_resource_repr(self, layer_name, workspace_name):
        resource = Resource.Resource()
        return resource.get_gs_resource_metadata(layer_name=layer_name, workspace_name=workspace_name)

    def get_resources_repr(self):
        resource = Resource.Resource()
        return resource.get_gs_resources_metadata()

    def getWorkspaceNames(self):
        workspace = Workspaces.GsWorkspace()
        return workspace.getWorkspacesAsDict()

    def get_workspace_names_as_list(self):
        gs_workspace = Workspaces.GsWorkspace()
        return gs_workspace.getWorkspaceNames()

    def createGsWorkspace(self, workspaceName):
        workspace = Workspaces.GsWorkspace()
        return workspace.createWorkspace(workspaceName)

    def deleteGsWorkspace(self, workspaceName):
        workspace = Workspaces.GsWorkspace()
        return workspace.deleteWorkspace(workspaceName)

    def createGsCoverageStore(self, store_name, workspace_name, file_path):
        gs_store = Stores.GsStore()
        return gs_store.createCoverageStore(datastoreName=store_name, workspace=workspace_name, filePath=file_path)

    def publish_postgis_table_in_geoserver(self, table_name, workspaceId, store_name):
        workspace = Workspace.query.filter_by(wsid=workspaceId).first()
        if workspace is not None:
            '''Creating store in geoserver and publishing'''
            gs_store = Stores.GsStore()
            if store_name is not None:
                return gs_store.publishFeatureType(table_name, store_name, workspace.name, 'EPSG:4326')
            return None
        return None

    def get_published(self):
        return self.getAllWMSMetadata()

    def get_unpublished(self):
        return Base.GeoserverConnection().getUnpublishedLayers()

    def publish_layer(self, layer_name, workspace_name, publish=True):
        wms = WMS.WMSConnection()
        return wms.publishLayer(resourceName=layer_name, workspaceName=workspace_name, publish=publish)

    # --- Keywords --- #

    def get_keywords(self, layerName, workspaceName):
        return Base.GeoserverConnection().getLayerKeywords(layerName=layerName, workspaceName=workspaceName)

    # --- Properties --- #

    def update_properties(self, layerName, workspaceName, properties):
        return Base.GeoserverConnection().updateLayerProperties(layerName=layerName, workspaceName=workspaceName,
                                                                properties=properties)

    # --- WCS --- #

    def downloadWCS(self, layerName):
        wcs = WCS.WCSConnection()
        return wcs.downloadWCSLayer(layerName=layerName)

    # --- BBOX --- #

    def recalculate_bbox(self, layer_name, workspace_name):
        b_box = bbox.BboxFactory()
        b_box.recalculate_bbox(resource_name=layer_name, workspace_name=workspace_name)

    def set_world_bbox(self, layer_name, workspace_name):
        b_box = bbox.BboxFactory()
        b_box.set_bbox_on_resource_to_max(resource_name=layer_name, workspace_name=workspace_name)

        # --- STORE --- #

    def create_postgis_store_in_gs(self, workspace_name, schema):
        store_name = self.name_fac.create_store_name(workspace_name=workspace_name)
        return Stores.GsStore().createPostGisStore(workspaceName=workspace_name, store_name=store_name, schema=schema)

    def create_coverage_store_in_gs(self, file_path, workspace_name, store_name):
        store_name = self.name_fac.create_coverage_store_name(store_name=store_name)
        return Stores.GsStore().createCoverageStore(datastoreName=store_name, workspace_name=workspace_name, filePath=file_path)
