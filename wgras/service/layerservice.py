from ..postgisconnection.shp_to_postgis import ShapeFileToPostGis
from ..mongodbconnection.layerdocument import LayerDocument
from ..postgisconnection.csvToPostGis import CSVToPostGis
from ..postgisconnection.base import PostGisConnection
from ..postgisconnection.table import Table
from ..postgisconnection.pg_import import PostGisImport
from .base import Service
from ..models import *
from .geoserverservice import GeoServerService
from postgisservice import PostgisService
from ..namefactory.name_factory import NameFactory
from .. import app


class Layerservice(Service):
    def __init__(self):
        super(Layerservice, self).__init__()
        self.name_fac = NameFactory()

    def get_vector_layers_metadata(self):
        vector_layers_from_db = Vectorlayer.query.all()
        return [lyr.getObj() for lyr in vector_layers_from_db]

    def get_vector_layer_metadata_by_name(self, name):
        vector_layer_from_db = Vectorlayer.query.filter_by(name=name).first()
        return vector_layer_from_db.getObj()

    def get_vector_layer_metadata_by_id(self, layer_id):
        vector_layer_from_db = Vectorlayer.query.filter_by(
            layerid=layer_id).first()
        return vector_layer_from_db.getObj()

    def get_raster_layers_metadata(self):
        raster_layers_from_db = Rasterlayer.query.all()
        return [lyr.getObj() for lyr in raster_layers_from_db]

    def get_raster_layer_metadata_by_name(self, name):
        raster_layer_from_db = Rasterlayer.query.filter_by(name=name).first()
        return raster_layer_from_db.getObj()

    def get_raster_layer_metadata_by_id(self, layer_id):
        raster_layer_from_db = Rasterlayer.query.filter_by(
            layerid=layer_id).first()
        return raster_layer_from_db.getObj()

    def get_wcs_layer_metadata(self):
        gs_service = GeoServerService()
        return gs_service.getAllWCSMetadata()

    def get_wcs__layer_metadata_by_name(self, name):
        gs_service = GeoServerService()
        return gs_service.getWCSMetadata(name)

    def get_elevation_layers_metadata(self):
        gs_service = GeoServerService()
        return gs_service.get_elevation_metadata()

    def create_ows_metadata(self, name, url, description, category):
        ows_metadata = Externalows(name=name, url=url, description=description, category=category)
        try:
            db.session.add(ows_metadata)
            db.session.commit()
            return ows_metadata.getObj()
        except Exception as e:
            app.logger.warning(e.message)
            return None

    def delete_ows_metadata(self, ows_id):
        ows_metadata = Externalows.query.filter_by(owsid=ows_id).first()
        if ows_metadata is not None:
            metadata_obj = ows_metadata.getObj()
            try:
                db.session.delete(ows_metadata)
                db.session.commit()
                return metadata_obj
            except Exception as e:
                app.logger.warning(e.message)
                return None
        app.logger.warning('Could not find ows metadata with id {}'.format(ows_id))
        return None

    # --- Vector Layer creation methods --- #

    # def create_vector_layer_from_shp_file(self, shp_path, table_name, workspace_id, published, schema,
    #                                       category, description):
    #     shp_to_pg = ShapeFileToPostGis()
    #     pg_service = PostgisService()
    #     gs_service = GeoServerService()
    #     workspace = Workspace.query.filter_by(wsid=workspace_id).first()
    #     table_name = self.name_fac.create_layer_name(table_name)
    #     store_name = gs_service.create_postgis_store_in_gs(
    #         workspace_name=workspace.name, schema=schema)
    #     created = shp_to_pg.create_postgis_table_from_shp(path_to_shp=shpPath, table_name=table_name, schema='public',
    #                                                       srs=4326)
    #     geom_type = pg_service.get_geom_type_for_table(table_name=table_name)
    #     if created is not None and geom_type is not None:
    #         published_in_gs = gs_service.publish_postgis_table_in_geoserver(table_name=tableName,
    #                                                                         workspaceId=workspaceId,
    #                                                                         store_name=store_name)
    #         if published_in_gs:
    #             vector_layer_obj = pg_service.create_db_layer(geom_type=geom_type, name=table_name, title=table_name,
    #                                                           workspaceId=workspace.wsid, store_name=store_name,
    #                                                           published=published, category=category,
    #                                                           description=description)
    #
    #             return vector_layer_obj
    #         return None
    #     return None

    def create_vector_layer_from_shp_file(self, document_id, layer_name, workspaceId, published, schema,
                                          category, description):
        mongo_doc = LayerDocument()
        pg_service = PostgisService()
        pg_import = PostGisImport()
        gs_service = GeoServerService()
        workspace = Workspace.query.filter_by(wsid=workspaceId).first()
        table_name = self.name_fac.create_layer_name(layer_name)
        store_name = gs_service.create_postgis_store_in_gs(
            workspace_name=workspace.name, schema=schema)
        shp_path = mongo_doc.get_shp_file_path(document_id=document_id)
        created = pg_import.create_table_from_shp(path_to_shp=shp_path, table_name=table_name, schema=schema, srs=4326)
        geom_type = pg_service.get_geom_type_for_table(table_name=table_name)
        if created is not None and geom_type is not None:
            published_in_gs = gs_service.publish_postgis_table_in_geoserver(table_name=table_name,
                                                                            workspaceId=workspace.wsid,
                                                                            store_name=store_name)
            if published_in_gs:
                vector_layer_obj = pg_service.create_db_layer(geom_type=geom_type, name=table_name, title=table_name,
                                                              workspaceId=workspace.wsid, store_name=store_name,
                                                              published=published, category=category,
                                                              description=description)
                return vector_layer_obj
            return None
        return None

    def create_vector_layer_from_csv_file(self, filePath, csvFileName, tableName, workspaceId, published, schema,
                                          description, category,
                                          latField='lat', lonField='lon'):
        csv_to_postgis = CSVToPostGis()
        pg_conn = PostGisConnection()
        gs_service = GeoServerService()
        pg_service = PostgisService()
        table_name = self.name_fac.create_layer_name(tableName)
        workspace = Workspace.query.filter_by(wsid=workspaceId).first()
        store_name = gs_service.create_postgis_store_in_gs(
            workspace_name=workspace.name, schema=schema)
        created = csv_to_postgis.create_postgis_table_from_csv(filePath=str(filePath), tableName=str(table_name),
                                                               latField=str(latField), lonField=str(lonField))
        geom_type = pg_conn.get_geom_type_for_table(table_name=table_name)
        if created is not None and geom_type is not None:
            published_in_gs = gs_service.publish_postgis_table_in_geoserver(table_name=tableName,
                                                                            workspaceId=workspaceId,
                                                                            store_name=store_name)
            if published_in_gs:
                vector_layer_obj = pg_service.create_db_layer(geom_type=geom_type, name=table_name, title=table_name,
                                                              workspaceId=workspace.wsid, store_name=store_name,
                                                              published=published, description=description,
                                                              category=category)
                return vector_layer_obj
            return None
        return None

    def readCSV(self, filePath):
        return CSVToPostGis().readCSV(filePath=filePath)

    def create_new__vector_layer_with_geometry(self, workspace_id, layer_name_dirty, layer_title, geometry_type, schema,
                                               fields, geometry, published, description, category):
        pg_service = PostgisService()
        gs_service = GeoServerService()

        workspace = Workspace.query.filter_by(wsid=workspace_id).first()

        layer_name = self.name_fac.create_layer_name(suffix=layer_name_dirty)
        store_name = gs_service.create_postgis_store_in_gs(
            workspace_name=workspace.name, schema=schema)
        table_created = None

        if workspace is not None and store_name is not None:
            if geometry_type == 'Point':
                '''create postgis point table'''
                table_created = pg_service.create_point_table_from_geojson(layer_name=layer_name, schema_name='public',
                                                                           field_definition=fields, points=geometry)

            elif geometry_type == 'MultiPoint':
                '''create postgis point table'''
                table_created = pg_service.create_multipoint_table_from_geojson(layer_name=layer_name,
                                                                                schema_name='public',
                                                                                field_definition=fields,
                                                                                points=geometry)

            elif geometry_type == 'LineString':
                '''create postgis line table'''
                table_created = pg_service.create_line_table_from_geojson(layer_name=layer_name, schema_name='public',
                                                                          field_definition=fields, lines=geometry)

            elif geometry_type == 'MultiLineString':
                '''create postgis line table'''
                table_created = pg_service.create_multiline_table_from_geojson(layer_name=layer_name,
                                                                               schema_name='public',
                                                                               field_definition=fields, lines=geometry)

            elif geometry_type == 'Polygon':
                table_created = pg_service.create_polygon_table_from_geojson(layer_name=layer_name,
                                                                             schema_name='public',
                                                                             field_definition=fields, polygons=geometry)

            elif geometry_type == 'MultiPolygon':
                table_created = pg_service.create_multipolygon_table_from_geojson(layer_name=layer_name,
                                                                                  schema_name='public',
                                                                                  field_definition=fields,
                                                                                  polygons=geometry)

            if table_created is not None:
                '''publish to geoServer'''
                geom_type = pg_service.get_geom_type_for_table(
                    table_name=layer_name)
                published_in_gs = gs_service.publish_postgis_table_in_geoserver(table_name=layer_name,
                                                                                workspaceId=workspace.wsid,
                                                                                store_name=store_name)
                if published_in_gs:
                    #gs_service.recalculate_bbox(layer_name=layer_name, workspace_name=workspace.name)
                    db_layer_obj = pg_service.create_db_layer(geom_type=geom_type, name=layer_name, title=layer_title,
                                                              workspaceId=workspace.wsid, store_name=store_name,
                                                              published=published, description=description,
                                                              category=category)

                    return db_layer_obj
                app.logger.warning(
                    'Could not publish postgis table {} in workspace: {}, with store name {} in geoserver'.format(
                        layer_name, workspace.name, store_name))
                return None
            app.logger.warning('Could not create table in postGIS for geom type {}'.format(geometry_type))
            return None
        app.logger.warning('Could not find workspace with id {} or store {}'.format(workspace_id, store_name))
        return None

    def create_new_empty_vector_layer(self, workspace_id, layer_name_dirty, layer_title, geometry_type, schema, fields,
                                      published, description, category):
        pg_service = PostgisService()
        gs_service = GeoServerService()
        workspace = Workspace.query.filter_by(wsid=workspace_id).first()

        layer_name = self.name_fac.create_layer_name(suffix=layer_name_dirty)
        store_name = gs_service.create_postgis_store_in_gs(
            workspace_name=workspace.name, schema=schema)
        table_created = None

        if workspace is not None and store_name is not None:
            if geometry_type == 'Point':
                '''create postgis point table'''
                table_created = pg_service.create_empty_point_table(layer_name=layer_name, schema_name='public',
                                                                    fields=fields)

            elif geometry_type == 'MultiPoint':
                table_created = pg_service.create_empty_multipoint_table(layer_name=layer_name, schema_name='public',
                                                                         fields=fields)

            elif geometry_type == 'LineString':
                '''create postgis line table'''
                table_created = pg_service.create_empty_line_table(layer_name=layer_name, schema_name='public',
                                                                   fields=fields)

            elif geometry_type == 'MultiLineString':
                table_created = pg_service.create_empty_multiline_table(layer_name=layer_name, schema_name='public',
                                                                        fields=fields)

            elif geometry_type == 'Polygon':
                table_created = pg_service.create_empty_polygon_table(layer_name=layer_name, schema_name='public',
                                                                      fields=fields)

            elif geometry_type == 'MultiPolygon':
                table_created = pg_service.create_empty_multipolygon_table(layer_name=layer_name, schema_name='public',
                                                                           fields=fields)

            if table_created is not None:
                '''publish to geoServer'''
                epsgCode = 'EPSG:4326'

                if store_name is not None:
                    geom_type = pg_service.get_geom_type_for_table(
                        table_name=layer_name)
                    published_in_gs = gs_service.publish_postgis_table_in_geoserver(table_name=layer_name,
                                                                                    workspaceId=workspace.wsid,
                                                                                    store_name=store_name)
                    if published_in_gs:
                        gs_service.set_world_bbox(layer_name=layer_name, workspace_name=workspace.name)
                        created = pg_service.create_db_layer(geom_type=geom_type, name=layer_name, title=layer_title,
                                                             workspaceId=workspace.wsid, store_name=store_name,
                                                             published=published, description=description,
                                                             category=category)

                        return created
                    self.logger.info('Feature could not be published')
                    return None
                self.logger.info('Store name was none')
                return None
            self.logger.info('Table was non or metadata was none')
            return None
        self.logger.info('Workspace or store name was none')
        return None

    # --- Raster layer creation methods --- #

    def create_raster_layer_from_geotiff(self, file_path, workspace_id, layer_name_dirty, published, haselevationdata,
                                         description, category):
        ''' When creating a coverage store, the layer name can not be set, but is automatically set to the store name'''
        gs_service = GeoServerService()
        pg_service = PostgisService()
        workspace = Workspace.query.filter_by(wsid=workspace_id).first()
        layer_name = self.name_fac.create_layer_name(suffix=layer_name_dirty)
        store_name = gs_service.create_coverage_store_in_gs(
            workspace_name=workspace.name, file_path=file_path, store_name=layer_name)

        if store_name is not None:
            if haselevationdata:
                geom_type = 'Elevation'
                return pg_service.create_db_layer(geom_type=geom_type, name=store_name, title=store_name,
                                                  workspaceId=workspace.wsid, store_name=store_name,
                                                  published=published, file_path=file_path, description=description,
                                                  category=category)
            else:
                geom_type = 'Raster'
                return pg_service.create_db_layer(geom_type=geom_type, name=store_name, title=store_name,
                                                  workspaceId=workspace.wsid, store_name=store_name,
                                                  published=published, file_path=None, description=description,
                                                  category=category)

        return None

    # --- Insert geometry methods --- #

    def insert_geojson_point_in_layer(self, layer_id, geojson_point):
        service = PostgisService()
        return service.insert_geojson_point(layer_id, geojson_point)

    def insert_geojson_line_in_layer(self, layer_id, geojson_line):
        service = PostgisService()
        return service.insert_geojson_line(layer_id, geojson_line)

    def insert_geojson_polygon_in_layer(self, layer_id, geojson_polygon):
        service = PostgisService()
        return service.insert_geojson_polygon(layer_id, geojson_polygon)

    def insert_wkt(self):
        pass

    # --- Attribute methods --- #

    def get_attributes_for_layer_by_name(self, layerName, workspaceName):
        return Table().get_layer_attributes(layer_name=layerName)

    def get_attributes_for_layer_by_id(self, layer_id):
        layer = Layer.query.filter_by(layerid=layer_id).first()
        return Table().get_layer_attributes(layer_name=str(layer.name))

    # --- Publish methods --- #

    def publishLayer(self, layerName, workspaceName, publish):
        gs_service = GeoServerService()
        return gs_service.publish_layer(layer_name=layerName, workspace_name=workspaceName, publish=publish)

    def getPublishedLayers(self):
        gs_service = GeoServerService()
        return gs_service.getAllWMSMetadata()

    def getUnpublishedLayers(self):
        gs_service = GeoServerService()
        return gs_service.get_unpublished()

    # --- Keywords --- #

    def getKeywordsForLayer(self, layerName, workspaceName):
        gs_service = GeoServerService()
        return gs_service.get_keywords(layerName=layerName, workspaceName=workspaceName)

    # --- Properties --- #

    def updatePropertiesForLayer(self, layerName, workspaceName, properties):
        gs_service = GeoServerService()
        return gs_service.update_properties(layerName=layerName, workspaceName=workspaceName,
                                            properties=properties)

    def is_geomtype_supported(self, geom_type):
        return Geomtype.query.filter_by(geom_type=geom_type).first() is not None

    def is_attribute_type_supported(self, attribute_type):
        return Attributetype.query.filter_by(attributetype=attribute_type).first() is not None

    # ---- Description & Category ----#
    def update_layer_metadata(self, layer_id, description, category):
        layer = Layer.query.filter_by(layerid=layer_id).first()
        existing_category = Layercategory.query.filter_by(name=category).first()
        if layer is not None:
            if existing_category is None:
                new_category = Layercategory(name=category)
                layer.category = new_category
                layer.description = description
                db.session.commit()
                return layer.getObj()
            else:
                layer.category = existing_category
                layer.description = description
                db.session.commit()
                return layer.getObj()
        return None