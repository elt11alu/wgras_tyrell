from .base import Service
from ..namefactory.name_factory import NameFactory
from ..models import *
from ..systemaccess.accessCreator import AccessCreator
from .geoserverservice import GeoServerService
from .layerservice import Layerservice


class UserService(Service):
    def __init__(self):
        super(UserService, self).__init__()

    def getCountries(self):
        return Country.query.all()

    def getCountry(self, isocode):
        return Country.query.filter_by(isocode=isocode).first()

    def createCountry(self, isocode):
        country = Availablecountry.query.filter_by(isocode=isocode).first()
        if country is not None and Country.query.filter_by(isocode=isocode).first() is None:
            country.publish()
            return Country.query.filter_by(isocode=isocode).first()
        return None

    def deleteCountry(self, isocode):
        country = Availablecountry.query.filter_by(isocode=isocode).first()
        if country:
            obj = country.getObj()
            country.unPublish()
            return obj
        return None

    def getOrganizations(self):
        return Organization.query.all()

    def getOrganization(self, orgid):
        return Organization.query.filter_by(orgid=orgid).first()

    def createOrganization(self, isocode, fullname, shortname):
        country = Country.query.filter_by(isocode=isocode).first()
        if country:
            org = Organization(fullname=fullname, shortname=shortname, country=country)
            db.session.add(org)
            db.session.commit()
            return org.getObj()
        return None

    def deleteOrganization(self, orgid):
        org = Organization.query.filter_by(orgid=orgid).first()
        if org:
            resp = org.getObj()
            db.session.delete(org)
            db.session.commit()
            return resp
        return None

    def getUsers(self):
        return User.query.all()

    def getSuperAdmins(self):
        return Superadmin.query.all()

    def getCountryAdmins(self):
        return Countryadmin.query.all()

    def getCountryAdminsByIsocode(self, isocode):
        country = Country.query.filter_by(isocode=isocode).first()
        if country is not None:
            return Countryadmin.query.filter_by(country=country).all()
        return None

    def createCountryAdmin(self, username, password, email, isocode):
        c = Country.query.filter_by(isocode=isocode).first()
        access_creator = AccessCreator()
        gs_service = GeoServerService()
        name_fac = NameFactory()
        if c is not None:
            newadmin = Countryadmin(username=username, email=email, country=c,
                                    password=bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()))
            db.session.add(newadmin)
            db.session.commit()
            ws_name = name_fac.create_private_workspace_name(newadmin.username)
            gs_workspace = gs_service.createGsWorkspace(ws_name)
            if gs_workspace is not None:
                db_workspace = Privateworkspace(name=ws_name, user=newadmin)
                db.session.add(db_workspace)
                db.session.commit()

                access_creator.create_access_for_country_admin(userid=newadmin.userid)
                self._create_private_layers(user=newadmin, workspace=db_workspace)
                return newadmin.getObj()
        return None

    def deleteCountryAdmin(self, userid):
        user = Countryadmin.query.filter_by(userid=userid).first()
        if user is not None:
            userObj = user.getObj()
            db.session.delete(user)
            db.session.commit()
            return userObj
        return None

    def getCountryUsers(self):
        return Countryuser.query.all()

    def getCountryUsersByIsocode(self, isocode):
        country = Country.query.filter_by(isocode=isocode).first()
        if country is not None:
            return Countryuser.query.filter_by(country=country).all()
        return None

    def createCountryUser(self, username, email, password, isocode):
        c = Country.query.filter_by(isocode=isocode).first()
        access_creator = AccessCreator()
        gs_service = GeoServerService()
        name_fac = NameFactory()
        if c is not None:
            newuser = Countryuser(username=username, email=email, country=c,
                                  password=bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()))
            db.session.add(newuser)
            db.session.commit()

            ws_name = name_fac.create_private_workspace_name(newuser.username)
            gs_workspace = gs_service.createGsWorkspace(ws_name)
            if gs_workspace is not None:
                db_workspace = Privateworkspace(name=ws_name, user=newuser)
                db.session.add(db_workspace)
                db.session.commit()

                access_creator.create_access_for_country_user(userid=newuser.userid)
                self._create_private_layers(user=newuser, workspace=db_workspace)
                return newuser.getObj()
        return None

    def deleteCountryUser(self, userid):
        user = Countryuser.query.filter_by(userid=userid).first()
        if user is not None:
            userObj = user.getObj()
            db.session.delete(user)
            db.session.commit()
            return userObj
        return None

    def getOrgadmins(self):
        return Orgadmin.query.all()

    def getOrgadminsByOrgId(self, orgid):
        org = Organization.query.filter_by(orgid=orgid).first()
        if org is not None:
            return Orgadmin.query.filter_by(organization=org).all()
        return None

    def createOrgAdmin(self, username, password, email, orgid):
        org = Organization.query.filter_by(orgid=orgid).first()
        access_creator = AccessCreator()
        gs_service = GeoServerService()
        name_fac = NameFactory()
        if org is not None:
            newuser = Orgadmin(username=username, email=email, organization=org,
                               password=bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()))
            db.session.add(newuser)
            db.session.commit()
            ws_name = name_fac.create_private_workspace_name(newuser.username)
            gs_workspace = gs_service.createGsWorkspace(ws_name)
            if gs_workspace is not None:
                db_workspace = Privateworkspace(name=ws_name, user=newuser)
                db.session.add(db_workspace)
                db.session.commit()

                access_creator.create_access_for_org_admin(userid=newuser.userid)
                self._create_private_layers(user=newuser, workspace=db_workspace)
                return newuser.getObj()

        return None

    def deleteOrgadmin(self, userid):
        user = Orgadmin.query.filter_by(userid=userid).first()
        if user is not None:
            userObj = user.getObj()
            db.session.delete(user)
            db.session.commit()
            return userObj
        return None

    def getConversationsForUser(self, user):
        return [c.getObj() for c in user.conversations.all()]

    def send_message_to_new_conversation(self, conversationTitle, messageText, sender, recieverid):
        reciever = User.query.filter_by(userid=recieverid).first()
        print reciever
        if reciever is not None:
            conversation = self.getConversation(sender, reciever)
            if conversation is None:
                conversation = self.createConversation(sender, reciever, conversationTitle)
                message = Message(message=messageText, sender=sender, conversation=conversation)
                db.session.commit()
                return message
            else:
                message = Message(message=messageText, sender=sender, conversation=conversation)
                db.session.commit()
                return message

        return None

    def send_message_to_existing_conversation(self, conversationId, messageText, senderId):
        conversation = Conversation.query.filter_by(id=conversationId).first()
        sender = User.query.filter_by(userid=senderId).first()
        if conversation is not None:
            if sender is not None:
                message = Message(message=messageText, sender=sender, conversation=conversation)
                db.session.add(message)
                db.session.commit()
                return message.getObj()
            return None
        return None

    def getConversation(self, currentUser, participant):
        conversations = currentUser.conversations
        for conv in conversations:
            if participant in conv.participants:
                return conv
        return None

    def createConversation(self, currentUser, reciever, conversationTitle):
        conv = Conversation(title=conversationTitle)
        conv.participants.append(currentUser)
        conv.participants.append(reciever)
        db.session.commit()
        return conv

    def getConversationById(self, conversationId):
        conversation = Conversation.query.filter_by(id=conversationId).first()
        if conversation:
            return conversation.getObj()
        return None

    def delete_conversation(self, conversationId):
        conversation = Conversation.query.filter_by(id=conversationId).first()
        if conversation is not None:
            db.session.delete(conversation)
            db.session.commit()
            return True
        return None

    def getUser(self, username=None, userid=None):
        if username is not None:
            return User.query.filter_by(username=username).first().getObj()
        elif userid is not None:
            return User.query.filter_by(userid=userid).first().getObj()
        else:
            return None

    def getUsersObj(self):
        users = User.query.all()
        userList = []
        for user in users:
            userList.append(user.getObj())
        return userList

    def get_workspaces(self):
        return Workspace.query.all()

    def existsUser(self, email, username=None, userId=None):
        existsemail = User.query.filter_by(email=email).first()
        if username is not None:
            existsuser = User.query.filter_by(username=username).first()
            return existsemail is not None or existsuser is not None
        elif userId is not None:
            existsuser = User.query.filter_by(userid=userId).first()
            return existsemail is not None or existsuser is not None
        return False

    def _create_private_layers(self, workspace, user):
        layer_service = Layerservice()
        geom_names = ['Point', 'LineString', 'Polygon']
        category = 'Private layers'
        name_fac = NameFactory()
        for geom_name in geom_names:
            layer_name = name_fac.create_private_layer_name(user_name=user.username, suffix=geom_name)
            layer_title = name_fac.create_layer_title(title=geom_name)
            schema = 'public'
            layer_service.create_new_empty_vector_layer(workspace_id=workspace.wsid, layer_name_dirty=layer_name,
                                                        layer_title=layer_title, geometry_type=geom_name, schema=schema,
                                                        fields=[], published=True, category=category,
                                                        description='A {} layer created by {}'.format(geom_name,
                                                                                                      user.username))
