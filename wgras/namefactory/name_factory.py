
import random


class NameFactory():

    def __init__(self):
        self.public = 'public'
        self.admin = 'admin'
        self.org = '_org_'
        self.private = 'private'
        self.point = 'point'
        self.line = 'line'
        self.polygon = 'polygon'

    def create_store_name(self, workspace_name):
        return '{}{}{}'.format(self._format_workspace_name(workspace_name), '_', str(random.randint(1, 1000)))

    def create_coverage_store_name(self, store_name):
        return '{}{}{}'.format(self._format_workspace_name(store_name), '_', str(random.randint(1, 1000)))

    def create_country_workspace_name(self, country_name):
        return self._format_workspace_name(country_name)

    def create_admin_workspace_name(self, suffix):
        return '{}{}{}'.format(self.admin, '_', self._format_suffix(suffix))

    def create_org_workspace_name(self, country_name, org_short_name):
        return '{}{}{}'.format(country_name, '_', self._format_org_name(org_short_name))

    def create_public_workspace_name(self, suffix):
        return '{}{}{}'.format(self.public, '_', self._format_suffix(suffix))

    def create_private_workspace_name(self, user_name, suffix=None):
        if suffix is not None:
            return '{}{}{}{}'.format(self.private, '_', user_name, self._format_suffix(suffix))
        return '{}{}{}'.format(self.private, '_', user_name)

    def create_private_layer_name(self, user_name, suffix):
        return '{}{}{}'.format(user_name, '_', self._format_suffix(suffix))

    def create_layer_name(self, suffix):
        return self._format_suffix(suffix).lower()

    def create_layer_title(self, title):
        return '{}'.format(self._format_title(title))

    # --- PRIVATE METHODS --- #
    def _format_workspace_name(self, workspace_name):
        return workspace_name.replace(' ', '').replace('-', '_').replace('.', '_').lower()

    def _format_suffix(self, suffix):
        return suffix.replace(' ', '').replace('-', '_').replace('.', '_').lower()

    def _format_org_name(self, org_name):
        return org_name.replace(' ', '').replace('-', '_').replace('.', '_')

    def _format_title(self, title):
        return title.replace(' ', '').replace('.', '_').replace('-', '_')
