class BaseRestModel(object):
    def __init__(self,
                 layerName,
                 layerTitle,
                 workspaceName,
                 layerInfo,
                 bboxMinX,
                 bboxMinY,
                 bboxMaxX,
                 bboxMaxY,
                 keywords):
        self.layerName = layerName
        self.layerTitle = layerTitle
        self.workspaceName = workspaceName
        self.layerInfo = layerInfo
        self.bboxMinX = bboxMinX
        self.bboxMinY = bboxMinY
        self.bboxMaxX = bboxMaxX
        self.bboxMaxY = bboxMaxY
        self.keywords = keywords

    def getDict(self):
        return self.__dict__
