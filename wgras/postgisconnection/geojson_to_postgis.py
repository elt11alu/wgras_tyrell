import ogr
import osr
from base import PostGisConnection
from .. import app


class GeojsonToPostgis(PostGisConnection):
    def __init__(self):
        super(GeojsonToPostgis, self).__init__()

    #############################################
    #      Create table with geojson geom       #
    #                                           #
    #############################################

    def create_point_table_with_geometry(self, schema, table_name, json_points, field_definition):
        if self.create_empty_point_table(table_name=table_name, schema_name=schema, fields=field_definition):
            return self.insert_json_geom(table_name=table_name, json_geom=json_points)
        return False

    def create_multipoint_table_with_geometry(self, schema, table_name, json_points, field_definition):
        if self.create_empty_multipoint_table(table_name=table_name, schema_name=schema, fields=field_definition):
            return self.insert_json_geom(table_name=table_name, json_geom=json_points)
        return False

    def create_line_table_with_geometry(self, schema, table_name, json_lines, field_definition):
        if self.create_empty_line_table(table_name=table_name, schema_name=schema, fields=field_definition):
            return self.insert_json_geom(table_name=table_name, json_geom=json_lines)
        return False

    def create_multiline_table_with_geometry(self, schema, table_name, json_lines, field_definition):
        if self.create_empty_multiline_table(table_name=table_name, schema_name=schema, fields=field_definition):
            return self.insert_json_geom(table_name=table_name, json_geom=json_lines)
        return False

    def create_polygon_table_with_geometry(self, schema, table_name, json_polygons, field_definition):

        if self.create_empty_polygon_table(table_name=table_name, schema_name=schema, fields=field_definition):
            return self.insert_json_geom(table_name=table_name, json_geom=json_polygons)
        return False

    def create_multipolygon_table_with_geometry(self, schema, table_name, json_polygons, field_definition):

        if self.create_empty_multipolygon_table(table_name=table_name, schema_name=schema, fields=field_definition):
            return self.insert_json_geom(table_name=table_name, json_geom=json_polygons)
        return False


    #####################################
    #      Create empty geom tables     #
    #                                   #
    #####################################
    def create_empty_point_table(self, table_name, schema_name, fields):
        options = ['OVERWRITE=YES', 'SCHEMA={}'.format(schema_name)]
        geom_type = ogr.wkbPoint
        return self._create_empty_table(table_name=table_name, fields=fields, geom_type=geom_type, options=options)

    def create_empty_multipoint_table(self, table_name, schema_name, fields):
        options = ['OVERWRITE=YES', 'SCHEMA={}'.format(schema_name)]
        geom_type = ogr.wkbMultiPoint
        return self._create_empty_table(table_name=table_name, fields=fields, geom_type=geom_type, options=options)

    def create_empty_line_table(self, table_name, schema_name, fields):
        options = ['OVERWRITE=YES', 'SCHEMA={}'.format(schema_name)]
        geom_type = ogr.wkbLineString
        return self._create_empty_table(table_name=table_name, fields=fields, geom_type=geom_type, options=options)

    def create_empty_multiline_table(self, table_name, schema_name, fields):
        options = ['OVERWRITE=YES', 'SCHEMA={}'.format(schema_name)]
        geom_type = ogr.wkbMultiLineString
        return self._create_empty_table(table_name=table_name, fields=fields, geom_type=geom_type, options=options)

    def create_empty_polygon_table(self, table_name, schema_name, fields):
        options = ['OVERWRITE=YES', 'SCHEMA={}'.format(schema_name)]
        geom_type = ogr.wkbPolygon
        return self._create_empty_table(table_name=table_name, fields=fields, geom_type=geom_type, options=options)

    def create_empty_multipolygon_table(self, table_name, schema_name, fields):
        options = ['OVERWRITE=YES', 'SCHEMA={}'.format(schema_name)]
        geom_type = ogr.wkbMultiPolygon
        return self._create_empty_table(table_name=table_name, fields=fields, geom_type=geom_type, options=options)

    #####################################
    #     Insert geometry functions     #
    #                                   #
    #####################################

    def insert_json_geom(self, table_name, json_geom):
        try:
            postgis_data_store = ogr.Open(self.ogrConnString, 1)
            postgis_layer = postgis_data_store.GetLayerByName(str(table_name))
            if postgis_layer is not None:
                json_driver = ogr.GetDriverByName('GeoJSON')
                json_data_store = json_driver.Open(json_geom)
                json_layer = json_data_store.GetLayer()
                json_layer_def = json_layer.GetLayerDefn()
                postgis_layer_def = postgis_layer.GetLayerDefn()

                postgis_layer.StartTransaction()
                for json_feature in json_layer:
                    geom = json_feature.GetGeometryRef()
                    postgis_feature = ogr.Feature(postgis_layer_def)
                    postgis_feature.SetGeometry(geom)
                    # setting field values
                    for ii in range(0, postgis_layer_def.GetFieldCount()):
                        postgis_field_def = postgis_layer_def.GetFieldDefn(ii)
                        postgis_field_name = postgis_field_def.GetName()
                        for yy in range(0, json_layer_def.GetFieldCount()):
                            json_field_def = json_layer_def.GetFieldDefn(yy)
                            json_field_name = json_field_def.GetName()
                            if postgis_field_name == json_field_name:
                                field_name = postgis_layer_def.GetFieldDefn(ii).GetNameRef()
                                field_value = json_feature.GetField(yy)
                                postgis_feature.SetField(field_name, field_value)
                    postgis_layer.CreateFeature(postgis_feature)
                    postgis_feature.Destroy()

                postgis_layer.CommitTransaction()
                postgis_data_store.Destroy()
                json_data_store.Destroy()
                return True
        except Exception as e:
            app.logger.error('Exception in class Table, function insert_json_point, ', e)
            return False

    def get_layer_attributes(self, layer_name):
        conn = ogr.Open(self.ogrConnString)

        lyr = conn.GetLayer(layer_name)
        if lyr is None:
            return {"layerName": layer_name, "attributes":[]}

        lyrDefn = lyr.GetLayerDefn()
        attributes = []
        for i in range(lyrDefn.GetFieldCount()):
            fieldName = lyrDefn.GetFieldDefn(i).GetName()
            fieldTypeCode = lyrDefn.GetFieldDefn(i).GetType()
            fieldType = lyrDefn.GetFieldDefn(i).GetFieldTypeName(fieldTypeCode)
            fieldWidth = lyrDefn.GetFieldDefn(i).GetWidth()
            GetPrecision = lyrDefn.GetFieldDefn(i).GetPrecision()

            attributes.append({"name": fieldName, "attributeType": fieldType})

        conn.Destroy()
        return {"layerName": layer_name, "attributes": attributes}

    #####################################
    #     Private functions             #
    #                                   #
    #####################################
    def _create_empty_table(self, table_name, fields, geom_type, options):
        try:
            ogr_geom_type = geom_type

            ogrds = ogr.Open(self.ogrConnString)
            srs = osr.SpatialReference()
            srs.ImportFromEPSG(4326)
            layer = ogrds.CreateLayer(str(table_name), srs, ogr_geom_type, options)
            layer.StartTransaction()
            for field in fields:
                field_def = self._get_field_def(str(field['name']), str(field['attributeType']))
                if field_def is not None:
                    layer.CreateField(field_def)
            layer.CommitTransaction()
            ogrds.Destroy()
            return True
        except Exception as e:
            app.logger.info('Exception in class Table, function create_empty_table, exception: {} '.format(e))
            return False

    def _get_field_def(self, field_name, type):
        app.logger.info('Attempting to create field: {} with type: {}'.format(field_name, type))
        if type == 'String':
            return ogr.FieldDefn(field_name, ogr.OFTString)
        elif type == 'Integer':
            return ogr.FieldDefn(field_name, ogr.OFTInteger)
        elif type == 'Real':
            return ogr.FieldDefn(field_name, ogr.OFTReal)
        elif type == 'string':
            return ogr.FieldDefn(field_name, ogr.OFTString)
        elif type == 'number':
            return ogr.FieldDefn(field_name, ogr.OFTInteger)
        else:
            app.logger.info('Found unsupported attribute type: {}, with name: {}'.format(type, field_name))
            return None
