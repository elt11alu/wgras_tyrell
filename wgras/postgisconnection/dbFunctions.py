from base import PostGisConnection
from osgeo import ogr
import psycopg2

class DbFunctions(PostGisConnection):

    def __init__(self):
        super(DbFunctions, self).__init__()

    def createUser(self, userName, password):
        pass

    def setPassword(self, username, password):
        pass

    def createTable(self, tableName):
        conn = self._conn()
        cur = conn.cursor()
        query = 'CREATE TABLE test (id serial PRIMARY KEY, num integer, data varchar)'
        cur.execute(query)
        conn.commit()
        self._close(cur, conn)

    def getSpatialTableAsOgrLayer(self, tableName):
        conn = self._ogrConn()
        layer = conn.GetLayer(tableName)
        conn.Destroy()
        return layer

    def buffer(self, wkt, distance):
        conn = self._conn()
        cur = conn.cursor()
        query = "SELECT ST_AsText(ST_Buffer(ST_GeomFromText("+"'"+wkt+"'"+"), "+distance+", 'quad_segs=8'))"
        cur.execute(query)
        res = cur.fetchall()
        conn.commit()
        self._close(cur, conn)
        return res[0][0]

    def _conn(self):
        return psycopg2.connect(host=self.DB_SERVER, user=self.DB_USER, port=self.DB_PORT,
                                 password=self.DB_PASSWORD, database=self.DB_NAME)

    def _close(self, cur, conn):
        cur.close()
        conn.close()

    def _ogrConn(self):
        ogr.Open(self.ogrConnString)
