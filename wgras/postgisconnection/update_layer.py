import psycopg2
from base import PostGisConnection
from base import app

class UpdateLayer(PostGisConnection):
    def __init__(self):
        super(UpdateLayer, self).__init__()


    def update_attribute_value(self, layer_name, feature_id, attr_name, attr_value):
        try:
            sql = 'UPDATE {} SET {} = %s WHERE ogc_fid = %s'.format(layer_name,attr_name)
            conn = psycopg2.connect(self.DATABASE_URI)
            cur = conn.cursor()
            cur.execute(sql, (attr_value, feature_id,))
            conn.commit()
            cur.close()
            conn.close()

            return True
        except Exception as e:
            app.logger.error(e.message)
            return False

    def get_attribute_values(self, layer_name, feature_id):
        sql = "SELECT * FROM {} WHERE ogc_fid = %s".format(layer_name)
        conn = psycopg2.connect(self.DATABASE_URI)
        cur = conn.cursor()
        cur.execute(sql, (feature_id,))
        result = cur.fetchall()
        cur.close()
        conn.close()
        return result


    def get_attribute_list(self, layer_name, feature_id):
        try:
            sql = 'SELECT column_name, data_type FROM information_schema.columns WHERE table_name = %s'
            conn = psycopg2.connect(self.DATABASE_URI)
            cur = conn.cursor()
            cur.execute(sql, (layer_name,))
            attr_names_and_types = cur.fetchall()

            sql2 = "SELECT * FROM {} WHERE ogc_fid = %s".format(layer_name)
            cur.execute(sql2, (feature_id,))
            attr_values = cur.fetchone()
            cols = cur.description

            nbr = 0
            result = []
            for col in cols:
                col_name = col[0]
                if col_name != 'wkb_geometry':
                    col_type = self._get_attr_type(col_name, attr_names_and_types)
                    col_value = attr_values[nbr]
                    result.append({'name':col_name, 'dataType': col_type, 'value':str(col_value)})
                nbr+=1

            cur.close()
            conn.close()
            return result
        except Exception as e:
            app.logger.error(e.message)
            return False


    def _get_attr_type(self, col_name, info_list):
        for obj in info_list:
            if obj[0] == col_name:
                return obj[1]
