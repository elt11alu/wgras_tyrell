from base import PostGisConnection
from osgeo import ogr, osr
from .. import app
from ..mongodbmodels import Tmpvectorlayer


class ShapeFileToPostGis(PostGisConnection):

    def __init__(self):
        super(ShapeFileToPostGis, self).__init__()
        self.connString = "PG: host=%s dbname=%s user=%s password=%s" % (self.DB_SERVER, self.DB_NAME, self.DB_USER, self.DB_PASSWORD)

    def create_postgis_table_from_shp(self, path_to_shp, table_name, schema='public', srs=4326):
        try:
            driver = ogr.GetDriverByName("ESRI Shapefile")
            shp_data_source = driver.Open(path_to_shp)

            if shp_data_source is not None:
                shp_layer = shp_data_source.GetLayer()
                postgis_data_source = ogr.Open(self.connString)
                if postgis_data_source is not None:
                    shp_layer_geom_type = shp_layer.GetGeomType()
                    shp_layer_fields = self.get_layer_fields(shp_layer.GetLayerDefn())
                    #shp_layer_fields = shp_layer.GetLayerDefn()

                    # casting geomtypes, many shapefiles contain both polygon and multipolygon etc.
                    if shp_layer_geom_type == ogr.wkbPolygon:
                        shp_layer_geom_type = ogr.wkbMultiPolygon
                    elif shp_layer_geom_type == ogr.wkbLineString:
                        shp_layer_geom_type = ogr.wkbMultiLineString

                    # Creating the postgis table
                    srs_def = osr.SpatialReference()
                    srs_def.ImportFromEPSG(srs)
                    postgis_layer = postgis_data_source.CreateLayer(str(table_name), srs_def, shp_layer_geom_type, ['OVERWRITE=NO SCHEMA={}'.format(schema)])

                    # Adding fields from shape file to postgis table, supported types see get_field_def
                    for field in shp_layer_fields:
                        field_def = self.get_field_def(field)
                        postgis_layer.CreateField(field_def)

                    # Inserting geometry from shp layer in postgis layer
                    # casting geomtypes, many shapefiles contain both polygon and multipolygon etc.
                    for feature in shp_layer:
                        geom = feature.GetGeometryRef()
                        if geom.GetGeometryType() == ogr.wkbPolygon:
                            geom = ogr.ForceToMultiPolygon(geom)
                            feature.SetGeometryDirectly(geom)
                        elif geom.GetGeometryType() == ogr.wkbLineString:
                            geom = ogr.ForceToMultiLineString(geom)
                            feature.SetGeometryDirectly(geom)
                        postgis_layer.StartTransaction()
                        postgis_layer.CreateFeature(feature)
                        postgis_layer.CommitTransaction()
                        feature.Destroy()

                    # destroying data sources
                    postgis_data_source.Destroy()
                    shp_data_source.Destroy()
                    return postgis_layer.GetGeomType()
        except Exception as e:
            app.logger.info('Exception in ShapeFileToPostgis, function create_postgis_table_from_shp: {}'.format(e.message))
            return None

    def get_layer_fields(self, layer_def):

        fields = []

        for i in range(layer_def.GetFieldCount()):
            field_name = layer_def.GetFieldDefn(i).GetName()
            type_code = layer_def.GetFieldDefn(i).GetType()
            field_type = layer_def.GetFieldDefn(
                i).GetFieldTypeName(type_code)
            field_with = layer_def.GetFieldDefn(i).GetWidth()
            precision = layer_def.GetFieldDefn(i).GetPrecision()
            fields.append({"fieldName": field_name, "fieldType": field_type,
                           "fieldWidth": field_with, "precision": precision, "fieldTypeCode": type_code})
        return fields

    def get_field_def(self, field):

        field_type_code = field["fieldTypeCode"]
        field_name = field["fieldName"]

        # OFTInteger
        if field_type_code == 0:
            field_def = ogr.FieldDefn(field_name, ogr.OFTInteger)
            return field_def

        # OFTReal
        if field_type_code == 2:
            field_def = ogr.FieldDefn(field_name, ogr.OFTReal)
            return field_def

        # OFTString
        if field_type_code == 4:
            field_def = ogr.FieldDefn(field_name, ogr.OFTString)
            field_def.SetWidth(field["fieldWidth"])
            return field_def



#
#
#         OGRFieldType {
#   OFTInteger = 0, OFTIntegerList = 1, OFTReal = 2, OFTRealList = 3,
#   OFTString = 4, OFTStringList = 5, OFTWideString = 6, OFTWideStringList = 7,
#   OFTBinary = 8, OFTDate = 9, OFTTime = 10, OFTDateTime = 11,
#   OFTInteger64 = 12, OFTInteger64List = 13, OFTMaxType = 13
# }