import ogr, osr
from base import PostGisConnection
from .. import app
import json


class GeometryFunctions(PostGisConnection):
    def __init__(self):
        super(GeometryFunctions, self).__init__()

    def buffer_layer(self, layer_name, distance):
        """
        :param layer_name: name of table with geometry to buffer
        :param distance: Buffer distance
        :return: Array of buffered geoJSON features
        """
        try:
            feature_collection = {"type": "FeatureCollection",
                                  "features": []
                                  }
            postgis_data_store = ogr.Open(self.ogrConnString, 0)

            if postgis_data_store is not None:
                pg_layer = postgis_data_store.GetLayerByName(str(layer_name))

                if pg_layer is not None:
                    source = osr.SpatialReference()
                    source.ImportFromEPSG(4326)

                    target = osr.SpatialReference()
                    target.ImportFromEPSG(3857)

                    transform_to_3857 = osr.CoordinateTransformation(source, target)
                    transform_to_4326 = osr.CoordinateTransformation(target, source)
                    for feature in pg_layer:
                        geom = feature.GetGeometryRef()
                        new_feature = feature.Clone()
                        geom.Transform(transform_to_3857)
                        buffered_geom = geom.Buffer(distance=distance)
                        buffered_geom.Transform(transform_to_4326)
                        new_feature.SetGeometry(buffered_geom)
                        feature_collection['features'].append(new_feature.ExportToJson(as_object=True))
                    return str(json.dumps(feature_collection))

                app.logger.warning('Could not find layer to buffer')
            app.logger.warning('Could not open postgis connection. Connection parameters {}'.format(self.ogrConnString))

        except Exception as e:
            app.logger.warning(e.message)
            return False

    def buffer_feature(self, layer_name, feature_id, distance):
        try:
            feature_collection = {"type": "FeatureCollection",
                                  "features": []
                                  }
            postgis_data_store = ogr.Open(self.ogrConnString, 0)

            if postgis_data_store is not None:
                pg_layer = postgis_data_store.GetLayerByName(str(layer_name))
                if pg_layer is not None:

                    source_ref = osr.SpatialReference()
                    source_ref.ImportFromEPSG(4326)

                    target_ref = osr.SpatialReference()
                    target_ref.ImportFromEPSG(3857)

                    transform_to_3857 = osr.CoordinateTransformation(source_ref, target_ref)
                    transform_to_4326 = osr.CoordinateTransformation(target_ref, source_ref)
                    feature = pg_layer.GetFeature(int(feature_id))
                    if feature is not None:
                        geom = feature.GetGeometryRef()
                        new_feature = feature.Clone()
                        geom.Transform(transform_to_3857)
                        buffered_geom = geom.Buffer(distance=distance)
                        buffered_geom.Transform(transform_to_4326)
                        new_feature.SetGeometry(buffered_geom)
                        feature_collection['features'].append(new_feature.ExportToJson(as_object=True))
                        return str(json.dumps(feature_collection))
                    app.logger.warn('Could not find feature with ogc_fid {} in layer'.format(str(feature_id)))
                    return None
                app.logger.warning('Could not find layer to buffer')
            app.logger.warning('Could not open postgis connection. Connection parameters {}'.format(self.ogrConnString))
        except Exception as e:
            app.logger.warning(e.message)
            return False

    def within(self, points_table_name, polygon_table_name):
        """
        :param points_table_name: name  of table with point data
        :param polygon_table_name: name  of table with polygon data
        :return: array of geojson features that are inside any of the polygons in polygon table
        """

        try:
            source = osr.SpatialReference()
            source.ImportFromEPSG(4326)

            target = osr.SpatialReference()
            target.ImportFromEPSG(3857)

            transform_to_3857 = osr.CoordinateTransformation(source, target)
            transform_to_4326 = osr.CoordinateTransformation(target, source)

            postgis_data_store = ogr.Open(self.ogrConnString, 0)
            features_within = []

            if postgis_data_store is not None:
                points_layer = postgis_data_store.GetLayerByName(str(points_table_name))
                polygon_layer = postgis_data_store.GetLayerByName(str(polygon_table_name))
                if points_layer is not None and polygon_layer is not None:

                    for polygon_feature in polygon_layer:
                        points_layer.ResetReading()
                        new_poly_feature = polygon_feature.Clone()
                        polygon_geom = new_poly_feature.GetGeometryRef()
                        polygon_geom.Transform(transform_to_3857)
                        point_feature = points_layer.GetNextFeature()
                        while point_feature is not None:
                            new_point_feature = point_feature.Clone()
                            point_geom = new_point_feature.GetGeometryRef()
                            point_geom.Transform(transform_to_3857)
                            if point_geom.Within(polygon_geom):
                                features_within.append(point_feature)
                            point_feature = points_layer.GetNextFeature()
                    return [feature.ExportToJson() for feature in features_within]
                app.logger.warning('Could not find point layer or polygon layer')
            app.logger.warning('Could not open postgis connection. Connection parameters {}'.format(self.ogrConnString))

        except Exception as e:
            app.logger.warning(e.message)
            return False


    def query_by_attribute(self, layer_name, operator, attribute_name, attribute_value):
        """
        :param layer_name: name of layer to be queried
        :param attribute_name: name of attribute
        :param attribute_value: value of attribute
        :return: geojson featureCollection of matching features
        """

        try:
            feature_collection = {"type": "FeatureCollection",
                                  "features": []
                                  }
            postgis_data_store = ogr.Open(self.ogrConnString, 0)

            if postgis_data_store is not None:
                pg_layer = postgis_data_store.GetLayerByName(str(layer_name))
                # create an output datasource in memory
                outdriver = ogr.GetDriverByName('MEMORY')
                source = outdriver.CreateDataSource('memData')

                # open the memory datasource with write access
                tmp = outdriver.Open('memData', 1)

                # copy a layer to memory
                pipes_mem = source.CopyLayer(pg_layer, 'tmp_layer', ['OVERWRITE=YES'])

                # the new layer can be directly accessed via the handle pipes_mem or as source.GetLayer('pipes'):
                layer_copy = source.GetLayer('tmp_layer')

                # Query string
                query = '{}{}{}{}{}'.format(str(attribute_name), str(operator), "'", str(attribute_value), "'")

                # Write features to geojson, setting a query_id so that we can identify them in openlayers
                if layer_copy is not None:
                    matching_features = []
                    layer_copy.SetAttributeFilter(query)
                    layer_copy.CreateField(ogr.FieldDefn('query_id', ogr.OFTInteger))
                    feature = layer_copy.GetNextFeature()
                    while feature is not None:
                        feature.SetField('query_id', feature.GetFID())
                        feature_collection['features'].append(feature.ExportToJson(as_object=True))
                        feature.Destroy()
                        feature = layer_copy.GetNextFeature()
                    resp_string = str(json.dumps(feature_collection))
                    source.Destroy()
                    return resp_string
                app.logger.warning('Could not find layer with name {}'.format(layer_name))
            app.logger.warning('Could not open postgis connection. Connection parameters {}'.format(self.ogrConnString))

        except Exception as e:
            app.logger.warning(e.message)
            return False

    def getExtent(self, layer_name):
        try:
            postgis_data_store = ogr.Open(self.ogrConnString, 0)
            if postgis_data_store is not None:
                pg_layer = postgis_data_store.GetLayerByName(str(layer_name))
                if pg_layer is not None:
                    return pg_layer.GetExtent()
                return None
            app.logger.warning('Could not open connection to postgis')
            return None
        except Exception as e:
            app.logger.warning(e)
            return None

