from . import postgisConfig as config
import ogr
from .. import app


class PostGisConnection(object):
    def __init__(self):
        self.DATABASE_URI = config.DATABASE_URI
        self.DB_USER = config.DB_USER
        self.DB_PASSWORD = config.DB_PASSWORD
        self.DB_NAME = config.DB_NAME
        self.DB_SERVER = config.DB_SERVER
        self.DB_PORT = config.DB_PORT
        self.DB_TYPE = config.DB_TYPE
        self.DB_SCHEMA = config.DB_SCHEMA
        self.ogrConnString = "PG: host=%s dbname=%s user=%s password=%s" % (
            self.DB_SERVER, self.DB_NAME, self.DB_USER, self.DB_PASSWORD)

    def get_geom_type_for_table(self, table_name):
        data_store = ogr.Open(self.ogrConnString)
        postgis_layer = data_store.GetLayerByName(str(table_name))
        geom_type = postgis_layer.GetLayerDefn().GetGeomType()
        if geom_type == ogr.wkbPoint:
            return 'Point'
        elif geom_type == ogr.wkbMultiPoint:
            return 'MultiPoint'
        elif geom_type == ogr.wkbLineString:
            return 'LineString'
        elif geom_type == ogr.wkbMultiLineString:
            return 'MultiLineString'
        elif geom_type == ogr.wkbPolygon:
            return 'Polygon'
        elif geom_type == ogr.wkbMultiPolygon:
            return 'MultiPolygon'
        app.logger.warning('Geometry type not found for layer {}'.format(table_name))
        return None

    def get_layer_names(self):
        postgis_data_source = ogr.Open(self.ogrConnString)
        layer_list = []
        if postgis_data_source is not None:
            for layer in postgis_data_source:
                if layer not in layer_list:
                    layer_list.append(layer)
            layer_list.sort()
            postgis_data_source.Destroy()
            return layer_list
        return layer_list

    def get_nbr_of_features(self, layer_name, schema):
        postgis_data_source = ogr.Open(self.ogrConnString)
        feature_count = None
        if postgis_data_source is not None:
            layer = postgis_data_source.GetLayerByName(layer_name)
            if layer is not None:
                feature_count = layer.GetFeatureCount()
            postgis_data_source.Destroy()
            return feature_count
        postgis_data_source.Destroy()
        return feature_count

    def get_geom_type(self, layer_name, schema):
        postgis_data_source = ogr.Open(self.ogrConnString)
        geom_type_name = None
        if postgis_data_source is not None:
            layer = postgis_data_source.GetLayerByName(layer_name)
            if layer is not None:
                geom_type_code = layer.GetGeomType()
                geom_type_name = ogr.GeometryTypeToName(geom_type_code)
            postgis_data_source.Destroy()
            return geom_type_name
        postgis_data_source.Destroy()
        return geom_type_name

    def get_crs(self, layer_name, schema):
        postgis_data_source = ogr.Open(self.ogrConnString)
        spat_ref = None
        if postgis_data_source is not None:
            layer = postgis_data_source.GetLayerByName(layer_name)
            if layer is not None:
                spatial_ref = layer.GetSpatialRef()
                if spatial_ref.IsProjected:
                    spat_ref = spatial_ref.GetAttrValue('geogcs')
                postgis_data_source.Destroy()
                return spat_ref
            postgis_data_source.Destroy()
            return spat_ref
        postgis_data_source.Destroy()
        return spat_ref

    def get_attributes_and_types(self, layer_name, schema):
        postgis_data_source = ogr.Open(self.ogrConnString)
        attributes = []
        if postgis_data_source is not None:
            layer = postgis_data_source.GetLayerByName(layer_name)
            if layer is not None:
                lyrDefn = layer.GetLayerDefn()
                for i in range(lyrDefn.GetFieldCount()):
                    field_name = lyrDefn.GetFieldDefn(i).GetName()
                    field_type_code = lyrDefn.GetFieldDefn(i).GetType()
                    field_type_name = lyrDefn.GetFieldDefn(i).GetFieldTypeName(field_type_code)
                    attributes.append({'attrName': field_name, 'attrType': field_type_name})
                postgis_data_source.Destroy()
                return attributes
            postgis_data_source.Destroy()
            return attributes
        postgis_data_source.Destroy()
        return attributes

    def delete_table(self, layer_name, schema):
        postgis_data_source = ogr.Open(self.ogrConnString)
        if postgis_data_source is not None:
            layer = postgis_data_source.GetLayerByName(layer_name)
            if layer is not None:
                postgis_data_source.DeleteLayer(layer_name)
                postgis_data_source.Destroy()

    def exists_table(self, layer_name, schema):
        postgis_data_source = ogr.Open(self.ogrConnString)
        if postgis_data_source is not None:
            layer = postgis_data_source.GetLayerByName(layer_name)
            postgis_data_source.Destroy()
            return layer is not None

    def _get_ogr_field_types(self):
        return {

            "ogr.OFTInteger": 0,
            "ogr.OFTIntegerList": 1,
            "ogr.OFTReal": 2,
            "ogr.OFTRealList": 3,
            "ogr.OFTString": 4,
            "OFTStringList": 5,
            "ogr.OFTWideString": 6,
            "ogr.OFTWideStringList": 7,
            "ogr.OFTBinary": 8,
            "ogr.OFTDate": 9,
            "ogr.OFTTime": 10,
            "ogr.OFTDateTime": 11,
            "ogr.OFTInteger64": 12,
            "ogr.OFTInteger64List": 13,
            "ogr.OFTMaxType": 13
        }
