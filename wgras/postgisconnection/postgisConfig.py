from .. import app
# ----- Database ----- #
DATABASE_URI = app.config['SPATIAL_DATABASE_URI']
DB_USER = app.config['SPATIAL_DB_USER']
DB_PASSWORD = app.config['SPATIAL_DB_PASSWORD']
DB_NAME = app.config['SPATIAL_DB_NAME']
DB_SERVER = app.config['SPATIAL_DB_SERVER']
DB_PORT = app.config['SPATIAL_DB_PORT']
DB_TYPE = app.config['SPATIAL_DB_TYPE']
DB_SCHEMA = app.config['SPATIAL_DB_SCHEMA']