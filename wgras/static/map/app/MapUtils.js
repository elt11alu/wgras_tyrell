/**
 * Created by anton on 2015-09-14.
 */

Ext.define('VramApp.MapUtils', {
    requires: [
        'VramApp.utils.VectorStyle',
        'VramApp.GeometryDataTypes'
    ],

    extend: 'Ext.mixin.Observable',
    singleton: true,

    layerIdArray: [],

    getExtMapView: function () {
        return Ext.getCmp('mapView');
    },

    getOlMap: function () {
        try {
            return this.getExtMapView().getMap();
        } catch (err) {
            console.log('Could not find map', err);
        }
    },

    getOlView: function () {
        try {
            return this.getOlMap().getView();
        } catch (err) {
            console.log('Could not find view', err);
        }
    },

    getLayers: function () {
        try {
            return this.getOlMap().getLayers();
        } catch (err) {
            console.log('Could not find layers', err);
        }
    },

    getNbrOfLayers: function () {
        try {
            return this.getLayers().getArray().length;
        } catch (err) {
            console.log(err);
        }
    },

    getLayerByName: function (name) {
        try {
            var layers = this.getLayers();
            var layer;
            layers.forEach(function (mapLayer) {
                if (mapLayer.get('title') === name) {
                    layer = mapLayer;
                }
            });
            return layer;
        } catch (err) {
            console.log(err);
        }
    },

    getLayerByGeoserverName: function (name) {
        try {
            var layers = this.getLayers();
            var layer;
            layers.forEach(function (mapLayer) {

                if (mapLayer.get('geoServerName') === name) {
                    layer = mapLayer;
                }
            });
            return layer;
        } catch (err) {
            console.log(err);
        }
    },


    getLayerByDBId: function (dbId) {
        try {
            var layers = this.getLayers();
            var layer = false;
            layers.forEach(function (mapLayer) {
                if (mapLayer.get('layerId') === dbId) {
                    layer = mapLayer;
                }
            });
            return layer;
        } catch (err) {
            console.log(err);
        }
    },


    getLayerByLayerIdentifier: function (layerIdentifier) {
        try {
            var layers = this.getLayers();
            var layer;
            layers.forEach(function (mapLayer) {
                if (mapLayer.get('layerIdentifier') === layerIdentifier) {
                    layer = mapLayer;
                }
            });
            return layer;
        } catch (err) {
            console.log(err);
        }
    },

    getLayerIdentifier: function () {
        var id, found;
        id = this.generateLayerId();
        found = this.layerIdArray.indexOf(id) !== -1;
        while (found === true) {
            id = this.generateLayerId();
            found = this.layerIdArray.indexOf(id) !== -1;
        }
        this.layerIdArray.push(id);
        return id;
    },

    generateLayerId: function () {
        var possible, text;
        text = '';
        possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 30; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    },

    getSelectInteraction: function () {
        try {
            var interactions = this.getOlMap().getInteractions();
            var selectInteraction = null;
            interactions.forEach(function (action) {
                if (action instanceof ol.interaction.Select) {
                    selectInteraction = action;
                }
            });
            return selectInteraction;
        } catch (err) {
            console.log('Could not find select interaction', err);
        }

    },

    addBaseLayer: function (layer, title, visible) {
        try {
            var layerIdentifier = this.getLayerIdentifier();
            var isVisible = visible || false;
            layer.set('title', title);
            layer.set('dataType', VramApp.GeometryDataTypes.LayerTypes.BASELAYER);
            layer.set('isBaseLayer', true);
            layer.setVisible(isVisible);
            layer.set('persist', true);
            layer.set('layerIdentifier', layerIdentifier);
            this.addLayerToMap(layer);
            //this.notifyTreeStore();
            this.notifyTreeStoreSingleLayer(layer);
        } catch (e) {
            console.log(e);
        }
    },


    /** -- Old function: Do not use!! --

     * [addWMSOverlay description]
     * @param {[ol.layer]}              layer         [Ol3 raster layer that should be added to map]
     * @param {[string]}                geoServerName [the layer name in GeoServer, workspace:name, the workspace part is used as folder name in layer tree]
     * @param {[string]}                title         [the layer title in GeoServer, used as identifier and display name in layer tree]
     * @param {[array]}                 bbox          [Bounding box of the layer as [minx,miny,maxx,maxy], must be in EPSG:3857]
     * @param {[boolean]}               visible       [If the layer should be visible or not]
     * @param {[string]}                legendUrl     [URL pointing to the legend image]
     * @param {[VramApp.MetaDataModel]} metadata      [Used for adding metadata to the layer, see VramApp.model.MetaDataModel for supported fields]
     */
    /*

     addWMSOverlay: function(layer, geoServerName, title, bbox, visible, legendUrl, meta) {
     var folderName, layerVisible, legendURL, layerIdentifier;
     try {
     legendURL = legendUrl || false;
     folderName = geoServerName.split(':')[0] || 'default';
     layerVisible = visible || false;
     metadata = meta || {
     layerInfo: 'Not available'
     };
     layerIdentifier = this.getLayerIdentifier();
     metadata.dataType = VramApp.GeometryDataTypes.LayerTypes.WMS;
     metadata.layerTitle = title;
     metadata.category = folderName;
     layer.set('legendUrl', legendURL);
     layer.set('geoServerName', geoServerName);
     layer.set('title', title);
     layer.set('dataType', VramApp.GeometryDataTypes.LayerTypes.WMS);
     layer.set('geoServerExtent', bbox);
     layer.set('isBaseLayer', false);
     layer.set('overlayFolder', folderName);
     layer.setVisible(layerVisible);
     layer.set('persist', true);
     layer.set('layerIdentifier', layerIdentifier);
     this.addLayerToMap(layer, metadata);
     this.notifyTreeStore();
     } catch (e) {
     console.log(e)
     }
     },

     */

    /**
     * [function description]
     * @param  {ol.Layer.Vector}    layer       [description]
     * @param  {object}             required    [description]
     * @param  {object}             options     [description]
     * @return {[type]}                         [description]
     *
     *
     *  -- This is how you use the parameters--
     *
     *    required:{
     *		layer: ol.layer.Layer.Image({}) || ol.layer.Tile({}),
     *  	layerName: the name of the layer (string),
     *  	layerTitle: the title of the layer (string)
     *
     *	}
     *
     *    options: {
     * 		legendUrl : 'a url pointing to the legend image (string)',
     * 		folderName : 'used as folder in the layer tree, if not submitted layerName will be used (string)',
     * 		visible: 'visibility of layer (boolean)',
     * 		abstract: 'description of layer, used in layer info in layer tree, (string)'
     *   	bbox: 'bbox of layer in EPSG:3857, if not submitted world bounds are used, (array [minx,miny,maxx,maxy])'
     *
     *  }
     *
     *
     *
     *
     */



    addWMSOverlay: function (required, options) {

        var me, folderName, external, layerId, layerVisible, legendURL, layerIdentifier, workspaceName, wmsBbox, wmsLayer, wmsTitle, wmsName, metadata, iconCls;
        me = this;
        metadata = {};
        try {
            /**** Optional Parameters ****/
            legendURL = options.legendUrl || false;
            layerVisible = options.visible || false;
            metadata.layerInfo = options['abstract'] || 'No information available';
            layerIdentifier = this.getLayerIdentifier();
            wmsBbox = options.bbox || me.reprojectExtentTo3857([-180, -90, 180, 90], 'EPSG:4326');
            folderName = options.folderName || null;
            layerId = options.layerId || null;
            workspaceName = options.workspaceName || null;
            external = options.external || false;


            /**** Required parameters ****/
            if (required !== null && typeof required === 'object') {
                if (required.layer && (required.layer instanceof ol.layer.Image || required.layer instanceof ol.layer.Tile)) {
                    wmsLayer = required.layer
                } else {
                    throw 'Parameter wmsLayer is required!';
                }
                if (typeof required.layerTitle === 'string') {
                    wmsTitle = required.layerTitle;
                } else {
                    throw 'Parameter layerTitle is required!';
                }
                if (typeof required.layerName === 'string') {
                    wmsName = required.layerName;
                    folderName = folderName === null ? required.layerName.split(':')[0] : options.folderName
                } else {
                    throw 'Parameter layerName is required!';
                }
            } else {
                throw 'Parameter \'required\' must be an Object'
            }
            /**** Building the metadata  ****/
            metadata.dataType = VramApp.GeometryDataTypes.LayerTypes.WMS;
            metadata.layerTitle = wmsTitle
            metadata.category = folderName;

            /*** Setting the iconCls used for symbol in tree store ***/
            iconCls = 'raster-leaf-symbol'

            /*** Setting the layer ***/
            wmsLayer.set('legendUrl', legendURL);
            wmsLayer.set('layerName', wmsName);
            wmsLayer.set('title', wmsTitle);
            wmsLayer.set('dataType', VramApp.GeometryDataTypes.LayerTypes.WMS);
            wmsLayer.set('geoServerExtent', wmsBbox);
            wmsLayer.set('isBaseLayer', false);
            wmsLayer.set('overlayFolder', folderName);
            wmsLayer.setVisible(layerVisible);
            wmsLayer.set('persist', true);
            wmsLayer.set('layerIdentifier', layerIdentifier);
            wmsLayer.set('layerId', layerId);
            wmsLayer.set('workspaceName', workspaceName);
            wmsLayer.set('iconCls', iconCls);
            wmsLayer.set('external', external)
            this.addLayerToMap(wmsLayer, metadata);
            //this.notifyTreeStore();
            this.notifyTreeStoreSingleLayer(wmsLayer);
        } catch (e) {
            console.log(e)
        }
    },


    /* -- Old function: Do not use!! --

     addWFSOverlay: function(layer, geoServerName, title, bbox, visible, remote, ancestorLayer, meta) {
     var folderName, styleObj, layerVisible, isRemote, ancestor, layerIdentifier;
     try {

     folderName = geoServerName.split(':')[0] || 'default';
     styleObj = this.getDefaultStyle();
     layerVisible = visible || false;
     metadata = meta || {
     layerInfo: 'Not available'
     };
     isRemote = remote;
     ancestor = ancestorLayer || undefined;
     metadata.dataType = VramApp.GeometryDataTypes.LayerTypes.WFS;
     metadata.layerTitle = title;
     metadata.category = folderName;
     layerIdentifier = this.getLayerIdentifier();
     layer.set('geoServerName', geoServerName);
     layer.set('title', title);
     layer.set('dataType', VramApp.GeometryDataTypes.LayerTypes.WFS);
     layer.set('geoServerExtent', bbox);
     layer.set('isBaseLayer', false);
     layer.set('overlayFolder', folderName);
     layer.setStyle(styleObj.style);
     layer.set('styleInfo', styleObj.styleInfo);
     layer.set('persist', true);
     layer.set('remote', isRemote);
     layer.set('ancestorLayer', ancestor);
     layer.setVisible(layerVisible);
     layer.set('layerIdentifier', layerIdentifier);
     this.addLayerToMap(layer, metadata);
     this.notifyTreeStore();
     } catch (e) {
     console.log(e)
     }
     },

     */




    /**
     * [addWFSOverlay(): adds a vector layer to the map]
     * @param  {object} required required parameter as per below
     * @param  {object} options  optional parameter as per below
     * @return {none}          [description]
     *
     * --This is how to use the parameters:
     * required = {
     * 		layer: the layer to add to the map, ol.layer.vector
     * 		layerTitle: the title of the layer, used as visible name in layer tree, string
     * 		layerName: the name of the layer used as folder name in layer tree, string
     * }
     *
     * options = {
     * 		visible: if the layer should be visible initially, boolean
     * 		remote: if the layer is remote, if it exists on the geoserver, default is false, boolean
     * 		ancestor: if the layer has an ancestor layer, this is the name of that layer
     * 		bbox: bounding box of layer, array [xmin,ymin,xmax,ymax] in EPSG: 3857, default is world extent
     * 		abstract: used as layer information if available
     * 		folder: used as folder name in layer tree, default is layer name if not submitted
     * }
     */
    addWFSOverlay: function (required, options) {
        var me, folderName, styleObj, workspaceName, layerId, layerVisible, isRemote, ancestor, layerIdentifier, metadata, bbox, wfsLayer, wfsTitle, wfsName, iconCls;
        me = this;
        try {

            /*** Required parameters ***/
            if (required !== null && typeof required === 'object') {
                if (required.layer && required.layer instanceof ol.layer.Vector) {
                    wfsLayer = required.layer
                } else {
                    throw 'Parameter wfsLayer is required! Requires ol.layer.Vector but found ' + typeof required.layer;
                }
                if (typeof required.layerTitle === 'string') {
                    wfsTitle = required.layerTitle;
                } else {
                    throw 'Parameter layerTitle is required!';
                }
                if (typeof required.layerName === 'string') {
                    wfsName = required.layerName;
                    folderName = required.layerName.split(':')[0];
                } else {
                    throw 'Parameter layerName is required!';
                }
            } else {
                throw 'Parameter \'required\' must be an Object'
            }


            /*** Optional parameters ***/
            metadata = {};
            if (options !== null && typeof options === 'object') {
                layerVisible = options.visible || false;
                isRemote = options.remote || false;
                ancestor = options.ancestor || undefined;
                bbox = options.bbox || me.reprojectExtentTo3857([-180, -90, 180, 90], 'EPSG:4326');
                metadata.layerInfo = options['abstract'] || 'No information available';
                layerId = options.layerId || null;
                workspaceName = options.workspaceName
                if (typeof options.folder != 'undefined') {
                    folderName = options.folder
                }
            } else {
                layerVisible = false;
                isRemote = false;
                ancestor = undefined;
                bbox = me.reprojectExtentTo3857([-180, -90, 180, 90], 'EPSG:4326');
                metadata.layerInfo = 'No information available';
            }

            /*** Setting the layer style ***/
            styleObj = this.getDefaultStyle();

            /*** Building the metadata object ***/
            metadata.dataType = VramApp.GeometryDataTypes.LayerTypes.WFS;
            metadata.layerTitle = wfsTitle;
            metadata.category = folderName;

            /*** Setting the layer identifier ***/
            layerIdentifier = this.getLayerIdentifier();

            /*** Setting the iconCls used for symbol in tree store ***/
            iconCls = 'vector-leaf-symbol'

            /*** Setting the vector layer ***/
            wfsLayer.set('layerName', wfsName);
            wfsLayer.set('title', wfsTitle);
            wfsLayer.set('dataType', VramApp.GeometryDataTypes.LayerTypes.WFS);
            wfsLayer.set('geoServerExtent', bbox);
            wfsLayer.set('isBaseLayer', false);
            wfsLayer.set('overlayFolder', folderName);
            wfsLayer.setStyle(styleObj.style);
            wfsLayer.set('styleInfo', styleObj.styleInfo);
            wfsLayer.set('persist', true);
            wfsLayer.set('remote', isRemote);
            wfsLayer.set('ancestorLayer', ancestor);
            wfsLayer.setVisible(layerVisible);
            wfsLayer.set('layerIdentifier', layerIdentifier);
            wfsLayer.set('layerId', layerId);
            wfsLayer.set('workspaceName', workspaceName);
            wfsLayer.set('iconCls', iconCls);
            /*** Adding layer to map and metadata to metadata store ***/
            this.addLayerToMap(wfsLayer, metadata);

            /*** Notify the treestore, adds layer to layer tree ***/
            //this.notifyTreeStore();
            this.notifyTreeStoreSingleLayer(wfsLayer);
        } catch (e) {
            console.log(e)
        }
    },

    addTempLayer: function (layer, optional) {
        var styleObj, layerIdentifier, meta, me;
        me = this;
        try {
            if (typeof optional != 'undefined' && typeof optional == 'object') {
                if (optional.style && optional.style instanceof ol.style.Style) {

                    styleObj = {};
                    styleObj.style = optional.style;
                    styleObj.styleInfo = {}
                }
            } else {
                styleObj = me.getDefaultStyle();
            }
            meta = {};

            layerIdentifier = me.getLayerIdentifier();
            layer.set('persist', false);
            layer.set('layerIdentifier', layerIdentifier);
            layer.setStyle(styleObj.style);
            layer.set('styleInfo', styleObj.styleInfo);
            this.addLayerToMap(layer, meta);
            return layerIdentifier;

        } catch (err) {
            console.log(err)
        }
    },

    getTempLayer: function (layerIdentifier) {
        var me = this;
        var tmplayer = null;
        if (typeof layerIdentifier === 'string') {
            me.getLayers().forEach(function (layer) {
                if (layer.get('layerIdentifier') === layerIdentifier) {

                    tmplayer = layer;
                }
            });
            return tmplayer;
        } else {
            return null;
        }

    },

    /**
     * [addTempRasterLayer: adds a temporary raster layer to map]
     * @return {String} [Id: a string that can be used to remove the layer]
     */
    addTempRasterLayer: function (rasterLayer) {
        var layerIdentifier;
        try {

            layerIdentifier = this.getLayerIdentifier();
            layer.set('persist', false);
            layer.set('layerIdentifier', layerIdentifier);
            this.addLayerToMap(layer, meta);
            return layerIdentifier;
        }
        catch (err) {
            console.log(err);
        }

    },

    removeTempLayerFromMap: function (layerIdentifier) {
        var me = this;
        try {
            this.removeLayerByLayerIdentifier(layerIdentifier)
        } catch (err) {
            console.log(err);
        }
    },

    addLayerToMap: function (layer, metadata) {
        meta = (typeof metadata === 'object') ? metadata : {
            layerInfo: 'No information available.'
        };
        try {
            this.addToMetadataStore(layer.get('layerIdentifier'), meta);
            if(layer.get('name') ==='vl_susceptibility_846' || layer.get('title') === 'vl_susceptibility_846'){
                this.getOlMap().getLayers().insertAt(4, layer)
            }else{
                this.getOlMap().addLayer(layer);
            }

        } catch (err) {
            console.log(err);
        }
    },

    removeLayerByLayerIdentifier: function (layerIdentifier) {
        var me = this;
        try {
            if (layerIdentifier) {
                this.getLayers().forEach(function (layer) {
                    if (layer.get('layerIdentifier') === layerIdentifier) {
                        me.getOlMap().removeLayer(layer);
                    }
                });

            } else {
                throw 'Parameter layerIdentifier is required in removeLayerByLayerIdentifier'
            }
        } catch (err) {
            console.log(err);
        }
    },


    removeLayerByLayerName: function (layerName) {
        try {
            var layer = this.getLayerByName(layerName);
            if (layer) {
                return this.getOlMap().removeLayer(layer);
            }
        } catch (err) {
            console.log(err);
        }
    },

    panToCoords: function (coords) {
        try {
            this.getOlView().setCenter(coords);

        } catch (e) {
            console.log(e);
        }
    },

    panToExtent: function (extent) {
        var EXTENT, SIZE, OPTIONS;
        try {

            // [minx, miny, maxx, maxy]
            EXTENT = extent;
            SIZE = this.getOlMap().getSize();
            OPTIONS = {
                maxZoom: 12
            };
            this.getOlView().fit(EXTENT, SIZE, OPTIONS);

        } catch (e) {
            console.log(e)
        }
    },

    panTo4326Extent: function(extent){
        var EXTENT, SIZE, OPTIONS;
        try {
            if (!this._valid4326Extent(extent)) {
                console.warn('not a valid extent, falling back on world extent.');
                extent = [-180, -90, 180, 90]
            }

            // [minx, miny, maxx, maxy]
            EXTENT = this.reprojectExtentTo3857(extent, 'EPSG:4326');
            SIZE = this.getOlMap().getSize();
            OPTIONS = {
                maxZoom: 12
            };
            this.getOlView().fit(EXTENT, SIZE, OPTIONS);

        } catch (e) {
            console.log(e)
        }
    },

    panTo3857Extent: function(extent){
        var EXTENT, SIZE, OPTIONS;
        try {
            if (!this._valid3857Extent(extent)) {
                console.warn('not a valid extent, falling back on world extent.');
                extent = [-180, -90, 180, 90]
            }

            // [minx, miny, maxx, maxy]
            EXTENT = this.reprojectExtentTo3857(extent, 'EPSG:4326');
            SIZE = this.getOlMap().getSize();
            OPTIONS = {
                maxZoom: 12
            };
            this.getOlView().fit(EXTENT, SIZE, OPTIONS);

        } catch (e) {
            console.log(e)
        }
    },

    panToLayer: function (layerName) {
        try {
            var layer = this.getLayerByName(layerName);
            var map = this.getOlMap();
            var extent = layer.get('geoServerExtent') ? layer.get('geoServerExtent') : layer.getSource().getExtent();
            this.panToExtent(extent);
        } catch (err) {
            console.log(err);
        }
    },

    panToLayerById: function (layerId) {
        try {
            var layer, me;
            me = this;
            var allLayerStore = Ext.data.StoreManager.lookup('allLayersStore');
            var bboxStore = Ext.data.StoreManager.lookup('bboxstore');
            var record = allLayerStore.findRecord('layerId', layerId);
            if(record!=null){
                var gs_rec = bboxStore.findLayerRecord(record.get('workspaceName'), record.get('layerName'));
                if(gs_rec){
                    var bbox = [parseFloat(gs_rec.get('bboxMinX')), parseFloat(gs_rec.get('bboxMinY')), parseFloat(gs_rec.get('bboxMaxX')), parseFloat(gs_rec.get('bboxMaxY'))];
                    me.panTo4326Extent(bbox)
                }else{
                throw "Unable to zoom, record was null!"
            }
            }else{
                throw "Unable to zoom, record was null!"
            }
            bboxStore.reload();
            allLayerStore.reload();
            /*var success = function (resp) {
                var obj, bbox, jsonResp;
                jsonResp = Ext.decode(resp.responseText);
                obj = jsonResp.extent;
                bbox = [parseFloat(obj.minx), parseFloat(obj.miny), parseFloat(obj.maxx), parseFloat(obj.maxy)];
                me.panTo4326Extent(bbox);
            };
            var fail = function (resp) {
                console.log(resp)
            };
            this._get_layer_extent(layerId, success, fail)*/


        } catch (err) {
            console.log(err);
        }
    },

    getExtentByLayerId: function(layerId){
        var layer, me;
            me = this;
            var allLayerStore = Ext.data.StoreManager.lookup('allLayersStore');
            var bboxStore = Ext.data.StoreManager.lookup('bboxstore');
            var record = allLayerStore.findRecord('layerId', layerId);
            if(record){
                bboxStore.reload();
                allLayerStore.reload();
                return bboxStore.getBbox(record.get('workspaceName'), record.get('layerName'));
            }

        return null;
    },

    reprojectTo3857: function (coords, from) {
        // [minx, miny, maxx, maxy]
        return ol.proj.transform(coords, from, 'EPSG:3857');
    },

    reprojectExtentTo3857: function (extent, from) {
        if (extent[0] < -170 || !isFinite(extent[0])) {
            extent[0] = -170;
        }
        if (extent[1] < -85 || !isFinite(extent[1])) {
            extent[1] = -85;
        }
        if (extent[2] > 175 || !isFinite(extent[2])) {
            extent[2] = 175;
        }
        if (extent[3] > 85 || !isFinite(extent[3])) {
            extent[3] = 85;
        }

        return ol.proj.transformExtent(extent, from, 'EPSG:3857');
    },

    getDefaultStyle: function () {
        return VramApp.utils.VectorStyle.defaultStyle();
    },

    notifyTreeStore: function () {
        Ext.GlobalEvents.fireEvent('LayerAddedToMap');
    },

    notifyTreeStoreSingleLayer: function(layer){
        Ext.GlobalEvents.fireEvent('addLayerToTreeStore', {layer:layer});
    },

    addToMetadataStore: function (layerId, metadata) {
        if (metadata && layerId) {
            if (metadata !== null && typeof metadata === 'object') {
                var model = Ext.create('VramApp.model.MetaDataModel');
                model.set('layerInfo', metadata.layerInfo);
                model.set('layerIdentifier', layerId);
                model.set('category', metadata.category);
                model.set('layerTitle', metadata.layerTitle);
                model.set('dataType', metadata.dataType)
                Ext.data.StoreManager.lookup('metadatastore').add(model);
            }

        }

    },

    _get_layer_extent: function (layerId, success, fail) {
        Ext.Ajax.request({
            url: '/layer/extent/'.concat(layerId),
            method: 'GET',

            success: function (response, opts) {
                success(response, id)
            },

            failure: function (response, opts) {
                fail(response, id);
            }
        });
    },

    _valid4326Extent: function (extent) {
        if (extent[0] < -180 || !isFinite(extent[0])) {
            return false;
        }
        if (extent[1] < -90 || !isFinite(extent[1])) {
            return false;
        }
        if (extent[2] > 180 || !isFinite(extent[2])) {
            return false;
        }
        if (extent[3] > 90 || !isFinite(extent[3])) {
            return false;
        }
        return true;
    },

    _fix4326Extent: function (extent) {
        if (extent[0] < -180 || !isFinite(extent[0])) {
            extent[0] = -170;
        }
        if (extent[1] < -90 || !isFinite(extent[1])) {
            extent[1] = -85;
        }
        if (extent[2] > 180 || !isFinite(extent[2])) {
            extent[2] = 180;
        }
        if (extent[3] > 90 || !isFinite(extent[3])) {
            extent[3] = 85;
        }
    },

    _valid3857Extent: function(extent_3857){

        if (extent[0] < -20026376.39 || !isFinite(extent[0])) {
            return false;
        }
        if (extent[1] < -20048966.10 || !isFinite(extent[1])) {
            return false;
        }
        if (extent[2] > 20026376.39 || !isFinite(extent[2])) {
            return false;
        }
        if (extent[3] > 20048966.10 || !isFinite(extent[3])) {
            return false;
        }

        return true;
    },

    _fix3857Extent: function(extent){
        if (extent[0] < -20026376.39 || !isFinite(extent[0])) {
            extent[0] = -20026376.39;
        }
        if (extent[1] < -20048966.10 || !isFinite(extent[1])) {
            extent[0] = -20048966.10;
        }
        if (extent[2] > 20026376.39 || !isFinite(extent[2])) {
            extent[0] = 20026376.39;
        }
        if (extent[3] > 20048966.10 || !isFinite(extent[3])) {
            extent[0] = 20048966.10;
        }
        return extent
    }


});