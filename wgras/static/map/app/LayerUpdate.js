/**
 * Created by anton on 2016-08-19.
 */

Ext.define('VramApp.LayerUpdate', {
    singleton: true,
    requires: [
        'VramApp.utils.VectorStyle',
        'VramApp.GeometryDataTypes'
    ],

    postFeaturesToExistingLayer: function (temp_layer, layerId, success_func, fail_func) {
        try {

            var url, geoJson, json_data, type;
            type = this._checkHasOneGeometryType(temp_layer);
            if (type != false) {
                switch (type) {
                    case VramApp.GeometryDataTypes.geomTypes.point:
                        url = '/point';
                        break;
                    case VramApp.GeometryDataTypes.geomTypes.line:
                        url = '/line';
                        break;
                    case VramApp.GeometryDataTypes.geomTypes.polygon:
                        url = '/polygon';
                        break;
                    default:
                        throw 'Geometry is not supported!';
                        break;
                }
                geoJson = this._getLayerAsGeoJson(temp_layer);
                json_data = JSON.stringify({layerId: layerId, geometry: geoJson});

                var success = function (resp) {
                    Ext.toast('Layer '.concat(' saved.'));
                    success_func(resp)
                };

                var fail = function (jqXHR) {
                    fail_func(jqXHR)
                    console.log(jqXHR)
                };

                this._POST(url, json_data, success, fail);


            } else {
                throw 'A layer can only have one geometry type!'
            }

        } catch (e) {
            console.log(e);
        }
    },

    postNewLayerWithGeometry: function (temp_layer, layer_name, layer_title, workspace_id, fields, geomType, category, description, success_func, fail_func) {
        try {

            var url, geoJson, json_data;
            if (geomType) {
                switch (geomType) {
                    case VramApp.GeometryDataTypes.geomTypes.point:
                        url = '/layer/point/with_geometry';
                        break;
                    case VramApp.GeometryDataTypes.geomTypes.line:
                        url = '/layer/line/with_geometry';
                        break;
                    case VramApp.GeometryDataTypes.geomTypes.polygon:
                        url = '/layer/polygon/with_geometry';
                        break;
                    case VramApp.GeometryDataTypes.geomTypes.multipoint:
                        url = '/layer/multipoint/with_geometry';
                        break;
                    case VramApp.GeometryDataTypes.geomTypes.multiline:
                        url = '/layer/multiline/with_geometry';
                        break;
                    case VramApp.GeometryDataTypes.geomTypes.multipolygon:
                        url = '/layer/multipolygon/with_geometry';
                        break;
                    default:
                        throw 'Geometry is not supported!';
                        break;
                }
                geoJson = this._getLayerAsGeoJson(temp_layer);
                json_data = JSON.stringify({
                    layerName: layer_name,
                    layerTitle: layer_title,
                    workspaceId: workspace_id,
                    geometry: geoJson,
                    fields: fields,
                    category: category,
                    description: description
                });

                var success = function (resp) {
                    Ext.toast('Layer '.concat(' saved.'));
                    success_func(resp)
                };

                var fail = function (jqXHR) {
                    fail_func(jqXHR)
                    console.log(jqXHR)
                };

                this._POST(url, json_data, success, fail);


            } else {
                throw 'A layer must have a geometry type!'
            }

        } catch (e) {
            console.log(e);
        }
    },

    postNewEmptyLayer: function (layer_name, layer_title, workspace_id, fields, geomTypeName, category, description, success_func, fail_func) {
        var url, json_data, type;
        switch (geomTypeName) {
            case VramApp.GeometryDataTypes.geomTypes.point:
                url = '/layer/point';
                break;
            case VramApp.GeometryDataTypes.geomTypes.line:
                url = 'layer/line';
                break;
            case VramApp.GeometryDataTypes.geomTypes.polygon:
                url = 'layer/polygon';
                break;
            case VramApp.GeometryDataTypes.geomTypes.multipoint:
                url = '/layer/multipoint';
                break;
            case VramApp.GeometryDataTypes.geomTypes.multiline:
                url = 'layer/multiline';
                break;
            case VramApp.GeometryDataTypes.geomTypes.multipolygon:
                url = 'layer/multipolygon';
                break;
            default:
                throw 'Geometry is not supported!';
                break;
        }

        json_data = JSON.stringify(
            {
                layerName: layer_name,
                layerTitle: layer_title,
                workspaceId: workspace_id,
                fields: fields,
                category: category,
                description: description
            });

        var success = function (resp) {
            Ext.toast('Layer '.concat(layer_name, ' saved.'));
            success_func(resp)
        };

        var fail = function (jqXHR) {
            fail_func(jqXHR);
            console.log(jqXHR)
        };

        this._POST(url, json_data, success, fail);

    },


    _POST: function (url, jsonData, success, fail) {
        $.ajax({
            timeout: 60000,
            url: url,
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            data: jsonData,
            crossDomain: true
        }).done(function (json) {
            success(json)


        }).fail(function (jqXHR) {
            fail(jqXHR)

        });

    },
    /**
     *
     * @param layer
     * @private
     */
    _getLayerAsGeoJson: function (layer) {
        var format = new ol.format.GeoJSON();
        var features = layer.getSource().getFeatures();
        var features_format= format.writeFeatures(features, {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'})

        return features_format
    },

    /**
     *
     * @param layer
     * @returns false if layer contains multiple types, otherwise the geomType as string, (Point, LineString, Polygon)
     * @private
     */
    _checkHasOneGeometryType: function (layer) {
        var features, type, suggested, result;
        features = layer.getSource().getFeatures();
        suggested = features[0].getGeometry().getType();
        result = true;
        features.forEach(function (el, ind, arr) {
            type = el.getGeometry().getType();
            if (type !== suggested) {
                result = false;
            }
        });
        return result ? suggested : false;
    },
    /**
     *
     * @param layer
     * @returns {Array}
     * @private
     */
    _getLayerAttributesAndTypes: function (layer) {
        var props = layer.getProperties();
        var fields = [];
        $.each(props, function (key, val) {
            var type = typeof val;
            if (type === 'string' || type === 'number') {
                fields.push({'name': key, dataType: type});
            }

        });
        return fields
    }
});
