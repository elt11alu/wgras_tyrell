Ext.define("VramApp.InputValidationModel",{

    singleton:true,
    
    validatePassword:function(password){
        var reg = new RegExp("^[a-zA-Z0-9_]+$");
        return reg.test(password);
    },
    
    validateUsername: function(username){
        var reg = new RegExp("^[a-zA-Z0-9]+$");
        return reg.test(username);
    },
    
    validateLayerTitle: function(layername){
        var reg = new RegExp("^[a-zA-Z0-9]+$");
        return reg.test(layername);
    },

    validateLocationSearch:function(searchText){
        var reg =  new RegExp("^[a-zA-Z0-9äöåÄÖÅ, .]+$");
        return reg.test(searchText);
    }

});