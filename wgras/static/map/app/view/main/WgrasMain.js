/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('VramApp.view.main.WgrasMain', {
    extend: 'Ext.panel.Panel',
    xtype: 'app-main',
    layout: 'border',
    id: 'mainView',
    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',
        'VramApp.view.hp.Hpanel',
        'VramApp.view.layerList.LayerGrid',
        'VramApp.view.layerList.TreeLayerGrid',
        'VramApp.view.main.MainController',
        'VramApp.view.map.MapView'
    ],

    controller: 'main',
    

    ui: 'navigation',

    plugins: 'viewport',



    items: [{
        xtype: 'Hpanel',
        region: 'north'
    }, {
        xtype: 'mapView',
        region: 'center'



    }, {
        xtype:'layerlist',
        //xtype: 'treelayergrid',
        region: 'west'


    }]



});