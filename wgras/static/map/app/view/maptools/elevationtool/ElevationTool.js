/**
 * Created by antonlu66 on 11/18/2015.
 */
Ext.define('VramApp.view.maptools.elevationtool.ElevationTool', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.button.Button',
        'Ext.form.Panel',
        'Ext.form.field.Display',
        'Ext.form.field.Number',
        'Ext.grid.Panel',
        'Ext.slider.Single',
        'VramApp.view.maptools.elevationtool.ElevationToolController',
        'VramApp.view.maptools.elevationtool.ElevationToolModel'
    ],


    xtype: 'elevationtool',
    icon:VramApp.WGRASAppGlobals.resources()+'resources/mapIcons/elevation.svg',

    viewModel: {
        type: 'elevationtool'
    },

    controller: 'elevationtool',

    /*Specific configs*/
    width: 500,
    height: 500,
    title: 'Flooding tool',
    autoShow: true,
    closable:false,

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        items: [
            {
                xtype: 'button',
                text: 'close',
                handler:'onClose'

            }]
    }],

    items: [
        {
            xtype: 'grid',
            border: true,
            reference: 'elevationLayerGrid',
            store: {
                type: 'elevationDataStore'
            },
            selModel: {
                selType: 'checkboxmodel',
                checkOnly: false,
                mode: 'SINGLE'
            },
            columns: [
                {
                    text: 'Elevation data',
                    dataIndex: 'layerTitle',
                    flex: 1
                }
            ]
        },
        {
            xtype: 'form',
            title:'Input values',
            bodyPadding:10,
            items: [
                {
                    xtype:'displayfield',
                    reference:'currentElevationValue',
                    fieldLabel:'Current value',
                    value:0
                },
                {
                    xtype:'slider',
                    width: 400,
                    fieldLabel:'Threshold value',
                    reference:'elevationValueSlider',
                    minValue: 0,
                    maxValue: 6000,
                    increment:1
                },
                {
                    xtype: 'numberfield',
                    reference: 'maxElevationInputField',
                    enableKeyEvents:true,
                    value:0,
                    fieldLabel: 'Threshold max'
                },
                {
                    xtype: 'button',
                    text: 'Extract polygons',
                    handler: 'onExtractPolygons'
                }
            ]
        }
    ]
});
