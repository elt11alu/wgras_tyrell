/**
 * Created by anton on 2016-08-20.
 */
Ext.define('VramApp.view.maptools.createNewLayer.emptyLayer.CreateNewLayerWindowViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.emptylayerviewmodel',

    data:
        {
            informationText: '<p><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>&nbsp; You are creating an empty geometry layer. Please fill in the required information.</p>',
            msg: ''
        }
    ,

    stores: {
        categorystore: {

            fields: [
        { name: 'name'},
        {name: 'categoryid'}
    ],

            autoLoad: true,
            model: 'VramApp.model.Countries',
            sorters: 'name',

            proxy: {
                type: 'ajax',
                url: "/metadata/layers/categories/simple",
                headers: {
                    'Accept': 'application/json'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'categories'
                }
            }
        },
        
        writableworkspacesstore: {

            fields: [{name: 'name'}, {name: 'workspaceId'}],


            autoLoad: true,

            proxy: {
                type: 'ajax',
                headers: {
                    'Accept': 'application/json'
                },
                url: '/workspaces/private',

                reader: {
                    type: 'json',
                    rootProperty: 'workspaces'
                }

            }
        },

        geomTypeStore: {

            autoLoad: true,
            fields: [
                {name: 'geomType'},
                {name: 'geomTypeId'},
                {name: 'commonName'}
            ],

            proxy: {
                type: 'ajax',
                headers: {
                    'Accept': 'application/json'
                },
                url: '/types/geometry',

                reader: {
                    type: 'json',
                    rootProperty: 'geomTypes'
                }

            },

            isSupported: function(geomType){
                var data = this.getData();
                var found = false;
                data.each(function(record, index){
                    if(geomType===record.get('geomType')){
                        found = true
                    }
                });
                return found;
            }
        },

        attributeStore:{
            autoLoad: false,
            fields: [
                {name: 'name'},
                {name: 'dataType'}
            ]
        },

        availableDataTypes: {
            autoLoad: true,
            fields: [
                {name: 'attributeType'},
                {name: 'openLayersName'},
                {name: 'attributeId'}
            ],

            proxy: {
                type: 'ajax',
                headers: {
                    'Accept': 'application/json'
                },
                url: '/types/attributes',

                reader: {
                    type: 'json',
                    rootProperty: 'attributeTypes'
                }

            },

            isSupported: function(attributeType){
                var data = this.getData();
                var found = false;
                data.each(function(record, index){
                    if(attributeType===record.get('openlayersName')){
                        found = true
                    }
                });
                return found;
            },

            mapper: function(attributeType){
                var data = this.getData();
                var found = false;
                data.each(function(record, index){
                    if(attributeType===record.get('openlayersName')){
                        return record.get('attributeType')
                    }
                });
            }
        }
    }
});
