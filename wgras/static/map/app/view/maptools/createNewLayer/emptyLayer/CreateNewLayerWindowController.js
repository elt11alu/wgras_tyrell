/**
 * Created by anton on 2016-08-19.
 */

Ext.define('VramApp.view.maptools.createNewLayer.emptyLayer.CreateNewLayerWindowController', {
    extend: 'Ext.app.ViewController',
    requires: [],
    alias: 'controller.createnewemptylayerwindowcontroller',


    init: function () {
        this.getView().on('beforeclose', this.beforeClose, this);
    },

    closeWindow: function (btn) {
        this.getView().close()
    },

    beforeClose: function () {
        this.fireEvent('toggleHeadPanelButtons', false);
    },

    onAddAttribute: function () {
        // Create a model instance
        var rec = {
            name: '',
            attributeType: ''
        };

        this.getViewModel().getStore('attributeStore').insert(0, rec);
        this.lookupReference('attributeGrid').getPlugin('attributeEditorPlugin').startEditByPosition({
            row: 0,
            column: 0
        })
    },

    removeAttribute: function (btn) {
        this.getViewModel().getStore('attributeStore').remove(btn.getWidgetRecord())
    },

    postLayer: function () {
        if (this.getView().getFeatures() != null) {
            this.postLayerWithGeometry()
        } else {
            this.postLayerWithoutGeometry();
        }
    },

    postLayerWithoutGeometry: function () {
        var layer_name, layer_title, workspace_id, fields, category, description, geomTypeName, success_func, fail_func, formValues, fieldsArr, loadMask, me;
        me = this;
        formValues = this.lookupReference('createNewLayerForm').getValues();
        layer_name = formValues.layer_name;
        layer_title = formValues.layer_title;
        geomTypeName = formValues.geomType;
        workspace_id = formValues.workspaceId;
        category = formValues.category;
        description = formValues.description;
        fieldsArr = [];
        fields = this.getViewModel().getStore('attributeStore').getData();
        $.get('layer/exists/'.concat(workspace_id, '/', layer_name), function (resp) {
            if (resp.exists) {
                Ext.Msg.alert('Status', 'Layer name already exists, choose another name');
            } else {


                fields.each(function (record, index, len) {
                    fieldsArr.push({'name': record.get('name'), 'attributeType': record.get('attributeType')})
                });
                loadMask = VramApp.utils.Loader.getLoader(me.getView(), 'Creating layer')
                success_func = function (resp) {
                    loadMask.hide();
                    me.addLayerToMap(resp);
                    me.closeWindow()
                };
                fail_func = function (resp) {
                    loadMask.hide();
                };
                loadMask.show();
                try {
                    VramApp.LayerUpdate.postNewEmptyLayer(layer_name, layer_title, workspace_id, fieldsArr, geomTypeName, category, description, success_func, fail_func)

                }
                catch (e) {
                    loadMask.hide();
                    console.warn(e)
                }
            }
        });

    },

    addLayerToMap: function (respObj) {
        var vectorMetadata, rasterMetadata;

        vectorMetadata = respObj.vectorMetadata;
        rasterMetadata = respObj.rasterMetadata;

        // SHOULD BE A GEOSERVER LAYER
        var vector_layer = VramApp.LayerLoader.getGeoServerWFS(vectorMetadata.workspaceName, vectorMetadata.layerName);
        var bbox = vector_layer.getSource().getExtent();

        var vector_required = {
            layer: vector_layer,
            layerName: vectorMetadata.layerName,
            layerTitle: vectorMetadata.layerTitle
        };

        var vector_options = {
            layerId: vectorMetadata.layerId,
            visible: false,
            abstract: vectorMetadata.description,
            bbox: bbox,
            folder: vectorMetadata.workspaceName,
            workspaceName: vectorMetadata.workspaceName
        };
        VramApp.MapUtils.addWFSOverlay(vector_required, vector_options);

        var raster_layer = VramApp.LayerLoader.getGeoServerTiledWMS(rasterMetadata.workspaceName, rasterMetadata.layerName);

        var required_raster = {
            layer: raster_layer,
            layerName: rasterMetadata.layerName,
            layerTitle: rasterMetadata.layerTitle
        };

        var options_raster = {
            layerId: rasterMetadata.layerId,
            visible: false,
            abstract: rasterMetadata.description,
            bbox: bbox,
            folderName: rasterMetadata.workspaceName,
            workspaceName: rasterMetadata.workspaceName
        };
        VramApp.MapUtils.addWMSOverlay(required_raster, options_raster);
        Ext.toast('Layers saved and added to map');

    }
});
