/**
 * Created by anton on 2016-08-19.
 */

Ext.define('VramApp.view.maptools.createNewLayer.withGeometry.CreateNewLayerWindowController', {
    extend: 'Ext.app.ViewController',
    requires: [],
    alias: 'controller.createnewlayerwithgeometrywindowcontroller',


    init: function () {
        this.getView().on('beforeclose', this.beforeClose, this);
        this.setWindowConfigsForPredefinedGeometry();
    },

    setWindowConfigsForPredefinedGeometry: function () {
        if (this.getView().getFeatures() != null) {
            this.setNumberOfFeatures();
            this.setGeomTypeCombo();
            this.setInformationText();
            this.setAttributes()

        }
    },

    setAttributes: function () {
        var layerId, me, loadMask, attributeGrid, attrStore;
        attrStore = this.getViewModel().getStore('attributeStore');
        me = this;
        this.getView().on('afterrender', function () {
            attributeGrid = me.lookupReference('attributeGrid');
            loadMask = VramApp.utils.Loader.getLoader(attributeGrid, 'Loading attributes');
            loadMask.show()

            attrStore.load({
                url: '/resource/attributes/'.concat(me.getView().getLayerId()),
                callback: function (records, operation, success) {
                    loadMask.hide()
                }
            });
        });


    },

    setNumberOfFeatures: function () {
        this.lookupReference('nbrOfFeaturesField').setValue(this.getView().getFeatures().length);
    },

    setGeomTypeCombo: function () {
        var features, combo, store, geomType;
        features = this.getView().getFeatures();
        combo = this.lookupReference('geomTypeCombo');
        store = this.getViewModel().getStore('geomTypeStore');

        geomType = features[0].getGeometry().getType();
        store.on({
            load: {
                fn: function () {
                    if (store.isSupported(geomType)) {
                        combo.setValue(geomType);
                        combo.setReadOnly(true);
                    }
                },
                scope: this,
                single: true
            }
        });
        store.reload()


    },

    closeWindow: function (btn) {
        this.getView().close()
    },

    beforeClose: function () {
        this.fireEvent('toggleHeadPanelButtons', false);
    },

    onAddAttribute: function () {
        // Create a model instance
        var rec = {
            name: '',
            attributeType: ''
        };

        this.getViewModel().getStore('attributeStore').insert(0, rec);
        this.lookupReference('attributeGrid').getPlugin('attributeEditorPlugin').startEditByPosition({
            row: 0,
            column: 0
        })
    },

    removeAttribute: function (btn) {
        this.getViewModel().getStore('attributeStore').remove(btn.getWidgetRecord())
    },

    setInformationText: function () {
        this.getViewModel().set('informationText', '<p><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>&nbsp; You are creating a layer with predefined geometry, some settings are not available.</p>')
    },

    postLayer: function () {
        this.postLayerWithGeometry()
    },

    postLayerWithGeometry: function () {
        var layer_name, temp_layer, layer_title, workspace_id, category, description, fields, geomTypeName, success_func, fail_func, features, formValues, fieldsArr, loadMask;
        var me = this;
        formValues = this.lookupReference('createNewLayerForm').getValues();
        layer_name = formValues.layer_name;
        layer_title = formValues.layer_title;
        geomTypeName = formValues.geomType;
        workspace_id = formValues.workspaceId;
        category = formValues.category;
        description = formValues.description;
        fieldsArr = [];
        fields = this.getViewModel().getStore('attributeStore').getData();
        features = this.getView().getFeatures();

        $.get('layer/exists/'.concat(workspace_id, '/', layer_name), function (resp) {
            if (resp.exists) {
                Ext.Msg.alert('Status', 'Layer name already exists, choose another name');
            } else {


                fields.each(function (record, index, len) {
                    fieldsArr.push({'name': record.get('name'), 'attributeType': record.get('attributeType')})
                });
                loadMask = VramApp.utils.Loader.getLoader(me.getView(), 'Creating layer');

                temp_layer = new ol.layer.Vector({
                    source: new ol.source.Vector({
                        features: features
                    })
                });

                success_func = function (resp) {
                    loadMask.hide();
                    me.addLayerToMap(resp);
                    me.closeWindow()
                };
                fail_func = function (resp) {
                    loadMask.hide();
                };

                loadMask.show();
                try {
                    VramApp.LayerUpdate.postNewLayerWithGeometry(temp_layer, layer_name, layer_title, workspace_id, fieldsArr, geomTypeName, category, description, success_func, fail_func)

                }
                catch (e) {
                    loadMask.hide();
                    console.log(e)
                }
            }
        })
    },

    addLayerToMap: function (respObj) {
        var vectorMetadata, rasterMetadata;

        vectorMetadata = respObj.vectorMetadata;
        rasterMetadata = respObj.rasterMetadata;

        // SHOULD BE A GEOSERVER LAYER
        var vector_layer = VramApp.LayerLoader.getGeoServerWFS(vectorMetadata.workspaceName, vectorMetadata.layerName);
        var bbox = vector_layer.getSource().getExtent();

        var vector_required = {
            layer: vector_layer,
            layerName: vectorMetadata.layerName,
            layerTitle: vectorMetadata.layerTitle
        };

        var vector_options = {
            layerId: vectorMetadata.layerId,
            visible: false,
            abstract: vectorMetadata.description,
            bbox: bbox,
            folder: vectorMetadata.workspaceName,
            workspaceName: vectorMetadata.workspaceName
        };
        VramApp.MapUtils.addWFSOverlay(vector_required, vector_options);

        var raster_layer = VramApp.LayerLoader.getGeoServerTiledWMS(rasterMetadata.workspaceName, rasterMetadata.layerName);

        var required_raster = {
            layer: raster_layer,
            layerName: rasterMetadata.layerName,
            layerTitle: rasterMetadata.layerTitle
        };

        var options_raster = {
            layerId: rasterMetadata.layerId,
            visible: false,
            abstract: rasterMetadata.description,
            bbox: bbox,
            folderName: rasterMetadata.workspaceName,
            workspaceName: rasterMetadata.workspaceName
        };
        VramApp.MapUtils.addWMSOverlay(required_raster, options_raster);
        Ext.toast('Layers saved and added to map');

    }
});
