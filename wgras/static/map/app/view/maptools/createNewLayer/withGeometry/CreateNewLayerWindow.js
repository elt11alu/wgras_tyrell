/**
 * Created by anton on 2016-08-19.
 */

Ext.define('VramApp.view.maptools.createNewLayer.withGeometry.CreateNewLayerWindow', {
    extend: 'Ext.window.Window',
    requires: [
        'VramApp.view.maptools.createNewLayer.withGeometry.CreateNewLayerWindowController',
        'VramApp.view.maptools.createNewLayer.withGeometry.CreateNewLayerWindowViewModel'
    ],
    xtype: 'createnewlayerwithgeometrywindow',

    viewModel: {
        type: 'withgeometryviewmodel'
    },

    title: 'Create new Layer',

    controller: 'createnewlayerwithgeometrywindowcontroller',


    width: 500,
    height: 500,
    bodyPadding: 10,
    constrain: true,
    closable: true,
    config: {
        features: null,
        layerId: null
    },
    layout: 'fit',


    items: [
        {
            xtype: 'panel',
            scrollable: true,
            bodyPadding: 10,
            tbar: [
                {
                    xtype: 'displayfield',
                    reference: 'informationTextField',
                    glyph: 0xf129,
                    bind: {
                        value: '{informationText}'
                    }
                }

            ],
            items: [
                {
                    xtype: 'form',
                    reference: 'createNewLayerForm',
                    fieldDefaults: {
                        labelAlign: 'right',
                        msgTarget: 'side'
                    },
                    layout: 'anchor',

                    items: [
                        {
                            xtype: 'fieldset',
                            collapsible: true,
                            title: 'Layer details',
                            defaultType: 'textfield',
                            defaults: {
                                anchor: '100%'
                            },

                            items: [
                                {
                                    allowBlank: false,
                                    fieldLabel: 'Layer name',
                                    name: 'layer_name',
                                    emptyText: 'Layer name',
                                    regex: new RegExp("^[a-zA-Z0-9_]{3,40}$"),
                                    invalidText: 'Use only a-z, 0-9 and _'
                                },
                                {
                                    allowBlank: false,
                                    fieldLabel: 'Layer title',
                                    name: 'layer_title',
                                    emptyText: 'Layer title',
                                    regex: new RegExp("^[a-zA-Z0-9_]{3,40}$"),
                                    invalidText: 'Use only a-z, 0-9 and _'
                                },
                                {
                                    allowBlank: false,
                                    fieldLabel: 'Features nbr',
                                    name: 'nbrOfFeatures',
                                    reference: 'nbrOfFeaturesField',
                                    value: 0,
                                    hidden:true,
                                    xtype: 'numberfield',
                                    readOnly: true
                                },
                                {
                            xtype: 'combo',
                            valueField: 'name',
                            displayField:'name',
                                    name: 'category',
                            fieldLabel: 'Category',
                            reference: 'category',
                            regex: new RegExp("^[a-zA-Z0-9 .,]{3,50}$"),
                            regexText: 'Use only a-z A-Z  and 0-9',
                            allowBlank:false,
                            bind: {
                                store: '{categorystore}'
                            }
                        }, {
                                    xtype: 'textarea',
                                    name:'description',
                                    fieldLabel: 'Description',
                                    reference: 'description',
                                    regex: new RegExp("^[a-zA-Z0-9 .,\']{3,50}$"),
                                    regexText: 'Use only a-z A-Z  and 0-9',
                                    allowBlank: true
                                },
                                {
                                    allowBlank: false,
                                    xtype: 'combo',
                                    reference: 'geomTypeCombo',
                                    fieldLabel: 'Geometry',
                                    name: 'geomType',
                                    valueField: 'geomType',
                                    displayField: 'geomType',
                                    queryMode: 'local',

                                    bind: {
                                        store: '{geomTypeStore}',
                                        value: '{geomTypeStore.geomType}'
                                    },
                                    readOnly: false
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            title: 'Attributes',
                            collapsible: true,
                            defaults: {
                                anchor: '100%'
                            },

                            items: [
                                {
                                    xtype: 'grid',
                                    reference: 'attributeGrid',
                                    plugins: {ptype: 'cellediting', clicksToEdit: 1, pluginId: 'attributeEditorPlugin'},
                                    selModel: 'cellmodel',
                                    tbar: [{
                                        text: 'Add',
                                        handler: 'onAddAttribute',
                                        glyph: 0xf196
                                    }],
                                    height: 150,
                                    scrollable: true,
                                    bind: {
                                        store: '{attributeStore}'
                                        //value: '{attributeStore.attributeType}'
                                    },
                                    columns: [
                                        {
                                            header: 'Name',
                                            name: 'attributeName',
                                            dataIndex: 'name',
                                            displayField: 'name',
                                            flex: 1,
                                            /*editor: {
                                                allowBlank: false,
                                                regex: new RegExp("^[a-zA-Z0-9_]{3,40}$"),
                                                invalidText: 'Use only a-z, 0-9 and _'
                                            }*/
                                        },
                                        {
                                            header: 'Data type',
                                            dataIndex: 'attributeType',
                                            displayField: 'attributeType',
                                            name: 'attributeType',
                                            width: 130,
                                            /*editor: new Ext.form.field.ComboBox({
                                                typeAhead: true,
                                                triggerAction: 'all',
                                                valueField: 'attributeType',
                                                displayField: 'openLayersName',
                                                forceSelection: true,
                                                bind: {
                                                    store: '{availableDataTypes}',
                                                    //value: '{availableDataTypes.attributeType}'
                                                }
                                            })*/
                                        }, {
                                            xtype: 'widgetcolumn',

                                            dataIndex: 'name',
                                            widget: {
                                                width: 30,
                                                textAlign: 'left',
                                                xtype: 'button',
                                                disabled:true,
                                                glyph: 0xf1f8,
                                                handler: 'removeAttribute',
                                                style: {
                                                    backgroundColor: 'red'
                                                }
                                            }
                                        }
                                    ]
                                }

                            ]
                        },
                        {
                            xtype: 'fieldset',
                            title: 'Workspace',
                            collapsible: true,
                            defaultType: 'textfield',
                            defaults: {
                                anchor: '100%'
                            },

                            items: [
                                {
                                    allowBlank: false,
                                    fieldLabel: 'Workspace',
                                    reference: 'workspaceCombo',
                                    xtype: 'combo',
                                    valueField: 'workspaceId',
                                    queryMode: 'local',
                                    displayField: 'name',
                                    name: 'workspaceId',
                                    bind: {
                                        store: '{writableworkspacesstore}',
                                        value: '{writableworkspacesstore.name}',
                                        //selection:'{writableworkspacesstore.workspaceId}'

                                    }
                                }, {
                                    xtype: 'displayfield',
                                    value: '',
                                    fieldLabel: 'type',
                                    hidden: true,
                                    bind: {
                                        value: '{workspaceCombo.selection.type}',
                                        hidden: '{!workspaceCombo.selection.type}'
                                    }
                                },
                                {
                                    xtype: 'displayfield',
                                    value: '',
                                    fieldLabel: 'country',
                                    hidden: true,
                                    bind: {
                                        value: '{workspaceCombo.selection.country}',
                                        hidden: '{!workspaceCombo.selection.country}'
                                    }
                                },
                                {
                                    xtype: 'displayfield',
                                    value: '',
                                    fieldLabel: 'organization',
                                    hidden: true,
                                    bind: {
                                        value: '{workspaceCombo.selection.organization}',
                                        hidden: '{!workspaceCombo.selection.organization}'
                                    }
                                }
                            ]
                        }

                    ]
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Create layer',
            formBind: true,
            handler: 'postLayer'
        },
        {
            text: 'Cancel',
            handler: 'closeWindow'
        }
    ]
});
