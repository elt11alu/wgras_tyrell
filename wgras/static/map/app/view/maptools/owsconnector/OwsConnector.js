/**
 * Created by anton on 2016-01-16.
 */
Ext.define('VramApp.view.maptools.owsconnector.OwsConnector', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.button.Button',
        'Ext.form.Panel',
        'Ext.form.field.Display',
        'Ext.form.field.Text',
        'Ext.grid.Panel',
        'Ext.grid.column.Widget',
        'Ext.layout.container.Accordion',
        'Ext.layout.container.Fit',
        'Ext.layout.container.VBox',
        'Ext.panel.Panel',
        'VramApp.view.maptools.owsconnector.OwsConnectorController',
        'VramApp.view.maptools.owsconnector.OwsConnectorModel'
    ],

    xtype: 'owsconnector',


    viewModel: {
        type: 'owsconnector'
    },

    controller: 'owsconnector',

    /*window specific configs*/
    autoShow: true,
    width: 360,
    bodyPadding: 5,
    glyph: 0xf0c1,
    closable: false,
    title: 'Create connection',


    items: [
        {
            xtype: 'panel',

            items: [
                {
                    xtype: 'fieldset',
                    defaults: {
                        anchor: '100%'
                    },
                    title: 'Instructions',
                    collapsible: true,
                    items: [
                        {


                            items: [
                                {
                                    xtype: 'displayfield',
                                    value: '<b>Instructions: </b>This tool creates a connection to an external OGC compliant map service. <br>' +
                                    'Provide a valid GetCapabilities URL in the field below and press connect. <br>' +
                                    'Available layers will be shown in the table below.'
                                }
                            ]

                        }
                    ]
                },
                {

                    xtype: 'container',
                    defaults: {
                        anchor: '100%'
                    },
                    items: [

                        {
                            xtype: 'grid',
                            frame: true,
                            columnLines: true,
                            reference: 'connectiongrid',
                            minHeight: 200,
                            maxHeight: 250,
                            margin: '0 0 5px 0',
                            columns: [
                                {
                                    text: 'Data sets',
                                    dataIndex: 'layerName',
                                    flex: 1
                                },
                                {
                                    width: 80,
                                    xtype: 'widgetcolumn',
                                    widget: {
                                        glyph: 0xf067,
                                        width: 40,
                                        xtype: 'button',
                                        dataIndex: 'layerName',
                                        tooltip: 'Add to map',
                                        style: {

                                            border: 0
                                        },
                                        handler: 'onAddLayer'

                                    }
                                }


                            ],
                            bind:{
                                store: '{owsconnectionstore}'
                            }

                        },
                        {
                            xtype: 'form',
                            defaults: {
                                anchor: '100%'
                            },
                            items: [
                                {
                                    xtype: 'combo',
                                    fieldLabel: 'Proposed services',
                                    typeAhead: true,
                                    triggerAction: 'all',
                                    valueField: 'url',
                                    displayField: 'name',
                                    forceSelection: true,
                                    listeners:{
                                        select: 'onSelectExistingService'
                                    },
                                    bind: {
                                        store: '{externalowsstore}'
                                    }
                                },
                                {
                                    xtype: 'displayfield',
                                    value: 'Provide a valid GetCapabilities URL:'
                                },
                                {
                                    xtype: 'textfield',
                                    emptyText: 'GetCapabilities URL',
                                    vtype: 'url',
                                    bind: {
                                        value: '{url}'
                                    }
                                },
                                {
                                    xtype: 'button',
                                    anchor: '30%',
                                    margin: 5,
                                    formBind: true,
                                    scale: 'small',
                                    text: 'Connect',
                                    handler: 'onConnect'
                                },
                                {
                                    xtype: 'button',
                                    margin: 5,
                                    anchor: '30%',
                                    scale: 'small',
                                    text: 'Close',
                                    handler: 'onClose'
                                }
                            ]
                        }


                    ]

                }


            ]
        }

    ]
});
