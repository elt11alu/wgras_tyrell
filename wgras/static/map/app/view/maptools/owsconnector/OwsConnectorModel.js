/**
 * Created by anton on 2016-01-16.
 */
Ext.define('VramApp.view.maptools.owsconnector.OwsConnectorModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.owsconnector',

    stores: {
        externalowsstore: {

            fields: [{name: 'url'}, {name: 'description'}, {name: 'category'}, {name: 'name'}, {name: 'owsId'}],


            autoLoad: true,

            proxy: {
                type: 'ajax',
                headers: {
                    'Accept': 'application/json'
                },
                url: '/metadata/externalows',

                reader: {
                    type: 'json',
                    rootProperty: 'services'
                }

            }
        },
        owsconnectionstore: {

            fields: [
                {name: 'layerName'},
                {name: 'layerTitle'},
                {name: 'abstract'}
            ],
            storeId: 'owsconnectionstore',

            autoLoad: false,

            proxy: {
                type: 'ajax',
                actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
                url: '/process/externalservice',
                paramsAsJson: true,
                headers: {'Content-Type': "application/json"},
                reader: {
                    type: 'json',
                    rootProperty: 'layers'
                }
            }

        }

    },

    data: {
        url: ''
    }
});