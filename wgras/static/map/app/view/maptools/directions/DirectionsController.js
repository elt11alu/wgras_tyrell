/**
 * Created by anton on 6/13/16.
 */

Ext.define('VramApp.view.maptools.directions.DirectionsController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.directionscontroller',

    requires: [
        'VramApp.MapUtils',
        'VramApp.utils.Loader'
    ],

    tempLayerId: false,
    listenerKey: false,
    mapMarkerLayer: false,

    init: function () {
        this.addMapListener();
    },

    addMapListener: function () {
        var map, listenerKey, me, clicked;
        me = this;
        map = VramApp.MapUtils.getOlMap();

        clicked = []
        this.listenerKey = map.on('click', function (evt) {
            var coords = evt.coordinate;
            var transformedCoords = ol.proj.toLonLat(coords, 'EPSG:3857');
            clicked.push(transformedCoords);
            switch (clicked.length) {
                case 1:
                    me.addMapMarkerFrom(coords);
                    break;
                case 2:
                    me.addMapMarkerTo(coords);
                    me.getDirections(clicked);
                    break;
                case 3:
                    clicked = [];
                    clicked.push(transformedCoords);
                    VramApp.MapUtils.getOlMap().removeOverlay(me.markerTo)
                    me.addMapMarkerFrom(coords);
                    me.clearRouteLayer();
                    break;
            }

        }, this);
    },

    getMarkerHtml: function(){
        var wrap = document.createElement('div')
        var pin = document.createElement('div')
        var pulse = document.createElement('div')
        pin.className = 'pin'
        pulse.className = 'pulse'
        wrap.appendChild(pin)
        wrap.appendChild(pulse)
        return wrap;

    },

    addMapMarkerFrom: function(coords){
        var htmlMarker = this.getMarkerHtml()
        VramApp.MapUtils.getOlMap().removeOverlay(this.markerFrom);
        this.markerFrom = new ol.Overlay({
        position: coords,
        positioning: 'bottom-left',
        element: htmlMarker,
        stopEvent: false
      });

        VramApp.MapUtils.getOlMap().addOverlay(this.markerFrom);
        //this.mapMarkerLayer = this.getNewTempLayer();
        //VramApp.MapUtils.addTempLayer(this.mapMarkerLayer);
    },

    addMapMarkerTo: function(coords){
        var htmlMarker = this.getMarkerHtml()
        VramApp.MapUtils.getOlMap().removeOverlay(this.markerTo);
        this.markerTo = new ol.Overlay({
        position: coords,
        positioning: 'bottom-left',
        element: htmlMarker,
        stopEvent: false
      });

        VramApp.MapUtils.getOlMap().addOverlay(this.markerTo);
    },

    setMarker: function (coords) {
        var features;
        if (this.mapMarkerLayer) {
            features = this.mapMarkerLayer.getSource().getFeatures();
            switch (features.length) {
                case 0:
                    this.mapMarkerLayer.getSource().addFeature(new ol.Feature({
                        geometry: new ol.geom.Point(coords)
                    }));
                    break;
                case 1:
                    this.mapMarkerLayer.getSource().addFeature(new ol.Feature({
                        geometry: new ol.geom.Point(coords)
                    }));
                    break;
                case 2:
                    this.mapMarkerLayer.getSource().clear();
                    this.mapMarkerLayer.getSource().addFeature(new ol.Feature({
                        geometry: new ol.geom.Point(coords)
                    }));
                    break;
                default:
                    this.mapMarkerLayer.getSource().clear();

            }
        }
    },

    getDirections: function (coords) {
        var url, me, loadMsk;
        me = this;
        url = this.generateUrl(coords);
        loadMsk = VramApp.utils.Loader.getLoader(this.getView(), 'Calculating path...');
        loadMsk.show();
        $.ajax({
            timeout: 5000,
            url: url,
            type: "GET",
            dataType: 'json',
            crossDomain: true
        }).done(function (json) {
            me.jsonresp = json;
            me.generateRoute(json);
            me.generateDescription(json);
            loadMsk.hide();

        }).fail(function (jqXHR) {
            loadMsk.hide();
            console.log(jqXHR)
            me.setErrorMessage('Could not calculate route, try a more precise area.')
        });
    },

    generateUrl: function (coords) {
        var key, pointa, pointb, vehicle;
        pointa = coords[1][1].toString().concat('%2C', coords[1][0].toString())
        pointb = coords[0][1].toString().concat('%2C', coords[0][0].toString())
        vehicle = this.lookupReference('vehicle').getValue();
        key = VramApp.WGRASUrl.GRAPHHOPPERAPIKEY
        return VramApp.WGRASUrl.GRAPHHOPPERBASEURL.concat('point=', pointa, '&point=', pointb, '&vehicle=', vehicle, '&points_encoded=false', '&key=', key)
    },

    generateRoute: function (json) {
        var path, line, layerId, source, layer, transformed, style;
        path = json.paths[0].points.coordinates;
        line = new ol.geom.LineString([]);
        path.forEach(function (el, ind, arr) {
            var c = [el[0], el[1]];
            line.appendCoordinate(c);
        });
        transformed = line.transform('EPSG:4326', 'EPSG:3857');

        layer = VramApp.MapUtils.getTempLayer(this.tempLayerId)
        if (layer != null) {
            source = layer.getSource();
            source.clear();
            source.addFeature(new ol.Feature({geometry: transformed}));
        } else {
            layer = this.getNewTempLayer();
            source = layer.getSource();
            source.clear();
            source.addFeature(new ol.Feature({geometry: transformed}));
            style = this.getOlStyle();
            this.tempLayerId = VramApp.MapUtils.addTempLayer(layer, {style: style})
        }
    },

    clearRouteLayer: function(){
        var layer, source;
        layer = VramApp.MapUtils.getTempLayer(this.tempLayerId)
        source = layer.getSource();
        source.clear();
    },

    getNewTempLayer: function () {
        return new ol.layer.Vector({
            source: new ol.source.Vector({features: []})
        });
    },

    getOlStyle: function () {
        var fill = new ol.style.Fill({
            color: 'rgba(255,255,255,0.4)'
        });
        var stroke = new ol.style.Stroke({
            color: '#ff3399',
            width: 2
        });

        return new ol.style.Style({
            image: new ol.style.Circle({
                fill: fill,
                stroke: stroke,
                radius: 5
            }),
            fill: fill,
            stroke: stroke
        })

    },

    generateDescription: function (json) {

        var distance = json.paths[0].distance;
        this.formatDistance('kilometer', distance)


    },

    onDistanceFormatChange: function (radio, newVal, oldVal) {
        this.formatDistance(newVal['distance-format'])
    },

    formatDistance: function (distanceFormat, distance) {
        var distField = this.lookupReference('distance');
        var dist = distance ? distance : distField.getValue();
        var newDist = 0;
        var label = '';

        switch (distanceFormat) {

            case 'kilometer':
                newDist = Math.round(dist) / 1000;
                label = 'Distance in kilometer';
                break;
            case 'meter':
                newDist = dist * 1000;
                label = 'Distance in meter';
                break;
        }
        distField.setValue(newDist);
        distField.setFieldLabel(label)


    },

    setErrorMessage: function(msg){
        this.lookupReference('mainMsgField').setValue('<div style="color:#c6453b"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> '+msg+'</div>')
    },

    onClose: function () {
        this.tempLayerId ? VramApp.MapUtils.removeLayerByLayerIdentifier(this.tempLayerId) : false;
        this.fireEvent('toggleHeadPanelButtons', false);
        this.listenerKey ? VramApp.MapUtils.getOlMap().unByKey(this.listenerKey) : false;
        var map = VramApp.MapUtils.getOlMap();
        map.removeOverlay(this.markerFrom);
        map.removeOverlay(this.markerTo);
        this.getView().close()
    }
});
