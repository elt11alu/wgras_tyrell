/**
 * Created by anton on 2015-11-01.
 */
Ext.define('VramApp.view.maptools.geoLocationTool.GeoLocationController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.geolocationcontroller',

    requires: [
        'VramApp.InputValidationModel'
    ],

    /**
     * Called when the view is created
     */
    init: function (view) {
        var me = this;
        view.on({
            specialkey: this.checkEnterKey,
            scope: me
        });
        view.on({
            change:this.removeOverlay,
            scope:me
        });
    },

    checkEnterKey: function (field, e) {
        var value = field.getValue();
        if (e.getKey() === e.ENTER) {
            this.checkSearchInput(value);
        }
    },

    checkSearchInput: function (value) {
        if (VramApp.InputValidationModel.validateLocationSearch(value)) {
            this.buildUrl(value);
        }
    },

    buildUrl: function (value) {
        var URL;
        var query = value;
        var format = this.getView().getViewModel().getData().queryParams.format;
        URL = VramApp.WGRASUrl.LOCATIONSEARCH_URL.concat(query).concat('?format=').concat(format);
        this.executeSearch(URL);
    },

    executeSearch: function (URL) {
        var me = this;
        Ext.Ajax.request({
            url: URL,

            success: function (response, opts) {
                if (response) {
                    var jsonResponse;
                    try {
                        jsonResponse = Ext.decode(response.responseText);
                        var extent4326 = jsonResponse[0].boundingbox;
                        var minx = parseFloat(extent4326[2]);
                        var miny = parseFloat(extent4326[0]);
                        var maxx = parseFloat(extent4326[3]);
                        var maxy = parseFloat(extent4326[1]);
                        var extent4326Floats = [minx, miny, maxx, maxy];
                        var extent = VramApp.MapUtils.reprojectExtentTo3857(extent4326Floats, 'EPSG:4326');
                        var name = jsonResponse[0].display_name;
                        var type = jsonResponse[0].type;
                        var el = document.createElement('div');
                        el.style.color = "rgb(255,255,255)";
                        el.style.background = "rgba(0,0,0,0.5)";
                        el.style.padding = '4px';
                        el.style.borderRadius = '5px';
                        el.innerHTML= '<b>'+name+'</b>';
                        var btn = document.createElement('button');
                        btn.style.marginLeft = '5px';
                        btn.style.borderRadius = '5px';
                        btn.style.background = "rgba(255,255,255,0.5)";
                        btn.textContent = 'X';
                        el.appendChild(btn);

                        me.marker = new ol.Overlay({
                            position: ol.extent.getCenter(extent),
                            positioning: 'top-right',
                            element: el,
                            stopEvent: false
                        });

                        btn.addEventListener("click",function(){me.removeOverlay()});
                        VramApp.MapUtils.getOlMap().addOverlay(me.marker);
                        VramApp.MapUtils.panToExtent(extent);
                    } catch (err) {
                        console.log(err);
                    }


                }
            },

            failure: function (response, opts) {
                console.warn('server-side failure with status code ' + response.status);
            }
        });
    },
    removeOverlay:function(){
        if(this.marker){
            VramApp.MapUtils.getOlMap().removeOverlay(this.marker)
        }
    }


});
