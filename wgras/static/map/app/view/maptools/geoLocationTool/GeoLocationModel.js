/**
 * Created by anton on 2015-11-01.
 */
Ext.define('VramApp.view.maptools.geoLocationTool.GeoLocationModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.geolocation',


    data: {
        'queryParams': {
            'format': 'json',
            'accept-language': 'en'
        },
        'defaultText':'marmorvej 51, copenhagen'

    }


})
;