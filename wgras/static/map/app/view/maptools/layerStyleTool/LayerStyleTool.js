/**
 * Created by anton on 2015-09-28.
 */

Ext.define('VramApp.view.maptools.layerStyleTool.LayerStyleTool', {

    extend: 'Ext.window.Window',
    xtype: 'layerStyleWindow',

    requires: [
        'VramApp.view.maptools.layerStyleTool.LayerStyleToolController'
    ],

    controller:'layerstylecontroller',



    title:{},
    layer:{},

    glyph: 0xf1fc,

    autoShow: true,

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        items: [{
            text: 'Default style',
            handler:'onDefaultStyle'
        }/*,{
            text:'Attribute based',
            handler:'onAttributeBasedStyling'
        }*/]
    }],

    items: [{
        xtype: 'form',
        reference: 'layerstyleform',
        padding: 10,
        layout: 'anchor',
        defaults: {
            anchor: '85%'
        },
        items: [{
            xtype: 'displayfield',
            value: 'Fill color'
        }, {
            xtype: 'colorpicker',
            reference:'fillColorPicker'


        }, {
            fieldLabel: 'Fill opacity',
            xtype: 'slider',
            name: 'fillOpacity',
            reference:'fillopacityslider',
            increment: 1,
            minValue: 0,
            maxValue: 10,
            width: 250,
            value: 50
        }, {
            xtype: 'displayfield',
            value: 'Stroke color'
        }, {
            xtype: 'colorpicker',
            reference:'strokeColorPicker'


        }, {
            xtype: 'slider',
            fieldLabel: 'Strokeopacity',
            name: 'strokeOpacity',
            reference:'strokeopacityslider',
            width: 250,
            value: 2,
            increment: 1,
            minValue: 0,
            maxValue: 10
        }, {
            xtype: 'numberfield',
            fieldLabel: 'Stroke width: ',
            name: 'strokewidth',
            reference:'strokewidthfield',
            value: 6,
            minValue: 0
        }, {

            xtype: 'numberfield',
            fieldLabel: 'Point radius: ',
            name: 'pointradius',
            reference:'pointradiusfield',
            value: 6,
            minValue: 0
        }],
        buttons: [{
            text: 'Ok',
            handler:'onStyleClick'

        }, {
            text: 'Close',
            handler:'onCloseClick'
        }]

    }]
});
