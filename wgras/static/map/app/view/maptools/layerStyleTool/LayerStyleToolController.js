/**
 * Created by anton on 2015-09-28.
 */

Ext.define('VramApp.view.maptools.layerStyleTool.LayerStyleToolController', {
    extend: 'Ext.app.ViewController',
    alias: "controller.layerstylecontroller",
    requires: [
        'VramApp.utils.LayerColors',
        'VramApp.MapUtils',
        'VramApp.utils.VectorStyle'
    ],

    init: function (view) {
        var styleMap = this.extractStyleValuesForLayer(view.layerId);
        this.setColorPickers(styleMap);
        this.setStyleSliders(styleMap);
    },

    setColorPickers: function (styleMap) {
        var fillColorPicker = this.lookupReference('fillColorPicker');
        var strokeColorPicker = this.lookupReference('strokeColorPicker');
        var colorsArray = VramApp.utils.LayerColors.getAllColorsArray();
        fillColorPicker.colors = colorsArray;
        strokeColorPicker.colors = colorsArray;
        fillColorPicker.select(styleMap.fill.color, true);
        strokeColorPicker.select(styleMap.stroke.color, true);
    },

    onStyleClick: function (button) {
        var layerName = this.getView().layer;
        var layerId = this.getView().layerId;
        var map = VramApp.MapUtils.getOlMap();
        var layer = VramApp.MapUtils.getLayerByDBId(layerId);
        var values = this.lookupReference('layerstyleform').getValues();
        var fillColor = this.lookupReference('fillColorPicker').getValue();
        var strokeColor = this.lookupReference('strokeColorPicker').getValue();
        var newStyle = {
            fill: {
                color: fillColor,
                opacity: values.fillOpacity
            },
            stroke: {
                color: strokeColor,
                opacity: values.strokeOpacity,
                width: values.strokewidth
            },
            image: {
                fill: {
                    color: fillColor,
                    opacity: values.fillOpacity
                },
                stroke: {
                    color: strokeColor,
                    opacity: values.strokeOpacity,
                    width: values.strokewidth
                },
                radius: values.pointradius
            }
        };
        this.styleLayer(layer, newStyle);

    },

    extractStyleValuesForLayer: function (layerId) {
        var layer = VramApp.MapUtils.getLayerByDBId(layerId);
        var styleMap = layer.get('styleInfo');
        return styleMap;
    },

    setStyleSliders: function (styleMap) {
        var multiplier = 10;
        var pointRadiusField = this.lookupReference('pointradiusfield');
        var fillOpacitySlider = this.lookupReference('fillopacityslider');
        var strokeOpacitySlider = this.lookupReference('strokeopacityslider');
        var strokeWidthField = this.lookupReference('strokewidthfield');
        var pointradius = styleMap.image.radius;
        var fillopacity = styleMap.fill.opacity * multiplier;
        var strokeopacity = styleMap.stroke.opacity * multiplier;
        var strokewidth = styleMap.stroke.width;
        pointRadiusField.setValue(pointradius);
        fillOpacitySlider.setValue(fillopacity);
        strokeOpacitySlider.setValue(strokeopacity);
        strokeWidthField.setValue(strokewidth);


    },

    styleLayer: function (layer, newStyle) {
        var newStyleObj = VramApp.utils.VectorStyle.getNewOlStyle(newStyle);
        layer.setStyle(newStyleObj.style);
        layer.set('styleInfo',newStyleObj.styleInfo);
    },

    onAttributeBasedStyling:function(){
        var layerTitle = this.getView().layer;
        Ext.create({
            xtype:'attributeBasedStyling',
            title:'Attribute based styling',
            layerTitle : layerTitle
        });
    },

    onDefaultStyle:function(){
        var me = this;
        Ext.Msg.show({
            title:'Apply default style?',
            message: 'The previous style will be overwritten.',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    var styleObj = VramApp.MapUtils.getDefaultStyle();
                    var layerId = me.getView().layerId;
                    var layer = VramApp.MapUtils.getLayerByDBId(layerId);
                    layer.setStyle(styleObj.style);
                    layer.set('styleInfo', styleObj.styleInfo);
                }
            }
        });
    },

    onCloseClick: function (button) {
        this.view.close()
    }

});
