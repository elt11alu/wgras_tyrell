/**
 * Created by anton on 12/19/16.
 */

Ext.define('VramApp.view.maptools.editLayer.EditLayerController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.editlayercontroller',

    init: function () {
        var me = this;
        var map = VramApp.MapUtils.getOlMap();
        this.listenerFunc = function (event) {
            me.getFeatureProperties(event, me)
        };
        this.selectInteraction = VramApp.MapUtils.getSelectInteraction();
        this.selectListenerKey = this.selectInteraction.on('select', me.listenerFunc);

        this.lookupReference('attributegrid').on('edit', this.onEditAttribute, this);
    },

    getFeatureProperties: function (event, me) {

        if (event.selected.length > 0) {
            try {

                me.layerId = me.selectInteraction.getLayer(event.selected[0]).get('layerId');
                me.featureId = event.selected[0].get('ogc_fid');

                var loadMask = VramApp.utils.Loader.getLoader(me.getView(), 'Loading attributes..').show();

                me.getViewModel().getStore('attributestore').load({
                    url: '/layer/'.concat(me.layerId, '/feature/', me.featureId),
                    callback: function (records, operation, success) {
                        loadMask.hide()
                    }
                });

            } catch (err) {
                console.warn(err)
            }

        }
    },

    onEditAttribute: function (editor, context) {
        /*
         layer_id = request.json['layerId']
         feature_id = request.json['featureId']
         attr_name = request.json['attrName']
         attr_value = request.json['attr_value']
         */
        var value = context.record.get('value');
        var attr_name = context.record.get('name');
        var layerId = this.layerId;
        var featureId = this.featureId;
        var me = this;
        if (attr_name !== 'ogc_fid') {
            var success = function (resp) {
                var resp = Ext.decode(resp.responseText);
                if(resp.updated){
                    me.getViewModel().set('feedbackMessage', '<p style="color:cadetblue"><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>&nbsp;Attribute ' + attr_name + ' was updated.</p>')
                }
            };
            var fail = function (resp) {
                me.getViewModel().set('feedbackMessage', '<p style="color:red"><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>&nbsp;Attribute ' + attr_name + ' could not be updated</p>')
            };
            var url = '/layer/feature/';

            var data = JSON.stringify({
                layerId: layerId,
                featureId: featureId,
                attrName: attr_name,
                attr_value: value
            });

            VramApp.HTTPRequests.POST(url, success, fail, data);
        }


    },

    onClose: function () {
        this.fireEvent('toggleHeadPanelButtons', false);
        ol.Observable.unByKey(this.selectListenerKey);
        this.getView().close()
    }
});
