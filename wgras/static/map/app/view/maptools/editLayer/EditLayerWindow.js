/**
 * Created by anton on 12/19/16.
 */

Ext.define('VramApp.view.maptools.editLayer.EditLayerWindow', {
    extend: 'Ext.window.Window',

    requires: [
        'VramApp.view.maptools.editLayer.EditLayerController',
        'VramApp.view.maptools.editLayer.EditLayerModel'

    ],

    xtype: 'editlayerwindow',
    width: 400,
    height: 500,
    title: 'Edit attributes',
    scrollable: true,
    bodyPadding: 10,
    constrain: true,
    closable: false,

    controller: 'editlayercontroller',
    viewModel: {
        type: 'editlayermodel'
    },

    layout: {
        type: 'vbox',
        pack: 'start',
        align: 'stretch'
    },

    items: [
        {
            xtype: 'displayfield',
            flex: 1,
            bind: {
                value: '{instructions}'
            }
        },
{
            xtype: 'displayfield',
            flex: 1,
            bind: {
                value: '{feedbackMessage}'
            }
        },
        {
            //Grid for displaying attributes for a specific feature
            xtype: 'grid',
            viewConfig: {
                //removes red triangle in updated cell
                 markDirty: false
            },
            maxHeight: 300,
            reference: 'attributegrid',
            flex: 3,
            title: 'Attributes',
            bind: {
                store: '{attributestore}'
            },

            plugins: [{
                ptype: 'rowediting',
                clicksToMoveEditor: 1,
                autoCancel: true
            }],

            columns: [{
                text: 'Name',
                displayField: 'name',
                flex: 1,
                sortable: false,
                dataIndex: 'name'
            }, {
                text: 'Value',
                displayField: 'value',
                width: 95,
                sortable: false,
                dataIndex: 'value',
                editor: {
                    // defaults to textfield if no xtype is supplied
                    allowBlank: false
                }
            }, {
                text: 'Type',
                displayField: 'dataType',
                width: 80,
                sortable: false,
                dataIndex: 'dataType'
            }
            ]
        },
        {
            xtype: 'button',
            text: 'Close',
            handler: 'onClose'
        }
    ]

});