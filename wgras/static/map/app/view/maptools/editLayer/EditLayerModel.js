/**
 * Created by anton on 12/19/16.
 */

Ext.define('VramApp.view.maptools.editLayer.EditLayerModel',{
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.editlayermodel',

    stores:{

        attributestore:{
            autoLoad: false,
            fields: [
                {name: 'name'},
                {name: 'value'},
                {name: 'dataType'}
            ],

            proxy: {
                type: 'ajax',
                headers: {
                    'Accept': 'application/json'
                },
                url: '',

                reader: {
                    type: 'json',
                    rootProperty: 'attributes'
                }

            }
        }

    },

    data: {
        instructions: '<p><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>&nbsp; Click on object in the map and edit its attribute values in the list below. ' +
        '<i>Note: This tool only works for vector data and the layer must be activated in the layer list to the left.</i></p>',

        feedbackMessage: ''
    }

});
