Ext.define('VramApp.view.maptools.BufferTool.BufferViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.bufferviewmodel',

    data:
        {
            informationLayerBased: '<p><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>&nbsp; Select a layer from the list and provide a buffer distance. This will compute a buffer around all features in that layer.</p>',
            informationFeatureBased: '<p><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>&nbsp; Select a layer from the list, click on a feature on the map and provide a buffer distance. This will compute a buffer for a single selected feature.</p>',
            bufferDistanceLayer: 0,
            bufferDistanceFeature: 0,
            featureBufferKeepAttributes:false,
            layerBufferKeepAttributes:false
        }
});

