Ext.define('VramApp.view.maptools.BufferTool.Buffer', {
    extend: 'Ext.window.Window',
    alias: 'widget.buffertool',
    xtype: 'buffertoolwindow',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Display',
        'Ext.form.field.Number',
        'Ext.form.field.Text',
        'Ext.slider.Single',
        'Ext.layout.container.Anchor',
        'VramApp.view.maptools.BufferTool.BufferController',
        'VramApp.view.maptools.BufferTool.BufferViewModel'
    ],

    width: 510,
    maxHeight: 500,
    scrollable: 'y',
    title: 'Buffer',
    autoShow: true,
    closable: true,
    bodyPadding: 10,
    controller: 'buffercontroller',
    viewModel: {
        type: 'bufferviewmodel'
    },

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        items: [
            {
                xtype: 'button',
                text: 'close',
                handler: 'onClose'

            },
            {
                xtype: 'displayfield',
                reference: 'mainMsgField'
            }
        ]
    }],

    items: [

        {
            xtype: 'grid',
            title: 'Available layers',
            border: true,
            reference: 'bufferlayergrid',
            selModel: {
        type: 'checkboxmodel',
        // Disables sorting by header click, though it will be still available via menu
        mode: 'SINGLE',
    },
            store: {
                type: 'importvectorstore'
            },
            columns: [
                {
                    dataIndex: 'layerName',
                    text: 'Name',
                    flex: 1
                },
                {
                    dataIndex: 'description',
                    text: 'Description',
                    flex: 1
                }
            ],
            maxHeight: 200,
            margin: '0 0 5 0'
        },

        {
            xtype: 'tabpanel',
            reference: 'toolTabPanel',
            plain: false,
            defaults: {
                bodyPadding: 10,
                scrollable: true
            },

            items: [
                {
                    xtype: 'form',
                    title: 'Layer',
                    defaults:{
                        anchor: '100%'
                    },
                    items: [
                        {
                            xtype: 'fieldset',
                            title: 'Buffer layer',
                            items: [
                                {
                                    xtype: 'displayfield',
                                    bind: '{informationLayerBased}'
                                },
                                {
                                    xtype: 'slider',
                                    fieldLabel: 'Buffer distance (meters)',
                                    width: 350,
                                    value: 0,
                                    minValue: 0,
                                    maxValue: 100000,
                                    bind: '{bufferDistanceLayer}'
                                },
                                {
                                    xtype: 'numberfield',
                                    minValue: 0,
                                    fieldLabel: 'Buffer distance (meters)',
                                    bind: {
                                        value: '{bufferDistanceLayer}'
                                    }
                                },
                                {
                                    xtype: 'checkbox',
                                    boxLabel: 'Keep attributes in output',
                                    hidden: true,
                                    bind: {
                                        value: '{layerBufferKeepAttributes}'
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: 'Buffer',
                                    handler: 'onBufferLayer'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'form',
                    title: ' Single feature',

                    defaults:{
                        anchor: '100%'
                    },
                    items: [
                        {
                            xtype: 'fieldset',
                            title: 'Buffer single feature',
                            items: [
                                {
                                    xtype: 'displayfield',
                                    bind: '{informationFeatureBased}'
                                },
                                {
                                    xtype: 'slider',
                                    fieldLabel: 'Buffer distance (meters)',
                                    width: 350,
                                    value: 0,
                                    minValue: 0,
                                    maxValue: 100000,
                                    bind: '{bufferDistanceFeature}'
                                },
                                {
                                    xtype: 'numberfield',
                                    fieldLabel: 'Buffer distance (meters)',
                                    bind: '{bufferDistanceFeature}'
                                },
                                {
                                    xtype: 'displayfield',
                                    fieldLabel: 'Feature id'
                                },
                                {
                                    xtype: 'checkbox',
                                    boxLabel: 'Keep attributes in output',
                                    bind: {
                                        value: '{featureBufferKeepAttributes}'
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: 'Buffer',
                                    handler: 'onBufferFeature'
                                }
                            ]
                        }
                    ]
                }

            ]
        }


    ]

});