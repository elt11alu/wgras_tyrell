/**
 * Created by anton on 5/20/16.
 */

Ext.define('VramApp.view.maptools.floodFill.FloodFillViewModel',{
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.floodfillviewmodel',

    data: {
        globalSearchInstructions: 'Draw Bounding box of area of interest and "digitize" watercourse by clicking several point-coordinates.<br>' +
        'Further, provide a water-level increase in meters',

    }
});
