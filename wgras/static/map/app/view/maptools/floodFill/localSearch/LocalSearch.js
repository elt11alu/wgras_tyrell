/**
 * Created by anton on 5/20/16.
 */

Ext.define('VramApp.view.maptools.floodFill.localSearch.LocalSearch', {
    extend: 'Ext.form.Panel',
    requires: [
        'VramApp.view.maptools.floodFill.localSearch.LocalSearchController',
        'VramApp.view.maptools.floodFill.localSearch.LocalSearchViewModel'
    ],
    controller: 'localsearch.localsearchcontroller',
    viewModel: 'localsearchviewmodel',
    xtype: 'localsearchform',
    bodyPadding: 10,
    layout: 'anchor',
    items: [
        {
            xtype: 'displayfield',
            bind: {
                value: '{localSearchInstructions}'
            }
        },
        {
            xtype: 'displayfield',
            reference: 'currentLocalElevationValue',
            fieldLabel: 'Current value',
            anchor: '75%'
        },
        {
            xtype: 'slider',
            fieldLabel: 'Threshold value',
            reference: 'tresholdslider',
            anchor: '100%',
            bind:{
                value: '{localThreshold}',
                maxValue: '{sliderMax}',
                minValue: '{sliderMin}'
            }
        },
        {
            xtype: 'numberfield',
            reference: 'thresholdValue',
            fieldLabel: 'Threshold value',
            anchor: '75%',
            bind:{
                value: '{localThreshold}',
                maxValue: '{sliderMax}',
                minValue: '{sliderMin}'
            }
        },
        {
            xtype: 'button',
            text: 'Extract polygons',
            handler: 'onExtractPolygons'
        }
    ]
});
