/**
 * Created by anton on 9/13/16.
 */

Ext.define('VramApp.view.maptools.floodFill.localSearch.LocalSearchViewModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.localsearchviewmodel',

    data: {
        localSearchInstructions: '<p><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>&nbsp; Calculates the flooded area given a location and a raise in water level from that location.<br>' +
        'Provide a location by clicking inside the white box.<br>' +
        'Provide a threshold value by enter any positive number in the threshold value field.</p>',
        localThreshold: 0,
        sliderMin: 0,
        sliderMax: 1000
    }

});
