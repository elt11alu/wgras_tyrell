/**
 * Created by anton on 5/23/16.
 */

Ext.define('VramApp.view.maptools.floodFill.localSearch.LocalSearchController', {
    extend: 'Ext.app.ViewController',
    requires: [
        'VramApp.view.maptools.createNewLayer.withGeometry.CreateNewLayerWindow'
    ],
    alias: 'controller.localsearch.localsearchcontroller',

    listen: {
        controller: {
            '#floodfillmaincontroller': {
                elevationDataSelected: 'initializeTool',
                elevationDataDeselected: 'clearMap',
                closingFloodFillTool: 'clearMap'
            }
        }
    },

    init: function () {

    },

    initializeTool: function (toolId, record) {
        if (toolId === 'localfloodfill') {
            if (record) {
                this.currentRecord = record;
                this.layerName = record.get('layerName');
                this.layerId = record.get('layerId');
                this.addElevationDataToMap(record);
                //this.activateGetFeatureInfoClick();
            }
        }
    },

    addElevationDataToMap: function (record) {
        var layerName, workspaceName, minValue, maxValue, layerId;
        layerName = record.get('layerName');
        layerId = record.get('layerId');
        workspaceName = record.get('workspaceName');
        minValue = parseInt(record.get('minValue'), 10);
        maxValue = parseInt(record.get('maxValue'), 10);
        var bbox = VramApp.MapUtils.reprojectExtentTo3857([
            parseFloat(record.get('minx')),
            parseFloat(record.get('miny')),
            parseFloat(record.get('maxx')),
            parseFloat(record.get('maxy'))], 'EPSG:4326');
        this.clearMap();
        this.addElevationLayer(workspaceName, layerName, minValue, maxValue, bbox, layerId);
    },

    addElevationLayer: function (workspaceName, layerName, minValue, maxValue, bbox, layerId) {
        var me, rasterlayer, elevationSource, rasterSource, rasterImage;
        me = this;
        rasterlayer = VramApp.LayerLoader.getGeoServerElevationWMS(workspaceName, layerName);

        this.elevationLayer = rasterlayer;

        elevationSource = rasterlayer.getSource();
        rasterSource = new ol.source.Raster({
            sources: [elevationSource],
            operationType: 'image',
            operation: me.growRegion,
            lib: {
                next4Edges: me.next4Edges
            }

        });


        rasterImage = new ol.layer.Image({
            opacity: 0.7,
            source: rasterSource
        });
        this.rasterImage = rasterImage;

        this.lookupReference('currentLocalElevationValue').on('change', function (field) {
            rasterSource.changed();
        }, this);

        this.lookupReference('thresholdValue').on('change', function () {
            if (this.lookupReference('currentLocalElevationValue').getValue() > 0) {
                rasterSource.changed();
            }

        }, this);

        var coordinate = [];
        this.mapListenerKey = VramApp.MapUtils.getOlMap().on('click', function (event) {
            var map, view, layer, source, viewResolution, url, handleFeatureInfoResp;
            map = VramApp.MapUtils.getOlMap();
            coordinate = event.coordinate;
            view = map.getView();
            layer = VramApp.MapUtils.getLayerByDBId(this.layerId);
            source = layer.getSource();
            me.coordinates = ol.proj.toLonLat(coordinate);
            viewResolution = /** @type {number} */ (view.getResolution());
            url = source.getGetFeatureInfoUrl(
                event.coordinate, viewResolution, 'EPSG:3857',
                {'INFO_FORMAT': 'application/json'});

            handleFeatureInfoResp = function (response) {
                var obj = Ext.decode(response.responseText);
                if (obj.features.length > 0) {
                    var properties = obj.features[0].properties;
                    for (var p in properties) {
                        me.lookupReference('currentLocalElevationValue').setValue(properties[p]);
                    }
                }
            }

            VramApp.LayerLoader.getWMSFeatureInfo(url, handleFeatureInfoResp, this);

        }, this);


        rasterSource.on('beforeoperations', function (event) {
            var map, data, threshold;
            map = VramApp.MapUtils.getOlMap();
            data = event.data;
            threshold = this.lookupReference('thresholdValue').getValue();
            data.delta = ((threshold) / ((maxValue - minValue))) * (255);
            if (coordinate) {
                data.pixel = map.getPixelFromCoordinate(coordinate);
            }
        }, this);

        VramApp.MapUtils.getOlMap().getLayers().insertAt(6,rasterImage);
        VramApp.MapUtils.panToExtent(bbox);

    },

    growRegion: function (inputs, data) {
        var image = inputs[0];
        var seed = data.pixel; //clicked pixel
        var delta = parseInt(data.delta); // increase
        if (!seed) {
            return image;
        }

        seed = seed.map(Math.round);
        var width = image.width;
        var height = image.height;
        var inputData = image.data;
        var outputData = new Uint8ClampedArray(inputData);
        var seedIdx = (seed[1] * width + seed[0]) * 4;
        var seedR = inputData[seedIdx];
        var threshold = seedR + delta;

        var edge = [seed];
        while (edge.length) {
            var newedge = [];
            for (var i = 0, ii = edge.length; i < ii; i++) {
                // As noted in the Raster source constructor, this function is provided
                // using the `lib` option. Other functions will NOT be visible unless
                // provided using the `lib` option.
                var next = next4Edges(edge[i]);
                for (var j = 0, jj = next.length; j < jj; j++) {
                    var s = next[j][0], t = next[j][1];
                    if (s >= 0 && s < width && t >= 0 && t < height) {
                        var ci = (t * width + s) * 4;
                        var cr = inputData[ci];
                        var ca = inputData[ci + 3];
                        // if alpha is zero, carry on
                        if (ca === 0) {
                            continue;
                        }
                        if (Math.abs(seedR - cr) < delta) {
                            outputData[ci + 2] = 255;
                            newedge.push([s, t]);
                        }
                        // mark as visited
                        inputData[ci + 3] = 0;
                    }
                }
            }
            edge = newedge;
        }
        return {data: outputData, width: width, height: height};
    },

    next4Edges: function (edge) {
        var x = edge[0], y = edge[1];
        return [
            [x + 1, y],
            [x - 1, y],
            [x, y + 1],
            [x, y - 1]
        ];
    },

    activateGetFeatureInfoClick: function () {
        var map = VramApp.MapUtils.getOlMap();
        var me = this;
        this.mapListenerKey ? map.unByKey(this.mapListenerKey) : false;
        var view = map.getView();
        var layer = VramApp.MapUtils.getLayerByGeoserverName(this.layerName)
        var source = layer.getSource();
        this.mapListenerKey = map.on('singleclick', function (evt) {
            me.coordinates = ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326')
            var viewResolution = /** @type {number} */ (view.getResolution());
            var url = source.getGetFeatureInfoUrl(
                evt.coordinate, viewResolution, 'EPSG:3857',
                {'INFO_FORMAT': 'application/json'});

            VramApp.LayerLoader.getWMSFeatureInfo(url, this.handleWMSFeatureInfoResponse, this);
        }, this);
    },

    handleWMSFeatureInfoResponse: function (response, me) {
        var obj = Ext.decode(response.responseText);
        if (obj.features.length > 0) {
            var properties = obj.features[0].properties;
            for (var p in properties) {
                me.lookupReference('currentLocalElevationValue').setValue(properties[p]);
            }
        }

    },

    clearMap: function () {
        var map = VramApp.MapUtils.getOlMap();
        this.elevationLayer ? map.removeLayer(this.elevationLayer) : false;
        this.rasterImage ? map.removeLayer(this.rasterImage) : false;
        this.mapListenerKey ? map.unByKey(this.mapListenerKey) : false; // For featureinfo listener
    },

    onExtractPolygons: function () {
        var me, layerId, threshold, loadMask, coordinates, requestObj, success, fail;
        me = this;
        layerId = this.layerId;
        threshold = this.lookupReference('thresholdValue').getValue()
        coordinates = this.coordinates;
        var panel = this.getView();
        loadMask = new Ext.LoadMask({
            msg: 'Calculating...',
            target: panel
        });
        if (coordinates && coordinates.length > 0) {
            if (parseInt(threshold) > 0 && layerId) {
                loadMask.show();
                requestObj = {
                    layerId: layerId,
                    increase: threshold,
                    lon: coordinates[0],
                    lat: coordinates[1]
                };

                success = function (resp) {
                    var obj = Ext.decode(resp.responseText)
                    me.addResultToMap(obj.features)
                    loadMask.hide();
                };

                fail = function (resp) {
                    var obj = Ext.decode(resp.responseText);
                    var error = obj.errorMessage;
                    loadMask.hide();
                    me.fireEvent('elevationError', error)
                };
                VramApp.LayerLoader.getContourLayer(JSON.stringify(requestObj), success, fail)
            } else {
                this.fireEvent('elevationError', 'Please provide a threshold value')
            }
        } else {
            this.fireEvent('elevationError', 'Please click on the map')
        }


    },

    addResultToMap: function (featureCollection) {
        var format, bufferedFeatures;
        format = new ol.format.GeoJSON();
        bufferedFeatures = format.readFeatures(featureCollection, {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
        this.promptSave(bufferedFeatures)
    },

    promptSave: function (features) {
        var nbrOfFeatures = features.length;
        Ext.MessageBox.show({
            title: 'Save layer?',
            msg: 'Buffer returned '.concat(nbrOfFeatures, ' features.', ' Do you wish to save to a new layer? If no, the layer will still be added to the map as a temporary layer.'),
            buttons: Ext.MessageBox.YESNO,
            scope: this,
            fn: function (btn) {
                this.afterPrompt(btn, features)
            },
            icon: Ext.MessageBox.QUESTION
        });
    },

    afterPrompt: function (btn, features) {
        if (btn === 'yes') {
            this.showCreateLayerWindow(features)
        } else {
            this.addAsTempLayer(features)
        }
    },
    addAsTempLayer: function (features) {
        var layer = new ol.layer.Vector({
            source: new ol.source.Vector({features: features})

        });
        var required = {
            layer: layer,
            layerTitle: 'temp_flood_lyr',
            layerName: 'temp_flood_lyr'
        };
        var options = {
            visible: true,
            folder: 'Temp flooding'
        };
        VramApp.MapUtils.addWFSOverlay(required, options);
    },

    showCreateLayerWindow: function (features) {
        var ancestorLayerId = this.currentRecord.get('layerId');
        Ext.create({
            xtype: 'createnewlayerwithgeometrywindow',
            features: features,
            layerId: ancestorLayerId
        }).show();
    }
});
