/**
 * Created by anton on 5/20/16.
 */

Ext.define('VramApp.view.maptools.floodFill.FloodFillToolMain', {
    extend: 'Ext.window.Window',

    xtype: 'floodfilltoolmain',

    viewModel: {
        type: 'floodfillviewmodel'
    },

    controller: 'floodfillmaincontroller',

    requires: [
        'Ext.button.Button',
        'Ext.form.Panel',
        'Ext.form.field.Display',
        'Ext.form.field.Number',
        'Ext.grid.Panel',
        'Ext.slider.Single',
        'VramApp.view.maptools.floodFill.FloodFillViewModel',
        'VramApp.view.maptools.floodFill.globalSearch.GlobalSearch',
        'VramApp.view.maptools.floodFill.localSearch.LocalSearch',
        'VramApp.view.maptools.floodFill.elevationDataGrid.ElevationDataGrid',
        'VramApp.view.maptools.floodFill.FloodFillMainController'
        //'VramApp.view.maptools.elevationtool.ElevationToolController',
        //'VramApp.view.maptools.elevationtool.ElevationToolModel'
    ],

    /*Specific configs*/
    width: 500,
    title: 'Flooding tool',
    autoShow: true,
    closable: false,
    bodyPadding: 10,

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        items: [
            {
                xtype: 'button',
                text: 'close',
                handler: 'onClose'

            },
            {
            xtype: 'displayfield',
            reference: 'mainMsgField'
        }
        ]
    }],

    items: [

        {
            xtype: 'elevationdatagrid',
            height: 100
        },

        {
            xtype: 'tabpanel',
            reference: 'toolTabPanel',
            plain: false,
            defaults: {
                bodyPadding: 10,
                scrollable: true
            },

            items: [

                {
                    title: 'Local search',
                    toolId: 'localfloodfill',
                    items: [

                        {
                            xtype: 'localsearchform'
                        }
                    ]
                }

                /*{
                    title: 'Watercourse search',
                    toolId: 'globalfloodfill',
                     height: 450,
                    items: [
                        {
                            xtype: 'displayfield',
                            bind: {
                                value: '{globalSearchInstructions}'
                            }
                        },
                        {
                            xtype: 'globalsearchform'
                        }
                    ]
                }*/
            ]
        }


    ]
});
