/**
 * Created by anton on 5/20/16.
 */

Ext.define('VramApp.view.maptools.floodFill.elevationDataGrid.ElevationDataGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'elevationdatagrid',

    reference: 'elevationdatagrid',
    scrollable: true,

    store: {
        type: 'elevationDataStore'
    },
    selModel: {
        selType: 'checkboxmodel',
        checkOnly: false,
        mode: 'SINGLE'
    },
    columns: [
        {
            text: 'Elevation data',
            dataIndex: 'layerTitle',
            flex: 1
        }
    ]

});
