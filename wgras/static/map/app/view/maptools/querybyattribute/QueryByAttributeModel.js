/**
 * Created by antonlu66 on 11/16/2015.
 */
Ext.define('VramApp.view.maptools.querybyattribute.QueryByAttributeModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.querybyattribute',

    stores: {

        

        vectorLayerStore:{
            autoLoad: true,
            fields: [
                {name: 'layerName'},
                {name: 'layerId'}
            ],

            proxy: {
                type: 'ajax',
                headers: {
                    'Accept': 'application/json'
                },
                url: '/metadata/wfs',

                reader: {
                    type: 'json',
                    rootProperty: 'layers'
                }

            }
        },
        attributeStore:{
            autoLoad: false,
            fields: [
                {name: 'name'},
                {name: 'attributeType'}
            ],

            proxy: {
                type: 'ajax',
                headers: {
                    'Accept': 'application/json'
                },
                //url: '/resource/attributes/<string:workspaceName>/<string:layerName>',

                reader: {
                    type: 'json',
                    rootProperty: 'attributes'
                }

            }
        }


    },

    data: {
        operatorValue: '',
        layerAttribute: '',
        attributeValue: '',
        queryExpression: ''
    }
});