/**
 * Created by antonlu66 on 11/16/2015.
 */
Ext.define('VramApp.view.maptools.querybyattribute.QueryByAttribute', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.button.Button',
        'Ext.button.Segmented',
        'Ext.data.Store',
        'Ext.form.Panel',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Display',
        'Ext.form.field.Text',
        'Ext.layout.container.VBox',
        'VramApp.view.maptools.querybyattribute.QueryByAttributeController',
        'VramApp.view.maptools.querybyattribute.QueryByAttributeModel',
    ],

    xtype: 'querybyattribute',


    viewModel: {
        type: 'querybyattribute'
    },

    controller: 'querybyattribute',

    title: 'Query by attribute',
    autoShow: true,
    bodyPadding: 5,
    width: 300,
    closable: false,

    icon: VramApp.WGRASAppGlobals.resources() + 'resources/mapIcons/query.svg',

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        items: [
            {
                xtype: 'combo',
                id: 'lkshdfjhsdfjkhkjhsdjhhsdjhsdfcombo',
                reference: 'layerSelector',
                fieldLabel: 'Choose data',
                displayField: 'layerName',
                queryMode: 'local',
                bind: {
                    store: '{vectorLayerStore}',
                    //value: '{writableworkspacesstore.name}',
                    selection: '{vectorLayerStore.layerId}'

                }


            }]
    }],

    items: [
        {
            xtype: 'grid',
            title: 'Attributes',
            reference: 'attributeGrid',
            border: true,
            scrollable: true,
            height: 200,
            loadMask: true,
            bind: {
                store: '{attributeStore}'
            },

            selModel: {
                selType: 'checkboxmodel',
                checkOnly: false,
                mode: 'SINGLE'
            },

            columns: [
                {
                    text: 'Name',
                    dataIndex: 'name',
                    flex: 1
                },
                {
                    text: 'Type',
                    dataIndex: 'attributeType',
                    flex: 1
                }
            ]

        },
        {
            xtype: 'form',
            defaultType: 'fieldcontainer',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [{
                xtype: 'segmentedbutton',
                padding: 5,
                items: [{
                    text: '=',
                    scale: 'small',
                    operatorExpr: '=',
                    handler: 'onOperatorSelection'
                }, {
                    text: "<",
                    scale: 'small',
                    operatorExpr: '<',
                    handler: 'onOperatorSelection'
                }, {
                    text: ">",
                    scale: 'small',
                    operatorExpr: '>',
                    handler: 'onOperatorSelection'
                }]

            },
                {
                    xtype: 'textfield',
                    reference: 'valueField',
                    padding: 5,
                    fieldLabel: 'Attribute value',
                    enableKeyEvents: true
                },

                {
                    xtype: 'textfield',
                    reference: 'expressionField',
                    padding: 5,
                    fieldLabel: 'Expression',
                    readOnly: true
                },
                {
                    xtype: 'displayfield',
                    reference: 'queryMessage'
                }


            ]
        }],
    buttons: [
        {
            text: 'close',
            handler: 'onClose'
        },
        {
            reference: 'submitButton',
            text: 'Query',
            handler: 'onSubmit'
        }
    ]
})
;