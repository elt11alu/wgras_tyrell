Ext.define('VramApp.view.maptools.printTool.PrintController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.printcontroller',

    init:function(){

    },

    onCreatePdf: function(btn) {
        var pdf, values, orientation;
        values = this.getFormValues();

        pdf = this.getPdfDoc(this.getPdfOrientation(values));
        pdf.setFontSize(20);
        pdf.setTextColor(100);
        pdf.text(20, 20, this.getPdfTitle(values));
        pdf.setFontSize(10);
        pdf.setTextColor(150);
        pdf.text(20,25,'-'+this.getPdfSubtitle(values));
        pdf.addImage(this.getCanvasData(), 'JPEG', 0, 30, 297, 0);
        this.savePdf(pdf,this.getPdfFileName(values));
    },

    savePdf: function(pdf,fileName) {
        var me = this;
        var loader = VramApp.utils.Loader.getLoader(me.getView(), 'Generating pdf..');
        loader.show();
        try{
           pdf.save(fileName);
        }catch(err){
            loader.hide();
        }
        window.setTimeout(function(){
            loader.hide();
        },5000);

    },

    getPdfDoc: function(orientation) {
        return new jsPDF(orientation, undefined, 'a4');
    },

    getCanvasData: function() {
        var canvas;
        canvas = document.getElementsByTagName('canvas')[0]
        return canvas.toDataURL('image/png');
    },

    getPdfOrientation: function(formValues) {
        return formValues.printMapOrientation || 'landscape';
    },

    getPdfTitle:function(values){
        return values.mapTitle || 'WGRAS';
    },

    getPdfSubtitle:function(values){
        return values.mapSubTitle || 'Web GIS for Risk Assessment'
    },

    getPdfFileName:function(values){
        return values.fileName || 'MapPdf';
    },

    getFormValues: function() {
        return this.lookupReference('printmapform').getValues();

    },

    onCancel: function() {
        this.fireEvent('toggleHeadPanelButtons', false);
        this.getView().close()
    }
});
