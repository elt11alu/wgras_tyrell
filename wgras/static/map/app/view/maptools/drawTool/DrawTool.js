Ext.define('VramApp.view.maptools.drawTool.DrawTool', {
    extend: 'Ext.window.Window',
    xtype: 'drawtoolwindow',
    collapsible: true,
    requires: [
        'Ext.button.Button',
        'Ext.button.Segmented',
        'Ext.form.Panel',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Display',
        'Ext.form.field.Text',
        'VramApp.view.maptools.drawTool.DrawToolController',
        'VramApp.store.PrivateLayerStore'
    ],
    alias: 'widget.drawtool',
    glyph: 0xf040,
    controller: 'drawtoolcontroller',
    bodyPadding: 10,
    title: 'Draw',
    closable: true,
    autoShow: true,

    scrollable: true,

    items: {
        xtype: 'form',
        reference: 'form',
        padding: 10,

        defaults: {
            boxLabelAlign: 'before',
            anchor: '90%'
        },
        items: [{
            items: [{
                xtype: 'segmentedbutton',
                height: 50,
                width: 300,
                listeners: {
                    toggle: 'onGeomTypeToggle'
                },
                items: [{
                    text: 'Point',
                    pressed: true,
                    isPoint: true,
                    reference: 'pointBtn'

                }, {
                    text: 'Line',
                    isLine: true,
                    reference: 'lineBtn'

                }, {
                    text: 'Polygon',
                    isPolygon: true,
                    reference: 'polygonBtn'
                }]
            }]
        },
            {
                xtype: 'displayfield',
                maxWidth: 300,
                value: "<p><b>Click on button above to select geometry. To save drawing check checkbox below and and choose a name for your layer.</b></p>"
                + "<p><i>For line and polygon: double click on mouse button to see result. Pressing shift key allows freehand drawing.</i></p>"
            }, {
                xtype: 'checkbox',
                name: 'layerbox',
                boxLabel: 'Save',
                checked: false,
                handler: function (chBox, checked) {
                    var textfield = chBox.up('form').down('combo');
                    var saveBtn = chBox.up('form').down('button[id=saveDrawnLayerBtn]');
                    textfield.setDisabled(!checked);
                    saveBtn.setDisabled(!checked);
                }
            }, {
                xtype: 'combo',
                disabled:true,
                fieldLabel: 'Layer',
                store: {
                    type: 'privatelayerstore'
                },
                queryMode: 'local',
                displayField: 'layerName',
                valueField: 'layerId'

            }, {
                xtype: 'displayfield',
                hidden: true,
                reference: 'errorMessage'
            }],
        buttons: [
            {
                xtype: 'button',
                text: 'Save layer',
                id: 'saveDrawnLayerBtn',
                disabled: true,
                handler: 'closeandsave'
            }, {
                text: 'Cancel',
                handler: function (button) {
                    button.up('form').up('window').destroy();
                }

            }]

    }
});