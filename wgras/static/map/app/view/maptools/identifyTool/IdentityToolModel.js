/**
 * Created by anton on 2015-09-15.
 */

Ext.define('VramApp.view.maptools.identityTool.IdentityToolModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.identitytoolmodel',

    stores: {

        attributestore: {
            autoLoad: false,
            fields: [
                {name: 'name'},
                {name: 'value'},
                {name: 'dataType'}
            ],

            proxy: {
                type: 'ajax',
                headers: {
                    'Accept': 'application/json'
                },
                url: '',

                reader: {
                    type: 'json',
                    rootProperty: 'attributes'
                }

            }
        }
    },

    data: {
        attributeList: "<p><b>Make sure the map layer is activated by checking checkbox in <i>active</i> column in map data panel. "
        + "Then click on feature to see its attributes.</b></p>"
        + "<p><i>Note: this tool only works for data of type vector</i></p>",

        instructions: "<b>Click on feature</b>"
    }
});
