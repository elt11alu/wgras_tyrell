Ext.define('VramApp.view.maptools.measureTool.MeasureBySelection', {

    requires: [
        'VramApp.MapUtils',
        'VramApp.GeometryDataTypes'
    ],
    id: 'measureBySelectionTool',
    startMeasureBySelect: function () {
        var map = VramApp.MapUtils.getOlMap();
        var select = VramApp.MapUtils.getSelectInteraction();
        select.setActive(true);
        var me = this;
        var div = this.createMainDiv();
        var arrow = this.createArrow();
        this.overlay = this.getOverlay(div);
        map.addOverlay(this.overlay);
        this.listenerKey = select.on("select", function (event) {
            var overlayelement = me.overlay.getElement();
            var result = " ";

            if (event.selected.length > 0) {
                var feature = event.selected[0];
                var geometry = feature.getGeometry();
                var coordinates = event.mapBrowserEvent.coordinate;
                var area = 0;
                var length = 0;

                //'Point', 'LineString', 'LinearRing', 'Polygon', 'MultiPoint', 'MultiLineString', 'MultiPolygon', 'GeometryCollection', 'Circle'
                switch (geometry.getType()) {
                    case VramApp.GeometryDataTypes.geomTypes.point:

                        result = "<p style='margin:0'>No area or length for type point</p>";
                        break;
                    case VramApp.GeometryDataTypes.geomTypes.multipoint:

                        result = "<p style='margin:0'>No area or length for type multipoint</p>";
                        break;
                    case VramApp.GeometryDataTypes.geomTypes.line:

                        length = me.formatLength(geometry.getLength());
                        result += "<p style='margin:0'>Length: " + length + "</p>";
                        break;
                    case VramApp.GeometryDataTypes.geomTypes.multiline:

                        length = me.formatLength(me.getMultiLineLength(geometry));
                        result = "<p style='margin:0'>Length: " + length + "</p>";
                        break;
                    case VramApp.GeometryDataTypes.geomTypes.polygon:

                        area = me.formatArea(geometry.getArea());
                        result = "<p style='margin:0'>Area: " + area + "</p>";
                        break;
                    case VramApp.GeometryDataTypes.geomTypes.multipolygon:

                        area = me.formatArea(geometry.getArea());
                        result = "<p style='margin:0'>Area: " + area + "</p>";
                        break;
                    case VramApp.GeometryDataTypes.geomTypes.geometrycollection:
                         area = me.formatArea(me.getgeomCollectionArea(geometry));
                        result = "<p style='margin:0'>Area: " + area + "</p>";
                        break;
                    case VramApp.GeometryDataTypes.geomTypes.circle:

                        area = me.formatArea(me.getCircleArea(geometry.getRadius()));
                        result = "<p style='margin:0'>Area: " + area + "</p>";
                        break;
                    case VramApp.GeometryDataTypes.geomTypes.linearring:

                        area = me.formatArea(geometry.getArea());
                        result = "<p style='margin:0'>Area: " + area + "</p>";
                        break;

                }

                overlayelement.innerHTML = result;
                overlayelement.appendChild(arrow);
                me.overlay.setElement(overlayelement);
                me.overlay.setPosition(coordinates)
            } else {

                me.overlay.setPosition(undefined);

            }
        });


    },

    stopMeasureBySelect: function () {
        var select = VramApp.MapUtils.getSelectInteraction();
        var map = VramApp.MapUtils.getOlMap();
        if (this.listenerKey) {
            ol.Observable.unByKey(this.listenerKey);
            map.removeOverlay(this.overlay);
            VramApp.MapUtils.getSelectInteraction().setActive(false);
        }
    },


    createMainDiv: function () {
        var div = document.createElement('div');
        div.style.color = "rgb(255,255,255)";
        div.style.background = "rgb(0,0,0)";
        div.style.padding = "5px 4px";
        div.style.opacity = "0.8";
        div.style.border = "2px solid";
        div.style.borderColor = "#FFCC00";
        div.style.borderRadius = "4px";

        return div;
    },

    createArrow: function () {

        var arrow = document.createElement('div');
        arrow.style.width = 0;
        arrow.style.height = 0;
        arrow.style.borderTop = "10px solid transparent";
        arrow.style.borderBottom = "10px solid transparent";
        arrow.style.borderRight = "10px solid";
        arrow.style.borderRightColor = "rgb(0,0,0)";

        arrow.style.position = "absolute";
        arrow.style.marginLeft = "-12px";
        arrow.style.marginTop = "-9px";
        arrow.style.top = "50%";

        return arrow;
    },

    getOverlay: function (div) {
        return new ol.Overlay({

            element: div,
            positioning: 'center-left',
            offset: [20, 0]
        });

    },
    
    getgeomCollectionArea:function(geometry){
        var geomArray = geometry.getGeometries();
        var area = 0;
        geomArray.forEach(function(item,index,array){
         
           if(item instanceof ol.geom.Polygon){
               area +=item.getArea();
           } 
        });
        return area;
    },
    
    getgeomCollectionLength:function(geometry){
         var geomArray = geometry.getGeometries();
        var length = 0;
        geomArray.forEach(function(item,index,array){
           if(item instanceof ol.geom.LineString){
               length +=item.getLength();
           } 
        });
        return length;
    },

    getCircleArea: function (radius) {
        return Math.PI * (Math.pow(radius, 2));

    },

    getMultiLineLength:function(multilinestring){
        var lineStrings = multilinestring.getLineStrings();
        var length = 0;
        lineStrings.forEach(function(item,index,array){
            length+=item.getLength();
        });
        return length;
    },

    formatLength: function (olLength) {

        var length;

        length = Math.round(olLength * 100) / 100;

        var output;
        if (length > 100) {
            output = (Math.round(length / 1000 * 100) / 100) +
                ' ' + 'km';
        } else {
            output = (Math.round(length * 100) / 100) +
                ' ' + 'm';
        }
        return output;
    },

    formatArea: function (area) {

        var output;
        if (area > 10000) {
            output = (Math.round(area / 1000000 * 100) / 100) +
                ' ' + 'km<sup>2</sup>';
        } else {
            output = (Math.round(area * 100) / 100) +
                ' ' + 'm<sup>2</sup>';
        }
        return output;
    }


});
