/**
 * Created by antonlu66 on 11/11/2015.
 */
Ext.define('VramApp.view.maptools.layerinfotool.LayerInfoToolModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.layerinfotool',

    stores: {
        /*
        A declaration of Ext.data.Store configurations that are first processed as binds to produce an effective
        store configuration. For example:

        users: {
            model: 'LayerInfoTool',
            autoLoad: true
        }
        */
    },

    data: {
        /* This object holds the arbitrary data that populates the ViewModel and is then available for binding. */
    }
});