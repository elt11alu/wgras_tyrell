/**
 * Created by antonlu66 on 11/11/2015.
 */
Ext.define('VramApp.view.maptools.layerinfotool.LayerInfoTool', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.form.field.Display',
        'Ext.grid.plugin.RowExpander',
        'VramApp.view.maptools.layerinfotool.LayerInfoToolController',
        'VramApp.view.maptools.layerinfotool.LayerInfoToolModel'
    ],


    xtype: 'layerinfotool',
    autoShow:true,
    title: 'Map information',
    bodyPadding: 5,
    glyph: 0xf129,
    width: 300,
    maxHeight:400,
    y:200,
    scrollable: true,


    viewModel: {
        type: 'layerinfotool'
    },

    controller: 'layerinfotool',

    items: [{
        xtype:'displayfield',
        reference:'layerInfoField'
    }]

});