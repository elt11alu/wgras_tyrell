/**
 * Created by antonlu66 on 11/11/2015.
 */
Ext.define('VramApp.view.maptools.layerinfotool.LayerInfoToolController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.layerinfotool',

    /**
     * Called when the view is created
     */
    init: function() {

        if (!this.getView().errorMessage) {
            this.setLayerInfo();
        }

    },

    setLayerInfo: function() {
        var view, layerInfo, layerTitle, dataType, category, userDefName, layerName, layer, metaDataStore;
        try {
            metaDataStore = Ext.StoreManager.lookup('metadatastore');
            view = this.getView();
            layerName = view.layerName;
            layerIdentifier = view.layerIdentifier;
            record = metaDataStore.findRecord('layerIdentifier', layerIdentifier);
            this.setLayerInfoFieldValue(record.get('layerInfo'), record.get('layerTitle'), record.get('dataType'), record.get('category'), layerName)
        } catch (err) {
            console.warn(err);
        }

    },

    setLayerInfoFieldValue: function(layerInfo, layerTitle, dataType, category, userDefName) {
        var valueString =
            '<b>Name: </b>' + layerTitle + '<br>' + '<br>' + '<b>User defined name: </b>' + userDefName + '<br>' + '<br>' + '<b>Category: </b>' + category + '<br>' + '<br>' + '<b>Abstract: </b>' + layerInfo + '<br>' + '<br>' + '<b>Type: </b>' + dataType;
        this.lookupReference('layerInfoField').setValue(valueString);
    }
});
