/**
 * Created by anton on 2015-11-25.
 */
Ext.define('VramApp.view.maptools.nearestobjecttool.NearestObjectToolController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.nearestobjecttool',

    requires: [
        'Ext.data.StoreManager'
    ],

    /**
     * Called when the view is created
     */
    init: function () {
        this.loadStore();
        this.setTempLayer();
    },

    inputLayer: null,
    tempLayer: null,

    loadStore: function(){
        var store = this.lookupReference('inputLayerSelector').getStore();
        var layers = VramApp.MapUtils.getOlMap().getLayers().getArray();
        $.each(layers, function(index, layer){
            try {
                if (layer instanceof ol.layer.Vector && typeof layer.get('layerIdentifier') != 'undefined') {
                    store.add({title: layer.get('title'), layerIdentifier: layer.get('layerIdentifier')});

                }
            }catch (err){
                console.log(err)
            }
        });
    },

    onInputLayerSelection: function (combo, record) {
        var layerTitle = record.get('title');
        var layerIdentifier = record.get('layerIdentifier');
        this.setInputLayerVisibility(false);
        this.setInputLayer(layerIdentifier);
        this.setInputLayerVisibility(true);
        this.activateMapListener();

    },

    setInputLayer: function (layerIdentifier) {
        this.inputLayer = null;
        this.inputLayer = VramApp.MapUtils.getLayerByLayerIdentifier(layerIdentifier);
    },

    setTempLayer: function () {
        this.tempLayer = null;
        this.tempLayer = new ol.layer.Vector({
            source: new ol.source.Vector({
                features: []
            }),
            style: new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.8)'
                }),
                stroke: new ol.style.Stroke({
                    color: '#000000',
                    lineDash: [10, 10],
                    width: 2
                }),
                image: new ol.style.Circle({
                    radius: 5,
                    stroke: new ol.style.Stroke({
                        color: 'rgba(0, 0, 0, 0.7)'
                    }),
                    fill: new ol.style.Fill({
                        color: 'rgba(255, 255, 255, 0.2)'
                    })
                })
            })
        });
        VramApp.MapUtils.addLayerToMap(this.tempLayer);
    },

    setInputLayerVisibility: function (visible) {
        this.inputLayer ? this.inputLayer.setVisible(visible) : false;
    },

    activateMapListener: function () {
        var coordinate, source, nearestFeature, geomType, result, lineTo, clickedPoint, distance, extent;
        this.tempLayer.getSource().clear();
        this.listenerKey ? ol.Observable.unByKey(this.listenerKey) : false;
        this.listenerKey = VramApp.MapUtils.getOlMap().on('singleclick', function (event) {
                try {
                    this.tempLayer.getSource().clear();
                    coordinate = event.coordinate;
                    source = this.inputLayer.getSource();
                    nearestFeature = source.getClosestFeatureToCoordinate(coordinate);
                    geomType = nearestFeature.getGeometry().getType();
                    result = nearestFeature.getGeometry().getClosestPoint(coordinate);

                    if (result) {
                        lineTo = new ol.Feature({
                            geometry: new ol.geom.LineString([coordinate, result])
                        });
                        clickedPoint = new ol.Feature({
                            geometry: new ol.geom.Point(coordinate)
                        });
                        distance = lineTo.getGeometry().getLength();
                        this.setDistance(distance);
                        this.setFeatureInformation(nearestFeature);
                        clickedPoint.setStyle(new ol.style.Style({text: new ol.style.Text({text: 'Start', scale: 2, fill:new ol.style.Fill({color:'#000000'})})}));
                        this.tempLayer.getSource().addFeatures([lineTo, clickedPoint]);

                        extent = this.tempLayer.getSource().getExtent();
                        VramApp.MapUtils.panToExtent(extent);
                    } else {
                        this.setErrorMessage('No feature found')
                    }
                } catch (err) {
                    console.log(err)
                }
            },
            this
        );
    },

    setDistance: function (distance) {
        this.lookupReference('distanceField').setValue(Math.round(distance) + ' meters');
    },

    setErrorMessage: function (msg) {
        this.lookupReference('distanceField').setValue('<span style="color:red">' + msg + '</span>');
    },

    setFeatureInformation: function (feature) {
        var me = this;
        try {
            var properties = feature.getProperties();
            if (properties) {

                var list = '<table class="identityTable" style="width:100%"><tr><th>Attr</th><th>Value</th></tr>'
                for (var p in properties) {
                    if (p != 'geometry' && p != 'the_geom') {
                        list += '<tr>';
                        list += '<td>' + p + '</td>';
                        list += '<td>' + properties[p] + '</td>';
                        list += '</tr>';
                    }
                }
                list += '</table>';

                me.getViewModel().setData({
                    attributeList: list
                });
            }
        } catch (err) {
            console.log(err)
        }
    },

    onClose: function () {
        this.tempLayer ? VramApp.MapUtils.getOlMap().removeLayer(this.tempLayer) : false;
        this.fireEvent('toggleHeadPanelButtons', false);
        this.inputLayer = null;
        this.listenerKey ? ol.Observable.unByKey(this.listenerKey) : false;
        this.getView().close();
    }
})
;