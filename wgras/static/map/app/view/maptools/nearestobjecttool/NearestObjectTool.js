/**
 * Created by anton on 2015-11-25.
 */
Ext.define('VramApp.view.maptools.nearestobjecttool.NearestObjectTool', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.form.field.ComboBox',
        'Ext.form.field.Display',
        'Ext.layout.container.VBox',
        'VramApp.view.maptools.nearestobjecttool.NearestObjectToolController',
        'VramApp.view.maptools.nearestobjecttool.NearestObjectToolModel'
    ],


    xtype: 'nearestobjecttool',


    viewModel: {
        type: 'nearestobjecttool'
    },

    controller: 'nearestobjecttool',

    autoShow: true,
    layout: 'vbox',
    closable: false,
    bodyPadding: 5,
    title: 'Find nearest object',
    maxWidth: 300,
    maxHeight: 400,
    scrollable: true,
    collapsible: true,

    icon: VramApp.WGRASAppGlobals.resources() + 'resources/mapIcons/nearestObject.svg',

    items: [
        {
            xtype: 'combo',
            reference: 'inputLayerSelector',
            fieldLabel: 'Input data',
            displayField: 'title',
            queryMode: 'local',
            store: Ext.create('Ext.data.Store', {
                fields: ['title', 'layerIdentifier'],
                data: []
            }),

            listeners: {
                'select': 'onInputLayerSelection'
            }

        },
        {
            xtype: 'displayfield',
            reference: 'distanceField',
            fieldLabel: 'Distance',
            value: 0
        },
        {
            xtype: 'displayfield',
            reference: 'foundFeatureField',
            bind: '{attributeList}'
        }
    ],

    buttons: [
        {
            text: 'Close',
            handler: 'onClose'
        }
    ]
});