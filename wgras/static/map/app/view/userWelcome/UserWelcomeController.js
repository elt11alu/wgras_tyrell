/**
 * Created by anton on 6/13/16.
 */

Ext.define('VramApp.view.userWelcome.UserWelcomeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.userwelcomecontroller',

    requires: [
        'VramApp.HTTPRequests'
    ],

    init: function () {
        this.getCurrentUser();

    },

    getCurrentUser: function () {
        var success, fail, obj, msg, me, user;
        me = this;
        success = function (resp) {
            user = Ext.decode(resp.responseText);
            me.getViewModel().set('user', user.username)
        };

        fail = function (resp) {
            msg = 'Sorry your layers could not be loaded, try again later.'
            me.setErrorMsg(msg)
        };

        VramApp.HTTPRequests.GET('/user/current', success, fail)
    },

        onSearch: function() {
        var grid = this.lookupReference('rastergrid'),
            // Access the field using its "reference" property name.
            filterField = this.lookupReference('nameFilterField'),
            filters = grid.store.getFilters();

        if (filterField.value) {
            this.nameFilter = filters.add({
                id            : 'nameFilter',
                property      : 'category',
                value         : filterField.value,
                anyMatch      : true,
                caseSensitive : false
            });
        } else if (this.nameFilter) {
            filters.remove(this.nameFilter);
            this.nameFilter = null;
        }
    },


    onClose: function () {
        this.getView().close()
    },

    setSuccessMsg: function (msg) {
        this.lookupReference('mainMsgField').setValue('<p style="color:green">' + msg + '</p>')
    },

    setErrorMsg: function (msg) {
        this.lookupReference('mainMsgField').setValue('<p style="color:red">' + msg + '</p>')
    }
});
