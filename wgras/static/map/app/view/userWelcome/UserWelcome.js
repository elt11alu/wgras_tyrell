/**
 * Created by anton on 6/13/16.
 */

Ext.define('VramApp.view.userWelcome.UserWelcome', {
    extend: 'Ext.window.Window',
    requires: [
        'VramApp.view.userWelcome.UserWelcomeController',
        'VramApp.view.userWelcome.UserWelcomeViewModel'
    ],
    controller: 'userwelcomecontroller',
    viewModel: 'userwelcomeviewmodel',
    xtype: 'userwelcomewindow',
    width: 500,
    height: 500,
    layout: 'anchor',
    titleAlign: 'center',
    bodyPadding: 10,

    bind: {
        title: 'Welcome {user}',
    },


    dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        items: [
            {
                xtype: 'button',
                text: 'close',
                handler: 'onClose'

            },
            {
                xtype: 'displayfield',
                reference: 'mainMsgField'
            }
        ]
    }],

    items: [
        {
            xtype: 'displayfield',
            reference: 'nbroflayerstext',
            anchor:'100%',
            bind: '{informationText}'
        },
        {

            xtype: 'textfield',
            reference: 'nameFilterField',
            fieldLabel: 'Search category',
            anchor: '100%',
            margin: 2,
            enableKeyEvents: true,
            listeners: {
                keyup: 'onSearch',
                buffer: 50
            }

        },
        {
            xtype: 'tabpanel',
            defaults: {
                bodyPadding: 10

            },

            items: [
                {
                    xtype: 'grid',
                    title: 'Raster data',
                    reference: 'rastergrid',
                    expanded: false,
                    height: 200,
                    columns: [
                        {
                            dataIndex: 'layerName',
                            text: 'Name',
                            flex: 1
                        },
                        {
                            dataIndex: 'category',
                            text: 'Category',
                            flex: 1
                        },
                        {
                            dataIndex: 'description',
                            text: 'Description',
                            flex: 1
                        }
                    ],
                    bind: {
                        store: '{raster}'
                    }
                },
                {
                    xtype: 'grid',
                    title: 'Vector data',
                    reference: 'vectorgrid',
                    expanded: false,
                    height: 200,
                    columns: [
                        {
                            dataIndex: 'layerName',
                            text: 'Name',
                            flex: 1
                        },
                        {
                            dataIndex: 'category',
                            text: 'Category',
                            flex: 1
                        },
                        {
                            dataIndex: 'description',
                            text: 'Description',
                            flex: 1
                        }
                    ],
                    bind: {
                        store: '{vector}'
                    }
                }
            ]
        }

    ]
});
