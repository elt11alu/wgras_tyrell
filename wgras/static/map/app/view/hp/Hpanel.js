Ext.define('VramApp.view.hp.Hpanel', {

    extend: 'Ext.panel.Panel',
    xtype: 'Hpanel',
    id: 'hpanel',
    region: 'north',
    controller: 'toolsController',
    collapsible: true,
    requires: [
        'Ext.button.Button',
        'Ext.button.Split',
        'Ext.menu.Menu',
        'Ext.toolbar.Fill',
        'Ext.toolbar.Spacer',
        'VramApp.WGRASAppGlobals',
        'VramApp.view.hp.HeadPanelController',
        'VramApp.view.maptools.geoLocationTool.GeoLocationTool'
    ],
    margin: '0 0 2 0',

    title: VramApp.WGRASAppGlobals.appTitle(),
    header: {
        iconCls: "fa fa-globe fa-lg",

        html: '<div align="right" style="margin-right: 50px;">' +
        '<img src= "static/map/resources/logos/mainIcon.png" alt="Collaborators" height="40" width="252" align="middle" style="margin:8px 20px 8px 20px; background:white;" title="Collaborators"></div>',
        height: 70

    },

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        reference: 'headpaneltoolbar',
        defaults: {
            scale: 'small'
        },
        items: [{
            text: 'Menu',
            xtype: 'splitbutton',
            glyph: 0xf0c9,
            menu: new Ext.menu.Menu({
                items: [

                    {
                        text: 'Print map',
                        glyph: 0xf02f,
                        handler: 'onPrintMap'
                    },
                    {
                        text: 'Logout',
                        glyph: 0xf08b,
                        handler: 'onLogoutClick'

                    }, {
                        text: 'About',
                        glyph: 0xf129,
                        handler: 'onAboutClick'
                    }


                ]
            })

        }, ' ', {

            text: 'Data',
            glyph: 0xf24d,
            xtype: 'splitbutton',
            menu: new Ext.menu.Menu({
                items: [
                    {
                        text: 'New layer',
                        glyph: 0xf278,
                        handler: 'onCreateNewLayer'
                    },
                    /*{
                        text: 'Import data',
                        id: 'hp-importLayerButton',
                        glyph: 0xf067,
                        handler: 'openImport'
                    },*/
                    {
                        text: 'Create connection',
                        glyph: 0xf129,
                        handler: 'onCreateConnection'

                    }
                ]
            })

        }, ' ', {
            text: 'Analysis',
            xtype: 'splitbutton',
            menu: new Ext.menu.Menu({
                items: [
                    {
                        text: 'Buffer area',
                        id: 'hp-bufferButton',
                        icon: VramApp.WGRASAppGlobals.resources() + 'resources/mapIcons/buffer.svg',
                        handler: 'bufferaction'

                    },
                    {
                        text: 'Query',
                        icon: VramApp.WGRASAppGlobals.resources() + 'resources/mapIcons/query.svg',
                        handler: 'onQueryByAttribute'

                    },
                    {
                        text: 'Flooding',
                        glyph: 0xf043,
                        handler: 'onElevationAction'
                    },

                    /*{
                        text: 'Flood Routing',
                        glyph: 0xf1b9,
                        handler: 'onFloodRoutingAction'
                    },*/

                    {
                        text: 'Point in polygon',
                        icon: VramApp.WGRASAppGlobals.resources() + 'resources/mapIcons/pointInPolygon.svg',
                        handler: 'onPointInPolygon'
                    },

                    {
                        text: 'Nearest object',
                        icon: VramApp.WGRASAppGlobals.resources() + 'resources/mapIcons/nearestObject.svg',
                        handler: 'onNearestObject'
                    },

                    {
                        text: 'Evacuation planning',
                        glyph: 0xf071,
                        handler: 'onEvacuationPlanning'
                    }
                ]
            }),
            glyph: 0xf085

        },
            ' ', {
                text: 'Measure',
                icon: VramApp.WGRASAppGlobals.resources() + 'resources/mapIcons/measure.svg',
                xtype: 'button',
                handler: 'onMeasure'


            }, ' ', {
                text: 'Draw',
                glyph: 0xf040,
                id: 'tools-drawButton',
                handler: 'drawaction'

            }, ' ', {
                text: 'Identify',
                id: 'hp-identifybutton',
                glyph: 0xf245,
                handler: 'identifyaction'

            },' ', {
                text: 'Edit',
                id: 'hp-editbutton',
                glyph: 0xf044,
                handler: 'onEditLayer'

            }, ' ', {
                xtype: 'geolocationtool'

            }, {
                text: 'Directions',
                glyph: 0xf277,
                handler: 'onDirections'
            },'->', {
                xtype: 'button',
                text: 'Log out',
                glyph: 0xf08b,
                handler: 'onLogoutClick',
                handleMouseEvents: false

            }
        ]
    }]


})
;
