Ext.define('VramApp.view.hp.HeadPanelController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.toolsController',


    requires: [
        'VramApp.view.about.About',
        'VramApp.view.importTool.ImportTool',
        'VramApp.view.maptools.BufferTool.Buffer',
        'VramApp.view.maptools.drawTool.DrawTool',
        'VramApp.view.maptools.elevationtool.ElevationTool',
        'VramApp.view.maptools.identifyTool.IdentifyTool',
        'VramApp.view.maptools.measureTool.MeasureTool',
        'VramApp.view.maptools.nearestobjecttool.NearestObjectTool',
        'VramApp.view.maptools.owsconnector.OwsConnector',
        'VramApp.view.maptools.pointinpolygon.PointInPolygon',
        'VramApp.view.maptools.querybyattribute.QueryByAttribute',
        'VramApp.view.maptools.floodRouting.FloodRouting',
        'VramApp.view.maptools.printTool.Printool',
        'VramApp.view.maptools.evacuationPlanningTool.EvacuationPlanningTool',
        'VramApp.view.maptools.floodFill.FloodFillToolMain',
        'VramApp.view.maptools.directions.Directions',
        'VramApp.view.maptools.createNewLayer.emptyLayer.CreateNewLayerWindow',
        'VramApp.view.maptools.editLayer.EditLayerWindow'
    ],

    listen: {
        controller: {
            '*': {
                //fired by all tool controllers after tool is closed
                toggleHeadPanelButtons: 'toggleAllButtons'
            }
        }
    },

    toggleAllButtons: function (condition) {
        try {
            this.lookupReference('headpaneltoolbar').setDisabled(condition);
        } catch (e) {
            console.log(e);
        }

    },

    mapUtils: function () {
        return VramApp.MapUtils
    },

    onLogoutClick: function () {
        window.location.assign('/logout');
    },

    openImport: function () {
        var win = Ext.create({
            xtype: 'importtoolwindow'
        });
        //win.alignTo(Ext.getCmp('mapView'), "tr-tr", [-30, 30]);
        this.toggleAllButtons(true);
    },

    drawaction: function (button) {

        var win = Ext.create({
            xtype: 'drawtoolwindow'
        });
        //win.alignTo(Ext.getCmp('mapView'), "tr-tr", [-30, 30]);
        this.toggleAllButtons(true);


    },

    bufferaction: function (button) {
        var win = Ext.create({
            xtype: 'buffertoolwindow'
        });
        //win.alignTo(Ext.getCmp('mapView'), "tr-tr", [-30, 30]);
        this.toggleAllButtons(true);

    },

    identifyaction: function (button) {

        var win = Ext.create({
            xtype: 'identifytoolwindow'
        });
        //win.alignTo(Ext.getCmp('mapView'), "tr-tr", [-30, 30]);
        this.toggleAllButtons(true);
    },

    onMeasure: function () {
        var win = Ext.create({
            xtype: "measuretoolwindow"
        });
        //win.alignTo(Ext.getBody(), "tr-tr", [-30, 30]);
        this.toggleAllButtons(true);
    },

    onQueryByAttribute: function () {
        var win = Ext.create({
            xtype: 'querybyattribute'
        });
        //win.alignTo(Ext.getCmp('mapView'), "tr-tr", [-30, 30]);
        this.toggleAllButtons(true);
    },

    onElevationAction: function () {
        var win = Ext.create({
            xtype: 'floodfilltoolmain'
        });
        //win.alignTo(Ext.getCmp('mapView'), "tr-tr", [-30, 30]);
        this.toggleAllButtons(true);
    },


    onPointInPolygon: function (btn) {
        var win = Ext.create({
            xtype: 'pointinpolygon'
        });
        //win.alignTo(Ext.getCmp('mapView'), "tr-tr", [-30, 30]);
        this.toggleAllButtons(true);
    },

    onNearestObject: function () {
        var win = Ext.create({
            xtype: 'nearestobjecttool'
        });
        //win.alignTo(Ext.getCmp('mapView'), "tr-tr", [-30, 30]);
        this.toggleAllButtons(true);
    },

    onAboutClick: function (btn) {
        var win = Ext.create({
            xtype: 'aboutWindow'
        });
        this.toggleAllButtons(true);
    },

    onCreateConnection: function (btn) {
        var win = Ext.create({
            xtype: 'owsconnector'
        });

        this.toggleAllButtons(true);
    },

    onPrintMap: function () {
        var win = Ext.create({
            xtype: 'printtool'
        });
        this.toggleAllButtons(true);
    },

    onFloodRoutingAction: function () {
        var win = Ext.create({
            xtype: 'floodroutingtool'
        });
        //win.alignTo(Ext.getCmp('mapView'), "tr-tr", [-30, 30]);
        this.toggleAllButtons(true);
    },
    onFloodAnalysis: function () {
        var win = Ext.create({
            xtype: 'floodAnalysisTool'
        });
        //win.alignTo(Ext.getCmp('mapView'), "tr-tr", [-30, 30]);
        this.toggleAllButtons(true);
    },

    onEvacuationPlanning: function (btn) {
        var win = Ext.create({
            xtype: 'evacuationplanningtool'
        });
        //win.alignTo(Ext.getCmp('mapView'), "tr-tr", [-30, 30]);
        this.toggleAllButtons(true);
    },
    
    onDirections: function(){
        var win = Ext.create({
            xtype: 'directionstool'
        });
        //win.alignTo(Ext.getCmp('mapView'), "tr-tr", [-30, 30]);
        this.toggleAllButtons(true);
    },

    onCreateNewLayer: function () {
        var win = Ext.create({
            xtype: 'createnewemptylayerwindow'
        }).show();
        this.toggleAllButtons(true);
    },

    onEditLayer: function () {
        var win = Ext.create({
            xtype: 'editlayerwindow'
        }).show();
        this.toggleAllButtons(true);

    }

});
