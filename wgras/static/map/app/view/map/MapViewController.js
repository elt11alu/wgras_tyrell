Ext.define('VramApp.view.map.MapViewController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.mapController',

    requires: [
        'Ext.data.StoreManager',
        'VramApp.LayerLoader',
        'VramApp.GeometryDataTypes'
    ],

    init: function (view) {
        var me = this;

        me.initVectorStore();
        me.addControls();
        me.addInteractions();
        me.addBaseLayers();
        me.addWMSLayers();
        me.addWFSLayers();


    },

    initVectorStore: function () {
        var store = Ext.data.StoreManager.lookup('importedVectorLayers');
        this.getView().getMap().getLayers().on('add', function (el) {
            var layer = el.element;
            if (layer.get('dataType') === VramApp.GeometryDataTypes.LayerTypes.WFS) {
                store.add({
                    geoServerName: layer.get('geoServerName'),
                    title: layer.get('title'),
                    dataType: layer.get('dataType'),
                    geoServerExtent: layer.get('geoServerExtent'),
                    isBaseLayer: layer.get('isBaseLayer'),
                    overlayFolder: layer.get('overlayFolder'),
                    persist: layer.get('persist')
                })
            }
        });
    },

    addControls: function () {
        var mousePositionControl, extentControl, scaleLine, map, disclaimer, disclaimerControl;

        map = this.getView().getMap();
        mousePositionControl = new ol.control.MousePosition();
        extentControl = new ol.control.ZoomToExtent();
        scaleLine = new ol.control.ScaleLine();
        disclaimer = this.createDisclaimer();

        disclaimerControl = new ol.control.Control({
            element: disclaimer
        });
        map.addControl(scaleLine);
        map.addControl(mousePositionControl);
        map.addControl(extentControl);
        map.addControl(disclaimerControl);
    },

    addInteractions: function () {
        var selectInteraction, map, view;

        view = this.getView();
        map = view.getMap();
        selectInteraction = new ol.interaction.Select({
            layers: function (layer) {
                return layer.get('selectable');
            }
        });
        view.on('afterRender', function () {
            map.addInteraction(selectInteraction);
            selectInteraction.setActive(true);
        });
    },

    addBaseLayers: function () {
        var osmLayer, bingMapLayer, worldBorders, openTopoMap;

        bingMapLayer = VramApp.LayerLoader.getSatellite();
        osmLayer = VramApp.LayerLoader.getOSM();
        openTopoMap = VramApp.LayerLoader.getOpenTopoMap();


        this.addBaseLayerToMap(bingMapLayer, 'Satellite map', true);
        this.addBaseLayerToMap(osmLayer, 'Open street map', false);
        this.addBaseLayerToMap(openTopoMap, 'Open topo map', false);

    },

    addWMSLayers: function () {
        var store, me, layerName, layerTitle, minX, minY, maxX, maxY, layerId, workspace, bbox, layerInfo, legendUrl, layer, required, options;
        store = Ext.getStore('rasterStore');
        me = this;
        
        store.load({
            scope: this,
            callback: function (records, operation, success) {
                records.forEach(function (el, index, arr) {
                    layerName = el.get("layerName");
                    workspace = el.get("workspaceName");
                    layerTitle = el.get('layerTitle');
                    minX = el.get('minx');
                    minY = el.get('miny');
                    maxX = el.get('maxx');
                    maxY = el.get('maxy');
                    bbox = VramApp.MapUtils.reprojectExtentTo3857([minX, minY, maxX, maxY], 'EPSG:4326');
                    layerInfo = el.get('description');
                    layerId = el.get('layerId');
                    layer = VramApp.LayerLoader.getGeoServerTiledWMS(workspace, layerName);


                    required = {
                        layer: layer,
                        layerName: layerName,
                        layerTitle: layerTitle
                    };

                    options = {
                        legendUrl: legendUrl,
                        layerId: layerId,
                        visible: false,
                        abstract: layerInfo,
                        bbox: bbox,
                        folderName: workspace,
                        workspaceName: workspace
                    };
                    me.addWMSLayerToMap(required, options)
                    //me.addWMSLayerToMap(layer, layerName, title, VramApp.MapUtils.reprojectExtentTo3857([minX, minY, maxX, maxY], 'EPSG:4326'), false, legendUrl, metadata);
                }, this);



            }
        });

        /*store.on('add', function (r_store, el, index) {
                    layerName = el.get("layerName");
                    workspace = el.get("workspaceName");
                    layerTitle = el.get('layerTitle');
                    minX = el.get('bboxMinX');
                    minY = el.get('bboxMinY');
                    maxX = el.get('bboxMaxX');
                    maxY = el.get('bboxMaxY');
                    bbox = VramApp.MapUtils.reprojectExtentTo3857([minX, minY, maxX, maxY], 'EPSG:4326')
                    layerInfo = el.get('layerInfo');
                    legendUrl = el.get('legendUrl');
                    layerId = el.get('layerId')
                    layer = VramApp.LayerLoader.getGeoServerTiledWMS(workspace, layerName);


                    required = {
                        layer: layer,
                        layerName: layerName,
                        layerTitle: layerTitle
                    };

                    options = {
                        legendUrl: legendUrl,
                        layerId: layerId,
                        visible: false,
                        abstract: layerInfo,
                        bbox: bbox
                    };
                    me.addWMSLayerToMap(required, options)
                });*/

    },

    addWFSLayers: function () {
        var store, layer, bbox, me, layerId, layerName, workspace, layerTitle, layerInfo, minX, minY, maxX, maxY, remote, ancestorLayer, metadata, required, options;
        store = Ext.getStore('vectorstore');
        me = this;

        store.load({
            scope: this,
            callback: function (records, operation, success) {
                records.forEach(function (el, index, arr) {
                    layerName = el.get("layerName");
                    workspace = el.get("workspaceName");
                    layerTitle = el.get('layerTitle');
                    minX = el.get('minx');
                    minY = el.get('miny');
                    maxX = el.get('maxx');
                    maxY = el.get('maxy');
                    layerInfo = el.get('description');
                    layerId = el.get('layerId');
                    layer = VramApp.LayerLoader.getGeoServerWFS(workspace, layerName);
                    bbox = VramApp.MapUtils.reprojectExtentTo3857([minX, minY, maxX, maxY], 'EPSG:4326')
                    required = {
                        layer: layer,
                        layerName: layerName,
                        layerTitle: layerTitle
                    };

                    options = {
                        visible: false,
                        abstract: layerInfo,
                        layerId: layerId,
                        bbox: bbox,
                        folder: workspace,
                        workspaceName: workspace
                    };
                    //  console.log(required, options)

                    //layer, geoServerName, title, bbox, visible, remote, ancestorLayer, metadata
                    me.addWFSLayerToMap(required, options);
                }, this);

            }
        });

        /*store.on('add', function (v_store, el, index) {
            layerName = el.get("layerName");
                    workspace = el.get("workspaceName");
                    layerTitle = el.get('layerTitle');
                    minX = el.get('bboxMinX');
                    minY = el.get('bboxMinY');
                    maxX = el.get('bboxMaxX');
                    maxY = el.get('bboxMaxY');
                    layerInfo = el.get('layerInfo');
                    layerId = el.get('layerId');
                    layer = VramApp.LayerLoader.getGeoServerWFS(workspace, layerName);
                    bbox = VramApp.MapUtils.reprojectExtentTo3857([minX, minY, maxX, maxY], 'EPSG:4326')
                    required = {
                        layer: layer,
                        layerName: layerName,
                        layerTitle: layerTitle
                    };

                    options = {
                        visible: false,
                        abstract: layerInfo,
                        layerId: layerId,
                        bbox: bbox
                    };
                    //  console.log(required, options)

                    //layer, geoServerName, title, bbox, visible, remote, ancestorLayer, metadata
                    me.addWFSLayerToMap(required, options);
        });*/


    },

    addWFSLayerToMap: function (required, options) {
        try {
            VramApp.MapUtils.addWFSOverlay(required, options);
            //VramApp.MapUtils.addWFSOverlay(layer, geoServerName, title, bbox, visible, true, false, metadata)
        } catch (err) {
            console.log(err);
        }
    },


    addWMSLayerToMap: function (required, options) { //layer, geoServerName, title, bbox, visible, legendUrl, metadata) {
        try {
            VramApp.MapUtils.addWMSOverlay(required, options)
            //VramApp.MapUtils.addWMSOverlay(layer, geoServerName, title, bbox, visible, legendUrl, metadata);
        } catch (err) {
            console.log(err);
        }

    },

    addBaseLayerToMap: function (layer, title, visible) {
        try {
            VramApp.MapUtils.addBaseLayer(layer, title, visible);
        } catch (err) {
            console.log(err);
        }

    },


    createDisclaimer: function () {


        var disclaimerDiv = document.createElement("div");
        disclaimerDiv.className = "whoDisclaimer ol-unselectable ol-control";
        var innerDiv = document.createElement("div");
        innerDiv.className = 'whoDisclaimerInner';
        disclaimerDiv.innerHTML = 'DISCLAIMER';
        innerDiv.innerHTML = '<b><i>The designations employed and the presentation' +
            ' of this material do not imply the expression of any opinion ' +
            'whatsoever on the part of the World Health Organization concerning ' +
            'the legal status of any country, territory, city or area or of its authorities, ' +
            'or concerning the delimitation of its frontiers and boundaries.</i></b>';
        disclaimerDiv.appendChild(innerDiv);

        return disclaimerDiv;

    },

    getUserWorkspace: function (callback) {
        var me = this;
        $.ajax({
            timeout: 7000,
            url: '/user/workspace',
            type: "GET",
            dataType: 'json',
            contentType: 'application/json',
            crossDomain: true
        }).done(function (json) {
            me.userWorkspace = json.userWorkspace
            callback()

        }).fail(function (jqXHR) {
            console.log(jqXHR)
        });
    }
});
