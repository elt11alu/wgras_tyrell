/**
 * Created by anton on 2015-12-23.
 */

Ext.define('VramApp.view.about.About',{
    extend: 'Ext.window.Window',
    requires:[
        'Ext.form.field.TextArea',
        'Ext.layout.container.Fit',
        'VramApp.view.about.AboutController',
        'VramApp.view.about.AboutViewModel'
    ],
    xtype:'aboutWindow',
    glyph:0xf129,
    viewModel: {
        type: 'about'
    },
    title:'About',

    controller: 'about',
    width:300,
    autoShow:true,
    layout:'fit',
    items:{
        xtype:'textarea',
        reference:'aboutText',
        value: ''
    }





});
