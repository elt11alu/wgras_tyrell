/**
 * Created by anton on 2015-12-23.
 */

Ext.define('VramApp.view.about.AboutViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.about',

    requires: [
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    stores: {
        /*
         A declaration of Ext.data.Store configurations that are first processed as binds to produce an effective
         store configuration. For example:

         users: {
         model: 'ElevationTool',
         autoLoad: true
         }
         */

        aboutStore: {
            autoLoad: false,
            fields: [{
                name: 'about', type: 'string'
            }],
            proxy: {
                type: 'ajax',
                actionMethods: {
                    read: 'POST'
                },
                url: '/about',
                reader: {
                    type: 'json'
                }
            }
        }
    },

    data: {
        about: 'about'
        /* This object holds the arbitrary data that populates the ViewModel and is then available for binding. */
    }
});