/**
 * Created by antonlu66 on 11/12/2015.
 */
Ext.define('VramApp.view.layerList.wgraslegends.WgrasLegends', {
    extend: 'Ext.grid.Panel',

    requires: [
        'Ext.layout.container.VBox',
        'VramApp.view.layerList.wgraslegends.WgrasLegendsController',
        'VramApp.view.layerList.wgraslegends.WgrasLegendsModel'
    ],


    xtype: 'wgraslegends',
    bodyPadding: 10,

    viewModel: {
        type: 'wgraslegends'
    },

    store: Ext.create('Ext.data.Store', {
        data: []
    }),

    controller: 'wgraslegends',
    scrollable: true,

    title: 'Legend',
    columns: [
        {
            text: 'Layer',
            dataIndex: 'displayName',
            flex: 2
        },
        {
            text: 'Legend',
            flex: 3,
            dataIndex: 'legendImgUrl',
            renderer: function (val) {
                if (val != false) {
                    return '<img src="' + val + '" />';
                }
                return 'Not available.'


            }
        }

    ]
});