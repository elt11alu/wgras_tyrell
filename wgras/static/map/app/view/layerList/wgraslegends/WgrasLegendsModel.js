/**
 * Created by antonlu66 on 11/12/2015.
 */
Ext.define('VramApp.view.layerList.wgraslegends.WgrasLegendsModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.wgraslegends',

    stores: {
        /*
        A declaration of Ext.data.Store configurations that are first processed as binds to produce an effective
        store configuration. For example:

        users: {
            model: 'WgrasLegends',
            autoLoad: true
        }
        */
    },

    data: {
        /* This object holds the arbitrary data that populates the ViewModel and is then available for binding. */
    }
});