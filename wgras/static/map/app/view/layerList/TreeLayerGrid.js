/**
 * Created by anton on 2015-10-29.
 */
Ext.define('VramApp.view.layerList.TreeLayerGrid', {
    extend: 'Ext.tree.Panel',

    requires: [
        'Ext.grid.column.Action',
        'Ext.grid.column.Check',
        'Ext.tree.Panel',
        'VramApp.view.layerList.TreeLayerController',
        'VramApp.store.LayerTreeStore'
    ],
    controller: 'treelayer',

    height: 300,
    xtype: 'treelayergrid',
    id: 'layerTreePanelGrid',
    alias: 'widget.layerTreePanel',

    useArrows: true,
    rootVisible: false,
    reference: 'layertreepanel',
    rowLines: false,
    lines: false,
    scrollable:true,
    collapsible:true,
    //collapsible: true,
    //collapsed: true,
    //header: true,
    /*tools: [{
        type: 'plus',
        tooltip: 'Add layer',
        handler: function (evt, toolElement, panelHeader, toolObject) {
            Ext.create({
                    xtype: 'importtoolwindow'
            });
        }
    }, {
        type: 'help',
        tooltip: 'Help',
        handler: function (evt, toolElement, panelHeader, toolObject) {
            this.fireEvent('helpClick', evt, toolElement, panelHeader,
                toolObject)
        }

    }
    ],*/

        listeners: {
        beforecheckchange: 'onBeforeCheckChange'
    },

    columns: [
        {
            //this is so we know which column will show the tree
            xtype: 'treecolumn',
            text: 'Name',
            flex: 3,
            dataIndex: 'displayName',
            tooltip:'Map data',
            sortable:false
        }, {
            xtype: 'actioncolumn',
            text: 'Tools',
            tooltip:'Tools',
            sortable:false,
            flex: 1,
            border: false,

            items: [
                {
                    icon: VramApp.WGRASAppGlobals.resources()+'resources/mapIcons/zoomIn.png',
                    iconCls: 'actioncol-icons',
                    tooltip: 'Zoom to map',
                    handler: 'zoomToExtent',
                    isDisabled: function (view, rowIdx, colIdx, item, record) {
                        return (!record.data.leaf || record.data.isBaseLayer)
                    }


                },
                {
                    icon: VramApp.WGRASAppGlobals.resources()+'resources/mapIcons/brush.png',
                    tooltip: 'Style map',
                    iconCls: 'actioncol-icons',
                    handler: 'openStyleWindow',
                    isDisabled: function (view, rowIdx, colIdx, item, record) {
                        return (!record.data.leaf || record.data.isBaseLayer)

                    }


                }, {
                    icon: VramApp.WGRASAppGlobals.resources()+'resources/mapIcons/info.png',
                    tooltip: 'Info',
                    iconCls: 'actioncol-icons',
                    handler: 'openLayerInfo',
                    isDisabled: function (view, rowIdx, colIdx, item, record) {
                        return (!record.data.leaf);

                    }
                }/*,{
                 icon: "resources/delete.png",
                 tooltip: 'Delete',
                 iconCls: 'actioncol-icons',
                 handler: 'deleteLayer',
                 isDisabled: function (view, rowIdx, colIdx, item, record) {
                 return (!record.data.leaf || record.data.isBaseLayer)
                 }
                 }*/


            ]
        },
        {
            xtype: 'checkcolumn',
            sortable:false,
            reference: 'selectCheckBox',
            flex: 1,
            text: 'Active',
            dataIndex: 'selectable',
            tooltip: 'Make data selectable on map, only available for vector data',
            border: false,
            renderer: function (val, m, rec) {
                var show;
                show = rec.get('dataType') === VramApp.GeometryDataTypes.LayerTypes.WFS;
                return show ? (new Ext.ux.CheckColumn()).renderer(val) : '<div unselectable="on" class="x-grid-cell-inner x-grid-cell-inner-checkcolumn" style="text-align:center; font-size: small">--</div>';
            }

        }
    ],
    store: {
        type: 'layertreestore'
    }/*Ext.create('Ext.data.TreeStore', {

        alias: 'store.layertreestore',
        storeId: 'layertreestore',
        root: {
            text: 'Layers',
            expanded: true,

            children: [
                {
                    displayName: 'Base Maps',
                    folderName: 'baseLayers',
                    isBaseLayerFolder: true,
                    children: []
                },
                {
                    displayName: 'Additional maps',
                    folderName: 'overlays',
                    expanded: true,
                    isOverLayFolder: true,
                    children: []
                }
            ]
        }
    })*/

});