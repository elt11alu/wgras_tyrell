/**
 * Created by anton on 2015-10-29.
 */
Ext.define('VramApp.view.layerList.TreeLayerController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.treelayer',
    id: 'layerTreePanelController',
    requires: [
        'VramApp.model.LayerModel',
        'VramApp.view.maptools.layerStyleTool.LayerStyleTool',
        'VramApp.view.maptools.layerStyleTool.WMSStyleTool',
        'VramApp.view.maptools.layerinfotool.LayerInfoTool'
    ],

    listen: {
        global: {

            /*layerAddedToMap can be fired by any class, to indicate a layer had been added to map,
             * params:
             * folderName || undefined*/
            layerAddedToMap: 'updateStore',
            addLayerToTreeStore: 'addLayerToStore',
            layerVisibilityChange: 'toggleVisibilityCheckbox'

        },
        controller: {
            '*': {
                layerSelectableChange: 'toggleSelectCheckBox',
                layerVisibilityChange: 'toggleVisibilityCheckbox'
            }
        }
    },

    init: function () {
        this.addLayerVisibilityListener();
        this.addStoreListener();
        this.updateStore();

        this.control({
            'layerTreePanel checkcolumn': {
                checkchange: this.setSelectable
            }
        })

    },
    addVisibilityListenerToLayer: function (layer) {
        layer.on('change:visible', function (event) {
            var visible = event.target.get(event.key);
            this.toggleVisibilityCheckbox(layer.get('layerIdentifier'), visible);
        }, this);
    },

    toggleVisibilityCheckbox: function (layerIdentifier, visible) {
        var store;
        try {
            store = this.getView().getStore();
            store.findNode('layerIdentifier', layerIdentifier).set('checked', visible);
        } catch (err) {
            console.log(err);
        }

    },

    addStoreListener: function () {
        this.getView().getStore().on({
            update: function (store, record, operation, modifiedFieldNames, details, eOpts) {
                store.commitChanges();
            }
        })
    },

    addLayerVisibilityListener: function () {
        var baseLayerFolder = this.getBaseLayerRootFolder();
        this.getView().on('checkChange', function (node, checked) {


            var layer;
            if (node.get('isBaseLayer')) {
                if (checked) {
                    baseLayerFolder.eachChild(function (childNode) {
                        layer = VramApp.MapUtils.getLayerByName(childNode.get('displayName'));
                        if (node != childNode) {
                            childNode.set('checked', !checked);
                            layer.setVisible(!checked);
                        } else {
                            layer.setVisible(checked);
                        }

                    });
                }

            } else {
                layer = VramApp.MapUtils.getLayerByLayerIdentifier(node.get('layerIdentifier'));
                layer.setVisible(checked);
                this.notifyLegendPanel(checked, layer.get('layerName'), node.get('displayName'), node.get('dataType'), layer.get('workspaceName'), node.get('layerIdentifier'), node.get('external'));
            }

        }, this);
    },

    notifyLegendPanel: function (checked, layerName, displayName, dataType, workspaceName, layerIdentifier, external) {
        if (typeof external == 'undefined') {
            external = false;
        }
        this.fireEvent('toggleLegend', checked, layerName, displayName, dataType, workspaceName, layerIdentifier, external);
    },

    deleteLayer: function (grid, rowIndex) {
        var store = grid.getStore();
        var layerName = store.getAt(rowIndex).get('displayName');
        if (VramApp.MapUtils.removeLayerFromMap(layerName)) {
            store.removeAt(rowIndex);
            this.updateStore();
        }

    },

    zoomToExtent: function (grid, rowIndex, colIndex) {
        var layerId = grid.getStore().getAt(rowIndex).get('layerId');
        VramApp.MapUtils.panToLayerById(layerId);

    },

    openStyleWindow: function (grid, rowIndex, colIndex, type) {
        var layerId = grid.getStore().getAt(rowIndex).get('layerId');
        var layerName = grid.getStore().getAt(rowIndex).get('displayName');
        var layer = VramApp.MapUtils.getLayerByDBId(layerId);
        if (layer instanceof ol.layer.Vector) {
            Ext.create({
                xtype: 'layerStyleWindow',
                title: 'Layer style for ' + layerName,
                layerId: layerId,
                layer: layerName
            });
        } else if (layer instanceof ol.layer.Tile) {
            Ext.create({
                xtype: 'wmsstyletool',
                title: 'Layer style for ' + layerName,
                layerId: layerId,
                layer: layerName
            });
        }
    },

    openLayerInfo: function (grid, rowIndex, colIndex, type) {
        var layerName, errorMessage, store, record, layerIdentifier;
        try {
            record = grid.getStore().getAt(rowIndex);
            layerName = record.get('displayName');
            layerIdentifier = record.get('layerIdentifier');
            errorMessage = false;
        } catch (err) {
            errorMessage = true;
        }

        Ext.create({
            xtype: 'layerinfotool',
            layerName: layerName,
            errorMessage: errorMessage,
            layerIdentifier: layerIdentifier
        })
    },

    toggleSelectCheckBox: function (layerId, selectable) {
        try {

            var record = Ext.data.StoreManager.lookup('layertreestore').findNode('layerId', layerId);
            if (record != null) {
                record.set('selectable', selectable)
            }
        } catch (err) {
            console.log(err)
        }

    },

    setSelectable: function (component, rowIndex, checked, eOpts) {
        var layerId, record, layer;
        record = this.getView().getStore().getAt(rowIndex);
        if (record.get('isBaseLayerFolder') || record.get('isOverLayFolder') || record.get('isOverLaySubFolder') || record.get('dataType') === undefined) {
            record.set('selectable', false);
        } else {
            layerId = record.get('layerId');
            layer = VramApp.MapUtils.getLayerByDBId(layerId);
            checked ? layer.set('selectable', true) : layer.set('selectable', false);
        }
    },

    updateStore: function () {
        var layers = VramApp.MapUtils.getLayers();
        var me = this;
        layers.forEach(function (layer, index, array) {
            var layerTitle = layer.get('title');
            var select = layer.get('selectable');
            var folderName = layer.get('overlayFolder');
            var layerModel = Ext.create('VramApp.model.LayerModel');
            var visible = layer.getVisible();
            var dataType = layer.get('dataType');
            var layerIdentifier = layer.get('layerIdentifier');
            var layerId = layer.get('layerId');
            var iconCls = layer.get('iconCls');
            var external = layer.get('external');
            if (layer.get('persist')) {
                me.addVisibilityListenerToLayer(layer);
                if (layer.get('isBaseLayer')) {
                    layerModel.set({
                        leaf: true,
                        displayName: layerTitle,
                        checked: visible,
                        isBaseLayer: true,
                        dataType: dataType,
                        layerIdentifier: layerIdentifier
                    });
                    me.insertBaseLayerInRootFolder(layerModel);
                } else {

                    layerModel.set({
                        leaf: true,
                        displayName: layerTitle,
                        checked: visible,
                        isBaseLayer: false,
                        dataType: dataType,
                        layerIdentifier: layerIdentifier,
                        iconCls: iconCls,
                        layerId: layerId,
                        idProperty: layerIdentifier,
                        external: external
                    });
                    var folderDisplayName = folderName || 'Default';
                    var folderInternalName = folderDisplayName.toLowerCase();
                    me.createSubFolderInOverlay(folderInternalName, folderDisplayName);
                    me.insertOverlayInSubFolder(layerModel, folderInternalName);

                }
            }
            //Ext.toast("Layer " + layerTitle+" added");

        });
    },

    addLayerToStore: function (layerObj) {
        var me = this;
        var layer = layerObj.layer;
        var layerTitle = layer.get('title');
        var select = layer.get('selectable');
        var folderName = layer.get('overlayFolder');
        var layerModel = Ext.create('VramApp.model.LayerModel');
        var visible = layer.getVisible();
        var dataType = layer.get('dataType');
        var layerIdentifier = layer.get('layerIdentifier');
        var layerId = layer.get('layerId');
        var iconCls = layer.get('iconCls');
        var external = layer.get('external');
        if (layer.get('persist')) {
            me.addVisibilityListenerToLayer(layer);
            if (layer.get('isBaseLayer')) {
                layerModel.set({
                    leaf: true,
                    displayName: layerTitle,
                    checked: visible,
                    isBaseLayer: true,
                    dataType: dataType,
                    layerIdentifier: layerIdentifier
                });
                me.insertBaseLayerInRootFolder(layerModel);
            } else {

                layerModel.set({
                    leaf: true,
                    displayName: layerTitle,
                    checked: visible,
                    isBaseLayer: false,
                    dataType: dataType,
                    layerIdentifier: layerIdentifier,
                    iconCls: iconCls,
                    layerId: layerId,
                    idProperty: layerIdentifier,
                    external: external
                });
                var folderDisplayName = folderName || 'Default';
                var folderInternalName = folderDisplayName.toLowerCase();
                me.createSubFolderInOverlay(folderInternalName, folderDisplayName);
                me.insertOverlayInSubFolder(layerModel, folderInternalName);

            }
        }
    },

    getLayerTreeStore: function () {
        return this.getView().getStore();
    },

    getLayerTreeRoot: function () {
        return this.getLayerTreeStore().getRoot();
    },

    insertBaseLayerInRootFolder: function (baseLayer) {
        var baseLayerFolder = this.getBaseLayerRootFolder();
        var exists = this.findBaseLayer(baseLayer.get('displayName'));
        if (baseLayerFolder && baseLayer && !exists) {
            baseLayerFolder.appendChild(baseLayer);
        }

    },
    insertOverlayInRootFolder: function (overlay) {
        var overlayFolder = this.getOverlayRootFolder();
        var exists = this.findOverlay(overlay.get('layerIdentifier'));

        if (overlayFolder && overlay && !exists) {
            overlayFolder.appendChild(overlay);
        }
    },
    insertOverlayInSubFolder: function (overlay, folderName) {
        var subFolder = this.findOverlaySubFolder(folderName);
        var exists = this.findOverlay(overlay.get('layerIdentifier'));
        if (subFolder && overlay && exists === null) {
            subFolder.appendChild(overlay);
        }
    },

    findBaseLayer: function (layerName) {
        var baseLayerRootFolder = this.getBaseLayerRootFolder();
        return baseLayerRootFolder.findChildBy(function (node) {
            if (node.get('displayName') === layerName) {
                return true;
            }
        }, this, false);


    },

    findOverlay: function (layerIdentifier) {
        var overlayRootFolder = this.getOverlayRootFolder();
        return overlayRootFolder.findChildBy(function (node) {
            if (node.get('layerIdentifier') === layerIdentifier) {
                return true;
            }
        }, this, true);


    },

    findOverlaySubFolder: function (folderName) {
        var overlayRootFolder = this.getOverlayRootFolder();
        return overlayRootFolder.findChildBy(function (node) {
            if (node.get('folderName') === folderName) {
                return true;
            }
        }, this, true);
    },

    getBaseLayerRootFolder: function () {
        var root = this.getLayerTreeRoot();
        return root.findChildBy(function (node) {
            if (node.get('isBaseLayerFolder')) {
                return true;
            }

        }, this, false);
    },

    getOverlayRootFolder: function () {
        var root = this.getLayerTreeRoot();
        return root.findChildBy(function (node) {
            if (node.get('isOverLayFolder')) {
                return true;
            }

        }, this, false);
    },

    createSubFolderInOverlay: function (folderName, displayName) {
        var folder = {
            displayName: displayName,
            folderName: folderName,
            isOverLaySubFolder: true,
            children: []
        };
        var overlayFolder = this.getOverlayRootFolder();
        var exists = this.findOverlaySubFolder(folderName);
        if (overlayFolder && !exists) {
            overlayFolder.appendChild(folder);
        }
    }

});
