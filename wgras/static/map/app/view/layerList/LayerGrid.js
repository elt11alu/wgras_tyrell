Ext.define('VramApp.view.layerList.LayerGrid', {
    extend: 'Ext.panel.Panel',

    alias: 'widget.layergrid',
    id: 'layergrid',
    title: 'Map Data',
    rowLines: true,
    xtype: 'layerlist',

    requires: [

        'Ext.layout.container.VBox',
        'VramApp.view.layerList.LayerGridController',
        'VramApp.view.layerList.TreeLayerGrid',
        'VramApp.view.layerList.wgraslegends.WgrasLegends'
    ],

    width: 400,
    rezisable: false,
    collapsible: true,
    collapsed: true,
    header: true,
    layout: {
        type: 'vbox',
        pack: 'start',
        align: 'stretch'
    },
    store: {
        type: 'layerstore'
    },
    controller: 'layergridcontroller',

    items:[
        {
            xtype:'treelayergrid',
            flex:1,
            resizable:true

        },
        {
            xtype:'wgraslegends',
            flex:1,
            collapsible:true,
            resizable:true
        }
    ]



});