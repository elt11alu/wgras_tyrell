Ext.define('VramApp.view.importTool.ImportTool', {
    extend: 'Ext.window.Window',
    xtype: 'importtoolwindow',
    alias: 'widget.importTool',

    requires: [
        'Ext.form.ComboBox',
        'Ext.form.field.Display',
        'Ext.form.field.Radio',
        'VramApp.view.importTool.ImportController',
        'VramApp.view.importTool.ImportGrid'
    ],

    controller: 'importcontroller',
    bodyPadding: 10,
    title: 'Import map data',
    closable: true,
    autoShow: true,
    width: 550,
    height: 500,
    loadMask: true,
    glyph: 0xf067,


    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        items: [{
            xtype: 'combo',
            reference: 'workspaceCombo',
            fieldLabel: 'Category',
            labelStyle:'font-weight:bold',
            store: {
                type: 'wgrasworkspacestore'
            },
            loadMask: true,
            viewConfig: {
                loadingText: "Loading layers..."
            },
            queryMode: 'local',
            displayField: 'name',
            valueField: 'name',
            listeners: {
                'select': 'onWorkspaceChange'
            }
        }, {
            xtype: 'radio',
            boxLabel: 'Raster data',
            name: 'dataType',
            inputValue: 'rasterdata',
            id: 'raster-radio',
            checked: true,
            geometry: VramApp.GeometryDataTypes.LayerTypes.RASTER,
            handler: 'onDataTypeChange'

        }, {
            xtype: 'radio',
            boxLabel: 'Vector data',
            name: 'dataType',
            inputValue: 'vectordata',
            id: 'vector-radio',
            geometry: VramApp.GeometryDataTypes.LayerTypes.VECTOR,
            handler: 'onDataTypeChange'
        }]
    }],


    items: [{

        xtype: 'wgrasimportgrid'

        }, {
        xtype: 'displayfield',
        value: '',
        reference: 'importErrorMsg'
        }, {
        xtype: 'toolbar',
        region: 'south',

        items: [{
            text: 'Import',
            handler: 'startImport',
            glyph: 0xf067
            }, {
            text: 'Close',
            handler: 'closeWindow',
            glyph: 0xf00d
            }]
        }]

});