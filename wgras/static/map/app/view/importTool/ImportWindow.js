Ext.define('VramApp.view.importTool.ImportWindow', {
    extend: 'Ext.window.Window',
    xtype: 'importTool-importWindow',

    requires: [
        'Ext.form.Panel'
    ],

    bodyPadding: 10,
    title: 'Chose a name',
    closable: true,
    autoShow: false,
    controller:'importwindowcontroller',


    items: {
        xtype: 'form',
        defaultType:'textfield',
        defaults: {
            msgTarget:'under'
        },
        reference: 'importLayerForm',
        
        buttons: [{
            text: 'Import',
            formBind: true,
            handler:'completeImport'
        },{
            text:'Cancel',
            handler:function(){
            this.up('window').destroy();
            }
        }]
    }
})