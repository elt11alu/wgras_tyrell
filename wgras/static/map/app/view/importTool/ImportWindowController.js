Ext.define('VramApp.view.importTool.ImportWindowController', {
    extend: 'Ext.app.ViewController',
    requires: [
        'VramApp.utils.VectorStyle',
        'VramApp.InputValidationModel',
        'Ext.window.Toast'
    ],
    alias: 'controller.importwindowcontroller',

    completeImport: function (button) {
        var layerName, title, layerType, userInputLayerName, textFieldArray, bbox, me;
        me = this;
        var form = this.lookupReference('importLayerForm').getForm();
        textFieldArray = form.getFields().items;
        var store = Ext.getCmp("layerTreePanelGrid").getStore();

        textFieldArray.forEach(function (element, index, array) {
            var exists = store.findExact('displayName', element.getValue());
            if (exists < 0) {
                if (VramApp.InputValidationModel.validateLayerTitle(element.getValue())) {
                    layerName = element.layerName;
                    title = element.layerTitle;
                    userInputLayerName = element.getValue();
                    bbox = me.getBoundingBox(element.bbox);
                    layerType = element.layerType;

                    if (layerType === VramApp.GeometryDataTypes.LayerTypes.WMS) {
                        me.addWMS(layerName, title, userInputLayerName, bbox);
                    } else {
                        me.addWFS(layerName, title, userInputLayerName, bbox);
                    }
                } else {
                    form.findField('inputname').markInvalid('Invalid layer name');
                }
            } else {
                form.findField('inputname').markInvalid("Layer name already exists");
            }
        });
    },

    addWMS: function (layerName, title, userInput, bbox) {
        var layer, bboxReprojected;
        layer = new ol.layer.Tile({
            //title: userInput,
            //geoServerName: layerName,
            source: new ol.source.TileWMS({
                url: VramApp.WGRASUrl.BASEURL.concat('wms'),
                projection: 'EPSG:3857',
                params: {
                    'LAYERS': layerName,
                    'CRS': 'EPSG:3857',
                    'Tiled': true
                },
                serverType: 'geoserver'
            })
        });
        this.giveMeAToast();
        bboxReprojected = VramApp.MapUtils.reprojectExtentTo3857(bbox, 'EPSG:4326');
        var required = {
            layer: layer,
            layerTitle: userInput,
            layerName: layerName
        };
        var options = {
            folderName: 'Imported wms',
            visible: true,
            abstract: 'layer info',
            bbox: bboxReprojected
        }
        VramApp.MapUtils.addWMSOverlay(required,options);
        this.closeWindow();
    },


    addWFS: function (layerName, title, userInputLayerName, bbox) {
        var me = this;
        var myMask = new Ext.LoadMask({
            msg: 'Adding layers...',
            target: this.view
        });

        var sourceVector = new ol.source.Vector({
            loader: function (extent) {

                $.ajax(VramApp.WGRASUrl.BASEURL.concat('wfs'), {
                    type: 'GET',
                    data: {
                        service: 'WFS',
                        version: '1.1.0',
                        request: 'GetFeature',
                        typename: layerName,
                        srsname: 'EPSG:3857',
                        bbox: extent.join(',') + ',EPSG:3857'
                    }
                }).done(function (response) {
                    loadFeatures(response);
                });
            },
            strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
                maxZoom: 12
            })),
            projection: 'EPSG:3857'
        });

        var loadFeatures = function (response) {
            var formatWFS = new ol.format.WFS();
            sourceVector.addFeatures(formatWFS.readFeatures(response));

        };
        var clearLoadMask = function () {
            myMask.hide();
        };

        var styleObj = me.getDefaultStyle();

        var layerVector = new ol.layer.Vector({
            source: sourceVector
        });
        var bboxReprojected = VramApp.MapUtils.reprojectExtentTo3857(bbox, 'EPSG:4326');
        var required = {
            layer: layerVector,
            layerTitle: userInputLayerName,
            layerName: layerName
        };
        var options = {
            folder: 'Imported wfs',
            bbox: bboxReprojected,
            remote: true,
            visible: true
        }
        this.giveMeAToast();
        VramApp.MapUtils.addWFSOverlay(required, options);
        this.closeWindow();
    },

    getDefaultStyle: function () {
        return VramApp.utils.VectorStyle.defaultStyle();
    },

    getBoundingBox: function (bbox) {
        var ii;
        try {
            var boundingArray = [];
            for (ii = 0; ii < bbox.length; ii++) {
                boundingArray[ii] = parseFloat(bbox[ii]);
            }
        } catch (e) {
            console.log(e);
        }
        return boundingArray;
    },

    giveMeAToast: function () {
        VramApp.MapUtils.getOlMap().getLayers().once('add', function (item, b, c) {
            try {

                Ext.toast("<b><i>" + item.element.get('title') + "</i></b>" + " was added to map");
            } catch (err) {
                console.log(err);
            }

        });
    },

    closeWindow: function () {
        this.getView().destroy();
    }


});