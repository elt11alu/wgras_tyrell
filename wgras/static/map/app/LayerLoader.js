/**
 * Created by antonlu66 on 11/11/2015.
 */
Ext.define('VramApp.LayerLoader', {

    singleton: true,

    getExternalTiledWMS_V130: function (URL, layerName) {

        var layer;
        layer = new ol.layer.Tile({
            source: new ol.source.TileWMS({
                hidpi: false,
                url: URL,
                projection: 'EPSG:3857',
                params: {
                    'LAYERS': layerName,
                    'CRS': 'EPSG:3857',
                    'Tiled': true
                }
            })
        });

        return layer;
    },

    getExternalTiledWMS_V111: function (URL, layerName) {

        var layer;
        layer = new ol.layer.Tile({
            source: new ol.source.TileWMS({
                hidpi: false,
                url: URL,
                projection: 'EPSG:3857',
                params: {
                    'LAYERS': layerName,
                    'SRS': 'EPSG:3857',
                    'Tiled': true,
                    'VERSION': '1.1.1'
                }
            })
        });

        return layer;
    },

    getExternalWFS: function (URL, layerName) {

    },

    getLocalLayer: function (layerName) {
        var vectorLayer;
        var URL;
        URL = 'resources/layers/'.concat(layerName).concat('.geojson');
        vectorLayer = new ol.layer.Vector({
            source: new ol.source.Vector({
                url: URL,
                format: new ol.format.GeoJSON()
            })
        });
        return vectorLayer;
    },

    getGeoServerTiledWMS: function (workspace, layerName) {
        var URL, layer;

        URL = VramApp.WGRASUrl.BASEURL().concat(workspace).concat('/').concat('wms');

        layer = new ol.layer.Tile({
            source: new ol.source.TileWMS({
                crossOrigin: 'Anonomous', //setting this attribute will abort creation of pdf*/
                url: URL,
                projection: 'EPSG:3857',
                params: {
                    'LAYERS': layerName,
                    'CRS': 'EPSG:3857',
                    'Tiled': true
                },
                serverType: 'geoserver'
            })
        });

        return layer;
    },

        getGeoServerElevationWMS: function (workspace, layerName) {
        var URL, layer;

        URL = VramApp.WGRASUrl.BASEURL().concat(workspace).concat('/').concat('wms');

        layer = new ol.layer.Tile({
            source: new ol.source.TileWMS({
                crossOrigin: 'Anonomous', //setting this attribute will abort creation of pdf*/
                url: URL,
                projection: 'EPSG:3857',
                params: {
                    'LAYERS': layerName,
                    'CRS': 'EPSG:3857',
                    'Tiled': true
                },
                serverType: 'geoserver'
            })
        });

        return layer;
    },

    getGeoServerImageWMS: function (workspace, layerName) {
        var URL;
        URL = VramApp.WGRASUrl.BASEURL.concat(workspace).concat('/').concat('wms');

        return new ol.layer.Image({
            source: new ol.source.ImageWMS({
                url: URL,
                params: {
                    'LAYERS': layerName
                },
                serverType: 'geoserver'
            })
        });
    },

    getGeoServerImageWMSWithViewParams: function (workspace, layerName, params, loadListener) {
        var URL;
        URL = VramApp.WGRASUrl.BASEURL.concat(workspace).concat('/').concat('wms');
        var layer = new ol.layer.Image({
            source: new ol.source.ImageWMS({
                url: URL,
                params: {
                    'LAYERS': layerName,
                    'viewparams': params.join(';'),
                    'CRS': 'EPSG:3857'
                },
                serverType: 'geoserver'
            })
        });
        layer.getSource().on('imageloadend', function () {
            loadListener()
        });
        layer.getSource().on('imageloaderror', function () {
            loadListener()
        });
        return layer
    },

    getGeoServerWFS: function (workspace, layerName) {
        var URL = VramApp.WGRASUrl.BASEURL().concat('/wfs');

        var sourceVector = new ol.source.Vector({
            loader: function (extent) {

                $.ajax(URL, {
                    type: 'GET',
                    data: {
                        service: 'WFS',
                        version: '1.1.0',
                        request: 'GetFeature',
                        typename: layerName,
                        srsname: 'EPSG:3857',
                        bbox: extent.join(',') + ',EPSG:3857'
                    }
                }).done(function (response) {
                    loadFeatures(response);
                });
            },
            strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
                maxZoom: 12
            })),
            projection: 'EPSG:3857'
        });

        var loadFeatures = function (response) {
            var formatWFS = new ol.format.WFS();
            sourceVector.addFeatures(formatWFS.readFeatures(response));

        };

        return new ol.layer.Vector({
            source: sourceVector
        });


    },

    getOSM: function () {
        return new ol.layer.Tile({
            source: new ol.source.OSM()
        });
    },

    getSatellite: function () {
        var apiKey = VramApp.WGRASUrl.BINGMAPSAPIKEY;
        return new ol.layer.Tile({
            source: new ol.source.BingMaps({
                key: apiKey,
                imagerySet: 'Aerial'
            })
        });
    },

    getOpenTopoMap: function () {

        return new ol.layer.Tile({
            source: new ol.source.XYZ({
                url: "https://{a-c}.tile.opentopomap.org/{z}/{x}/{y}.png"
            })
        });
    },

    getContourLayer: function (inputData, successCallback, failCallback) {
        var URL = '/process/extractPolygon';
        Ext.Ajax.request({
            url: URL,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            jsonData: inputData,
            success: function (response, opts) {
                successCallback(response);
            },

            failure: function (response, opts) {
                failCallback(response)
            }
        });
    },

    getWMSFeatureInfo: function (URL, callback, me) {
        Ext.Ajax.request({
            url: URL,

            success: function (response, opts) {
                callback(response, me);
            },

            failure: function (response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });
    },

    getElevationDataInfo: function (layerName, workspace, successCallback, failCallback) {
        var URL = '/process/raster/statistics/'.concat(layerName)
        Ext.Ajax.request({

            url: URL,
            defaultHeaders: {
                Accept: 'application/json'
            },
            success: function (response, opts) {
                successCallback(response);
            },

            failure: function (response, opts) {
                failCallback(response)
            }
        });
    },

    getEvacuationPlanningLayer: function (inputData, successCallback, failCallback) {
        var URL = '/evacplanning';
        Ext.Ajax.request({
            url: URL,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            jsonData: inputData,
            success: function (response, opts) {
                successCallback(response);
            },

            failure: function (response, opts) {
                failCallback(response)
            }
        });
    },


});
