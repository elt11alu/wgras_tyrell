/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */

loadingMsk = Ext.get("loadingMask");
loadingMessage = document.getElementById('loadingMessage');

Ext.define('VramApp.Application', {
    extend: 'Ext.app.Application',

    name: 'VramApp',

    requires: [
        'Ext.util.Cookies',
        'VramApp.GeometryDataTypes',
        'VramApp.WGRASUrl',
        'VramApp.view.importTool.ImportWindowController',
        'VramApp.view.main.WgrasMain',
        'VramApp.MapUtils',
        'VramApp.WGRASAppGlobals',
        'VramApp.HTTPRequests'

    ],

    views: [
        'VramApp.view.main.WgrasMain'
    ],


    stores: [
        'VramApp.store.LayerStore',
        'VramApp.store.ImportWCSStore',
        'VramApp.store.ImportRasterStore',
        'VramApp.store.ImportVectorStore',
        'VramApp.store.ImportWorkspaceStore',
        'VramApp.store.LayerInfoStore',
        'VramApp.store.AttributeStore',
        'VramApp.store.ElevationDataStore',
        'VramApp.store.ImportedVectorLayers',
        'VramApp.store.OwsConnectionStore',
        'VramApp.store.MetaDataStore',
        'VramApp.store.ImportWCSStore',
        'VramApp.store.PrivateLayerStore',
        'VramApp.store.BoundingBoxStore',
        'VramApp.store.AllLayersStore'
    ],


    loadingMsk: {},
    loadingMessages: {},


    launch: function () {

        Ext.onReady(function () {

            setTimeout(function () {
                loadingMsk.fadeOut({
                    duration: 500,
                    remove: true
                });

            }, 200);

        }, {
            dom: false
        });

        this.initializeFonts();
        this.checkUserAgent();
        this.startApplication();
    },

    startApplication: function () {
        Ext.create({xtype: 'app-main'});
    },

    initializeFonts: function () {
        Ext.setGlyphFontFamily('FontAwesome');
    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    },

    checkUserAgent: function () {
        var title = "Welcome to WGRAS application";
        var message;
        var notsupported = false;
        if (Ext.isChrome && Ext.chromeVersion != 0 && Ext.chromeVersion < 20) {
            message = "You are using an old version of Chrome web browser. This application is best used with a version above 20. Please, consider upgrading.";
            notsupported = true;
        }
        if (Ext.isIE) {
            message = "This application does not work well with internet explorer. Please consider using either Firefox or Chrome";
            notsupported = true;
        }
        if (Ext.isEdge) {
            message = "This application does not work well with internet explorer. Please consider using either Firefox or Chrome";
            notsupported = true;
        }

        if (notsupported) {
            Ext.Msg.alert(title, message)
        }
    }
});
