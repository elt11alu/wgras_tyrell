/**
 * Created by anton on 2015-10-30.
 */
Ext.define('VramApp.model.LayerModel', {
    extend: 'Ext.data.TreeModel',

    fields: [
        {name:'leaf',type:'boolean'},
        {name:'displayName',type:'string'},
        {name:'checked',type:'boolean'},
        {name:'isBaseLayer', type:'boolean'},
        {name:'name',type:'string'},
        {name:'dataType', type:'string'}
    ]
});