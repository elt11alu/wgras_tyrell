Ext.define('VramApp.model.WorkspaceModel', {

    extend: 'Ext.data.Model',
    
    fields: [{
        name: 'name'
    },{
        name: 'href'
    }]
});