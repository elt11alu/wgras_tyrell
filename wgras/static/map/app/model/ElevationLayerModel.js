Ext.define('VramApp.model.ElevationLayerModel', {

    extend: 'Ext.data.Model',

    fields: [{
        name: 'layerName'
    }, {
        name: 'workspaceName'
    }, {
        name: 'layerId'
    },{
        name: 'store'
    },{
        name: 'maxx'
    }, {
        name: 'maxy'
    }, {
        name: 'minx'
    }, {
        name: 'miny'
    }, {
        name: 'minValue'
    }, {
        name: 'maxValue'
    }, {
        name: 'stdvValue'
    }, {
        name: 'meanValue'
    }]
});
