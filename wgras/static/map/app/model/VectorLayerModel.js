Ext.define('VramApp.model.VectorLayerModel', {

    extend: 'Ext.data.Model',


    fields: [{
        name: 'layerName'
    }, {
        name: 'layerTitle'
    }, {
        name: 'workspaceName'
    }, {
        name: 'description'
    }, {
        name: 'type'
    }, {
        name: 'store'
    }, {
        name: 'minx'
    }, {
        name: 'miny'
    }, {
        name: 'maxx'
    }, {
        name: 'maxy'
    },{
        name: 'category'
    }

    ]
});