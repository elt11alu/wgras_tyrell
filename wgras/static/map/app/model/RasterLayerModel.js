Ext.define('VramApp.model.RasterLayerModel', {

    extend: 'Ext.data.Model',

    fields: [{
        name: 'layerName'
    }, {
        name: 'layerTitle'
    }, {
        name: 'workspaceName'
    }, {
        name: 'layerInfo'
    }, {
        name: 'layerType'
    }, {
        name: 'crsForBoundingBox'
    }, {
        name: 'bboxMinX'
    }, {
        name: 'bboxMinY'
    }, {
        name: 'bboxMaxX'
    }, {
        name: 'bboxMaxY'
    }, {
        name:'hasElevationData'
    },
        {
        name:'layerId'
    }
    ]
});