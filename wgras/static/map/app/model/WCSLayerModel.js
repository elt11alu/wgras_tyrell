/**
 * Created by anton on 5/10/16.
 */

Ext.define('VramApp.model.WCSLayerModel', {

    extend: 'Ext.data.Model',

    fields: [{
        name: 'layerName'
    }, {
        name: 'workspaceName'
    }, {
        name: 'label'
    }, {
        name: 'bboxMaxX'
    }, {
        name: 'bboxMaxY'
    }, {
        name: 'bboxMinX'
    }, {
        name: 'bboxMinY'
    }, {
        name: 'hasElevationData'
    }, {
        name: 'minValue'
    }, {
        name: 'maxValue'
    }, {
        name: 'noDataValue'
    }]
});
