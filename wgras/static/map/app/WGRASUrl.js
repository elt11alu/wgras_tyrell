Ext.define('VramApp.WGRASUrl', {
    singleton: true,

    PRODUCTION: true,
    BASEURL: function(){
        if(this.PRODUCTION){
            //return "https://wgras-geo.gis.lu.se/geoserver/"
            return "https://wgras-geo.gis.lu.se/geoserver2/geoserver/"
        }
        return "http://localhost:8080/geoserver/"
    },

    BASEBACKEND: "/",
    REST_USER_URL:"/users",
    WORKSPACEURL: "/workspaces",
    LAYERINFOURL: "/metadata/wms",
    ATTRIBUTEURL: "/resource/attributes/",
    LOCATIONSEARCH_URL : 'https://nominatim.openstreetmap.org/search/',
    BINGMAPSAPIKEY:'Avq0f1YpFleQZB6Jcl-gaWKjxnE6y9ynoJxoQUP1I4rU49ikW1m_CkgKx8lgn9ZG',
    GRAPHHOPPERAPIKEY: '1ea15a54-c603-4777-9c33-de2990eed15b',
    GRAPHHOPPERBASEURL: 'https://graphhopper.com/api/1/route?'



});
