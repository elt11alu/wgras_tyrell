/**
 * Created by anton on 2016-01-16.
 */
Ext.define('VramApp.readers.WMSReaderV111', {

    alias: 'reader.wmsreaderv111',
    extend: 'Ext.data.reader.Json',
    rootProperty: "layers",

    toCapObj: function (data) {

        var desc, parser, cap, version, service, baseUrl;
        desc = {"layers": []};
        parser = new OpenLayers.Format.WMSCapabilities({defaultVersion: '1.1.1'});
        cap = parser.read(data);
        version = cap.version;
        service = cap.service.name;
        baseUrl = cap.capability.request.getmap.get.href;


        cap.capability.layers.forEach(function (el, ind, arr) {
            var layer = {
                "layerName": "",
                "layerTitle": "",
                "workspaceName": "",
                "layerInfo": "",
                "layerType": "",
                "crsForBoundingBox": "",
                "bboxMinX": "",
                "bboxMinY": "",
                "bboxMaxX": "",
                "bboxMaxY": "",
                "hasElevationData": "",
                "getMapUrl": ""

            };
            layer.layerName = el.name;
            layer.layerTitle = el.title;
            layer.workspaceName = el.name.split(":")[0];
            layer.layerInfo = el['abstract'];
            layer.layerType = "WMS";
            layer.crsForBoundingBox = "EPSG:4326";
            layer.bboxMinX = el.bbox["EPSG:4326"].bbox[0];
            layer.bboxMinY = el.bbox["EPSG:4326"].bbox[1];
            layer.bboxMaxX = el.bbox["EPSG:4326"].bbox[2];
            layer.bboxMaxY = el.bbox["EPSG:4326"].bbox[3];
            layer.hasElevationData = this.getHasElevationData(el.keywords);
            //layer.getMapUrl = this.createGetMapUrl(version, service, baseUrl, el)
            desc.layers.push(layer);
        }, this);
        return JSON.stringify(desc);
    },

    getHasElevationData: function (keyWordNode) {
        var hasElevationData = 0;
        keyWordNode.forEach(function (el, ind, arr) {
            if (el.value === "wgrasElevationData") {
                hasElevationData = 1;
            }
        });
        return hasElevationData;
    },

    createGetMapUrl: function (version, service, baseUrl, layerNode) {
        var URL, layers, styles, srs, bbox, width, height, format, getMap;
        requestType = 'request=GetMap'
        layers = '&layers=' + layerNode.name;
        styles = layerNode.styles.length > 0 ? '&styles=' + layerNode.styles[0].name : '&styles=';
        srs = (version === '1.1.1') ? '&srs=EPSG:3857' : '&crs=EPSG:3857';
        bbox = '&bbox='
            + layerNode.bbox["EPSG:4326"].bbox[0]
            + ','
            + layerNode.bbox["EPSG:4326"].bbox[1]
            + ','
            + layerNode.bbox["EPSG:4326"].bbox[2]
            + ','
            + layerNode.bbox["EPSG:4326"].bbox[3];
        width = '&width=512';
        height = '&height=512';
        format = '&format=image/jpg';
        URL = baseUrl;
        URL += requestType;
        URL += '&version=' + version;
        URL += layers;
        URL += styles;
        URL += srs;
        URL += bbox;
        URL += width;
        URL += height;
        URL += format;
        return URL;

    },

    // override
    getResponseData: function (response) {
        try {
            return Ext.decode(this.toCapObj(response.responseText));
        } catch (ex) {
            error = new Ext.data.ResultSet({
                total: 0,
                count: 0,
                records: [],
                success: false,
                message: ex.message
            });
            this.fireEvent('exception', this, response, error);
            console.log(error);
            return error;
        }
    }

});