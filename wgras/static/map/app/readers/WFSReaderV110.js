/**
 * Created by anton on 2016-01-14.
 */
Ext.define('VramApp.readers.WFSReaderV110', {

    extend: 'Ext.data.reader.Json',
    alias: 'reader.wfsreader',
    rootProperty: "layers",


    toCapObj: function (data) {
        var desc = {"layers": []};

        var parser = new OpenLayers.Format.WFSCapabilities({defaultVersion: '1.1.0'});
        var cap = parser.read(data);

        cap.featureTypeList.featureTypes.forEach(function (el, ind, arr) {
            var layer = {
                "layerName": "",
                "layerTitle": "",
                "workspaceName": "",
                "layerInfo": "",
                "layerType": "",
                "crsForBoundingBox": "",
                "bboxMinX": "",
                "bboxMinY": "",
                "bboxMaxX": "",
                "bboxMaxY": "",
                "hasElevationData": "",
                "getFeatureUrl":""

            };
            layer.layerName = el.name;
            layer.layerTitle = el.title;
            layer.workspaceName = el.name.split(":")[0];
            layer.layerInfo = el['abstract'];
            layer.layerType = "WFS";
            layer.crsForBoundingBox = "EPSG:4326";
            layer.bboxMinX = el.bounds.right;
            layer.bboxMinY = el.bounds.bottom;
            layer.bboxMaxX = el.bounds.left;
            layer.bboxMaxY = el.bounds.top;
            layer.hasElevationData = this.getHasElevationData(el.keywords);
            desc.layers.push(layer);
        }, this);
        return JSON.stringify(desc);
    },

    getHasElevationData: function (keyWordNode) {
        var hasElevationData = 0;
        for (var key in keyWordNode) {

            if(key==="landmarks"){
                hasElevationData=1;
            }
        }

        return hasElevationData;
    },

    // override
    getResponseData: function (response) {
        try {
            return Ext.decode(this.toCapObj(response.responseText));
        } catch (ex) {
            error = new Ext.data.ResultSet({
                total: 0,
                count: 0,
                records: [],
                success: false,
                message: ex.message
            });
            this.fireEvent('exception', this, response, error);
            console.log(error);
            return error;
        }
    }
});