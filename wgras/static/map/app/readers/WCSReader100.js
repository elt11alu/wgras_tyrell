/**
 * Created by ludvi on 2016-03-04.
 */
/**
 * Created by anton on 2016-01-08.
 */

Ext.define('VramApp.readers.WCSReader100', {
    extend: 'Ext.data.reader.Json',
    alias: 'reader.wcsreader100',
    rootProperty: "wcsLayers",


    toCapObj: function (data) {

        var desc, parser, cap, version, service, baseUrl;
        desc = {"wcsLayers": []};
        parser = new OpenLayers.Format.WCSCapabilities({defaultVersion: '1.0.0'});
        cap = parser.read(data);

        version = cap.version;
        service = cap.service.name;
        //baseUrl = cap.capability.request.getmap.get.href;

        cap.contentMetadata.forEach(function (el, ind, arr) {
            var layer = {
                "layerName": ""
            };

            layer.layerName = el.name;
            layer.workspaceName = el.name.split(':')[0];
            layer.layerTitle = el.label;
            layer.bboxMaxX = el.lonLatEnvelope.max.y;
            layer.bboxMaxY = el.lonLatEnvelope.max.x;
            layer.bboxMinX = el.lonLatEnvelope.min.y;
            layer.bboxMinY = el.lonLatEnvelope.min.x;
            layer.hasElevationData = this.getHasElevationData(el.keywords);
            desc.wcsLayers.push(layer);
        }, this);
        return JSON.stringify(desc);
    },

    getHasElevationData: function (keyWordArray) {
        var hasElevationData = 0;
        keyWordArray.forEach(function (el, ind, arr) {

            if (el === "hasElevationData") {
                hasElevationData = 1;
            }
        });
        return hasElevationData;
    },

    getWCSStatistics:function(layerName,successCallback,failCallback){
      var URL = 'http://localhost:8080/geoserver/ows?request=getRasterStatistics&service=xyz&version=1.0.0&layers='+layerName
        Ext.Ajax.request({

            url: URL,
            defaultHeaders: {
                Accept: 'application/json'
            },
            success: function (response, opts) {
                successCallback(response);
            },

            failure: function (response, opts) {
                failCallback(response)
            }
        });
    },

    // override
    getResponseData: function (response) {
        try {
            return Ext.decode(this.toCapObj(response.responseText));
        } catch (ex) {
            error = new Ext.data.ResultSet({
                total: 0,
                count: 0,
                records: [],
                success: false,
                message: ex.message
            });
            this.fireEvent('exception', this, response, error);
            console.log(error);
            return error;
        }
    }

});
