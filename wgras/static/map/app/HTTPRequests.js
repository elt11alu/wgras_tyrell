/**
 * Created by anton on 2015-12-14.
 */

Ext.define('VramApp.HTTPRequests',{
    singleton:true,

    GET:function(url,successFunc,failFunc, id){

        Ext.Ajax.request({
            url: url,
            method:'GET',

            success: function(response, opts) {
                successFunc(response, id)

            },

            failure: function(response, opts) {
               failFunc(response, id);
            }
        });
    },

    POST:function(url,successFunc,failFunc,jsonData){
        Ext.Ajax.request({
            timeout: 60000,
            url: url,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            jsonData: jsonData,

            success: function (response, opts) {
                successFunc(response);
            },

            failure: function (response, opts) {
                failFunc(response);
            }
        });
    },

    PUT:function(url,successFunc,failFunc,params){
        Ext.Ajax.request({
            url: url,
            method:'PUT',
            params:params,

            success: function(response, opts) {
                successFunc(response)

            },

            failure: function(response, opts) {
                failFunc(response);
            }
        });
    },

    DELETE:function(url,successFunc,failFunc,params){
        Ext.Ajax.request({
            url: url,
            method:'DELETE',
            params:params,

            success: function(response, opts) {
                successFunc(response)

            },

            failure: function(response, opts) {
                failFunc(response);
            }
        });
    }
});
