Ext.define('VramApp.store.LayerStore', {
    extend: 'Ext.data.Store',

    alias: 'store.layerstore',

    fields: [
        {name: 'displayName'}, {name: 'selectable'}
    ]

});