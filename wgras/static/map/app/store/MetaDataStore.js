Ext.define('VramApp.store.MetaDataStore', {

    extend: 'Ext.data.Store',
    requires:['VramApp.model.MetaDataModel'],
    alias: 'store.metadatastore',
    storeId: 'metadatastore',
    model:'VramApp.model.MetaDataModel'

});
