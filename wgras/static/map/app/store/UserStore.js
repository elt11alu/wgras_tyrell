Ext.define('VramApp.store.UserStore', {

    extend:'Ext.data.Store',
    
    model:'VramApp.model.CurrentUser',
    
    alias: 'store.userstore',
    
    proxy: {
        type: 'rest',
        url: VramApp.WGRASUrl.REST_USER_URL.concat('users'),
        username: VramApp.WGRASUrl.RESTUSERNAME,
        password: VramApp.WGRASUrl.RESTPASSWORD,
        withCredentials: true,

        reader: {
            type: 'xml',
            record: 'user',
            rootProperty: 'users'

        }
    }
});