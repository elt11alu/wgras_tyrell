Ext.define('VramApp.store.ImportVectorStore', {

    extend: 'Ext.data.Store',
    alias: 'store.importvectorstore',
    storeId:'vectorstore',
    requires: [
        'VramApp.model.VectorLayerModel',
    ],

    model: 'VramApp.model.VectorLayerModel',

    autoLoad: false,

    proxy: {
        type: 'ajax',

        headers: {
            'Accept': 'application/json'
        },
        url: '/metadata/wfs',
        reader: {
            type: 'json',
            rootProperty: 'layers'
        }
        //url:VramApp.WGRASUrl.BASEURL().concat( 'wfs?service=wfs&version=1.1.0&request=GetCapabilities'),

        //reader: 'wfsreader'
    }
})
;
