/**
 * Created by antonlu66 on 11/18/2015.
 */
Ext.define('VramApp.store.ElevationDataStore', {
    extend: 'Ext.data.Store',
    alias: 'store.elevationDataStore',

    requires:[
        'VramApp.model.ElevationLayerModel'
    ],
    model: 'VramApp.model.ElevationLayerModel',

    autoLoad: false,

    proxy: {
        type: 'ajax',

        headers: {
            'Accept': 'application/json'
        },
        url: '/metadata/elevation',
        reader: {
            type: 'json',
            rootProperty: 'layers',
        }
    }
});
