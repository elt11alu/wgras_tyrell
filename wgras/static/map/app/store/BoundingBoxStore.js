/**
 * Created by anton on 9/15/16.
 */

Ext.define('VramApp.store.BoundingBoxStore', {

    // This store reads data from geoserver getcapabilities document, hence it contains all available layers in the system

    extend: 'Ext.data.Store',
    requires: [
        'VramApp.model.RasterLayerModel',
        'VramApp.readers.WMSReaderV130',
        'VramApp.WGRASUrl'
    ],
    alias: 'store.bboxstore',

    storeId: 'bboxstore',

    autoLoad: true,

    fields:[
        {name: "layerName"},
        {name: "layerTitle"},
        {name: "workspaceName"},
        {name: "layerInfo"},
        {name: "layerType"},
        {name: "crsForBoundingBox"},
        {name: "bboxMinX"},
        {name: "bboxMinY"},
        {name: "bboxMaxX"},
        {name: "bboxMaxY"}

    ],

    proxy: {
        type: 'ajax',

        url: VramApp.WGRASUrl.BASEURL().concat('wms?request=getCapabilities'),
        //url: '/metadata/wms',
        /*reader: {
            type: 'json',
            rootProperty: 'layers'
        }*/
        
        reader: 'wmsreader130'
    },

    findLayerRecord: function(workspaceName, layerName){
        var geoserverName = ''.concat(workspaceName,':',layerName);
        var found = false;
        this.getData().each(function (record, index) {
            if (record.get('layerName')==geoserverName){
                found = record;
                return true;
            }
        });
        return found;
    },

    getBbox: function(workspaceName, layerName){
        var gs_rec = this.findLayerRecord(workspaceName, layerName);
        if(gs_rec){
            return [parseFloat(gs_rec.get('bboxMinX')), parseFloat(gs_rec.get('bboxMinY')), parseFloat(gs_rec.get('bboxMaxX')), parseFloat(gs_rec.get('bboxMaxY'))];
        }
        return false;
    }
});