/**
 * Created by anton on 8/18/16.
 */

Ext.define('VramApp.store.PrivateLayerStore', {

    extend: 'Ext.data.Store',
    alias: 'store.privatelayerstore',

    fields: [
        {
            name: 'layerName'
        }, {
            name: 'workspaceName'
        },{
            name: 'layerId'
        },{
            name: 'type'
        },{
            name: 'layerTitle'
        }
    ],


    storeId: 'privatelayerstore',


    autoLoad: true,

    proxy: {
        type: 'ajax',
        headers: {
            'Accept': 'application/json'
        },
        url: '/layers/vector/private',

        reader: {
            type: 'json',
            rootProperty: 'layers'
        }

    }

});
