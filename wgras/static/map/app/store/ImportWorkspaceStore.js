Ext.define('VramApp.store.ImportWorkspaceStore', {
    
    extend: 'Ext.data.Store',
    alias: 'store.wgrasworkspacestore',

    requires: [
        'VramApp.model.WorkspaceModel'
    ],


    storeId:'wgrasworkspacestore',
    model: 'VramApp.model.WorkspaceModel',


    aoutoLoad: false,

    proxy: {
        type: 'ajax',
        headers:{
        'Accept': 'application/json'
        },
        url: VramApp.WGRASUrl.WORKSPACEURL,

        reader: {
            type: 'json',
            rootProperty:'workspaces'
        }

    }

});