/**
 * Created by anton on 2015-10-03.
 */

Ext.define('VramApp.utils.LayerExtentProvider', {
    singleton: true,


    getLayerExtent: function (layerName,resumeFn) {


        var extentArray = [];


            $.ajax(VramApp.WGRASUrl.BASEURL.concat('wms'), {
            data: {
                service: 'wms',
                version: '1.3.0',
                request: 'GetCapabilities'
            }
        }).done(function (response) {
            var result = parseResponse(response, layerName, extentArray);
            resumeFn(result);
        });

        var parseResponse = function (response, layerName, extentArray) {

            var parser = new ol.format.WMSCapabilities();
            var result = parser.read(response);
            var layers = result.Capability.Layer.Layer;


            layers.forEach(function (layerObj, index, array) {

                if (layerObj.Name === layerName) {
                    var boundingNode = layerObj.BoundingBox;

                    boundingNode.forEach(function (node, index, array) {
                        extentArray.push({
                            'extent': node.extent,
                            'crs': node.crs
                        });
                    });
                }
            });
            return extentArray;

        };

    }


});
