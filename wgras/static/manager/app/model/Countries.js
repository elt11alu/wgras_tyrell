Ext.define('VramApp.model.Countries', {
    extend: 'Ext.data.Model',


    fields: [
        { name: 'countryName'},
        {name: 'countryId'}
    ]
});
