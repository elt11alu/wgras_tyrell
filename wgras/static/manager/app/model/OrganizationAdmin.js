/**
 * Created by anton on 2016-07-14.
 */

Ext.define('VramApp.model.OrganizationAdmin', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    fields: [
        { name: 'username',     type: 'string' },
        { name: 'userId',     type: 'string' },
        { name: 'type',      type: 'string' },
        { name: 'created',    type: 'string' },
        {name: 'email', type:'string'},
        {name: 'isocode', type:'string'},
        {name: 'countryName', type:'string'},
        {name: 'organization', type:'string'},
        {name: 'orgid', type: 'string'}
    ]
});
