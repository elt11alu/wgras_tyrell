/**
 * Created by anton on 4/7/16.
 */

Ext.define('VramApp.model.Layers', {
    extend: 'Ext.data.Model',


    fields: [
        { name: 'layerName'},
        { name: 'layerTitle'},
        { name: 'layerType'},
        { name: 'workspaceName'},
        { name: 'baseUrl'},
        { name: 'maxx'},
        { name: 'maxy'},
        { name: 'miny' },
        { name: 'minx'},
        {name: 'layerId'}

    ]
});
