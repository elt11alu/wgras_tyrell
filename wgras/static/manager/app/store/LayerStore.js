/**
 * Created by antonlu66 on 11/23/2015.
 */
Ext.define('VramApp.store.LayerStore', {
    extend: 'Ext.data.Store',

    alias: 'store.layerstore',
    storeId:'layerstore',

    autoLoad:true,
    model:'VramApp.model.LayerModel',
    sorters:'layerName',

    proxy: {
        type: 'ajax',
        url:"/getLayerConfig",
        headers:{
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty:'layerConfig'
        }
    }
});
