Ext.define('VramApp.store.WorkspaceStore', {
    extend: 'Ext.data.Store',

    alias: 'store.workspacestore',
    storeId:'workspacestore',

    fields: [
        {name:'name', type:'string'},
        {name: 'workspaceId', type: 'string'},
        {name:'type', type:'string'}
    ],
    autoLoad:true,
    proxy: {
        type: 'rest',
        url:"/workspaces/writable",
        reader: {
            type: 'json',
            rootProperty: 'workspaces'
        }
    }
});
