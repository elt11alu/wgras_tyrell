Ext.define('VramApp.store.CsvAttrStore', {
    extend: 'Ext.data.Store',

    alias: 'store.csvattrstore',
    storeId:'csvattrstore',

    autoLoad:false,

    model:'VramApp.model.CsvAttrModel',

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            rootProperty:'fields'
        }
    }
});
