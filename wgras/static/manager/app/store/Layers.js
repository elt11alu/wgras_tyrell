/**
 * Created by anton on 4/7/16.
 */

Ext.define('VramApp.store.Layers', {
    extend: 'Ext.data.Store',

    alias: 'store.layers',
    storeId:'layers',

    autoLoad:true,
    model:'VramApp.model.Layers',
    sorters:'layerName',

    proxy: {
        type: 'ajax',
        url:"/metadata/wms",
        headers:{
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty:'layers'
        }
    }
});
