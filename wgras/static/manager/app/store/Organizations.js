Ext.define('VramApp.store.Organizations', {
    extend: 'Ext.data.Store',

    alias: 'store.organizations',
    storeId:'organizations',

    autoLoad:true,
    model:'VramApp.model.Organization',
    sorters:'name',

    proxy: {
        type: 'ajax',
        url:"/organizations",
        headers:{
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty:'organizations'
        }
    }
});
