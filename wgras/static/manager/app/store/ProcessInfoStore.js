/**
 * Created by anton on 8/4/16.
 */
Ext.define('VramApp.store.ProcessInfoStore', {
    extend: 'Ext.data.Store',

    requires: [
        'VramApp.model.ProcessInfo'
    ],

    alias: 'store.processinfo',
    storeId: 'processinfo',

    autoLoad: false,
    model: 'VramApp.model.ProcessInfo'

});
