Ext.define('VramApp.store.UnpublishedCountries', {
    extend: 'Ext.data.Store',

    alias: 'store.unpublishedcountries',
    storeId:'unpublishedcountries',

    autoLoad:true,
    fields:[
        {name:'name', type:'string'},
        {name:'isocode', type:'string'},
        {name:'published', type: 'boolean'}
    ],

    proxy: {
        type: 'ajax',
        url:'/country/unpublished',
        headers:{
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty:'countries'
        }
    }
});
