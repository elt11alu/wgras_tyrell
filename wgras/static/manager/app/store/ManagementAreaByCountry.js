Ext.define('VramApp.store.ManagementAreaByCountry', {
    extend: 'Ext.data.Store',

    alias: 'store.managementareabycountry',
    storeId:'managementareabycountry',


    model:'VramApp.model.ManagementArea',
    sorters:'countryName',
    autoLoad:false,
    proxy: {
        url:'/users/country/',
        type: 'ajax',
        headers:{
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty:'managementareas'
        }
    }
});
