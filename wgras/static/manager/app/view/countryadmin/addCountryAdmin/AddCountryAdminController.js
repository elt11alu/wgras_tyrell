Ext.define('VramApp.view.countryadmin.addCountryAdmin.AddCountryAdminController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'VramApp.utilities.Loader'
    ],

    alias: 'controller.addcountryadmincontroller',
    id: 'addcountryadmincontroller',

        listen: {
        controller: {
            '#countryadmingridcontroller':{
                onAddCountryAdminWindowOpen: function(isocode){
                    this.isocode = isocode;
                }
            }
        }
    },

    init: function () {
    },

    onCreateCountryAdmin: function (btn) {
        var values = this.lookupReference('addcountryadminform').getValues();
        var msg = '';
        var me = this;
        var loader = VramApp.utilities.Loader.getLoader(this.getView(), 'Creating user and setting permissions, please wait..');
        if (values.pw1 === values.pw2) {
            values['isocode'] = this.isocode;
            var success = function (resp) {
                loader.hide();
                var obj = Ext.decode(resp.responseText)
                me.fireEvent('onUserAdded', obj.countryadmin.username);
                me.closeWindow();
            };
            var fail = function (resp) {
                loader.hide();
                var obj = Ext.decode(resp.responseText);
                me.fireEvent('onFail', obj.errorMessage);
                me.setAddAdminErrorMessage(obj.errorMessage);
            };
            var data = JSON.stringify(values);
            loader.show();
            VramApp.utilities.HTTPRequests.POST('/users/countryadmins', data, success, fail);
        } else {
            msg = 'Please confirm password.';
            alert(msg)
        }
    },

    setAddAdminErrorMessage: function(msg){
        this.lookupReference('addCountryAdminMsgField').setValue('<p style="color:red">'+msg+'</p>')
    },

    closeWindow: function(){
        this.getView().close();
    }


});

