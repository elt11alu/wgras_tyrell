/**
 * Created by anton on 2016-07-18.
 */

Ext.define('VramApp.view.countryadmin.addCountryAdmin.AddCountryAdmin', {
    extend: 'Ext.window.Window',
    controller: 'addcountryadmincontroller',
    requires: [
        'Ext.form.field.ComboBox',
        'Ext.form.field.Display',
        'VramApp.view.countryadmin.addCountryAdmin.AddCountryAdminController'
    ],
    title: 'Add country level admin',
    viewModel: true,
    width: 300,
    bodyPadding: 10,

    xtype: 'addCountryAdminWindow',

    tbar: [
        {
            xtype:'displayfield',
            reference:'addCountryAdminMsgField'
        }
    ],

    items: [
        {
          xtype:'form',
            reference: 'addcountryadminform',
            items: [

                {
            fieldLabel: 'Username',
            xtype: 'textfield',
            name: 'username',
            allowBlank: false,
            validator: function (val) {
                var errMsg = 'Use only a-z, A-Z and 0-9 and at least 5 characters';
                var reg = new RegExp("^[a-zA-Z0-9]+$");
                var valid = reg.test(val);
                return valid && val.length >= 5 ? true : errMsg;
            }
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Email',
            vtype: 'email',
            allowBlank: false,
            name : 'email'
        },
        {
            fieldLabel: 'Password',
            xtype: 'textfield',
            inputType: 'password',
            name: 'pw1',
            reference: 'pw1Field',
            allowBlank: false,
            validator: function (val) {
                var errMsg = 'Use only a-z, A-Z, 0-9 and _ and at least 8 characters';
                var reg = new RegExp("^[a-zA-Z0-9_]+$");
                var valid = reg.test(val);
                return valid && val.length >= 8 ? true : errMsg;
            }
        },
        {
            fieldLabel: 'Confirm password',
            inputType: 'password',
            xtype: 'textfield',
            name: 'pw2',
            allowBlank: false,
            validateOnBlur: true,
            validator: function (val) {
                var errMsg = 'Use only a-z, A-Z, 0-9 and _ at least 8 characters';
                var reg = new RegExp("^[a-zA-Z0-9_]+$");
                var valid = reg.test(val);
                return valid && val.length >= 8 ? true : errMsg;
            }

        },
        {
            xtype: 'button',
            text: 'Submit',
            formBind: true,
            handler: 'onCreateCountryAdmin'
        }

            ]
        }


    ]

});
