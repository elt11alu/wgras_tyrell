/**
 * Created by anton on 2016-07-18.
 */


Ext.define('VramApp.view.countryadmin.CountryAdminGridController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'VramApp.view.countryadmin.addCountryAdmin.AddCountryAdmin'
    ],

    
    alias: 'controller.countryadmingridcontroller',
    id: 'countryadmingridcontroller',

    listen: {
        controller: {
            '#countrygridcontroller':{
                onCountryAdminWindowOpen: function(isocode){
                    this.isocode = isocode;
                    this.reloadGrid()
                }
            },
            '*':{
                onUserAdded: function(username){
                    this.reloadGrid()
                    this.setSuccessMessage('User '.concat(username, ' was added.'))
                },
                onUserDeleted: function(username){
                    this.reloadGrid()
                    this.setSuccessMessage('User '.concat(username, ' was deleted.'))
                },
                onFail: function(msg){
                    this.reloadGrid()
                    this.setFailMessage('The user could not be updated. Reason: '.concat(msg))
                }
            }
        }
    },

    init: function (view) {

    },

    reloadGrid: function(){
        Ext.data.StoreManager.lookup('countryadminstore').loadAdmins(this.isocode)
    },
    
    onCreateCountryAdmin: function(){
        Ext.create({
            xtype: 'addCountryAdminWindow'
        }).show();

        this.fireEvent('onAddCountryAdminWindowOpen', this.isocode)

    },

    onEmail: function(btn){
        var recieverId = btn.getWidgetRecord().get('userId');

    },

    onDeleteCountryAdmin: function(btn){
        var adminId = btn.getWidgetRecord().get('userId');
        var me = this;
        var success = function(resp){
            var obj = Ext.decode(resp.responseText);
            me.onDeleteSuccess(obj);
            
        };
        var fail = function(resp){
            var obj = Ext.decode(resp.responseText);
            me.onDeleteFail(obj);
        };

        var data = JSON.stringify({'userId':adminId});

        VramApp.utilities.HTTPRequests.DELETE('/users/countryadmins',data,success,fail);
    },

    onDeleteSuccess: function(obj){
        var username = obj.countryadmin.username;
        this.fireEvent('onUserDeleted', username);
        Ext.toast('User '.concat(username, ' was deleted.'));
    },

    onDeleteFail: function(obj){
        var username = obj.errorMessage;
        this.fireEvent('onFail', username);
        Ext.toast('User '.concat(username, ' could not be deleted, try again later.'));
    },

    setSuccessMessage: function(msg){
        this.lookupReference('countryAdminGridMsgField').setValue('<p style="color:green">' + msg + '</p>');
    },

    setFailMessage: function(msg){
        this.lookupReference('countryAdminGridMsgField').setValue('<p style="color:red">' + msg + '</p>');
    }


});

