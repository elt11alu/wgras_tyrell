/**
 * Created by anton on 2016-07-18.
 */
Ext.define('VramApp.view.countryadmin.CountryAdminGrid',{
    extend: 'Ext.grid.Panel',

    xtype: 'countryadmingrid',
    reference: 'countryadmingrid',
    controller: 'countryadmingridcontroller',
    requires: [
        'VramApp.store.Countryadmin',
        'VramApp.view.countryadmin.CountryAdminGridController'

    ],

    tools: [
        {
            type: 'refresh',
            tooltip: 'Refresh',
            handler: 'reloadGrid'
        },
        {
            type: 'print',
            tooltip: 'Print list'
        }

    ],

    tbar: [
        {
            xtype: 'button',
            text: 'Add admin',
            glyph:0xf067,
            handler:'onCreateCountryAdmin'
        },
        {
            xtype:'displayfield',
            reference:'countryAdminGridMsgField'
        }
    ],

    store: {
        type: 'countryadminstore'
    },

    title: 'Country admin',

    columns: [
        {
            text: 'Name',
            flex: 1,
            sortable: true,
            dataIndex: 'username'
        },
        {
            text: 'Country',
            flex: 1,
            sortable: false,
            dataIndex: 'countryName'
        },

                {
            text: 'Email',
            width: 80,
            xtype: 'widgetcolumn',
            padding:3,
            widget: {
                glyph: 0xf003,
                width: 40,
                xtype: 'button',
                dataIndex: 'userId',
                tooltip: 'View users',
                style: {
                    backgroundColor: 'LightBlue',
                    border: 0
                },
                handler: 'onEmail'

            }
        },

        {
            text: 'Delete',
            width: 80,
            xtype: 'widgetcolumn',
            padding:3,
            widget: {
                glyph: 0xf014,
                width: 40,
                xtype: 'button',
                dataIndex: 'userId',
                tooltip: 'Delete admin',
                style: {
                    backgroundColor: 'LightCoral',
                    border: 0
                },
                handler: 'onDeleteCountryAdmin'

            }
        }
    ]
});