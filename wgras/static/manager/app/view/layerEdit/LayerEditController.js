/**
 * Created by anton on 5/19/16.
 */
Ext.define('VramApp.view.layerEdit.LayerEditController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.layereditcontroller',

    requires: [
        'VramApp.view.layerEdit.LayerEditForm'
    ],


    init: function () {
    },

    printPublishedLayerList: function () {
        Ext.core.DomHelper.append(document.body, {
            tag: 'iframe',
            id: 'downloadIframe',
            frameBorder: 0,
            width: 0,
            height: 0,
            css: 'display:none;visibility:hidden;height:0px;',
            src: '/metadata/export/excel'
        });
    },

    onEditLayer: function (btn) {
        var win, record;
        record = btn.getWidgetRecord()


        win = Ext.create({
            xtype: 'layerEditWin'
        }).show();
        win.down('[form]').loadRecord(record)
        this.loadKeywordStore(record);
    },

    loadKeywordStore: function (record) {
        var layerName, workspace, url, callback, failCallback, store;
        workspace = record.get('workspaceName');
        layerName = record.get('layerName').split(':')[1];
        keywords = record.get('keywords');
        store = Ext.data.StoreManager.lookup('layerkeywords')
        keywords.forEach(function (el, ind, arr) {
            store.add({
                keyword: el
            })
        });

    },

    onAddKeyword: function () {
        var keywordField, value;
        keywordField = this.lookupReference('newKeyword');

        if (keywordField.validate()) {
            value = keywordField.getValue()
            Ext.data.StoreManager.lookup('layerkeywords').add({
                keyword: value
            })
            keywordField.reset()
        }
    },

    onDeleteKeyword: function (btn) {
        var record, store;
        record = btn.getWidgetRecord();
        store = Ext.data.StoreManager.lookup('layerkeywords');
        store.remove(record)
    },

    onEditLayerSubmit: function () {
        var val, jsonstring, callback, failCallback, url, keywords, layerName, me;
        me = this;
        val = this.lookupReference('layereditform').getValues();
        layerName = val['layerName'];
        val['layerName'] = layerName.split(':')[1]
        val.keywords = [];
        keywords = this.lookupReference('keywordgrid').getStore().getData();
        keywords.each(function (item, ind, len) {
            val.keywords.push(item.get('keyword'))
        });

        jsonstring = JSON.stringify(val);
        url = '/layer/properties';
        callback = function (resp) {
            me.setEditLayerSuccessMessage(Ext.decode(resp.responseText).msg);
            me.fireEvent('layerEdited')
            me.updateGrids()
        };
        failCallback = function (resp) {
            me.setEditLayerFailMessage(Ext.decode(resp.responseText).msg)
        };
        VramApp.utilities.HTTPRequests.PUT(url, jsonstring, callback, failCallback);
    },

    onDeleteLayer: function (btn) {
        var layerName, msgTitle, msgBody;
        layerName = btn.getWidgetRecord().get('layerName');
        msgTitle = 'Delete Layer ' + layerName + '?';
        msgBody = 'This can not be undone, proceed? (This functionality is inactivated)';
        Ext.Msg.show({
            title: msgTitle,
            message: msgBody,
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.WARNING,
            fn: function (btn) {
                if (btn === 'yes') {
                } else {
                }
            }
        })

    },

    onPublishLayer: function (btn) {
        var layerName, successCallback, failCallback, url, jsonData, workspaceName, me, publish;
        me = this;
        layerName = btn.getWidgetRecord().get('layerName');
        workspaceName = btn.getWidgetRecord().get('workspaceName');
        publish = true;
        jsonData = JSON.stringify({"layerName": layerName, "workspaceName": workspaceName, "publish": publish});

        url = '/layer/publish';
        successCallback = function (resp) {
            me.setSuccessMessage(Ext.decode(resp.responseText).msg)
            me.updateGrids()

        };
        failCallback = function (resp) {
            me.setErrorMessage(Ext.decode(resp.responseText).msg)
        };
        VramApp.utilities.HTTPRequests.PUT(url, jsonData, successCallback, failCallback);

    },

    updateGrids: function () {
        Ext.data.StoreManager.lookup('layers').reload()
        Ext.data.StoreManager.lookup('unpublishedlayers').reload()
    },

    setEditLayerSuccessMessage: function (msg) {
        this.lookupReference('editLayerMessage').setValue('<span style="color:green">' + msg + '</span>');
    },

    setEditLayerFailMessage: function (msg) {
        this.lookupReference('editLayerMessage').setValue('<span style="color:red">' + msg + '</span>');
    },

    setSuccessMessage: function (msg) {
        Ext.toast(msg, 'Message', 'b');
    },

    onClose: function (btn) {
        btn.up('window').close()
    }
});