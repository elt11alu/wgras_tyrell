/**
 * Created by anton on 5/19/16.
 */

Ext.define('VramApp.view.layerEdit.LayerEdit', {
    extend: 'Ext.panel.Panel',
    xtype:'layeredit',
    controller:'layereditcontroller',
    requires:[
        'VramApp.view.layerEdit.EditLayerGrid',
        'VramApp.view.layerEdit.UnpublishedLayerGrid',
        'VramApp.view.layerEdit.LayerEditController'
    ],
    items: [
        {
            xtype: 'editlayergrid',
            reference: 'publishedgrid',
            margin:10
        },
        {
            xtype: 'unpublishedlayergrid',
            reference: 'unpublishedgrid',
            margin:10
        }
    ]
})
