/**
 * Created by anton on 4/11/16.
 */

Ext.define('VramApp.view.layerEdit.LayerEditForm', {
    extend: 'Ext.window.Window',
    xtype: 'layerEditWin',
    controller: 'layereditcontroller',
    requires: [
        'Ext.layout.container.Anchor'
    ],
    reference: 'layerEditWindow',
    bodyPadding: 10,
    title: 'Edit Layer',
    width: 300,


    items: [
        {
            xtype: 'form',
            layout: 'anchor',
            defaults: {
                anchor: '90%',
                msgTarget: 'under',
                validateOnBlur: false
            },
            reference: 'layereditform',
            defaultType: 'textfield',
            items: [{
                fieldLabel: 'Layer name',
                name: 'layerName',
                reference: 'layerName',
                readOnly: true,
                allowBlank: false,
                regex: new RegExp("^[a-zA-Z0-9_:]{3,50}$"),
                regexText: 'Use only a-z A-Z _ and 0-9'
            }, {
                fieldLabel: 'Layer title',
                name: 'layerTitle',
                reference: 'layerTitle',
                regex: new RegExp("^[a-zA-Z0-9_ ]{3,30}$"),
                regexText: 'Use only a-z A-Z _ and 0-9'
            }, {
                xtype: 'textareafield',
                fieldLabel: 'Layer info',
                name: 'layerInfo',
                reference: 'layerInfo',
                allowBlank: false,
                regex: new RegExp("^[a-zA-Z0-9_., ]{0,300}$"),
                regexText: 'Use only a-z A-Z _ . and 0-9'
            }, {
                fieldLabel: 'Category',
                name: 'workspaceName',
                reference: 'workspaceName',
                readOnly: true,
                allowBlank: false,
                validateOnBlur: true,
                regex: new RegExp("^[a-zA-Z0-9_]{3,30}$"),
                regexText: 'Use only a-z A-Z _ and 0-9'

            },{
                xtype:'checkbox',
                fieldLabel:'Publish',
                name:'enabled',
                uncheckedValue: false,
                inputValue:true,
                checked:true
                //This has to be loaded
            }]
        },
        {
            xtype: 'grid',
            reference: 'keywordgrid',
            height: 150,
            title: 'Keywords',
            store: {
                type: 'layerKeywords'
            },
            columns: [
                {
                    text: 'Name',
                    dataIndex: 'keyword',
                    flex: 1
                },
                {
                    text: 'Delete',
                    width: 80,
                    xtype: 'widgetcolumn',
                    padding:3,
                    widget: {
                        glyph: 0xf014,
                        width: 30,
                        xtype: 'button',
                        dataIndex: 'keyword',
                        tooltip: 'Delete keyword',
                        style: {
                            backgroundColor: 'LightCoral',
                            border: 0
                        },
                        handler: 'onDeleteKeyword'

                    }
                }
            ]

        },
        {
            xtype: 'textfield',
            fieldLabel: 'Add keyword',
            reference: 'newKeyword',
            allowBlank:false,
            //regex: new RegExp("^[a-zA-Z0-9_]{2,30}$"),
            //regexText: 'Use only a-z A-Z _ and 0-9',
            validator: function(val){
                var reg = new RegExp("^[a-zA-Z0-9_]{2,30}$");
                var btn = Ext.getCmp('addkeywordbutton');
                if(val.length > 0 && reg.test(val)){
                    btn.setDisabled(false)
                    return true
                }
                btn.setDisabled(true)
                return false;
            }
        },
        {
            xtype: 'button',
            id : 'addkeywordbutton',
            text: 'Add keyword',
            disabled:true,
            handler: 'onAddKeyword'
        },{
            xtype:'displayfield',
            reference: 'editLayerMessage'
        }
    ],


    buttons: [{
        text: 'Save Changes',
        handler: 'onEditLayerSubmit',
        formBind: true
    },
        {
            text:'Close',
            handler: 'onClose'
        }]
});
