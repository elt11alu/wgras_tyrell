/**
 * Created by anton on 6/2/16.
 */

/**
 * Created by anton on 5/19/16.
 */

Ext.define('VramApp.view.upload.uploadCSVWin.UploadCSVWindow', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.button.Button',
        'Ext.form.Panel',
        'Ext.form.field.Display',
        'Ext.form.field.Text',
        'Ext.grid.Panel',
        'Ext.grid.column.Widget',
        'Ext.layout.container.Accordion',
        'Ext.layout.container.Fit',
        'Ext.layout.container.VBox',
        'Ext.panel.Panel',
        "VramApp.view.upload.uploadCSVWin.UploadCSVWindowController",
        'VramApp.store.CsvAttrStore'
    ],

    xtype: 'uploadcsvwindow',


    controller: 'uploadcsvwindowcontroller',

    /*window specific configs*/
    autoShow: false,
    constrain:true,
    width: 600,
    scrollable: true,
    bodyPadding: 5,
    glyph: 0xf0c1,
    closable: true,
    title: 'Upload CSV',

    items: [{
        xtype: 'panel',
        layout: {
            type: 'accordion',
            titleCollapse: true,
            animate: true,
            activeOnTop: false
        },
        defaults: {
            bodyPadding: 5,
            margin: 5
        },
        items: [{
            title: 'Details',
            xtype: 'form',
            reference: 'csvDetailsForm',
            items: [
                {
                    title: 'Attribute fields',
                    xtype: 'grid',
                    viewConfig: {
                        markDirty: false
                    },
                    border: 1,
                    height: 200,
                    margin: 5,
                    scrollable: true,
                    columnLines: true,
                    flex: 1,
                    store: 'csvattrstore',
                    columns: [{
                        xtype: 'rownumberer'
                    }, {
                        text: 'Attribute',
                        dataIndex: 'attrName',
                        flex: 1
                    }]
                }, {
                    xtype: 'displayfield',
                    reference: 'fileName',
                    fieldLabel: 'File name'

                }]
        }, {
            xtype: 'form',
            title: 'Upload',
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Upload',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%'
                    },
                    items: [
                        {
                            xtype: 'displayfield',
                            value: '<b>Instructions: </b>' +
                            '<ol>' +
                            '<li>Check csv details</li>' +
                            '<li>Provide required information</li>' +
                            '<li>Press upload to upload the geometry to the database </li>' +
                            '</ol>'
                        }, {
                            xtype: 'textfield',
                            fieldLabel: 'Layer name',
                            name: 'layerName',
                            reference: 'layerName',
                            allowBlank: false,
                            regex: new RegExp("^[a-zA-Z0-9_]{3,20}$"),
                            regexText: 'Use only a-z A-Z _ and 0-9'
                        }, {
                            xtype: 'combo',
                            fieldLabel: 'Existing workspace',
                            name: 'existingWorkspaceName',
                            reference: 'existingWorkspaceName',
                            displayField: 'name',
                            valueField: 'workspaceId',
                            store: {
                                type: 'workspacestore'
                            },
                            id: 'useExistingWorkspace',
                            allowBlank: false
                        }, {
                            xtype: 'textfield',
                            fieldLabel: 'Category',
                            reference: 'category',
                            regex: new RegExp("^[a-zA-Z0-9 .,]{3,50}$"),
                            regexText: 'Use only a-z A-Z  and 0-9',
                            allowBlank:false
                        },{
                            xtype: 'textarea',
                            fieldLabel: 'Description',
                            reference: 'description',
                            regex: new RegExp("^[a-zA-Z0-9 .,\']{3,50}$"),
                            regexText: 'Use only a-z A-Z  and 0-9',
                            allowBlank:true
                        },{
                            xtype: 'displayfield',
                            reference: 'msgField'
                        }, {
                            xtype: 'button',
                            text: 'upload',
                            handler: 'onUploadToDb',
                            formBind: true
                        }
                    ]
                }
            ]

        }


        ]
    }

    ],
    buttons: [{
        text: 'Close',
        handler: 'onClose'
    }]
});


