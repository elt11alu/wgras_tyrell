/**
 * Created by anton on 9/15/16.
 */


Ext.define('VramApp.view.upload.uploadGeoTIFFWin.UploadGeotiffModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.uploadgeotiffmodel',

    data: {},

    stores: {

        categorystore: {

            fields: [
        { name: 'name'},
        {name: 'categoryid'}
    ],

            autoLoad: true,
            model: 'VramApp.model.Countries',
            sorters: 'name',

            proxy: {
                type: 'ajax',
                url: "/metadata/layers/categories/simple",
                headers: {
                    'Accept': 'application/json'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'categories'
                }
            }
        }
    }
});
