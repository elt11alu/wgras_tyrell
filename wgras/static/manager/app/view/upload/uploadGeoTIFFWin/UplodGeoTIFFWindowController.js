/**
 * Created by anton on 5/19/16.
 */

Ext.define("VramApp.view.upload.uploadGeoTIFFWin.UploadGeoTIFFWindowController", {
    extend: 'Ext.app.ViewController',
    alias: "controller.uploadgeotiffwindowcontroller",

    requires: [
        'VramApp.utilities.Loader'
    ],

    listen: {
        controller: {
            '*': {
                'geotiffUploaded': 'setGeotiffDetails'
            }
        }
    },

    init: function (view) {
    },


    setGeotiffDetails: function (statistics) {
        this.lookupReference('fileName').setValue(statistics.msg.fileName);
        this.lookupReference('maxValue').setValue(statistics.msg.maxValue);
        this.lookupReference('minValue').setValue(statistics.msg.minValue);
        this.lookupReference('meanValue').setValue(statistics.msg.meanValue);
        this.lookupReference('stdvValue').setValue(statistics.msg.stdvValue);
    },

    onUploadGeotiff: function () {
        var layerName, fileName, haselevationdata, me, loader, workspaceId, description, category;

        layerName = this.lookupReference('layerName').getValue();
        fileName = this.lookupReference('fileName').getValue();
        workspaceId = this.lookupReference('existingWorkspaceName').getValue();
        haselevationdata = this.lookupReference('haselevationdatacheckbox').getValue();
        description = this.lookupReference('description').getValue();
        category = this.lookupReference('category').getValue();
        me = this;
        loader = VramApp.utilities.Loader.getLoader(me.getView(), 'Creating layer '.concat(layerName));


        $.get('layer/exists/'.concat(workspaceId, '/', layerName), function (resp) {
            if (resp.exists) {
                Ext.Msg.alert('Status', 'Layer name already exists, choose another name');
            } else {
                loader.show();
                Ext.Ajax.request({
                    url: '/raster/geotiff/createLayer',
                    method: 'POST',
                    params: {
                        layerName: layerName,
                        fileName: fileName,
                        workspaceId: workspaceId,
                        hasElevationData: haselevationdata,
                        category: category,
                        description: description

                    },
                    success: function (response, opts) {
                        var obj, msg;
                        obj = Ext.decode(response.responseText);
                        msg = obj.msg;
                        me.setSuccessMsg(msg);
                        loader.hide();
                        me.fireEvent('layerUploaded');
                        me.showAlertAfterUpload(msg);

                    },

                    failure: function (response, opts) {
                        var obj, msg, me;
                        me = this;
                        try {
                            obj = Ext.decode(response.responseText);
                            msg = obj.msg;
                        } catch (err) {
                            msg = 'An error occured, pleas try later.'
                        } finally {
                            me.setFailMsg(msg);
                            loader.hide();
                            me.showAlertAfterUpload(msg);
                        }
                    }
                });
            }
        })

    },

    setSuccessMsg: function (msg) {
        this.lookupReference('msgField').setValue('<span style="color:green">' + msg + '</span>')
    },

    setFailMsg: function (msg) {
        this.lookupReference('msgField').setValue('<span style="color:red">' + msg + '</span>')
    },

    showAlertAfterUpload: function (msg) {
        var me = this;
        Ext.Msg.show({
            title: 'Upload',
            message: msg,
            buttons: Ext.Msg.OK,
            fn: function (btn) {
                if (btn === 'ok') {
                    me.onClose()
                }
            }
        });
    },

    onClose: function () {
        this.getView().close();
        this.fireEvent('onUploadGeoTIFFWindowClosed');
    }

})
