/**
 * Created by anton on 5/19/16.
 */

Ext.define('VramApp.view.upload.uploadGeoTIFFWin.UploadGeoTIFFWindow', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.button.Button',
        'Ext.form.Panel',
        'Ext.form.field.Display',
        'Ext.form.field.Text',
        'Ext.grid.Panel',
        'Ext.grid.column.Widget',
        'Ext.layout.container.Accordion',
        'Ext.layout.container.Fit',
        'Ext.layout.container.VBox',
        'Ext.panel.Panel',
        'VramApp.view.upload.uploadGeoTIFFWin.UploadGeotiffModel'
    ],

    xtype: 'uploadgeotiffwindow',


    controller: 'uploadgeotiffwindowcontroller',
    viewModel: 'uploadgeotiffmodel',

    /*window specific configs*/
    autoShow: false,
    constrain: true,
    width: 600,
    scrollable: true,
    bodyPadding: 5,
    glyph: 0xf0c1,
    closable: true,
    title: 'Upload GeoTIFF',

    items: [{
        xtype: 'panel',
        layout: {
            type: 'accordion',
            titleCollapse: true,
            animate: true,
            activeOnTop: false
        },
        defaults: {
            bodyPadding: 5,
            margin: 5
        },
        items: [{
            title: 'Details',
            xtype: 'form',
            reference: 'geotiffDetailsForm',
            items: [{
                xtype: 'displayfield',
                reference: 'fileName',
                fieldLabel: 'File name'

            }, {
                xtype: 'displayfield',
                reference: 'projField',
                hidden: true,
                fieldLabel: 'Projection'
            }, {
                xtype: 'displayfield',
                reference: 'maxValue',
                fieldLabel: 'Max cell value'

            }, {
                xtype: 'displayfield',
                reference: 'minValue',
                fieldLabel: 'Min cell value'
            }, {
                xtype: 'displayfield',
                reference: 'meanValue',
                fieldLabel: 'Mean cell value'
            }, {
                xtype: 'displayfield',
                reference: 'stdvValue',
                fieldLabel: 'Standard deviation value'
            }]
        }, {
            xtype: 'form',
            title: 'Upload',
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Upload',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%'
                    },
                    items: [
                        {
                            xtype: 'displayfield',
                            value: '<b>Instructions: </b>' +
                            '<ol>' +
                            '<li>Check geotiff details</li>' +
                            '<li>Provide required information</li>' +
                            '<li>Press upload to upload the geometry to the database </li>' +
                            '</ol>'
                        }, {
                            xtype: 'textfield',
                            fieldLabel: 'Layer name',
                            name: 'layerName',
                            reference: 'layerName',
                            allowBlank: false,
                            regex: new RegExp("^[a-zA-Z0-9_]{3,20}$"),
                            regexText: 'Use only a-z A-Z _ and 0-9'
                        }, {
                            xtype: 'combo',
                            fieldLabel: 'Existing workspace',
                            name: 'existingWorkspaceName',
                            reference: 'existingWorkspaceName',
                            displayField: 'name',
                            valueField: 'workspaceId',
                            store: {
                                type: 'workspacestore'
                            },
                            id: 'useExistingWorkspace',
                            allowBlank: false
                        }, {
                            xtype: 'combo',
                            valueField: 'name',
                            displayField: 'name',
                            fieldLabel: 'Category (create new or select from list)',
                            reference: 'category',
                            regex: new RegExp("^[a-zA-Z0-9 .,]{3,50}$"),
                            regexText: 'Use only a-z A-Z  and 0-9',
                            allowBlank: false,
                            bind: {
                                store: '{categorystore}'
                            }
                        }, {
                            xtype: 'textarea',
                            fieldLabel: 'Description',
                            reference: 'description',
                            regex: new RegExp("^[a-zA-Z0-9 .,\']{3,50}$"),
                            regexText: 'Use only a-z, A-Z, space and 0-9',
                            allowBlank: true
                        }, {
                            xtype: 'checkbox',
                            boxLabel: 'Elevation data',
                            reference: 'haselevationdatacheckbox'
                        }, {
                            xtype: 'displayfield',
                            reference: 'msgField'
                        }, {
                            xtype: 'button',
                            text: 'upload',
                            handler: 'onUploadGeotiff',
                            formBind: true
                        }
                    ]
                }
            ]

        }


        ]
    }

    ],
    buttons: [{
        text: 'Close',
        handler: 'onClose'
    }]
});
