/**
 * Created by anton on 2016-07-14.
 */
Ext.define('VramApp.view.country.CountryGrid',{
    extend: 'Ext.grid.Panel',

    xtype: 'countrygrid',
    reference: 'countrygrid',
    controller: 'countrygridcontroller',

    requires: [
        'VramApp.store.Countries',
        'VramApp.view.country.CountryGridController'
    ],

    tools: [
        {
            type: 'refresh',
            tooltip: 'Refresh',
            handler: 'reloadGrid'
        },
        {
            type: 'print',
            tooltip: 'Print list'
        }

    ],

    tbar: [
        {
            xtype: 'button',
            text: 'Add country',
            glyph:0xf067,
            handler:'onCreateCountry'
        },

        {
            xtype:'displayfield',
            reference:'countryGridMsgField'
        }
    ],

    store: {
        type: 'countries'
    },

    title: 'Countries',

    columns: [
        {
            text: 'Name',
            flex: 1,
            sortable: true,
            dataIndex: 'name'
        },
        {
            text: 'ISO',
            flex: 1,
            sortable: false,
            dataIndex: 'isocode'
        },

        {
            text: 'Admins',
            width: 80,
            xtype: 'widgetcolumn',
            padding:3,
            widget: {
                glyph: 0xf007,
                width: 40,
                xtype: 'button',
                dataIndex: 'isocode',
                tooltip: 'View admins',
                style: {
                    backgroundColor: 'LightBlue',
                    border: 0
                },
                handler: 'onViewAdmins'

            }
        },

                {
            text: 'Users',
            width: 80,
            xtype: 'widgetcolumn',
            padding:3,
            widget: {
                glyph: 0xf0c0,
                width: 40,
                xtype: 'button',
                dataIndex: 'isocode',
                tooltip: 'View users',
                style: {
                    backgroundColor: 'LightBlue',
                    border: 0
                },
                handler: 'onViewUsers'

            }
        },

        {
            text: 'Delete',
            width: 80,
            xtype: 'widgetcolumn',
            padding:3,
            widget: {
                glyph: 0xf014,
                width: 40,
                xtype: 'button',
                dataIndex: 'isocode',
                tooltip: 'Delete country',
                style: {
                    backgroundColor: 'LightCoral',
                    border: 0
                },
                handler: 'onDeleteCountry'

            }
        }
    ]
});