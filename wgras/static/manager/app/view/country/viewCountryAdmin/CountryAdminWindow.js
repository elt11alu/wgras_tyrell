/**
 * Created by anton on 2016-07-18.
 */

Ext.define('VramApp.view.country.viewCountryAdmin.CountryAdminWindow', {
    extend: 'Ext.window.Window',
    controller: 'countrygridcontroller',
    requires: [
        'VramApp.view.countryadmin.CountryAdminGrid'
    ],
    title: 'Country admins',
    viewModel: true,
    width: 500,
    height: 500,
    bodyPadding: 10,
    scrollable:true,

    xtype: 'countryAdminWindow',

    items: [
        {
            xtype: 'countryadmingrid'
        }

    ]
});

