/**
 * Created by anton on 2016-07-21.
 */

Ext.define('VramApp.view.country.viewCountryUser.CountryUserWindow',{
    extend: 'Ext.window.Window',
    requires: [
        'VramApp.view.countryuser.CountryUserGrid'
    ],

    title: 'Country users',
    viewModel: true,
    width: 500,
    height: 500,
    bodyPadding: 10,
    scrollable:true,

    xtype: 'countryUserWindow',

        items: [
        {
            xtype: 'countryusergrid'
        }

    ]
});
