/**
 * Created by anton on 2016-07-21.
 */

Ext.define('VramApp.view.country.addCountry.addCountryFromMap.Map', {
    extend: 'GeoExt.component.Map',
    requires: [
    ],
    xtype: 'createcountrymapview',
    id: 'createcountrymapview',
    border: 1,
    style: {
        borderColor: 'black',
        borderStyle: 'solid'
    },

    map: new ol.Map({
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            }),
            new ol.layer.Vector({
                layerName: 'countries',

                    source: new ol.source.Vector({
                        url: 'static/worldBorders/world.json',
                        format: new ol.format.GeoJSON()
                    }),
                    style: function (feature, res) {
                        return new ol.style.Style({
                            fill: new ol.style.Fill({
                                color: 'rgba(255, 255, 255, 0.6)'
                            }),
                            stroke: new ol.style.Stroke({
                                color: '#319FD3',
                                width: 1
                            })
                    })
                    }

            })
        ],
        view: new ol.View({
            center: ol.proj.fromLonLat([21.0117800, 52.2297700]), //lon,lat
            zoom: 2,
            minZoom: 2
        }),
        controls: ol.control.defaults({
            attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
                collapsible: false
            })
        }).extend([
            new ol.control.ZoomToExtent({
                extent: [-649972.9038032419, 2179730.6900624484, 5328014.204323823, 11503825.148401389]
            }),
            new ol.control.ScaleLine()
        ])
    })
});

