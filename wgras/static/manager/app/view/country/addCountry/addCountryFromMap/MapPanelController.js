/**
 * Created by anton on 8/1/16.
 */

Ext.define('VramApp.view.country.addCountry.addCountryFromMap.MapPanelController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.mappanelcontroller',

    requires: [
        'VramApp.utilities.MapLegend'
    ],

    highlight: false,
    clickedFeature: false,
    pontermoveFeatureOverlay: {},
    clicked: false,

    listen: {
        controller: {
            '#countrygridcontroller':{
                onCountryDeleted: 'setUnpublishedStyle',

            },
            '#addcountrycontroller':{
                onCountryAdded: 'setPublishedStyle'
            }
        }
    },

    init: function () {
        this.createPopupOverlay();
        this.addPointermoveHighLightLayer();
        this.addMapListener();
        this.getPublishedCountries();
        this.createLegend();
    },

    addMapListener: function () {

        var map = Ext.getCmp('createcountrymapview').getMap();
        var me = this;

        me.pointermoveListener = map.on('pointermove', function (evt) {
            if (evt.dragging) {
                return;
            }
            var pixel = map.getEventPixel(evt.originalEvent);
            me.displayFeatureInfoOnPointerMove(pixel, map, evt, me);


        });

        me.clikkListener = map.on('click', function (evt) {
            me.displayFeatureInfoOnClick(evt.pixel, map, evt, me);
        });
    },

    displayFeatureInfoOnPointerMove: function (pixel, map, evt, me) {

        var feature = map.forEachFeatureAtPixel(pixel, function (feature) {
            return feature;
        });
        if (feature) {
            var coordinate = evt.coordinate;
            var published = feature.get('isPublished') ? 'Published' : 'Unpublished';
            me.popup.innerHTML = '<b>Name</b>: ' + feature.get('name') + '<br><b>Status</b>: ' + published;
            me.popupOverlay.setPosition(coordinate);
        }else{
            me.popup.innerHTML = ''
            me.popupOverlay.setPosition();
        }


        if (feature !== me.highlight) {

            if (me.highlight) {
                me.pointermoveFeatureOverlay.getSource().removeFeature(me.highlight);
            }
            if (feature) {
                me.pointermoveFeatureOverlay.getSource().addFeature(feature);
            }
            me.highlight = feature;
        }
    },

    displayFeatureInfoOnClick: function (pixel, map, evt, me) {
        var feature = map.forEachFeatureAtPixel(pixel, function (feature) {
            return feature;
        });

        // if no feature is clicked
        if (!me.clicked) {

            // if the clicked position has a feature
            if (feature) {
                if (!feature.get('isPublished')) {
                    me.setErrorMessage('')
                    // update selected fields
                    var isocode = feature.getId();
                    window.currentIsocodeForPublishing = isocode;
                    var name = feature.get('name');
                    me.updateSelected(isocode, name, me);
                    me.clicked = true;
                    window.onPublishCountryFromMap = function(){
                        me.publishCountry(window.currentIsocodeForPublishing)
                    }
                    me.popup.innerHTML = '<b>Name</b>: ' + feature.get('name') + '<br><b>Status</b>: Unpublished <hr><button id="addCountryFromMapButton" onclick="onPublishCountryFromMap()" >Publish</button>';
                    me.popupOverlay.setPosition(evt.coordinate)
                    // if the clicked feature is not the previous clicked feature
                    if (feature !== me.clickedFeature) {
                        // if clicked feature is not false
                        if (me.clickedFeature) {

                        } else {
                            // turn off pointermove
                            me.togglePointerMove(false);

                        }
                        // set currently clicked feature to the clicked feature
                        me.clickedFeature = feature;

                    }
                }else{
                    me.setErrorMessage(feature.get('name').concat(' is already published.'))
                }

            }
        }else{
            this.onClearSelection()
        }
    },

    addClickHighLightLayer: function () {
        this.clickFeatureOverlay = new ol.layer.Vector({
            source: new ol.source.Vector(),
            style: new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: '#f66',
                    width: 2
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(255,255,0,0.1)'
                })
            })
        });
        var map = Ext.getCmp('createcountrymapview').getMap();
        map.addLayer(this.clickFeatureOverlay)
    },

    addPointermoveHighLightLayer: function () {
        this.pointermoveFeatureOverlay = new ol.layer.Vector({
            source: new ol.source.Vector(),
            style: function(feature, res){
                return new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: '#f00',
                    width: 2
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(255,0,0,0.5)'
                })

            })
            }
        });
        var map = Ext.getCmp('createcountrymapview').getMap();
        map.addLayer(this.pointermoveFeatureOverlay)
    },

    updateSelected: function (isocode, name, me) {
        this.lookupReference('name').setValue(name);
        this.lookupReference('isocode').setValue(isocode);
    },

    togglePointerMove: function (toggle) {
        var me = this;
        if (toggle) {
            var map = Ext.getCmp('createcountrymapview').getMap();
            me.pointermoveListener = map.on('pointermove', function (evt) {
                if (evt.dragging) {
                    return;
                }
                var pixel = map.getEventPixel(evt.originalEvent);
                me.displayFeatureInfoOnPointerMove(pixel, map, evt, me);

            });
        } else {
            ol.Observable.unByKey(this.pointermoveListener);
        }

    },

    onClearSelection: function () {
        this.togglePointerMove(true);
        this.setErrorMessage('');
        if (this.clickedFeature) {
            this.clickedFeature = false;
            this.clicked = false;
            this.popupOverlay.setPosition();
            this.updateSelected('', '');
        }
        this.clicked = false;
    },

    onAddCountry: function (btn) {
        var me = this;
        var isocode = this.lookupReference('isocode').getValue();
        if (isocode != undefined && isocode != '' && isocode != null) {
            var panel = btn.up('panel');
            me.publishCountry(isocode,panel);
        }
    },

    publishCountry: function(isocode, panel){
        var p = panel ? panel : Ext.getCmp('addcountrymappanel');
        var me = this;
        var loadMask = new Ext.LoadMask({
                msg: 'Publishing country...',
                target: p
            });
            var success = function (resp) {
                var obj = Ext.decode(resp.responseText);
                loadMask.hide();
                me.onPublishSuccess(obj);

            };
            var fail = function (resp) {
                var obj = Ext.decode(resp.responseText);
                loadMask.hide();
                me.onPublishFail(obj);
            };

            var data = JSON.stringify({'isocode': isocode});
            loadMask.show();
            VramApp.utilities.HTTPRequests.POST('/country', data, success, fail);
    },

    onPublishSuccess: function (result) {
        Ext.data.StoreManager.lookup('countries').reload();
        Ext.MessageBox.alert('Status', 'Country '.concat(result.country.name, ' was published!'));
        this.setSuccessMessage('Country '.concat(result.country.name, ' was published!'));
        this.onClearSelection();
        this.setPublishedStyle(result.country.isocode)
    },

    onPublishFail: function (result) {
        Ext.MessageBox.alert('Status', result.errorMessage);
    },

    createPopupOverlay: function () {
        var me = this;
        this.popup = this.createPopupDiv();
        this.popupOverlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
            element: me.popup,
            autoPan: false,
            positioning: 'bottom-center',
            offset: [0, -20]
        }));
        var map = Ext.getCmp('createcountrymapview').getMap();
        map.addOverlay(this.popupOverlay)
    },

    createPopupDiv: function () {
        var div = document.createElement('div');
        div.style.color = "rgb(0,0,0)";
        div.style.fontSize = 5;
        div.style.fontWeight = 500;
        div.style.background = "rgb(255,255,255)";
        div.style.padding = "5px 4px";
        div.style.opacity = "0.9";
        div.style.border = "1px solid";
        div.style.borderColor = "#000";
        div.style.borderRadius = "4px";

        return div;
    },

    createLegend: function(){
        var legendEntries = {
            'published': {
                color: '#D3D3D3',
                description: 'Published'
            },
            'unPublished':{
                color: 'rgba(255, 255, 255, 0.6)',
                description: 'Unpublished'
            }
        };

        var legend = VramApp.utilities.MapLegend.getMapLegend(legendEntries);
        var legendControl = new ol.control.Control({
            element: legend
        });
        var map = Ext.getCmp('createcountrymapview').getMap();
        map.addControl(legendControl)
    },

    getPublishedCountries: function () {
        var me = this;
        var success = function (resp) {
            var obj = Ext.decode(resp.responseText);
            me.onGetPublishedSuccess(obj);
        };
        var fail = function (resp) {
            var obj = Ext.decode(resp.responseText);
        };
        VramApp.utilities.HTTPRequests.GET('/countries/published', success, fail)
    },

    onGetPublishedSuccess: function (obj) {
        var me = this;
        var countryLayer = this.getCountryLayer();
        if (countryLayer != null) {
            var source = countryLayer.getSource();
            var key = source.once('change', function () {
                if (source.getState() === 'ready') {
                    obj.countries.forEach(function (el, ind, arr) {
                        me.setPublishedStyle(el.isocode);
                    });
                    ol.Observable.unByKey(key)
                }


            })
        }

    },

    setPublishedStyle: function (isocode) {

        var countryLayer = this.getCountryLayer();
        if (countryLayer != null) {
            var source = countryLayer.getSource();
            var feature = source.getFeatureById(isocode);

            if (feature != null) {

                var style = new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        color: '#000000',
                        width: 1
                    }),
                    fill: new ol.style.Fill({
                        color: '#D3D3D3'
                    })
                });
                feature.setStyle(style);
                feature.set('isPublished', true)
            }
        }
    },

    setUnpublishedStyle: function(isocode){

        var countryLayer = this.getCountryLayer();
        if (countryLayer != null) {
            var source = countryLayer.getSource();
            var feature = source.getFeatureById(isocode);

            if (feature != null) {
                feature.setStyle();
                feature.set('isPublished', false);
            }
        }
    },

    getCountryLayer: function () {
        var map = Ext.getCmp('createcountrymapview').getMap();
        var layers = map.getLayers();
        var layer = null;
        layers.forEach(function (el, ind, arr) {
            if (el.get('layerName') === 'countries') {
                layer = el;
            }
        });
        return layer;
    },

    setErrorMessage: function (msg) {
        this.lookupReference('addCountryFromMapMessageField').setValue('<p style="color:red">' + msg)
    },

    setSuccessMessage: function (msg) {
        this.lookupReference('addCountryFromMapMessageField').setValue('<p style="color:green">' + msg)
    },


});
