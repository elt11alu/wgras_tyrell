/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('VramApp.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.plugin.Responsive',
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',
        'VramApp.view.main.MainController',
        'VramApp.view.main.MainModel',
        'VramApp.view.map.MapGrid',
        'VramApp.view.upload.uploadMainWin.Upload',
        'VramApp.view.user.ExistingUsers',
        'VramApp.view.user.createUser.UserCreationForm',
        'VramApp.view.user.editUser.UserEditForm',
        'VramApp.view.layerEdit.LayerEdit',
        'VramApp.view.home.HeaderPanel',
        'VramApp.view.country.CountryGrid',
        'VramApp.view.organization.OrganizationGrid',
        'VramApp.view.country.addCountry.addCountryFromMap.MapPanel',
        'VramApp.view.processInfo.ProcessInfoPanel',
        'VramApp.view.externalows.ExternalOws',
        'VramApp.view.wgrasheader.WgrasHeader'
    ],


    controller: 'main',
    viewModel: 'main',

    ui: 'navigation',

    tabBarHeaderPosition: 1,
    titleRotation: 0,
    tabRotation: 0,
    scrollable: 'y',

    header: {

        layout: {
            align: 'stretchmax'
        },
        title: {
            bind: {
                text: 'WGRAS manager'
            },
            flex: 0
        },
        iconCls: 'fa-th-list',
    },

    tabBar: {
        flex: 1,
        layout: {
            align: 'stretch',
            overflowHandler: 'none'
        },
        scrollable: 'y'
    },

    responsiveConfig: {
        tall: {
            headerPosition: 'top'
        },
        wide: {
            headerPosition: 'left'
        }
    },

    defaults: {
        bodyPadding: 20,

        tabConfig: {
            plugins: 'responsive',
            responsiveConfig: {
                wide: {
                    iconAlign: 'left',
                    textAlign: 'left'
                },
                tall: {
                    iconAlign: 'top',
                    textAlign: 'center',
                    width: 120
                }
            }
        }
    },

    items: [{
        title: 'Home',
        iconCls: 'fa-home',
        id: 'home',
        redirectPath: 'home',
        items: [

            {
                xtype: 'headerpanel',
                layout: 'center'
            },
            {
                xtype: 'navigationPage'
            }
        ]
    }, {
        title: 'Links',
        glyph: 0xf061,
        id: 'links',
        redirectPath: 'links',
        items: [
            {
                xtype: 'wgrasheader'
            }
        ]
    }, {
        title: 'Upload Map',
        //glyph: 0xf0ee,
        glyph: 0xf061,
        id: 'upload',
        scrollable: true,
        redirectPath: 'upload',
        items: [{
            xtype: 'uploadForm'
        }]
    }, {
        title: 'Users',
        glyph: 0xf061,
        id: 'users',
        redirectPath: 'users',
        layout: {
            type: 'hbox',
            pack: 'start',
            align: 'stretch'
        },
        items: [
             {
                xtype: 'existingusers',
                frame: true,
                flex: 1,
                margin: '10 10 0 0'
            }

        ]
    }, {
        title: 'Countries',
        //glyph: 0xf0ac,
        glyph: 0xf061,
        id: 'countries',
        redirectPath: 'countries',
        scrollable: true,
        minHeight: 1200,
        layout: {
            type: 'vbox',
            pack: 'start',
            align: 'stretch'
        },
        items: [
            {
                xtype: 'countrygrid',
                frame: true,
                flex: 1,
                margin: '0 10 0 0',
                maxHeight: 400
            },
            {
                xtype: 'addcountrymappanel',
                flex: 1,
                margin: '10 10 0 0',
                minHeight: 700
            }

        ]
    }, {
        title: 'Organizations',
        //glyph: 0xf0e8,
        glyph: 0xf061,
        id: 'organizations',
        redirectPath: 'organizations',
        layout: {
            type: 'hbox',
            pack: 'start',
            align: 'stretch'
        },
        items: [{
            xtype: 'organizationgrid',
            frame: true,
            flex: 1,
            margin: '0 10 0 0'
        }

        ]
    }, /*{
     title: 'Edit Layers',
     id: 'layerEdit',
     //glyph: 0xf044,
     glyph: 0xf061,
     redirectPath: 'layerEdit',
     layout: {
     type: 'vbox',
     pack: 'start',
     align: 'stretch'
     },
     items: [
     {
     xtype: 'layeredit'
     }
     ]
     },*/ {
        title: 'Layer preview',
        id: 'map',
        //glyph: 0xf279,
        glyph: 0xf061,
        redirectPath: 'map',
        scrollable: true,
        minHeight: 1200,
        layout: {
            type: 'hbox',
            pack: 'start',
            align: 'stretch'
        },
        items: [
            {
            xtype: 'mapView',
            flex: 1,
            margin: '0 10 0 0'
        }, {
            xtype: 'mapgrid',
            flex: 1,
            margin: '0 0 0 10'
        }]

    }, {
        title: 'Process logs',
        id: 'process',
        //glyph: 0xf085,
        glyph: 0xf061,
        redirectPath: 'process',
        layout: {
            type: 'hbox',
            pack: 'start',
            align: 'stretch'
        },
        items: [{
            xtype: 'processinfopanel',
            flex: 1,
            bodyPadding: 10,
        }]

    },
        {
            title: 'External service',
            id: 'externalService',
            glyph: 0xf061,
            redirectPath: 'externalService',
            items: [
                {
                    xtype: 'externalowspanel'
                }
            ]
        }

    ]
});
