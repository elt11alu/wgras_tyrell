/**
 * Created by anton on 8/4/16.
 */

Ext.define('VramApp.view.processInfo.ProcessInfoPanel', {
    extend: 'Ext.panel.Panel',

    requires:[
        'VramApp.view.processInfo.ProcessInfoController'
    ],

    title: 'Current processes',
    xtype: 'processinfopanel',

    controller: 'processinfocontroller',

    items: [
        {
            xtype:'displayfield',
            value: '<p><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>&nbsp; The list below shows a log over the current processes.</p>'
        },
        {
            xtype: 'grid',
            store: {
                type: 'processinfo'
            },
            columns: [
                {
                    text: 'Name',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'processName'
                },
                {
                    text: 'Result',
                    flex: 3,
                    sortable: false,
                    dataIndex: 'processResult'
                },
                {
                    text: 'Status',
                    flex: 1,
                    sortable: false,
                    dataIndex: 'processStatus'
                }
            ]
        }
    ]
});
