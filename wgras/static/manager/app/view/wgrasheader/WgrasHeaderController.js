Ext.define('VramApp.view.wgrasheader.WgrasHeaderController',{

    extend: 'Ext.app.ViewController',
    alias: 'controller.wgrasheadercontroller',

    onLogout: function(){
    Ext.MessageBox.confirm('Confirm', 'Are you sure you want to logout?', function(btn){
        if(btn==='yes'){
            window.location.assign('/logout')
        }
    }, this);

    },

    onGoToMap: function(){
    Ext.MessageBox.confirm('Confirm', 'Are you sure you want to leave the manager application?', function(btn){
    if(btn==='yes'){
        window.location.assign('/map')
    }
    })
    }

});