Ext.define('VramApp.view.home.HomeView', {

    extend: 'Ext.container.Container',
    xtype: 'navigationPage',
    viewModel: {
        type: 'homeviewmodel'
    },
    //layout: 'center',
    requires: [
        'Ext.layout.container.Column',
        'Ext.layout.container.Center'
    ],

    items: [{
        xtype: 'panel',
        padding: 10,

        layout: {
            type: 'column'
        },
        defaults: {
            bodyPadding: 15,
            margin: 10

        },

    items: [
        {
            title: 'Some info 1',
            bind:{
               html: '{infoText1}'
            },
            columnWidth: 0.5
        },
        {
            title: 'Some Info 2',
            columnWidth: 0.5,
            bind:{
               html: '{infoText2}'
            }
        }
    ]

    }]
});