/**
 * Created by anton on 5/20/16.
 */

Ext.define('VramApp.view.home.HeaderPanel',{
    extend: 'Ext.panel.Panel',

    xtype: 'headerpanel',

    items:[
        {
            xtype: 'container',
            html: '<H1>Welcome to WGRAS Manager</H1>'
        }
    ]
});
