/**
 * Created by anton on 5/20/16.
 */

Ext.define('VramApp.view.home.HomeViewModel', {
    extend:'Ext.app.ViewModel',

    alias: 'viewmodel.homeviewmodel',

    data: {
        infoText1: 'Lorem ipsum dolor sit amet, commodo donec, adipiscing lacus, ' +
        'sed diam sed feugiat lorem wisi viverra, vestibulum sit odio conubia metus. ' +
        'Non est placerat magna ac vestibulum tristique, eu aenean pretium, ipsum ' +
        'convallis risus libero ligula. Consectetuer maecenas massa porta. ' +
        'Wisi sapien molestie pede diam etiam. Massa vitae ac tincidunt sit, ' +
        'praesent augue lobortis consectetuer, erat placerat pulvinar est amet, ' +
        'suspendisse convallis accumsan convallis justo. ' +
        'Nulla vivamus.Lacus sapien massa in mauris. Amet gravida porttitor per. ' +
        'Nec sem donec mauris justo pellentesque, duis pede a eu cum sodales sit, ' +
        'velit parturient, etiam in suspendisse, arcu vestibulum quis placerat ullamcorper sit est. ' +
        'Tortor eget accumsan aspernatur risus. Viverra nec amet quam suspendisse vel ante, ' +
        'tincidunt vestibulum eros quam gravida mauris suscipit, ' +
        'provident est erat faucibus magna vivamus leo, venenatis tincidunt eu ornare quis dui a. ' +
        'Nullam sit ut eget lacus pellentesque vitae, tincidunt quis mattis senectus quis torquent eget, ' +
        'phasellus mollis quis, montes mauris. ' +
        'Dolor vel, praesent litora lacus, nonummy tellus hendrerit ligula vel. ' +
        'Cras purus non velit enim volutpat nulla. Hendrerit rutrum imperdiet libero a at, ' +
        'sit felis amet, arcu fusce nec adipiscing vestibulum suspendisse ac, ' +
        'ut id sodales cras tempor vel fermentum, orci consectetuer egestas arcu. ' +
        'Ut nullam orci, id elit in hendrerit sed donec, velit enim sodales consectetuer imperdiet porta donec, ' +
        'mi nisl viverra cursus malesuada vulputate. Posuere varius, orci amet, pharetra imperdiet ' +
        'ante justo pede aenean blandit, varius mi, lorem quisque praesent.',

        infoText2: 'Lorem ipsum dolor sit amet, commodo donec, adipiscing lacus, ' +
        'sed diam sed feugiat lorem wisi viverra, vestibulum sit odio conubia metus. ' +
        'Non est placerat magna ac vestibulum tristique, eu aenean pretium, ipsum ' +
        'convallis risus libero ligula. Consectetuer maecenas massa porta. ' +
        'Wisi sapien molestie pede diam etiam. Massa vitae ac tincidunt sit, ' +
        'praesent augue lobortis consectetuer, erat placerat pulvinar est amet, ' +
        'suspendisse convallis accumsan convallis justo. ' +
        'Nulla vivamus.Lacus sapien massa in mauris. Amet gravida porttitor per. ' +
        'Nec sem donec mauris justo pellentesque, duis pede a eu cum sodales sit, ' +
        'velit parturient, etiam in suspendisse, arcu vestibulum quis placerat ullamcorper sit est. ' +
        'Tortor eget accumsan aspernatur risus. Viverra nec amet quam suspendisse vel ante, ' +
        'tincidunt vestibulum eros quam gravida mauris suscipit, ' +
        'provident est erat faucibus magna vivamus leo, venenatis tincidunt eu ornare quis dui a. ' +
        'Nullam sit ut eget lacus pellentesque vitae, tincidunt quis mattis senectus quis torquent eget, ' +
        'phasellus mollis quis, montes mauris. ' +
        'Dolor vel, praesent litora lacus, nonummy tellus hendrerit ligula vel. ' +
        'Cras purus non velit enim volutpat nulla. Hendrerit rutrum imperdiet libero a at, ' +
        'sit felis amet, arcu fusce nec adipiscing vestibulum suspendisse ac, ' +
        'ut id sodales cras tempor vel fermentum, orci consectetuer egestas arcu. ' +
        'Ut nullam orci, id elit in hendrerit sed donec, velit enim sodales consectetuer imperdiet porta donec, ' +
        'mi nisl viverra cursus malesuada vulputate. Posuere varius, orci amet, pharetra imperdiet ' +
        'ante justo pede aenean blandit, varius mi, lorem quisque praesent.',
    }

});
