/**
 * Created by anton on 2016-08-20.
 */
Ext.define('VramApp.view.externalows.ExternalOwsViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.extermalowsviewmodel',

    data: {
        informationText: '<p><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>&nbsp; Here you can add' +
        'external Web Map Service URL which will be stored and accessible in the map application. ' +
        'The stored services can be seen in the list to the left.<br>' +
        'The url should be the base url of the service, not the whole get capabilities url, see example in url field.',
        owsgridtitle: 'Existing services',
        formtitle: 'Add new service',
        servicemetadata:{
            name: '',
            description: '',
            category: '',
            url: 'https://firms.modaps.eosdis.nasa.gov/wms/'
        },
        messageText: ''
    },

    stores: {
        externalowsstore: {

            fields: [{name: 'url'}, {name: 'description'}, {name: 'category'}, {name: 'name'},{name: 'owsId'}],


            autoLoad: true,

            proxy: {
                type: 'ajax',
                headers: {
                    'Accept': 'application/json'
                },
                url: '/metadata/externalows',

                reader: {
                    type: 'json',
                    rootProperty: 'services'
                }

            }
        }


    }
});
