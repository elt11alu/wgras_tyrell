/**
 * Created by anton on 9/14/16.
 */
Ext.define('VramApp.view.externalows.ExternalOwsController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.externalowscontroller',

    init: function(){

    },

    onAddOWS: function(){
        var me = this;
        var serviceMetadata = this.getViewModel().get('servicemetadata');
        var success = function(resp){
            var jsonResp = Ext.decode(resp.responseText);
            me.onAddSucces(jsonResp)
        };
        var fail = function(resp){
            var jsonResp = Ext.decode(resp.responseText);
            me.setErrorMsg(jsonResp.errorMessage);
        };
        var data = JSON.stringify({
            name: serviceMetadata.name,
            url: serviceMetadata.url,
            description: serviceMetadata.description,
            category: serviceMetadata.category
        });
        VramApp.utilities.HTTPRequests.POST('/metadata/externalows', data, success, fail)
    },

    onAddSucces: function(jsonResp){
        this.setSuccessMsg('Service '.concat(jsonResp.service.name, ' was added.'));
        this.getViewModel().getStore('externalowsstore').reload();
    },

    onDeleteOWS: function(btn){
        var record, name;
        record = btn.getWidgetRecord();
        name = record.get('name');
        Ext.MessageBox.confirm('Confirm', 'Are you sure you want to delete metadata for service '.concat(name, '?'), function(btn, text){
            if(btn=='yes'){
                this.onConfirmDelete(record);
            }
        }, this);


    },

    onConfirmDelete: function(record){
        var owsId, me, success, fail, data;
        owsId = record.get('owsId');

        me = this;

        success = function(resp){
            var jsonResp = Ext.decode(resp.responseText);
            me.onDeleteSucces(jsonResp)
        };
        fail = function(resp){
            var jsonResp;
            jsonResp = Ext.decode(resp.responseText);
            me.setErrorMsg(jsonResp.errorMessage);
        };
        data = JSON.stringify({
            owsId: owsId
        });
        VramApp.utilities.HTTPRequests.DELETE('/metadata/externalows', data, success, fail)
    },

    onDeleteSucces: function(jsonResp){
        this.setSuccessMsg('Service '.concat(jsonResp.service.name, ' was deleted.'));
        this.getViewModel().getStore('externalowsstore').reload();
    },

    setErrorMsg: function(msg){
        var message = '<p style="color:indianred"><i class="fa fa-exclamation" aria-hidden="true"></i>&nbsp; '+msg+'</p>'
        this.getViewModel().set('messageText', message)
    },

    setSuccessMsg: function(msg){
        var message = '<p style="color:cadetblue"><i class="fa fa-info" aria-hidden="true"></i>&nbsp; '+msg+'</p>'
        this.getViewModel().set('messageText', message)
    }
});