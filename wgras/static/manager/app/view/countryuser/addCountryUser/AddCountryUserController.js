/**
 * Created by anton on 2016-07-21.
 */

Ext.define('VramApp.view.countryuser.addCountryUser.AddCountryUserController', {
    extend: 'Ext.app.ViewController',

    requires: [],

    alias: 'controller.addcountryusercontroller',
    id: 'addcountryusercontroller',

        listen: {
        controller: {
            '#countryusergridcontroller':{
                onAddCountryUserWindowOpen: function(isocode){
                    this.isocode = isocode;
                }
            }
        }
    },

    init: function () {
    },

    onCreateCountryUser: function (btn) {
        var values = this.lookupReference('addcountryuserform').getValues();
        var msg = '';
        var me = this;
        var loader = VramApp.utilities.Loader.getLoader(this.getView(), 'Creating user and setting permissions, please wait..');
        if (values.pw1 === values.pw2) {
            values['isocode'] = this.isocode;
            var success = function (resp) {
                loader.hide();
                var obj = Ext.decode(resp.responseText)
                me.fireEvent('onUserAdded', obj.countryuser.username);
                me.closeWindow();
            };
            var fail = function (resp) {
                loader.hide();
                var obj = Ext.decode(resp.responseText);
                me.fireEvent('onFail', obj.errorMessage);
                me.setAddUserErrorMessage(obj.errorMessage);
            };
            var data = JSON.stringify(values);
            loader.show();
            VramApp.utilities.HTTPRequests.POST('/users/countryusers', data, success, fail);
        } else {
            msg = 'Please confirm password.';
            alert(msg)
        }
    },

    setAddUserErrorMessage: function(msg){
        this.lookupReference('addCountryUserMsgField').setValue('<p style="color:red">'+msg+'</p>')
    },

    closeWindow: function(){
        this.getView().close();
    }


});


