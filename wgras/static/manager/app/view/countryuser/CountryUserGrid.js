/**
 * Created by anton on 2016-07-21.
 */

Ext.define('VramApp.view.countryuser.CountryUserGrid',{
    extend: 'Ext.grid.Panel',

    xtype: 'countryusergrid',
    reference: 'countryusergrid',
    id: 'countryusergrid',
    controller: 'countryusergridcontroller',
    requires: [
        'VramApp.store.Countryuser',
        'VramApp.view.countryuser.CountryUserGridController'

    ],

    tools: [
        {
            type: 'refresh',
            tooltip: 'Refresh',
            handler: 'reloadGrid'
        },
        {
            type: 'print',
            tooltip: 'Print list'
        }

    ],

    tbar: [
        {
            xtype: 'button',
            text: 'Add user',
            glyph:0xf067,
            handler:'onCreateCountryUser'
        },
        {
            xtype:'displayfield',
            reference:'countryUserGridMsgField'
        }
    ],

    store: {
        type: 'countryuserstore'
    },

    title: 'Country user',

    columns: [
        {
            text: 'Name',
            flex: 1,
            sortable: true,
            dataIndex: 'username'
        },
        {
            text: 'Country',
            flex: 1,
            sortable: false,
            dataIndex: 'countryName'
        },

                {
            text: 'Email',
            width: 80,
            xtype: 'widgetcolumn',
            padding:3,
            widget: {
                glyph: 0xf003,
                width: 40,
                xtype: 'button',
                dataIndex: 'userId',
                tooltip: 'View users',
                style: {
                    backgroundColor: 'LightBlue',
                    border: 0
                },
                handler: 'onEmail'

            }
        },

        {
            text: 'Delete',
            width: 80,
            xtype: 'widgetcolumn',
            padding:3,
            widget: {
                glyph: 0xf014,
                width: 40,
                xtype: 'button',
                dataIndex: 'userId',
                tooltip: 'Delete user',
                style: {
                    backgroundColor: 'LightCoral',
                    border: 0
                },
                handler: 'onDeleteCountryUser'

            }
        }
    ]
});
