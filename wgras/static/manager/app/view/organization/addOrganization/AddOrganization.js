/**
 * Created by anton on 2016-07-31.
 */

Ext.define('VramApp.view.organization.addOrganization.AddOrganization', {
    extend: 'Ext.window.Window',
    controller: 'addorganizationcontroller',
    requires: [
        'Ext.form.field.ComboBox',
        'Ext.form.field.Display',
        'VramApp.view.organization.addOrganization.AddOrganizationController'
    ],
    title: 'Publish organization',
    viewModel: true,
    width: 300,
    bodyPadding: 10,

    xtype: 'addOrganizationWindow',

    items: [
        {
            xtype: 'form',
            reference: 'addOrganizationform',
            publishes: 'valid',
            items: [
                {
                    xtype: 'combo',
                    valueField: 'isocode',
                    displayField: 'name',
                    name: 'isocode',
                    queryMode: 'local',
                    reference: 'publishedcountry',
                    store: {
                        type: 'countries',
                        autoLoad: true
                    },
                    fieldLabel: 'Choose country',
                    allowBlank: false,
                    publishes: 'value'
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Full name',
                    name: 'fullname',
                    reference: 'fullname',
                    allowBlank: false,
                    validator: function (val) {
                        var errMsg = 'Use only \'a-z, A-Z, 0-9 and . -\' and at least 5 characters';
                        var reg = new RegExp("^[a-zA-Z0-9 .-]{5,100}$");
                        var valid = reg.test(val);
                        return valid ? true : errMsg;
                    }
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Short name',
                    name: 'shortname',
                    reference: 'shortname',
                    validator: function (val) {
                        var errMsg = 'Use only \'a-z, A-Z, 0-9\' and 3-15 characters without spaces.';
                        var reg = new RegExp("^[a-zA-Z0-9]{3,15}$");
                        var valid = reg.test(val);
                        return valid ? true : errMsg;
                    }
                },
                {
                    xtype: 'button',
                    text: 'Publish',
                    handler: 'onPublishOrganization',
                    formBind: true
                }
            ]
        }


    ]

});
