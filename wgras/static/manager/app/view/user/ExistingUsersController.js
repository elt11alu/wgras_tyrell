/**
 * Created by anton on 8/1/16.
 */

Ext.define('VramApp.view.user.ExistingUsersController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.existinguserscontroller',
    id : 'existinguserscontroller',

    listen:{
        controller:{
            '*':{
                onUserAdded: 'reloadGrid'
            }
        }
    },

    init: function(){

    },

    onEmail: function(btn){
        this.fireEvent('onEmail', btn.getWidgetRecord())
    },

    reloadGrid: function(){
        Ext.data.StoreManager.lookup('existingsusersstore').reload();
    },

    onNameFilterKeyup: function() {
        var grid = this.lookupReference('existingusersgrid'),
            // Access the field using its "reference" property name.
            //filterField = Ext.getCmp('nameFilterField'),
            filterField = this.lookupReference('nameFilterField'),
            filters = grid.store.getFilters();

        if (filterField.value) {
            this.nameFilter = filters.add({
                id            : 'nameFilter',
                property      : 'username',
                value         : filterField.value,
                anyMatch      : true,
                caseSensitive : false
            });
        } else if (this.nameFilter) {
            filters.remove(this.nameFilter);
            this.nameFilter = null;
        }
    },

    onDeleteUser: function(btn){
        var user = btn.getWidgetRecord();
        Ext.MessageBox.confirm('Confirm', 'Are you sure you want to delete user '.concat(user.get('username'), '?'), function(btn){
            if(btn === 'yes'){
                this.doDeleteUser(user)
            }
        }, this);


    },

    doDeleteUser: function(userRecord){
        var me = this;
        var request = $.ajax({
            url: '/users',
            method: 'DELETE',
            contentType: 'application/json',
            data: JSON.stringify({userId: userRecord.get('userId')})
        });

        request.done(function( resp ) {
            Ext.MessageBox.alert('Deleted', 'User '.concat(resp.user.username, ' was deleted!'))
            me.reloadGrid()
        });

        request.fail(function( resp, textStatus ) {
            Ext.MessageBox.alert('Error', resp.responseJSON.errorMessage)
        });

    }
});
