/**
 * Created by anton on 5/19/16.
 */

Ext.define('VramApp.view.user.editUser.UserEditController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.usereditcontroller',

    init:function(){
    },

    onEditUserSubmit: function(btn) {
        var me = this;
        var olduser = btn.up('window').userToEdit;
        var request = VramApp.utilities.HTTPRequests;
        var form = btn.up('form');
        var values = form.getValues();
        var userName = values.username;
        var userId = values.username
        var role = values.role;
        var pw1 = values.pw1;
        var pw2 = values.pw2;
        var email = values.email;

        if (pw1 === pw2) {
            var user = JSON.stringify({
                newusername: userName,
                newrole: role,
                newpassword: pw2,
                userToEdit: olduser.get('userId'),
                email:email
            });
            var url = '/users';
            var success = function(response, opts) {
                me.reloadGrid();
                me.setUserEditSuccessMessage('User \'' + userName + '\' edited');
            };

            var failure = function(response, opts) {
                var jsonResp = Ext.decode(response.responseText);
                me.setUserEditErrorMessage(jsonResp.errorMessage);
            };
            request.PUT(url, user, success, failure);

        } else {
            me.setUserEditErrorMessage('Passwords must match')
        }
    },

    setUserEditErrorMessage: function(msg) {
        this.lookupReference('editFormMsgField').setValue('<div style="color:red;">' + msg + '</div>');
        this.lookupReference('userGridMsgField').setValue('<div style="color:red;">' + msg + '</div>');
    },

    setUserEditSuccessMessage: function(msg) {
        this.lookupReference('editFormMsgField').setValue('<div style="color:green;">' + msg + '</div>');
        this.lookupReference('userGridMsgField').setValue('<div style="color:green;">' + msg + '</div>');
    },

    reloadGrid: function() {
        Ext.data.StoreManager.lookup('existingsusersstore').reload()
    }
});
