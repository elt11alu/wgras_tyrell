/**
 * Created by anton on 2015-12-27.
 */

Ext.define('VramApp.view.user.editUser.UserEditForm', {
    extend: 'Ext.form.Panel',
    xtype: 'userEditForm',

    requires: [
        'Ext.layout.container.Anchor',
        'VramApp.view.user.editUser.UserEditController'
    ],
    controller:'usereditcontroller',
    layout: 'anchor',
    defaults: {
        anchor: '80%',
        msgTarget: 'under',
        validateOnBlur: false
    },

    reference: 'usereditform',
    defaultType: 'textfield',
    items: [{
        fieldLabel: 'Username',
        name: 'username',
        allowBlank: false,
        validator: function (val) {
            var errMsg = 'Use only a-z, A-Z and 0-9 and at least 5 characters';
            var reg = new RegExp("^[a-zA-Z0-9]+$");
            var valid = reg.test(val);
            return valid && val.length >= 5 ? true : errMsg;
        }
    }, {
        fieldLabel: 'created',
        name: 'created',
        readOnly: true
    },{
        fieldLabel: 'Email',
        name: 'email',
        vtype: 'email',
        allowBlank:false,
        reference: 'emailField'
    },{
        fieldLabel: 'Country',
        name: 'country',
        vtype: 'alpha',
        allowBlank:false,
        readOnly:true,
        reference: 'countryField'
    }, {
        fieldLabel: 'Managementarea',
        name: 'managementarea',
        vtype: 'alpha',
        allowBlank:false,
        readOnly:true,
        reference: 'managementareaField'
    },{
        fieldLabel: 'Password',
        inputType:'password',
        name: 'pw1',
        reference: 'pw1Field',
        allowBlank: false,
        validator: function (val) {
            var errMsg = 'Use only a-z, A-Z, 0-9 and _ and at least 8 characters';
            var reg = new RegExp("^[a-zA-Z0-9_]+$");
            var valid = reg.test(val);
            return valid && val.length >= 8 ? true : errMsg;
        }
    }, {
        fieldLabel: 'Confirm password',
        name: 'pw2',
        inputType:'password',
        allowBlank: false,
        validateOnBlur: true,
        validator: function (val) {
            var errMsg = 'Use only a-z, A-Z, 0-9 and _ at least 8 characters';
            var reg = new RegExp("^[a-zA-Z0-9_]+$");
            var valid = reg.test(val);
            return valid && val.length >= 8 ? true : errMsg;
        }

    }, {
        fieldLabel: 'Role',
        reference:'rolecombo',
        xtype: 'combo',
        name: 'role',
        allowBlank: false,
        displayField:'rolename',
        valueField:'rolecode',
        forceSelection: true,
        store: {
            type:'roles'
        }
    },{
        xtype: 'displayfield',
        reference: 'editFormMsgField',
        readOnly: true
    }
    ],

    buttons: [{
        text: 'Save user',
        handler: 'onEditUserSubmit',
        formBind: true
    }]
});
