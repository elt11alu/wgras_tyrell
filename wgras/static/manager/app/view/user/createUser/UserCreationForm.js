Ext.define('VramApp.view.user.createUser.UserCreationForm', {
    extend: 'Ext.form.Panel',
    requires:[
        'VramApp.view.user.createUser.UserCreationController',
        'Ext.layout.container.Anchor'
    ],
    xtype: 'userCreationForm',
    controller:'usercreationcontroller',
    layout: 'anchor',
    defaults: {
        anchor: '80%',
        msgTarget: 'under',
        validateOnBlur: false
    },

    reference: 'usercreationform',
    defaultType: 'textfield',
    items: [{
        fieldLabel: 'Username',
        name: 'username',
        valueField: 'userId',
        allowBlank: false,
        validator: function (val) {
            var errMsg = 'Use only a-z, A-Z and 0-9 and at least 5 characters';
            var reg = new RegExp("^[a-zA-Z0-9]+$");
            var valid = reg.test(val);
            return valid && val.length >= 5 ? true : errMsg;
        }
    }, {
        fieldLabel: 'Email',
        name: 'email',
        vtype: 'email',
        allowBlank: false,
        reference: 'emailField'
    }, {
        fieldLabel: 'Country',
        reference: 'countrycombo',
        xtype: 'combo',
        name: 'country',
        displayField: 'countryName',
        valueField: 'countryId',
        allowBlank: false,
        forceSelection: true,
        store: {
            type: 'countries'
        },
        listeners: {
            select: {
                fn: function (combo, record) {
                    var countryId = record.get('countryId');
                    var url = '/users/country/'.concat(countryId);
                    var store = Ext.data.StoreManager.lookup('managementareabycountry');
                    store.load({
                        url: url
                    })
                }
            }
        }
    }, {
        fieldLabel: 'Managementarea',
        name: 'managementarea',
        id: 'managementareacombo',
        displayField: 'managementareaName',
        xtype: 'combo',
        allowBlank: false,
        valueField: 'managementareaId',
        forceSelection: true,
        reference: 'managementareaField',
        store: {
            type: 'managementareabycountry'
        }
    }, {
        fieldLabel: 'Password',
        inputType: 'password',
        name: 'pw1',
        reference: 'pw1Field',
        allowBlank: false,
        validator: function (val) {
            var errMsg = 'Use only a-z, A-Z, 0-9 and _ and at least 8 characters';
            var reg = new RegExp("^[a-zA-Z0-9_]+$");
            var valid = reg.test(val);
            return valid && val.length >= 8 ? true : errMsg;
        }
    }, {
        fieldLabel: 'Confirm password',
        inputType: 'password',
        name: 'pw2',
        allowBlank: false,
        validateOnBlur: true,
        validator: function (val) {
            var errMsg = 'Use only a-z, A-Z, 0-9 and _ at least 8 characters';
            var reg = new RegExp("^[a-zA-Z0-9_]+$");
            var valid = reg.test(val);
            return valid && val.length >= 8 ? true : errMsg;
        }

    }, {
        fieldLabel: 'Role',
        xtype: 'combo',
        name: 'role',
        displayField: 'rolename',
        valueField: 'rolecode',
        allowBlank: false,
        forceSelection: true,
        store: {
            type: 'roles'
        }
    }, {
        xtype: 'displayfield',
        reference: 'creationFormMsgField',
        readOnly: true
    }],

    buttons: [{
        text: 'Save user',
        handler: 'onCreateUserSubmit',
        formBind: true
    }]
});
