Ext.define('VramApp.view.map.Map', {
    extend: 'GeoExt.component.Map',
    xtype: 'mapView',
    id: 'mapView',
    border: 1,
    style: {
        borderColor: 'black',
        borderStyle: 'solid'
    },
    map: new ol.Map({
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            })],
        view: new ol.View({
            center: ol.proj.fromLonLat([ 21.0117800,52.2297700]), //lon,lat
            zoom: 4
        }),
        controls: ol.control.defaults({
          attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
            collapsible: false
          })
        }).extend([
          new ol.control.ZoomToExtent({
            extent: [-649972.9038032419, 2179730.6900624484, 5328014.204323823, 11503825.148401389]
          })
        ])
    })
});
