/**
 * Created by anton on 2016-08-09.
 */


var urls = {
    get_current_user: '/user/current',
    get_conversations: '/conversations',
    get_messages_in_conversation: '/conversation/',
    post_new_conversation: '/conversation',
    post_message_to_conversation: '/conversation/message',
    get_users: '/users'
};

var PageHeader = React.createClass({

    render: function () {
        return (
            <div className="page-header">
                <h1>Welcome to WGRAS<br></br>
                    <small>Web GIS Risk Assessment and Analysis System</small>
                </h1>
            </div>
        )
    }
});

var UserPanel = React.createClass({

    getInitialState: function () {
        return {
            userInfo: {}
        };
    },

    getCurrentUserFromServer: function () {
        $.ajax({
            url: urls.get_current_user,
            dataType: 'json',
            cache: false,
            success: function (data) {
                this.setState({userInfo: data});
            }.bind(this),
            error: function (xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    componentDidMount: function (a) {
        this.getCurrentUserFromServer();
        //setInterval(this.getConversationsFromServer(), 5000);
    },

    render: function () {
        
        return (
            <div className="panel panel-primary">
                <div className="panel-heading">
                    <h3 className="panel-title">Hello {this.state.userInfo.username}</h3>
                </div>
                <div className="panel-body">
                    <UserAccordion userInfo={this.state.userInfo}/>
                </div>
            </div>
        )
    }
});

var NavigationPanel = React.createClass({
    render: function () {
        return (
            <div className="panel panel-primary">
                <div className="panel-heading">
                    <h3 className="panel-title">Applications</h3>
                </div>
                <div className="panel-body">
                    <div className="row">
                        <div className="col-md-6">
                            <MapNavigationPanel/>
                        </div>
                        <div className="col-md-6">
                            <ManagerNavigationPanel/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

var MapNavigationPanel = React.createClass({
    render: function () {
        return (
            <div className="panel panel-default">
                <div className="panel-body">
                    <MapBtn/>
                </div>
            </div>
        )
    }
});

var ManagerNavigationPanel = React.createClass({
    render: function () {
        return (
            <div className="panel panel-default">
                <div className="panel-body">
                    <ManagerBtn/>
                </div>
            </div>
        )
    }
});

var MapBtn = React.createClass({
    render: function () {
        return (

            <a className="btn btn-default btn-lg" href="/map"><span className="glyphicon glyphicon-map-marker"
                                                                    aria-hidden="true"></span> Map</a>
        )
    }
});

var ManagerBtn = React.createClass({
    render: function () {
        return (
            <a className="btn btn-default btn-lg" href="/manager" role="button"><span className="glyphicon glyphicon-cog"
                                                                    aria-hidden="true"></span> Manager</a>
        )
    }
});

var UserAccordion = React.createClass({
    render: function () {
        return (

            <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <ConversationPanel userInfo={this.props.userInfo} url={urls.get_conversations}/>
                <PersonalPanel userInfo={this.props.userInfo}/>
            </div>

        )
    }
});

var PersonalPanel = React.createClass({


    render: function () {

        return (
            <div className="panel panel-default">
                <div className="panel-heading" role="tab" id="headingTwo">
                    <h4 className="panel-title">
                        <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <span className="glyphicon glyphicon-menu-right" aria-hidden="true"></span>&nbsp;Personal
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" className="panel-collapse collapse" role="tabpanel"
                     aria-labelledby="headingTwo">
                    <div className="panel-body">
                        <PersonalForm userInfo={this.props.userInfo}/>
                    </div>
                </div>
            </div>
        )
    }
});

var PersonalForm = React.createClass({


    render: function () {
        var userInfo = this.props.userInfo
        var personalInfoItems = Object.keys(userInfo).map(function (key) {
            if (key!=='userId'){
                var value = userInfo[key];
                return (
                <PersonalInfoItem key={key} infoKey={key} infoValue={value}/>
            )
            }




        });

        return (
            <form className="form-horizontal">
                {personalInfoItems}
            </form>
        )
    }
});

var PersonalInfoItem = React.createClass({

    render: function () {

        return (
            <div className="form-group">
                <label className="col-sm-2 control-label">{this.props.infoKey}:</label>
                <div className="col-sm-10">
                    <p className="form-control-static">{this.props.infoValue}</p>
                </div>
            </div>
        )
    }
});

var ConversationPanel = React.createClass({

    getInitialState: function () {
        return {
            conversations: [],
            messages: [],
            activeConversation: null,
            users: []
        };
    },

    getConversationsFromServer: function () {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            success: function (data) {
                this.setState({conversations: data.conversations});
            }.bind(this),
            error: function (xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    componentDidMount: function () {
        var me = this;
        this.getConversationsFromServer();
        this.getUsersFromServer();
        setInterval(function(){
            me.getConversationsFromServer();
            if(me.state.activeConversation!=null){
                me.getMessagesInConversationFromServer(me.state.activeConversation);
            }
        },2000)

    },

    getMessagesInConversationFromServer: function (conversationId) {
        $.ajax({
            url: urls.get_messages_in_conversation.concat(conversationId),
            dataType: 'json',
            cache: false,
            success: function (data) {
                this.setState({messages: data.conversation.messages, activeConversation: conversationId});
            }.bind(this),
            error: function (xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },

    getUsersFromServer: function () {
        $.ajax({
            url: urls.get_users,
            dataType: 'json',
            cache: false,
            success: function (data) {
                this.setState({users: data.users});
            }.bind(this),
            error: function (xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },

    postReplyToServer: function (data, conversationId) {

        $.ajax({
            url: urls.post_message_to_conversation,
            method: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            cache: false,
            data: data,
            success: function (resp) {
                console.log(resp);
                this.getMessagesInConversationFromServer(conversationId)
            }.bind(this),
            error: function (xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },

    postNewConversation: function (data) {
        $.ajax({
            url: urls.post_new_conversation,
            method: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            cache: false,
            data: data,
            success: function (resp) {
                this.getConversationsFromServer();
                $('#newMessageModal').modal('hide')
            }.bind(this),
            error: function (xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },

    onUpdateMessagePanel: function (conversationId) {
        this.getMessagesInConversationFromServer(conversationId)

    },
    onNewMessageClick: function () {
        $('#newMessageModal').modal('show')
    },

    onReplyToMessage: function (messageText) {
        var data = {};
        data.senderId = this.props.userInfo.userId;
        data.conversationId = this.state.activeConversation;
        data.messageText = messageText;
        console.log(this.state.activeConversation);
        this.postReplyToServer(JSON.stringify(data), data.conversationId);


    },

    onStartNewConversation: function (values) {
        values.senderId = this.props.userInfo.userId;
        this.postNewConversation(JSON.stringify(values))
    },

    render: function () {

        return (
            <div className="panel panel-default">
                <div className="panel-heading" role="tab" id="headingOne">
                    <h4 className="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                           aria-expanded="true" aria-controls="collapseOne">
                            <span className="glyphicon glyphicon-menu-right" aria-hidden="true"></span>&nbsp;Messages
                            <span className="badge">{this.state.conversations.length}</span>
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" className="panel-collapse collapse" role="tabpanel"
                     aria-labelledby="headingOne">
                    <div className="panel-body">
                        <ConversationsPanel onNewMessageClick={this.onNewMessageClick}
                                            onUpdateMessagePanel={this.onUpdateMessagePanel}
                                            conversations={this.state.conversations} currentUser={this.props.userInfo}/>
                        <MessagePanel activeConversation={this.state.activeConversation}
                                      onReplyToMessage={this.onReplyToMessage} messages={this.state.messages}
                                      currentUser={this.props.userInfo}/>
                        <MessageModal users={this.state.users} onStartNewConversation={this.onStartNewConversation}/>
                    </div>
                </div>
            </div>
        )
    }
});

var MessagePanel = React.createClass({
    render: function () {
        return (
            <div className="col-md-5">
                <div className="panel panel-default">
                    <div className="panel-heading">Messages in thread</div>
                    <div className="panel-body">
                        <MessageListPanel messages={this.props.messages} currentUser={this.props.currentUser}/>
                        <br/>
                        <ReplyForm onReplyToMessage={this.props.onReplyToMessage} messages={this.props.messages}
                                   activeConversation={this.props.activeConversation}/>
                    </div>
                </div>
            </div>
        )
    }
});

var MessageListPanel = React.createClass({

    render: function () {
        var divStyle = {
            height: 200,
            overflowY: 'auto'
        };
        var messages = null;
        var currentUser = this.props.currentUser;
        if (this.props.messages != null) {
            messages = this.props.messages.map(function (message) {
                return (

                    <Message key={message.id} message={message.message} sender={message.sender}
                             currentUser={currentUser}/>

                )
            });
        }

        return (

            <div className="panel panel-default" style={divStyle}>
                <div className="panel-body">
                    {messages}
                </div>
            </div>

        )
    }
});

var Message = React.createClass({

    render: function () {
        var currentUserStyle = {
            backgroundColor: '#66a3ff',
            borderRadius:4,
            padding:5

        };
        var defaultStyle = {
            backgroundColor: '#b3b3b3',
            padding:5,
            borderRadius:4
        };
        var sender = this.props.sender.username === this.props.currentUser.username ? 'You' : this.props.sender.username;
        var divStyle = this.props.sender.username === this.props.currentUser.username ? currentUserStyle : defaultStyle;
        var className = this.props.sender.username === this.props.currentUser.username ? 'col-md-5 col-sm-5 col-xs-5 pull-right':'col-md-5 col-xs-5 col-sm-5 pull-left';
        return (
            <div className="row">
                <div className={className}><i><small>{sender} wrote:</small></i>
            <div key={this.props.message.id} style={divStyle}>
                {this.props.message}
            </div>
                    </div>
                </div>
        )


    }
});

var ConversationsPanel = React.createClass({

    render: function () {

        return (
            <div className="col-md-5">
                <div className="panel panel-default">
                    <div className="panel-heading">Active Threads&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<AddConversationButton
                        onNewMessageClick={this.props.onNewMessageClick}/></div>
                    <div className="panel-body">
                        <ConversationList onUpdateMessagePanel={this.props.onUpdateMessagePanel}
                                          conversations={this.props.conversations} currentUser={this.props.currentUser}/>
                    </div>
                </div>

            </div>
        )
    }

});

var AddConversationButton = React.createClass({

    onNewMessageClick: function () {
        this.props.onNewMessageClick()
    },

    render: function () {

        return (
            <button onClick={this.onNewMessageClick} type="button" className="btn btn-default btn-sm">&nbsp;<span
                className="glyphicon glyphicon-plus" aria-hidden="true"></span> New</button>
        )
    }

});

var ConversationList = React.createClass({

    render: function () {
        var divStyle = {
            height: 250,
            overflowY: 'auto'
        };

        var onUpdateMessagePanel = this.props.onUpdateMessagePanel;
        var currentUser = this.props.currentUser;

        var conversations = this.props.conversations;

        var conversationItems = conversations.map(function (conv) {

            return (

                <div key={conv.conversationId}>
                    <ConversationItem onUpdateMessagePanel={onUpdateMessagePanel} conversations={conv}
                                      key={conv.conversationId}
                                      title={conv.title} sender={conv.messages[0].sender}
                                      messages={conv.messages} conversationId={conv.conversationId} currentUser={currentUser}></ConversationItem>
                </div>
            );
        });
        return (

            <div className="list-group" style={divStyle}>
                {conversationItems}
            </div>

        )


    }
});


var ConversationItem = React.createClass({
    onUpdateMessagePanel: function () {
        this.props.onUpdateMessagePanel(this.props.conversationId)
    },
    render: function () {
        var sender = this.props.sender.username === this.props.currentUser.username?'You':this.props.sender.username;
        return (

            <button onClick={this.onUpdateMessagePanel} type="button" className="list-group-item">
                <span className="glyphicon glyphicon-envelope" aria-hidden="true"></span>&nbsp;
                <b>Subject:</b> {this.props.title}<b> From:</b> {sender}
            </button>

        )
    }
});

var MessageModal = React.createClass({

    render: function () {


        return (
            <div className="modal fade" tabIndex="-1" role="dialog" id='newMessageModal'>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                            <h4 className="modal-title">New message thread</h4>
                        </div>
                        <div className="modal-body">
                            <div className="panel panel-default">
                                <div className="panel-body">
                                    <NewMessageForm users={this.props.users}
                                                    onStartNewConversation={this.props.onStartNewConversation}/>
                                </div>
                            </div>
                            <br/>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

var NewMessageForm = React.createClass({

    getInitialState: function () {
        return {
            replyMessage: '',
            replySubject: '',
            recipient: '',
            formIsValid: true
        };
    },
    handleTextChange: function (e) {
        this.setState({replyMessage: e.target.value, formIsValid: true});
    },

    handleSubjectChange: function (e) {
        this.setState({replySubject: e.target.value, formIsValid: true});
    },


    handleSubmit: function (event) {
        event.preventDefault();
        var messageText = this.state.replyMessage.trim();
        var subjectText = this.state.replySubject.trim();
        var recipientId = this.refs.userSelect.value;

        if (this.checkMessageInput(messageText) && this.checkSubjectInput(subjectText)) {
            this.props.onStartNewConversation({
                messageText: messageText,
                subjectText: subjectText,
                recipientId: recipientId
            });
            this.setState({
                replyMessage: '',
                replySubject: '',
                recipient: ''
            });
        } else {
            this.setState({formIsValid: false})
        }
    },

    checkMessageInput: function (messageText) {
        var regex = /^[A-Za-z0-9 .,_/\n/'!?-@()/"]{1,1000}$/;
        return regex.test(messageText)
    },

    checkSubjectInput: function (subjectText) {
        var regex = /^[A-Za-z0-9 .,_/\n/'!?-@()]{1,100}$/;
        return regex.test(subjectText)
    },

    checkRecipientInput: function (recipientId) {
        var regex = /^[0-9]{1,3}$/;
        return regex.test(recipientId)
    },

    isDisabled: function () {
        return !(this.props.activeConversation != null);
    },

    render: function () {

        var options = this.props.users.map(function (user) {
            return (
                <Recipient key={user.userId} user={user}/>
            )
        });

        return (



            <form role="form" onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <label htmlFor="userSelect">To:</label>
                    <select className="form-control" id="userSelect" ref="userSelect">
                        {options}
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="newMessageSubject">Subject:</label>
                    <input type="text" className="form-control"
                           id="newMessageSubject" placeholder="Subject" onChange={this.handleSubjectChange}
                           value={this.state.replySubject}/>
                </div>
                <div className="form-group">
                    <label htmlFor="newMessageText">Message:</label>
            <textarea className="form-control" rows="3" id="newMessageText" placeholder="Message"
                      onChange={this.handleTextChange} value={this.state.replyMessage}></textarea>
                </div>

                <button type="submit" disabled={!this.state.formIsValid} className="btn btn-default"><span
                    className="glyphicon glyphicon-chevron-right"
                    aria-hidden="true"></span>Submit
                </button>
                <br/><br/>
                {!this.state.formIsValid ?
                    <div className="alert alert-danger" role="alert">Fields are empty or contain unallowed
                        characters.</div> : null}
            </form>

        )


    }
});

var Recipient = React.createClass({
    render: function () {
        return (
            <option value={this.props.user.userId}>{this.props.user.username}</option>
        )
    }
});


var ReplyForm = React.createClass({

    getInitialState: function () {
        return {replyMessage: ''};
    },
    handleTextChange: function (e) {
        this.setState({replyMessage: e.target.value});
    },

    handleSubmit: function (event) {
        event.preventDefault();
        var messageText = this.state.replyMessage.trim()
        if (this.checkMessageInput(messageText)) {
            this.props.onReplyToMessage(messageText);
            this.setState({replyMessage: ''})
        }
    },

    checkMessageInput: function (messageText) {
        var regex = /^[A-Za-z0-9 .,_/\n/'!?-@()/"]{1,1000}$/;
        return regex.test(messageText)
    },

    isDisabled: function () {
        return !(this.props.activeConversation != null);
    },

    render: function () {
        return (
            <form className="form-horizontal" onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <div className="col-md-12">
                        <input type="text" className="form-control" id="replytext" placeholder="Reply here"
                               onChange={this.handleTextChange} value={this.state.replyMessage}
                               disabled={this.isDisabled()}/>
                    </div>
                </div>
                <div className="form-group">
                    <div className="col-md-2">
                        <button type="submit" className="btn btn-default" disabled={this.isDisabled()}>Reply</button>
                    </div>
                </div>
            </form>

        )


    }
});

var Container = React.createClass({
    render: function () {
        return (
            <div className="container">
                <PageHeader/>
                <UserPanel/>
                <NavigationPanel/>

            </div>
        )
    }
});


var container = React.createElement(Container, null);

ReactDOM.render(container, document.getElementById('content'))
