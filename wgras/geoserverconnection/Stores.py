from .Base import GeoserverConnection
from ..postgisconnection import postgisConfig
import logging
from .. import app
import random


class GsStore(GeoserverConnection):
    def __init__(self):
        super(GsStore, self).__init__()

    def getStores(self, workspaceName):
        return self.geoServerCatalog.get_stores(workspace=workspaceName)

    def getStore(self, storeName, workspaceName):
        return self.geoServerCatalog.get_store(storeName, workspaceName)

    def getStoreNames(self):
        return [store.name for store in self.getStores()]

    def dispatch_store(self, workspace_name):
        stores_in_workspace = self.getStores(workspaceName=workspace_name)
        for store in stores_in_workspace:
            #app.logger.info('store type:' + str(store.type))
            #if store.type == 'PostGIS':
            if store.type == 'PostGIS (JNDI)':
                if len(store.get_resources()) > 200:
                    app.logger.info('{}{}{}'.format('Store contains: ', len(store.get_resources()), 'Store is full'))
                    return None
                else:
                    # app.logger.info(
                    #     '{}{}{}{}'.format('Store contains: ', len(store.get_resources()), ' Store is reused ',
                    #                       store.name))
                    return store
        #app.logger.info('{}'.format(' No store in workspace {}'.format(workspace_name)))
        return None

    def createPostGisStore(self, workspaceName, store_name, schema):
        try:

            store = self.dispatch_store(workspace_name=workspaceName)
            if store is not None:
                return store.name
            else:
                #app.logger.info('{}{}{}{}'.format('Creating store with name', store_name, ' in workspace ', workspaceName))
                catalog = self.geoServerCatalog
                catalog.reload()
                ds = catalog.create_datastore(store_name, workspaceName)
                # ds.connection_parameters.update({'host': postgisConfig.DB_SERVER,
                #                                  'port': postgisConfig.DB_PORT,
                #                                  'database': postgisConfig.DB_NAME,
                #                                  'user': postgisConfig.DB_USER,
                #                                  'passwd': postgisConfig.DB_PASSWORD,
                #                                  'dbtype': postgisConfig.DB_TYPE,
                #                                  'schema': schema,
                #                                  "Expose primary keys": 'true'})

                ds.connection_parameters.update({'jndiReferenceName': 'java:comp/env/jdbc/postgres',
                                                 'dbtype': postgisConfig.DB_TYPE,
                                                 'schema': schema,
                                                 "Expose primary keys": 'true'})

                catalog.save(ds)
                #catalog.reload()
                created_store = self.getStore(storeName=ds.name, workspaceName=workspaceName)
            if created_store is not None:
                return created_store.name
            return None
        except Exception as e:
            app.logger.error('Exception in Stores.py function createPostGisStore ' + e.message)
            return None

    # def dispatch_store(self, workspace_name):
    #     stores_in_workspace = self.getStores(workspaceName=workspace_name)
    #     for store in stores_in_workspace:
    #         if store.type == 'PostGIS':
    #             if len(store.get_resources()) > 10:
    #                 app.logger.info('{}{}{}'.format('Store contains: ', len(store.get_resources()), 'Store is full'))
    #                 return None
    #             else:
    #                 app.logger.info(
    #                     '{}{}{}{}'.format('Store contains: ', len(store.get_resources()), ' Store is reused ',
    #                                       store.name))
    #                 return store
    #     app.logger.info('{}'.format(' No store in workspace {}'.format(workspace_name)))
    #     return None
    #
    # def createPostGisStore(self, workspaceName, store_name, schema):
    #     try:
    #
    #         store = self.dispatch_store(workspace_name=workspaceName)
    #         if store is not None:
    #             return store.name
    #         else:
    #             app.logger.info('{}{}{}{}'.format('Creating store with name', store_name, ' in workspace ', workspaceName))
    #             catalog = self.geoServerCatalog
    #             catalog.reload()
    #             ds = catalog.create_datastore(store_name, workspaceName)
    #             ds.connection_parameters.update({'host': postgisConfig.DB_SERVER,
    #                                              'port': postgisConfig.DB_PORT,
    #                                              'database': postgisConfig.DB_NAME,
    #                                              'user': postgisConfig.DB_USER,
    #                                              'passwd': postgisConfig.DB_PASSWORD,
    #                                              'dbtype': postgisConfig.DB_TYPE,
    #                                              'schema': schema,
    #                                              "Expose primary keys": 'true'})
    #             catalog.save(ds)
    #             catalog.reload()
    #             created_store = self.getStore(storeName=ds.name, workspaceName=workspaceName)
    #         if created_store is not None:
    #             return created_store.name
    #         return None
    #     except Exception as e:
    #         app.logger.error('Exception in Stores.py function createPostGisStore ' + e.message)
    #         return None

    def createCoverageStore(self, datastoreName, workspace_name, filePath):
        catalog = self.geoServerCatalog
        workspace = catalog.get_workspace(name=workspace_name)
        if workspace is not None:
            catalog.create_coveragestore(name=datastoreName, workspace=workspace, data=filePath)
            catalog.reload()
            created_store = self.getStore(datastoreName, workspace)
            if created_store is not None:
                return created_store.name
            app.logger.info('Could not create store {}'.format(datastoreName))
            return None
        app.logger.info('Could retrieve workspace {}'.format(workspace_name))
        return None

    def publishFeatureType(self, layerName, datastoreName, workspaceName, epsgCode):

        try:
            catalog = self.geoServerCatalog
            catalog.reload()
            wsName = workspaceName
            workspace = catalog.get_workspace(wsName)
            ds = catalog.get_store(datastoreName, workspace=workspace)
            if ds is not None:
                ft = catalog.publish_featuretype(name=layerName, store=ds, native_crs=epsgCode, srs=epsgCode)
                if ft is not None:
                    catalog.save(ft)
                    catalog.reload()
                    created = catalog.get_resource(name=layerName, store=ds, workspace=workspace)
                    if created is not None:
                        return created.name
                    return None
                return None
            return None
        except Exception as e:
            app.logger.warning('Exception in Stores.py function publishFeatureType ' + e.message)
            return None
