from .Base import GeoserverConnection
from .. import app
import requests
from requests.auth import HTTPBasicAuth


class BboxFactory(GeoserverConnection):
    def __init__(self):
        super(BboxFactory, self).__init__()

    def get_bbox_for_resource(self, resource_name, workspace_name):
        ''' Returns [minx, maxx, miny, maxy] '''
        
        catalog = self.geoServerCatalog
        catalog.reload()
        resource = self.getResource(resourceName=resource_name, workspaceName=workspace_name)
        if resource is not None:
            return resource.latlon_bbox
        app.logger.info('Could not find resource with name {}'.format(resource_name))
        return None

    def recalculate_bbox(self, resource_name, workspace_name):
        catalog = self.geoServerCatalog
        catalog.reload()
        resource = self.getResource(resourceName=resource_name, workspaceName=workspace_name)
        if resource is not None:
            auth = HTTPBasicAuth(self.GS_USER, self.GS_PW)
            href = '{}'.format(resource.href)
            feature_type_xml = requests.get(href, auth=auth)

            app.logger.info(feature_type_xml.status_code)
            if feature_type_xml.status_code == 200:

                headers = {'Content-Type': 'application/xml'}
                requests.put(href, params={'recalculate': 'nativebbox,latlonbbox'},
                             data=feature_type_xml.text, headers=headers, auth=auth)
                catalog.reload()
                return self.get_bbox_for_resource(resource_name=resource_name, workspace_name=workspace_name)
            app.logger.warning('Could not update bbox for {}'.format(resource_name))
        app.logger.warning('Could not find resource {}'.format(resource_name))

    def set_bbox_on_resource(self, resource_name, workspace_name, minx, maxx, miny, maxy):
        catalog = self.geoServerCatalog
        catalog.reload()
        resource = self.getResource(resourceName=resource_name, workspaceName=workspace_name)
        if resource is not None:
            proj = resource.latlon_bbox[4]
            resource.latlon_bbox = (str(minx), str(maxx), str(miny), str(maxy), proj)
            catalog.save(resource)
            catalog.reload()

    def set_bbox_on_resource_to_max(self, resource_name, workspace_name):
        catalog = self.geoServerCatalog
        catalog.reload()
        resource = self.getResource(resourceName=resource_name, workspaceName=workspace_name)
        if resource is not None:
            proj = resource.latlon_bbox[4]
            resource.latlon_bbox = (str(-180), str(180), str(-90), str(90), proj)
            catalog.save(resource)
            catalog.reload()
