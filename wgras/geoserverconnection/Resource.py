from .Base import GeoserverConnection


class Resource(GeoserverConnection):
    def __init__(self):
        super(Resource, self).__init__()

    def get_gs_resource(self, layer_name, workspace_name):
        return self._get_catalog().get_resource(name=layer_name, workspace=workspace_name)

    def get_gs_resources(self):
        return self._get_catalog().get_resources()

    def get_gs_resources_in_workspace(self, workspace_name):
        return self._get_catalog().get_resources(workspace=workspace_name)

    def get_gs_resources_in_store(self, store_name):
        return self._get_catalog().get_resources(store=store_name)

    def get_gs_resources_metadata(self):
        return [self._create_metadata_obj(resource) for resource in self.get_gs_resources()]

    def get_gs_resource_metadata(self, layer_name, workspace_name):
        resource = self._get_catalog().get_resource(name=layer_name, workspace=workspace_name)
        return self._create_metadata_obj(resource=resource)

    def _create_metadata_obj(self, resource):
        return {
            'abstract': resource.abstract,
            'advertised': resource.advertised,
            'attributes': resource.attributes,
            'enabled': resource.enabled,
            'href': resource.href,
            'keywords': resource.keywords,
            'latlon_bbox': resource.latlon_bbox,
            'metadata': resource.metadata,
            'metadata_links': resource.metadata_links,
            'name': resource.name,
            'native_bbox': resource.native_bbox,
            'native_name': resource.native_name,
            'projection': resource.projection,
            'projection_policy': resource.projection_policy,
            'resource_type': resource.resource_type,
            'store': resource.store.name,
            'title': resource.title,
            'url_part_stores': resource.url_part_stores,
            'url_part_types': resource.url_part_types,
            'workspace': resource.workspace.name
        }

    def _get_catalog(self):
        cat = self.geoServerCatalog
        cat.reload()
        return cat
