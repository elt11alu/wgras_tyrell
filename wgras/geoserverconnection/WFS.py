from Base import GeoserverConnection
from owslib.wfs import WebFeatureService
from ..restmodels import BaseRestModel

class WFSConnection(GeoserverConnection):
    def __init__(self):
        try:
            super(WFSConnection, self).__init__()
            self.service = WebFeatureService(url='{}/wfs'.format(self.GS_URI), version='1.1.0')
        except Exception as e:
            'Exception in WFS.py __init__, ', e

    def getAllWFSMetadata(self):
        capabilities = self.service
        layers = []

        for key, val in capabilities.contents.iteritems():
            model = self._getModel(val)
            layers.append(model.getDict())
        return {'layers': layers}

    def getWFSMetadata(self, name):
        capabilities = self.service
        try:
            val = capabilities.contents[name]
            model = self._getModel(val)
            return model.getDict()
        except Exception as e:
            return None

    ####################################
    #          Private methods         #
    #                                  #
    ####################################

    def _getModel(self, val):
        return BaseRestModel.BaseRestModel(
            layerName=val.id,
            layerTitle=val.title,
            workspaceName=val.id.split(':')[0],
            layerInfo=val.abstract,
            bboxMinX=val.boundingBoxWGS84[0],
            bboxMinY=val.boundingBoxWGS84[1],
            bboxMaxX=val.boundingBoxWGS84[2],
            bboxMaxY=val.boundingBoxWGS84[3],
            keywords=val.keywords

        )