from systemaccess import accessControl
from .appexceptions.AuthenticationException import AuthenticationException
from service import geoserverservice, layerservice, postgisservice, processservice, userservice, workspaceservice, \
    postgis_geoserver_link, mongodbservice


class WgrasService(object):
    def __init__(self):
        pass

    # --- LAYER METHODS --- #

    def get_vector_layers_metadata(self, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.get_vector_layers_metadata()

    def get_vector_layer_metadata_by_name(self, name, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.get_vector_layer_metadata_by_name(name=name)

    def get_vector_layer_metadata_by_id(self, layer_id, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.get_vector_layer_metadata_by_id(layer_id=layer_id)
        pass

    def get_raster_layers_metadata(self, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.get_raster_layers_metadata()

    def get_raster_layer_metadata_by_name(self, user=None, name=None):
        layer_service = layerservice.Layerservice()
        return layer_service.get_raster_layer_metadata_by_name(name=name)

    def get_raster_layer_metadata_by_id(self, user=None, layer_id=None):
        layer_service = layerservice.Layerservice()
        return layer_service.get_raster_layer_metadata_by_id(layer_id=layer_id)

    def get_wcs_layers_metadata(self, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.get_wcs_layer_metadata()

    def get_wcs_layer_metadata_by_name(self, user=None, name=None):
        layer_service = layerservice.Layerservice()
        return layer_service.get_wcs__layer_metadata_by_name(name=name)

    def get_elevation_layers_metadata(self, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.get_elevation_layers_metadata()

    def create_ows_metadata(self, name, url, description, category, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.create_ows_metadata(name, url, description, category)

    def delete_ows_metadata(self, ows_id, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.delete_ows_metadata(ows_id=ows_id)

    def create_vector_layer_from_shp_file(self, mongo_doc_id, layer_name, workspace_id, category, description,
                                                  schema, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.create_vector_layer_from_shp_file(document_id=mongo_doc_id, layer_name=layer_name,
                                                               workspaceId=workspace_id, published=True,
                                                               schema=schema, category=category,
                                                               description=description)

    def read_csv(self, file_path, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.readCSV(filePath=file_path)

    def create_vector_layer_from_csv_file(self, file_path, csv_file_name, table_name, workspace_id, schema, description,
                                          category,
                                          lat_field_name='lat',
                                          lon_field_name='lon', user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.create_vector_layer_from_csv_file(filePath=file_path, csvFileName=csv_file_name,
                                                               tableName=table_name, workspaceId=workspace_id,
                                                               latField=lat_field_name, lonField=lon_field_name,
                                                               published=True, schema=schema, description=description,
                                                               category=category)

    def create_new_vector_layer_with_geometry(self, workspace_id, layer_name_dirty, layer_title, geometry_type,
                                              schema, fields, geometry, published, description, category, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.create_new__vector_layer_with_geometry(workspace_id, layer_name_dirty=layer_name_dirty,
                                                                    layer_title=layer_title,
                                                                    geometry_type=geometry_type, schema=schema,
                                                                    fields=fields, geometry=geometry,
                                                                    published=published,
                                                                    description=description, category=category)

    def create_new_empty_vector_layer(self, workspace_id, layer_name_dirty, layer_title, geometry_type, schema,
                                      fields, published, description, category, user=None):
        layer_service = layerservice.Layerservice()

        return layer_service.create_new_empty_vector_layer(workspace_id=workspace_id, layer_name_dirty=layer_name_dirty,
                                                           layer_title=layer_title, geometry_type=geometry_type,
                                                           schema=schema, fields=fields, published=published,
                                                           description=description, category=category)

    def create_raster_layer_from_geotiff(self, file_path, workspace_id, layer_name, published, description, category,
                                         haselevationdata=False, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.create_raster_layer_from_geotiff(file_path=file_path, workspace_id=workspace_id,
                                                              layer_name_dirty=layer_name, published=published,
                                                              haselevationdata=haselevationdata,
                                                              description=description,
                                                              category=category)

    def insert_geojson_point_in_layer(self, layer_id, geojson_point, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.insert_geojson_point_in_layer(layer_id=layer_id, geojson_point=geojson_point)

    def insert_geojson_line_in_layer(self, layer_id, geojson_line, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.insert_geojson_line_in_layer(layer_id=layer_id, geojson_line=geojson_line)

    def insert_geojson_polygon_in_layer(self, layer_id, geojson_polygon, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.insert_geojson_polygon_in_layer(layer_id=layer_id, geojson_polygon=geojson_polygon)

    def insert_wkt(self, user=None):
        layer_service = layerservice.Layerservice()
        pass

    def get_attributes_for_layer_by_name(self, layer_name, workspace_name, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.get_attributes_for_layer_by_name(layerName=layer_name, workspaceName=workspace_name)

    def get_attributes_for_layer_by_id(self, layer_id, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.get_attributes_for_layer_by_id(layer_id=layer_id)

    def publish_layer(self, layer_name, workspace_name, publish, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.publishLayer(layerName=layer_name, workspaceName=workspace_name, publish=publish)

    def get_published_layers(self, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.getPublishedLayers()

    def get_unpublished_layers(self, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.getUnpublishedLayers()

    def get_keywords_for_layer(self, layer_name, workspace_name, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.getKeywordsForLayer(layerName=layer_name, workspaceName=workspace_name)

    def update_properties_for_layer(self, layer_name, workspace_name, properties, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.updatePropertiesForLayer(layerName=layer_name, workspaceName=workspace_name,
                                                      properties=properties)


    def update_layer_metadata(self, layer_id, description, category, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.update_layer_metadata(layer_id=layer_id, description=description, category=category)


    def is_geomtype_supported(self, geom_type, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.is_geomtype_supported(geom_type=geom_type)

    def is_attribute_type_supported(self, attribute_type, user=None):
        layer_service = layerservice.Layerservice()
        return layer_service.is_attribute_type_supported(attribute_type=attribute_type)

        # --- POSTGIS METHODS --- #
        # def create_db_layer(self, geom_type, name, title, workspaceId, store_name):
        #     pass
        #
        # def create_point_table_from_geojson(self, layer_name, schema_name, points, field_definition):
        #     pass
        #
        # def create_line_table_from_geojson(self, layer_name, schema_name, lines, field_definition):
        #     pass
        #
        # def create_polygon_table_from_geojson(self, layer_name, schema_name, polygons, field_definition):
        #     pass
        #
        # def create_empty_point_table(self, layer_name, schema_name, fields):
        #     pass
        #
        # def insert_geojson_point(self, layer_id, geojson_point):
        #     pass
        #
        # def create_empty_line_table(self, layer_name, schema_name, fields):
        #     pass
        #
        # def insert_geojson_line(self, layer_id, geojson_line):
        #     pass
        #
        # def create_empty_polygon_table(self, layer_name, schema_name, fields):
        #     pass
        #
        # def insert_geojson_polygon(self, layer_id, geojson_polygon):
        #     pass
        #
        # def _recalculate_bbox(self, layer_name):
        #     pass
        #
        # def _set_world_bbox(self, layer_name):
        #     pass

        # --- GEOSERVER METHODS --- #

        # def getAllWMSMetadata(self):
        #     pass
        #
        # def getWMSMetadata(self, name):
        #     pass
        #
        # def getAllWCSMetadata(self):
        #     pass
        #
        # def getWCSMetadata(self, name):
        #     pass
        #
        # def getAllWFSMetadata(self):
        #     pass
        #
        # def getWFSMetadata(self, name):
        #     pass

    def get_resource_repr(self, layer_name, workspace_name, user=None):
        gs_service = geoserverservice.GeoServerService()
        return gs_service.get_resource_repr(layer_name=layer_name, workspace_name=workspace_name)

    def get_resources_repr(self, user=None):
        gs_service = geoserverservice.GeoServerService()
        return gs_service.get_resources_repr()

        # def get_gs_workspace_names(self):
        #     pass
        #
        # def create_gs_workspace(self, workspaceName):
        #     pass
        #
        # def delete_gs_workspace(self, workspaceName):
        #     pass
        #
        # def create_gs_coverage_store(self, store_name, workspace_name, file_path):
        #     pass
        #
        # def publish_postgis_table_in_geoserver(self, table_name, workspaceId, geom_type):
        #     pass
        #
        # def get_published(self):
        #     pass
        #
        # def get_unpublished(self):
        #     pass
        #
        # def set_published(self, layer_name, workspace_name, publish=True):
        #     pass
        #
        # def get_keywords(self, layerName, workspaceName):
        #     pass
        #
        # def update_properties(self, layerName, workspaceName, properties):
        #     pass
        #
        # def recalculate_bbox(self, layer_name):
        #     pass
        #
        # def set_world_bbox(self, layer_name):
        #     pass
        #
        # def download_wcs(self, layerName):
        # pass

    # --- WORKSPACE METHODS --- #

    def create_country_workspace(self, isocode, user=None):
        ws_service = workspaceservice.WorkspaceService()
        return ws_service.createCountryWorkspace(isocode=isocode)

    def delete_country_workspaces(self, workspace_names, user=None):
        ws_service = workspaceservice.WorkspaceService()
        return ws_service.deleteCountryWorkspaces(workspaceNames=workspace_names)

    def create_org_workspace(self, isocode, orgid, user=None):
        ws_service = workspaceservice.WorkspaceService()
        return ws_service.createOrganizationWorkspace(isocode=isocode, orgid=orgid)

    def delete_org_workspaces(self, workspace_names, user=None):
        ws_service = workspaceservice.WorkspaceService()
        return ws_service.deleteOrgWorkspaces(workspaceNames=workspace_names)

    def get_workspace_names(self, user=None):
        ws_service = workspaceservice.WorkspaceService()
        return ws_service.get_workspace_names()

    def get_gs_workspace_names_as_list(self, user=None):
        ws_service = workspaceservice.WorkspaceService()
        return ws_service.get_gs_workspace_names_as_list()

    def get_workspace_objects(self, user=None):
        ws_service = workspaceservice.WorkspaceService()
        return ws_service.get_workspace_objects()

    def get_country_workspaces(self, isocode, user=None):
        ws_service = workspaceservice.WorkspaceService()
        return ws_service.get_country_workspaces(isocode=isocode)

    def get_org_workspaces(self, user=None):
        ws_service = workspaceservice.WorkspaceService()
        return ws_service.get_org_workspaces()

    def get_public_workspaces(self, user=None):
        ws_service = workspaceservice.WorkspaceService()
        return ws_service.get_public_workspaces()

    def get_private_workspaces(self, user_id, user=None):
        ws_service = workspaceservice.WorkspaceService()
        return ws_service.get_private_workspaces(user_id=user_id)

    def get_admin_workspaces(self, user=None):
        ws_service = workspaceservice.WorkspaceService()
        return ws_service.get_admin_workspaces()

    def get_admin_workspace_for_admin(self, adminid, user=None):
        ws_service = workspaceservice.WorkspaceService()
        return ws_service.get_admin_workspace_for_admin(adminid=adminid)

    def delete_workspace_in_gs(self, name, user=None):
        ws_service = workspaceservice.WorkspaceService()
        return ws_service.delete_workspace_in_gs(name=name)

    # --- USER METHODS --- #

    def get_countries(self, user=None):
        u_service = userservice.UserService()
        return u_service.getCountries()

    def get_country(self, isocode, user=None):
        u_service = userservice.UserService()
        return u_service.getCountry(isocode=isocode)

    def create_country(self, isocode, user=None):
        u_service = userservice.UserService()
        return u_service.createCountry(isocode=isocode)

    def delete_country(self, isocode, user=None):
        u_service = userservice.UserService()
        return u_service.deleteCountry(isocode=isocode)

    def get_organizations(self, user=None):
        u_service = userservice.UserService()
        return u_service.getOrganizations()

    def get_organization(self, orgid, user=None):
        u_service = userservice.UserService()
        return u_service.getOrganization(orgid=orgid)

    def create_organization(self, isocode, fullname, shortname, user=None):
        u_service = userservice.UserService()
        return u_service.createOrganization(isocode=isocode, fullname=fullname, shortname=shortname)

    def delete_organization(self, orgid, user=None):
        u_service = userservice.UserService()
        return u_service.deleteOrganization(orgid=orgid)

    def get_users(self, user=None):
        u_service = userservice.UserService()
        return u_service.getUsers()

    def get_user(self, username=None, user_id=None, user=None):
        u_service = userservice.UserService()
        return u_service.getUser(username=username, userid=user_id)

    def get_user_objects(self, user=None):
        u_service = userservice.UserService()
        return u_service.getUsersObj()

    def get_superadmins(self, user=None):
        u_service = userservice.UserService()
        return u_service.getSuperAdmins()

    def get_all_country_admins(self, user=None):
        u_service = userservice.UserService()
        return u_service.getCountryAdmins()

    def get_country_admins_by_isocode(self, isocode, user=None):
        u_service = userservice.UserService()
        return u_service.getCountryAdminsByIsocode(isocode=isocode)

    def create_country_admin(self, username, password, email, isocode, user=None):
        u_service = userservice.UserService()
        return u_service.createCountryAdmin(username=username, password=password, email=email, isocode=isocode)

    def delete_country_Admin(self, userid, user=None):
        u_service = userservice.UserService()
        return u_service.deleteCountryAdmin(userid=userid)

    def get_country_users(self, user=None):
        u_service = userservice.UserService()
        return u_service.getCountryUsers()

    def get_country_users_by_isocode(self, isocode, user=None):
        u_service = userservice.UserService()
        return u_service.getCountryUsersByIsocode(isocode=isocode)

    def create_country_user(self, username, email, password, isocode, user=None):
        u_service = userservice.UserService()
        return u_service.createCountryUser(username=username, email=email, password=password, isocode=isocode)

    def delete_country_user(self, userid, user=None):
        u_service = userservice.UserService()
        return u_service.deleteCountryUser(userid=userid)

    def get_org_admins(self, user=None):
        u_service = userservice.UserService()
        return u_service.getOrgadmins()

    def get_org_admins_by_orgid(self, orgid, user=None):
        u_service = userservice.UserService()
        return u_service.getOrgadminsByOrgId(orgid=orgid)

    def create_org_admin(self, username, password, email, orgid, user=None):
        u_service = userservice.UserService()
        return u_service.createOrgAdmin(username=username, password=password, email=email, orgid=orgid)

    def delete_org_admin(self, userid, user=None):
        u_service = userservice.UserService()
        return u_service.deleteOrgadmin(userid=userid)

    def get_conversations_for_user(self, user=None):
        u_service = userservice.UserService()
        return u_service.getConversationsForUser(user=user)

    def send_message_to_new_conversation(self, conversation_title, message_text, sender, reciever_id, user=None):
        u_service = userservice.UserService()
        return u_service.send_message_to_new_conversation(conversationTitle=conversation_title,
                                                          messageText=message_text, sender=sender,
                                                          recieverid=reciever_id)

    def send_message_to_existing_conversation(self, conversation_id, message_text, sender_id, user=None):
        u_service = userservice.UserService()
        return u_service.send_message_to_existing_conversation(conversationId=conversation_id, messageText=message_text,
                                                               senderId=sender_id)

    def get_conversation(self, participant, user=None):
        u_service = userservice.UserService()
        return u_service.getConversation(currentUser=user, participant=participant)

    def create_conversation(self, reciever, conversation_title, user=None):
        u_service = userservice.UserService()
        return u_service.createConversation(currentUser=user, reciever=reciever, conversationTitle=conversation_title)

    def get_conversation_by_id(self, conversation_id, user=None):
        u_service = userservice.UserService()
        return u_service.getConversationById(conversationId=conversation_id)

    def delete_conversation(self, conversation_id, user=None):
        u_service = userservice.UserService()
        return u_service.delete_conversation(conversationId=conversation_id)

    def get_workspaces(self, user=None):
        u_service = userservice.UserService()
        return u_service.get_workspaces()

    def exists_user(self, email, username=None, user_id=None, user=None):
        u_service = userservice.UserService()
        return u_service.existsUser(email=email, username=username, userId=user_id)

    # --- PROCESS FUNCTIONS --- #

    def extract_polygon_from_raster(self, user, layer_id, coords, increase):
        process_service = processservice.ProcessService()
        return process_service.extractPolygonFromRaster(layer_id=layer_id, coords=coords, increase=increase)

    def do_buffer_by_layer_id(self, layer_id, distance, user=None):
        process_service = processservice.ProcessService()
        return process_service.buffer_layer(layer_id=layer_id, distance=distance)

    def do_buffer_on_feature(self, feature_id, layer_id, distance, user=None):
        process_service = processservice.ProcessService()
        return process_service.buffer_feature(feature_id=feature_id, layer_id=layer_id, distance=distance)

    def get_raster_statistics(self, user, file_path):
        process_service = processservice.ProcessService()
        return process_service.getRasterStatistics(filePath=file_path)

    def get_external_wms(self, user, url):
        process_service = processservice.ProcessService()
        return process_service.get_external_wms(url=url)

    def calculate_within(self, user, point_layer_id, polygon_layer_id):
        process_service = processservice.ProcessService()
        return process_service.calculate_within(point_layer_id=point_layer_id, polygon_layer_id=polygon_layer_id)

    def query_by_attribute(self, layer_id, attribute_name, attribute_value, operator, user=None):
        process_service = processservice.ProcessService()
        return process_service.query_by_attribute(layer_id=layer_id, attribute_name=attribute_name,
                                                  attribute_value=attribute_value, operator=operator)

    def download_wcs_layer(self, user, layer_name):
        return geoserverservice.GeoServerService().downloadWCS(layerName=layer_name)

    # --- Postgis GeoServer Link Methods --- #

    def get_workspace_diff(self, user):
        pg_gs_link = postgis_geoserver_link.PostgisGeoServerLink()
        return pg_gs_link.get_workspace_diff()

    def get_vector_layer_diff(self, user):
        pg_gs_link = postgis_geoserver_link.PostgisGeoServerLink()
        return pg_gs_link.get_vector_layer_diff()

    def get_raster_layer_diff(self, user):
        pg_gs_link = postgis_geoserver_link.PostgisGeoServerLink()
        return pg_gs_link.get_raster_layer_diff()

    def sync_db_to_geoserver(self, user):
        pg_gs_link = postgis_geoserver_link.PostgisGeoServerLink()
        pass

    # --- UUID --- #

    def generate_UUID(self):
        return userservice.UserService().generateUUID()

    # --- TYPE METHODS --- #

    def get_geom_types(self):
        return postgisservice.PostgisService().get_geom_types()

    def get_attribute_types(self):
        return postgisservice.PostgisService().get_attribute_types()

    # --- GEOSERVER METHODS --- #

    def create_gs_workspace(self, workspace_name):
        '''
        :param workspace_name: workspace name as string
        :return: workspace dictionary ()
        '''
        gs_service = geoserverservice.GeoServerService()
        return gs_service.createGsWorkspace(workspaceName=workspace_name)

    # --- MONGODB- METHODS --- #
    def shp_to_mongodb(self, file_path, user=None):
        service = mongodbservice.MongoDBService()
        return service.shp_to_mongodb(file_path=file_path)

    def mongodb_get_nbr_of_attributes(self, document_id, user=None):
        service = mongodbservice.MongoDBService()
        return service.get_nbr_of_attributes(document_id=document_id)

    def mongodb_get_nbr_of_features(self, document_id, user=None):
        service = mongodbservice.MongoDBService()
        return service.get_nbr_of_features(document_id=document_id)

    def mongodb_get_attributes_for_layer(self, document_id, user=None):
        service = mongodbservice.MongoDBService()
        return service.get_attributes_for_layer(document_id=document_id)

    def mongodb_delete_layer(self, document_id):
        service = mongodbservice.MongoDBService()
        return service.delete_layer(document_id=document_id)
