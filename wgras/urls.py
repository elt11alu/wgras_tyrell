from . import app, ZIP_SHP_UPLOAD_SET, GEOTIFF_UPLOAD_SET, CSV_UPLOAD_SET
from flask import render_template, request, flash, redirect, url_for, session, jsonify, send_file
from flask import g
from functools import wraps
from validator import Validator
from appexceptions.ErrorResponse import AppException
from .rasterCalculation import evacPlanning
from models import *
import wgrasservice
from geoEncoder import geoAddressEncoder
from geoserverconnection import WCS
from .rasterCalculation import floodFillWatercourse
import json
from sendEmail import SendEmail

from .systemaccess import accessControl
from .zipFileHandler.zipFileHandler import ZipReader
from flask import jsonify, request
from flask_uploads import UploadNotAllowed
import os
from service.postgisservice import PostgisService
from postgisconnection import update_layer


#
#
# # Wrapper function: checks that user is logged in
# # Can be invoked by setting
# # @login_required over view function
def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session and session['logged_in']:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('get_login'))

    return wrap


# # Wrapper function: checks that user is ADMIN
# # Can be invoked by setting
# # @admin_required over view function
def admin_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session and 'logged_in_user_role' in session:
            if session['logged_in_user_role'] == 'superadmin' or session['logged_in_user_role'] == 'countryadmin' or \
                            session['logged_in_user_role'] == 'orgadmin':
                return f(*args, **kwargs)
            else:
                flash('You are not allowed to access the manager application')
                return redirect(url_for('get_login'))
        else:
            flash('You are not allowed to access the manager application')
            return redirect(url_for('get_login'))

    return wrap


# Flask handles the InvalidUsage Exception
@app.errorhandler(AppException)
def handle_app_exception(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.errorhandler(500)
def error_500(e):
    app.logger.warning(e.message)
    return jsonify({'errorMessage': 'Something went wrong, please try again later.'}), 500


@app.before_request
def load_user():
    if 'user_id' in session:
        g.user = User.query.filter_by(id=session["user_id"]).first()


# #################################################################
# #                                                               #
# # View functions, the entrance for HTTP requests to our API     #
# #                                                               #
# #################################################################
#


@app.route('/wfstest')
def wfs_test():
    return render_template("wgras/static/wfstest/wfstest.html")


@app.route('/testmap')
def test_map():
    return render_template("wgras/static/testMap/testMap.html")


@app.route('/')
def index():
    return redirect(url_for('get_login'))


# # @login_required checks first,
# # if not logged in, user is redirected to /login
# # if logged in, this method starts the map application
@app.route('/map')
@login_required
def get_map():
    if not app.config['DEBUG']:
        return send_file("wgrasMapBuild.html")
    return send_file("wgrasMap.html")


# # Requests to /manager url
# # @login_required checks first then @admin_required,
# # if not logged in or not admin, user is redirected to /login
# # if logged in and admin, this method starts the manager application
@app.route('/manager')
@login_required
@admin_required
def get_manager():
    ac = accessControl.AccessControl()
    if ac.canAccessManagerApplication(g.user):
        if not app.config['DEBUG']:
            return send_file("wgrasManagerBuild.html")
        return send_file("wgrasManager.html")
    return redirect(url_for('get_login'))


# # Handles login of users from login.html,
# # Checks if HTTP method is POST,
# # if true credentials are compared entries in users table in db
# # for all other HTTP methods, user is sent back to /login again

@app.route('/login', methods=['POST', 'GET'])
def get_login():
    error = None
    if request.method == 'POST':
        attempted_username = request.form['userName']
        attempted_pw = request.form['pw']
        db_user = User.query.filter_by(username=attempted_username).first()
        if db_user is not None:
            if db_user.auth(attempted_username, attempted_pw.encode('utf-8')):
                session['logged_in'] = True
                session['logged_in_user_role'] = db_user.type
                session['user_id'] = db_user.id
                return redirect(url_for('sitemap'))
            else:
                flash('Wrong username or password')

        else:
            flash('wrong username or password')

    return render_template('wgras/static/login/login.html', error=error)


@app.route('/sitemap', methods=['GET'])
@login_required
def sitemap():
    return render_template('wgras/static/siteEntrance/siteEntrance.html')


# # Logs out a user
@app.route('/logout')
@login_required
def logout():
    session.pop('logged_in', None)
    session.pop('logged_in_user_role', None)
    session.pop('user_id', None)
    flash('You were logged out.')
    return redirect(url_for('get_login'))


@app.route('/about', methods=['POST'])
@login_required
def about():
    """
    about route
    :return: information about WGRAS, json format or 404 if not found
    """

    return jsonify(about='information about wgras')


#
# Get list of published countries
#
@app.route('/countries/published', methods=['GET'])
@login_required
@admin_required
def get_all_countries():
    user = g.user
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    countries = service.get_countries(user=user)
    countryList = []
    for c in countries:
        if ac.canReadCountry(user, c.isocode):
            countryList.append(c.getObj())
    return jsonify({'countries': countryList})


#
# Get list of unpublished countries
#
@app.route('/country/unpublished', methods=['GET'])
@login_required
def get_unpublished_countries():
    countries = Availablecountry.query.all()
    clist = [c.getObj() for c in countries if not c.published]
    return jsonify({'countries': clist})


#
# Get metadata of a single country
#
@app.route('/country/published/<string:isocode>', methods=['GET'])
@login_required
def get_country(isocode):
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    if ac.canReadCountry(g.user, isocode):
        country = service.get_country(user=g.user, isocode=isocode)
        if country is not None:
            return jsonify({'country': country.getObj()})
        raise AppException('No country could be found', 400)
    raise AppException('You are not allowed to access this resource')


#
# Publish new country
#
@app.route('/country', methods=['POST'])
@login_required
def publish_country():
    ac = accessControl.AccessControl()
    isocode = request.json['isocode']
    if Validator.validateIsocode(isocode):
        if ac.canWriteCountry(g.user, isocode):
            service = wgrasservice.WgrasService()
            country = service.create_country(user=g.user, isocode=isocode)
            if country is not None:
                workspace = service.create_country_workspace(
                    user=g.user, isocode=isocode)
                if workspace is not None:
                    return jsonify({'country': country.getObj(), 'processId': service.generate_UUID()}), 201
                raise AppException('Could not create country', 400)
            raise AppException('Could not create country', 400)
        raise AppException('You are not allowed to create new countries', 403)
    raise AppException('Not a valid isocode')


#
# Unpublish country
#

@app.route('/country', methods=['DELETE'])
@login_required
def unpublish_country():
    isocode = request.json['isocode']
    ac = accessControl.AccessControl()
    service = wgrasservice.WgrasService()
    if Validator.validateIsocode(isocode):
        if ac.canWriteCountry(g.user, isocode):
            c = service.get_country(user=g.user, isocode=isocode)
            workspace_names = [ws.name for ws in c.workspaces.all()]
            country = service.delete_country(user=g.user, isocode=isocode)
            if country is not None:
                deleted = service.delete_country_workspaces(
                    user=g.user, workspace_names=workspace_names)
                if deleted:
                    return jsonify({'country': country, 'processId': service.generate_UUID()}), 200
                return jsonify({'country': country}), 200
            raise AppException('Country could not be deleted'), 400
        raise AppException('You are not allowed to delete countries.', 403)
    raise AppException('Not a valid isocode')


#
# Get list of published organizations
#
@app.route('/organizations', methods=['GET'])
@login_required
def get_organizations():
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    orgs = service.get_organizations(user=g.user)
    orglist = []
    for o in orgs:
        if ac.canReadOrganization(g.user, o.orgid):
            orglist.append(o.getObj())
    return jsonify({'organizations': orglist})


@app.route('/organizations', methods=['POST'])
@login_required
def add_organizations():
    ac = accessControl.AccessControl()

    fullname = request.json['fullname']
    shortname = request.json['shortname']
    isocode = request.json['isocode']

    if Validator.validateOrganizationName(fullname):
        if Validator.validateOrganizationShortName(shortname):
            if ac.canWriteOrganization(g.user, isocode):
                service = wgrasservice.WgrasService()
                org = service.create_organization(
                    user=g.user, isocode=isocode, fullname=fullname, shortname=shortname)
                if org is not None:
                    service.create_org_workspace(
                        user=g.user, isocode=isocode, orgid=org['orgid'])
                    return jsonify({'organization': org, 'processId': service.generate_UUID()})
                raise AppException('Could not create organization', 400)
            raise AppException(
                'You are not allowed to create an organization in this country', 403)
        raise AppException(
            'Not a valid short name, use no dots or spaces and only 3 to 10 letters')
    raise AppException('Not a valid organization name')


@app.route('/organizations', methods=['DELETE'])
@login_required
def delete_organizations():
    ac = accessControl.AccessControl()
    service = wgrasservice.WgrasService()
    orgid = request.json['orgid']
    if Validator.validateUUID4(orgid):
        if ac.canWriteOrganization(g.user, isocode=None, orgid=orgid):
            org = service.get_organization(user=g.user, orgid=orgid)
            workspace_names = [ws.name for ws in org.workspaces.all()]
            org = service.delete_organization(user=g.user, orgid=orgid)
            if org is not None:
                deleted = service.delete_country_workspaces(
                    user=g.user, workspace_names=workspace_names)
                if deleted:
                    return jsonify({'organization': org, 'processId': service.generate_UUID()})
                return jsonify({'organization': org})
            raise AppException('Organization could not be deleted', 400)
        raise AppException(
            'You are not allowed to delete this organization', 403)
    raise AppException('Not a valid organization id')


# todo: add validation to all methods
# Security implemented to this point


@app.route('/users', methods=['GET'])
@login_required
# @admin_required
def get_users():
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    users = service.get_users(user=g.user)
    userList = []
    for u in users:
        if ac.canReadUser(g.user, u.userid):
            userList.append(u.getObj())
    return jsonify({'users': userList})


@app.route('/users', methods=['DELETE'])
@login_required
def delete_user_by_id():
    user_id = request.json['userId']
    ac = accessControl.AccessControl()
    if Validator.validateUUID4(user_id):
        user_to_del = User.query.filter_by(userid=user_id).first()
        if user_to_del is not None:
            user_obj = user_to_del.getObj()
            access = False
            if type(user_to_del) == Superadmin:
                access = ac.canDeleteSuperadmin(g.user)
            elif type(user_to_del) == Countryadmin:
                access = ac.canDeleteCountryadmin(g.user, user_id)
            elif type(user_to_del) == Countryuser:
                access = ac.canDeleteCountryUser(g.user, user_id)
            elif type(user_to_del) == Orgadmin:
                access = ac.canDeleteOrgAdmin(g.user, user_id)

            if access:
                private_ws = user_to_del.privateworkspace.first()
                private_layers = private_ws.layers.all()

                # Deleting private layers
                for l in private_layers:
                    db.session.delete(l)
                    db.session.commit()
                # Deleting private workspace
                db.session.delete(private_ws)
                db.session.commit()

                # Deleting user
                db.session.delete(user_to_del)
                db.session.commit()
                return jsonify({'success': True, 'user': user_obj})
            raise AppException('You are not allowed to delete this user.')
        raise AppException('User does not exist')
    raise AppException('Not a valid user id')


# @app.route('/users/treelayout', methods=['GET'])
# def get_users_tree():
#     users = User.query.all()
#     countries = Country.query.all()
#
#     tree = [
#         {
#             "name": 'WGRAS',
#             "parent": None,
#             "children": [
#                 {
#                     "name": "Countries",
#                     "parent": "WGRAS",
#                     "children": [
#
#                     ]
#                 }
#
#             ]
#
#         }
#     ]


@app.route('/user/<string:username>', methods=['GET'])
@login_required
def get_user_by_username(username):
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    if Validator.validateUsername(username=username):
        user = service.get_user(user=g.user, username=username)
        if user is not None:
            if ac.canReadUser(g.user):
                return jsonify({'user': user})
            raise AppException('You are not allowed to read this information')
        raise AppException('User {}{}'.format(username, ' was not found'))
    raise AppException('Username not valid')


@app.route('/users/countryadmins', methods=['GET'])
@login_required
def get_country_admins():
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    admins = service.get_all_country_admins(user=g.user)
    adminList = []
    for a in admins:
        if ac.canReadUser(g.user, a.userId):
            adminList.append(a.getObj())
    return jsonify({'countryadmins': adminList})


@app.route('/users/countryadmins', methods=['POST'])
@login_required
def create_country_admin():
    try:
        username = request.json['username']
        password = request.json['pw1']
        password2 = request.json['pw2']
        email = request.json['email']
        isocode = request.json['isocode']
        service = wgrasservice.WgrasService()
        ac = accessControl.AccessControl()
        exists = service.exists_user(user=g.user, email=email, username=username)
        if not exists:
            if (password == password2) and Validator.validatePassword(password):
                if Validator.validateUsername(username):
                    if Validator.validateEmail(email):
                        if ac.canCreateCountryAdmin(g.user, isocode):
                            newadmin = service.create_country_admin(user=g.user, username=username, password=password,
                                                                    email=email, isocode=isocode)
                            if newadmin is not None:
                                return jsonify({'countryadmin': newadmin, 'processId': service.generate_UUID()})
                            raise AppException('Isocode is not valid')
                        raise AppException(
                            'You are not allowed to create an admin in this country', 403)
                    raise AppException('Email not valid')
                raise AppException('User name not valid')
            raise AppException('Password not valid')
        raise AppException('User already exists')
    except Exception as e:
        app.logger.error(e.message)


@app.route('/users/countryadmins', methods=['DELETE'])
@login_required
def delete_country_admin():
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    userId = request.json['userId']
    if Validator.validateUUID4(userId):
        if ac.canDeleteCountryadmin(g.user, userId):
            user = service.delete_country_Admin(user=g.user, userid=userId)
            return jsonify({'countryadmin': user, 'processId': service.generate_UUID()})
        raise AppException('You are not allowed to delete this user.', 403)
    raise AppException('Not a valid user id', 400)


@app.route('/users/countryadmins/<string:isocode>', methods=['GET'])
@login_required
def get_country_admins_by_isocode(isocode):
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    if Validator.validateIsocode(isocode):
        admins = service.get_country_admins_by_isocode(
            user=g.user, isocode=isocode.upper())
        adminList = []
        if admins is not None:
            for a in admins:
                if ac.canReadUser(g.user, a.userid):
                    adminList.append(a.getObj())
            return jsonify({'countryadmins': adminList})
        raise AppException('Country does not exist', 400)
    raise AppException('ISO code is not valid', 400)


@app.route('/users/countryusers', methods=['GET'])
@login_required
def get_country_users():
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    users = service.get_country_users(user=g.user)
    uList = []
    for u in users:
        if ac.canReadUser(g.user, u.userid):
            uList.append(u.getObj())
    return jsonify({'countryusers': uList})


@app.route('/users/countryusers/<string:isocode>', methods=['GET'])
@login_required
def get_country_users_by_isocode(isocode):
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    if Validator.validateIsocode(isocode):
        users = service.get_country_users_by_isocode(
            user=g.user, isocode=isocode.upper())
        if users is not None:
            uList = []
            for u in users:
                if ac.canReadUser(g.user, u.userid):
                    uList.append(u.getObj())
            return jsonify({'countryusers': uList})
        raise AppException('Country does not exist')
    raise AppException('ISO code is not valid')


@app.route('/users/countryusers', methods=['POST'])
@login_required
def create_country_users():
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    username = request.json['username']
    password = request.json['pw1']
    password2 = request.json['pw2']
    email = request.json['email']
    isocode = request.json['isocode']
    if Validator.validateIsocode(isocode):
        if ac.canCreateCountryUser(g.user, isocode):
            if not service.exists_user(user=g.user, email=email, username=username):
                if (password == password2) and Validator.validatePassword(password):
                    if Validator.validateUsername(username):
                        if Validator.validateEmail(email):
                            newuser = service.create_country_user(user=g.user, username=username, email=email,
                                                                  password=password, isocode=isocode)
                            if newuser is not None:
                                return jsonify({'countryuser': newuser, 'processId': service.generate_UUID()})
                            raise AppException('Country does not exist')
                        raise AppException('Email not valid')
                    raise AppException('User name not valid')
                raise AppException('Password not valid')
            raise AppException('User already exists')
        raise AppException('You are not allowed to create this user')
    raise AppException('ISO code is not valid')


@app.route('/users/countryusers', methods=['DELETE'])
@login_required
def delete_country_users():
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    userid = request.json['userId']
    if Validator.validateUUID4(userid):
        if ac.canDeleteCountryUser(g.user, userid):
            user = service.delete_country_user(user=g.user, userid=userid)
            if user is not None:
                return jsonify({'countryuser': user})
            raise AppException('User does not exist')
        raise AppException('You are not allowed to delete this user')
    raise AppException('User id is not valid')


@app.route('/users/orgadmins', methods=['GET'])
@login_required
def get_org_admins():
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    admins = service.get_org_admins(user=g.user)
    orgadminList = []
    for a in admins:
        if ac.canReadUser(g.user, a.userid):
            orgadminList.append(a.getObj())
    return jsonify({'orgadmins': orgadminList})


@app.route('/users/orgadmins/<string:orgid>', methods=['GET'])
@login_required
def get_org_admins_by_org_id(orgid):
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    orgadminList = []
    if Validator.validateUUID4(orgid):
        admins = service.get_org_admins_by_orgid(user=g.user, orgid=orgid)
        if admins is not None:
            for a in admins:
                if ac.canReadUser(g.user, a.userid):
                    orgadminList.append(a.getObj())
            return jsonify({'orgadmins': orgadminList})
        raise AppException('Organization does not exist')
    raise AppException('Not a valid organization id')


@app.route('/users/orgadmins', methods=['POST'])
@login_required
def create_org_admin():
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    username = request.json['username']
    password = request.json['pw1']
    password2 = request.json['pw2']
    email = request.json['email']
    orgid = request.json['orgid']
    if ac.canCreateOrgAdmin(g.user, orgid):
        exists = service.exists_user(
            user=g.user, email=email, username=username)
        if not exists:
            if (password == password2) and Validator.validatePassword(password):
                if Validator.validateUsername(username):
                    if Validator.validateEmail(email):
                        newuser = service.create_org_admin(user=g.user, username=username, password=password,
                                                           email=email, orgid=orgid)
                        if newuser is not None:
                            return jsonify({'orgadmin': newuser})
                        raise AppException('Organization does not exist')
                    raise AppException('Email not valid')
                raise AppException('User name not valid')
            raise AppException('Password not valid')
        raise AppException('User already exists')
    raise AppException(
        'You are not allowed to create an admin in this organization')


@app.route('/users/orgadmins', methods=['DELETE'])
@login_required
def delete_org_admin():
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    userid = request.json['userId']
    if Validator.validateUUID4(userid):
        if ac.canDeleteOrgAdmin(g.user, userid):
            user = service.delete_org_admin(user=g.user, userid=userid)
            if user is not None:
                return jsonify({'orgadmin': user})
            raise AppException('User does not exist')
        raise AppException('You are not allowed to delete this user')
    raise AppException('User id is not valid')


@app.route('/users/superadmins', methods=['GET'])
@login_required
def get_superadmins():
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    admins = service.get_superadmins(user=g.user)
    sAdminList = []
    for a in admins:
        if ac.canReadUser(g.user, a.userid):
            sAdminList.append(a.getObj())
    return jsonify({'superadmins': sAdminList})


# #################################################################
# #                                                               #
# # Security implemented up to here                               #
# #                                                               #
# #################################################################


@app.route('/user/current', methods=['GET'])
@login_required
def get_current_user():
    return jsonify(g.user.getObj())


@app.route('/users/roles', methods=['GET'])
@login_required
@admin_required
def get_roles():
    roleList = []
    roles = g.user.getRoles()
    for r in roles:
        roleList.append({'rolename': r().type})
    return jsonify({'roles': roleList})


@app.route('/evacplanning', methods=['POST'])
@login_required
def asynch_evac_planning():
    try:
        # DATASET_METHOD (string): Defines how the input data will be read by the script. To read the data from a postgreSQL
        # database set the variable as 'database' and to read from CSV files set it as 'file'. In case of using the
        # 'database' method the connection parameters to the database server must be changed in 'routing' function of this
        # script and in case of using the 'file' method the CSV must be in the
        # same folder.
        DATASET_METHOD = 'file'

        # CONSTRAINT (boolean): Defines if the algorithm will consider some tye of spatial restriction for the analysis.
        # When it is False there is no constraints for the analysis and each building in the dataset could be allocated to
        # any safe area (which is the worse scenario). If its value is True the allocation will be restricted according to
        # to the available methods (see next paragraph).
        CONSTRAINT = True

        # CONSTRAINT_METHOD (string): If the CONSTRAINT variable was set as True this variable defines the type of
        # constraint to be used (otherwise it is ignored). If it is defined as 'distance' each building can be allocated
        # only to the safes areas within certain threshold around it (if there are no safe areas within that threshold the
        # closest one is selected). If this method is set as 'quantity' each building can be allocated to a fixed number of
        # safe areas (sorted by nearness).
        CONSTRAINT_METHOD = 'distance'

        # CONSTRAINT_VALUE (integer): If the CONSTRAINT variable was set as True this variable defines the threshold to be
        # used for the selected method (otherwise it is ignored). If the selected method was 'distance' this value sets the
        # threshold in meters to be used to select the potential safe areas for each building. If the selected method was
        # 'quantity' this value sets the amount of safe areas to be considered for each building (chosen by nearness) .
        CONSTRAINT_VALUE = 500

        # LOOPS (integer): Defines the number of iterations for the main AMOSA process. The higher the value the higher will
        # be the level of optimisation for each objective function (Capacity and Distance) but it will increase the
        # processing time.
        LOOPS = 22

        # *****************************************************************************************************************
        city = request.json['city']
        service = wgrasservice.WgrasService()
        task = evacPlanning.startEvacPlanning.apply_async(
            args=[city], task_id='evacplanning-' + service.generate_UUID())

        return jsonify({'progressStatus': url_for('taskstatus', task_id=task.id)}), 202
    except Exception as e:
        app.logger.info(e)
        raise AppException(
            message='Process is not available, please try later')


@app.route('/status/<task_id>')
@login_required
def taskstatus(task_id):
    task = evacPlanning.startEvacPlanning.AsyncResult(task_id)
    if task.state == 'PENDING':
        # job did not start yet
        response = {
            'state': task.state,
            'current': 0,
            'total': 1,
            'status': 'Pending...'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'current': task.info.get('current', 0),
            'total': task.info.get('total', 1),
            'status': task.info.get('status', '')
        }
        if 'result' in task.info:
            response['result'] = task.info['result']
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'current': 1,
            'total': 1,
            'status': str(task.info),  # this is the exception raised
        }
    return jsonify(response)


# ---- Email ----#

@app.route('/sendEmail', methods=['POST'])
@login_required
def send_email_to_user():
    recipient = str(request.json['email'])
    recipient_username = request.json['username']
    message = request.json['message']
    subject = request.json['subject']

    service = wgrasservice.WgrasService()
    if Validator.validateUsername(username=recipient_username):
        if Validator.validateEmail(email=recipient):
            if Validator.validateEmailText(message) and Validator.validateEmailText(subject):
                if service.get_user(user=g.user, username=recipient_username)['email'] == recipient:
                    sent = SendEmail.send_wgras_mail.apply_async(args=[recipient, message, subject],
                                                                 task_id='manager-' + service.generate_UUID())
                    return jsonify(
                        {'emailStatus': url_for('send_email_status', task_id=sent.id), 'processId': sent.id}), 202
                else:
                    raise AppException(
                        'Recipient name or adress not valid.', 400)
            else:
                raise AppException(
                    'Message contains unallowed characters', 400)
        raise AppException('Recipients address not valid', 400)
    raise AppException('Recipient username not valid', 400)


@app.route('/email/status/<task_id>')
@login_required
def send_email_status(task_id):
    if Validator.validateManagerCeleryId(task_id):
        response = SendEmail.getStatus(task_id=task_id)
        return jsonify(response)
    raise AppException('Task id not valid')


# --- Conversations --- #
@app.route('/conversation/<int:conversationId>', methods=['GET'])
@login_required
def get_conversation_by_id(conversationId):
    service = wgrasservice.WgrasService()
    conversation = service.get_conversation_by_id(
        user=g.user, conversation_id=conversationId)
    return jsonify({"conversation": conversation})


@app.route('/conversations', methods=['GET'])
@login_required
def get_conversation_for_user():
    service = wgrasservice.WgrasService()
    conversations = service.get_conversations_for_user(user=g.user)
    return jsonify({'conversations': conversations})


@app.route('/conversation', methods=['POST'])
@login_required
def create_new_conversation():
    conversationtitle = request.json['subjectText']
    messageText = request.json['messageText']
    recieveruserId = request.json['recipientId']
    senderid = request.json['senderId']
    sender = g.user
    if senderid == sender.userid:
        service = wgrasservice.WgrasService()
        message = service.send_message_to_new_conversation(user=g.user, conversation_title=conversationtitle,
                                                           message_text=messageText, sender=sender,
                                                           reciever_id=recieveruserId)
        if message is not None:
            return jsonify({'messageStatus': 'sent'})
        raise AppException('Message could not be sent')
    raise AppException('User id not valid')


@app.route('/conversation', methods=['DELETE'])
@login_required
def delete_conversation():
    conversationId = request.json['conversationId']
    service = wgrasservice.WgrasService()
    deleted = service.delete_conversation(
        user=g.user, conversation_id=conversationId)
    if deleted:
        return jsonify({'messageStatus': 'deleted'})
    raise AppException('Could not delete conversation')


@app.route('/conversation/message', methods=['POST'])
@login_required
def send_message_to_existing_conversation():
    service = wgrasservice.WgrasService()
    conversationId = request.json['conversationId']
    messageText = request.json['messageText']
    senderId = request.json['senderId']
    message = service.send_message_to_existing_conversation(user=g.user, conversation_id=conversationId,
                                                            message_text=messageText, sender_id=senderId)
    if message is not None:
        return jsonify({'messageStatus': 'sent'})
    raise AppException('could not send message')


####################################
#          Metadata urls           #
#                                  #
####################################
@app.route('/metadata/layers/categories/simple')
@login_required
def get_category_list():
    return jsonify({'categories': [cat.getSimpleObj() for cat in Layercategory.query.all()]})


@app.route('/metadata/layers/categories/full')
@login_required
def get_all_categories_and_layers():
    ac = accessControl.AccessControl()
    try:
        metadata = Layercategory.query.all()
        metadata_list = [l.getObj() for l in metadata]
        metadata_list_copy = []

        for ii in range(0, len(metadata_list)):
            layers = metadata_list[ii]['layers']
            layer_list = []
            for i in range(0, len(layers)):
                layer = layers[i]
                if ac.canReadWorkspaceByName(g.user, layer['workspaceName']):
                    layer_list.append(layer)
            if len(layer_list) > 0:
                metadata_list[ii]['layers'] = layer_list
                metadata_list_copy.append(metadata_list[ii])
        return jsonify({'layers': metadata_list_copy})
    except Exception as e:
        app.logger.info(e)
        raise AppException('Something went wrong', status_code=404)


@app.route('/metadata/all', methods=['GET'])
@login_required
def get_all_layers_metadata():
    ac = accessControl.AccessControl()
    try:
        metadata = Layer.query.all()
        metadata_list = []
        for meta in metadata:
            if ac.canReadWorkspaceByName(g.user, meta.workspace.name):
                metadata_list.append(meta.getObj())
        return jsonify({'layers': metadata_list})
    except Exception as e:
        app.logger.info(e)
        raise AppException('Something went wrong', status_code=404)


@app.route('/metadata/wms', methods=['GET'])
@login_required
def get_all_wms_metadata():
    ac = accessControl.AccessControl()
    try:
        service = wgrasservice.WgrasService()
        wmsMetadata = service.get_raster_layers_metadata(user=g.user)
        metadataList = []
        for metadata in wmsMetadata:
            if ac.canReadWorkspaceByName(g.user, metadata['workspaceName']):
                metadataList.append(metadata)
        return jsonify({'layers': metadataList})
    except Exception as e:
        app.logger.info(e)
        raise AppException('Something went wrong', status_code=404)


@app.route('/metadata/wms/<name>', methods=['GET'])
@login_required
def get_wms_metadata_by_name(name):
    ac = accessControl.AccessControl()
    service = wgrasservice.WgrasService()
    metadata = service.get_raster_layer_metadata_by_name(
        user=g.user, name=name)

    if metadata is not None:
        if ac.canReadWorkspaceByName(g.user, metadata['workspaceName']):
            return jsonify(metadata)
        raise AppException(
            'You are not allowed to access metadata for {}'.format(name), 401)
    raise AppException('No metadata is available for {}'.format(name), 400)


@app.route('/metadata/wfs', methods=['GET'])
@login_required
def get_all_wfs_metadata():
    try:
        ac = accessControl.AccessControl()
        service = wgrasservice.WgrasService()
        wfsMetadata = service.get_vector_layers_metadata(user=g.user)
        metadataList = []
        for metadata in wfsMetadata:
            if ac.canReadWorkspaceByName(g.user, metadata['workspaceName']):
                metadataList.append(metadata)
        return jsonify({'layers': metadataList})
    except Exception as e:
        app.logger.info(e)
        raise AppException('Something went wrong', status_code=404)


@app.route('/metadata/wfs/<name>', methods=['GET'])
@login_required
def get_wfs_metadata_by_name(name):
    ac = accessControl.AccessControl()

    service = wgrasservice.WgrasService()
    metadata = service.get_vector_layer_metadata_by_name(
        user=g.user, name=name)
    if metadata is not None:
        if ac.canReadWorkspaceByName(g.user, metadata['workspaceName']):
            return jsonify(metadata)
        raise AppException(
            'You are not allowed to access metadata for {}'.format(name), 401)
    raise AppException('No metadata is available for {}'.format(name), 400)


@app.route('/metadata/wcs', methods=['GET'])
@login_required
def get_all_wcs_metadata():
    try:
        ac = accessControl.AccessControl()
        service = wgrasservice.WgrasService()
        wcsMetadata = service.get_wcs_layers_metadata(user=g.user)
        metadataList = []
        for metadata in wcsMetadata['layers']:
            if ac.canReadWorkspaceByName(g.user, metadata['workspaceName']):
                metadataList.append(metadata)
        return jsonify({'layers': metadataList})
    except Exception as e:
        app.logger.info(e)
        raise AppException('Something went wrong', status_code=404)


@app.route('/metadata/wcs/<name>', methods=['GET'])
@login_required
def get_wcs_metadata_by_name(name):
    ac = accessControl.AccessControl()
    service = wgrasservice.WgrasService()
    metadata = service.get_wcs_layer_metadata_by_name(user=g.user, name=name)
    if metadata is not None:
        if ac.canReadWorkspaceByName(g.user, metadata['workspaceName']):
            return jsonify(metadata)
        raise AppException(
            'You are not allowed to access metadata for {}'.format(name), 401)
    raise AppException('No metadata is available for {}'.format(name), 400)


@app.route('/metadata/elevation', methods=['GET'])
@login_required
def get_elevation_metadata():
    ac = accessControl.AccessControl()

    service = wgrasservice.WgrasService()
    wmsMetadata = service.get_elevation_layers_metadata(user=g.user)
    metadataList = []
    for metadata in wmsMetadata:
        if ac.canReadWorkspaceByName(g.user, metadata['workspaceName']):
            metadataList.append(metadata)
    return jsonify({'layers': metadataList})


@app.route('/metadata/externalows', methods=['GET'])
@login_required
def get_existing_service_metadata():
    services = [ows_metadata.getObj() for ows_metadata in Externalows.query.all()]
    return jsonify({'services': services})


@app.route('/metadata/externalows', methods=['POST'])
@login_required
def create_new_service_metadata():
    try:
        name = request.json['name']
        url = request.json['url']
        description = request.json['description']
        category = request.json['category']
        service = wgrasservice.WgrasService()
        created = service.create_ows_metadata(name=name, url=url, description=description, category=category)
        if created is not None:
            return jsonify({'service': created})
        raise AppException('Could not create service metadata')
    except Exception as e:
        app.logger.warning(e.message)
        print e.message
        raise AppException('Something wen wrong')


@app.route('/metadata/externalows', methods=['DELETE'])
@login_required
def delete_existing_service_metadata():
    try:
        ows_id = request.json['owsId']
        service = wgrasservice.WgrasService()
        created = service.delete_ows_metadata(ows_id=ows_id)
        if created is not None:
            return jsonify({'service': created})
        raise AppException('Could not delete service metadata')
    except Exception as e:
        app.logger.warning(e.message)
        print e.message
        raise AppException('Something wen wrong')


'''
@app.route("/metadata/export/excel", methods=['GET'])
def export_records():
    try:
        service = spatialservice.SpatialService()
        metadata = service.getAllWMSMetadata()
        layers = metadata['layers']

        for l in layers:
            string = ''
            keywords = l['keywords']
            if len(keywords) > 0:
                for k in keywords:
                    string += k + ' ,'
            l['keywords'] = string
        return excel.make_response_from_records(layers, file_type='xlsx', file_name='wgrasLayers')
    except Exception as e:
        raise AppException('Something went wrong', status_code=404)
'''


@app.route('/metadata', methods=['PUT'])
def update_layer_metadata():
    '''Updates metadata of layer in postgresql database, only description and category gets updated'''
    try:
        layer_id = request.json['layerId']
        description = request.json['description']
        category = request.json['category']
        if Validator.validateUUID4(layer_id):
            if Validator.validateLayerInfo(description):
                if Validator.validateLayerInfo(category):
                    wgras_service = wgrasservice.WgrasService()
                    updated = wgras_service.update_layer_metadata(layer_id=layer_id, description=description,
                                                                  category=category, user=None)
                    if updated is not None:
                        return jsonify({'layer': updated})
                    raise AppException('Layer does not exist')
                raise AppException(message='Not a valid category')
            raise AppException(message='Not a valid description')
        raise AppException(message='Not a valid layer id')

    except KeyError as e:
        raise AppException(message='Unknown key')
    except Exception as e:
        raise AppException(message='Something went wrong')


#####################################
#          workspaces urls          #
#                                   #
#####################################


@app.route('/workspaces/writable', methods=['GET'])
@login_required
def get_writable_workspaces():
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    workspaces = service.get_workspaces(user=g.user)
    wsList = []
    for workspace in workspaces:
        if ac.canWriteWorkspace(g.user, workspace.wsid):
            wsList.append(workspace.getObj())
    return jsonify({'workspaces': wsList})


@app.route('/workspaces/readable', methods=['GET'])
@login_required
def get_readable_workspaces():
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    workspaces = service.get_workspaces(user=g.user)
    wsList = []
    for workspace in workspaces:
        if ac.canReadWorkspace(g.user, workspace.wsid):
            wsList.append(workspace.getObj())
    return jsonify({'workspaces': wsList})


# only for testing
@app.route('/accessworkspace')
@login_required
def can_access_workspace():
    workspaces = Workspace.query.all()
    wsList = []
    ac = accessControl.AccessControl()
    for ws in workspaces:
        canread = ac.canReadWorkspace(g.user, wsid=ws.wsid)
        canwrite = ac.canWriteWorkspace(g.user, wsid=ws.wsid)
        obj = ws.getObj()
        obj['readaccess'] = canread
        obj['writeaccess'] = canwrite
        wsList.append(obj)
    return jsonify({'access': wsList})

    # raise AppException('Something went wrong', status_code=404)


@app.route('/workspaces/private')
@login_required
def get_private_workspace():
    workspaces = Privateworkspace.query.filter_by(user=g.user).all()
    if workspaces is not None:
        return jsonify({'workspaces': [workspace.getObj() for workspace in workspaces]})
    raise AppException('No workspace available')


#####################################
#          Private layers           #
#                                   #
#####################################

@app.route('/layers/private')
@login_required
def get_private_layers():
    layer_list = []
    workspaces = Privateworkspace.query.filter_by(user=g.user).all()
    for workspace in workspaces:
        layers = Layer.query.filter_by(workspace=workspace).all()
        for layer in layers:
            layer_list.append(layer.getObj())

    return jsonify({'layers': layer_list})


@app.route('/layers/vector/private')
@login_required
def get_private_vector_layers():
    layer_list = []
    workspaces = Privateworkspace.query.filter_by(user=g.user).all()
    for workspace in workspaces:
        layers = Vectorlayer.query.filter_by(workspace=workspace).all()
        for layer in layers:
            layer_list.append(layer.getObj())

    return jsonify({'layers': layer_list})


#####################################
#          Upload urls              #
#                                   #
#####################################


@app.route('/vector/zip', methods=['POST'])
@login_required
def preview_shp_file():
    try:
        service = wgrasservice.WgrasService()
        if request.method == 'POST' and 'file' in request.files:
            zip_file_name = ZIP_SHP_UPLOAD_SET.save(request.files['file'])
            zipreader = ZipReader(filePath=ZIP_SHP_UPLOAD_SET.path(zip_file_name),
                                  unzipPath=os.path.join(app.config['UPLOADS_DEFAULT_DEST'], 'unzippedShp'))
            unique_names = zipreader.getUniqueFileNames()

            # Also checks that file is valid shp file
            if len(unique_names) == 1:
                zipreader.unzip()
                zipreader.remove_zip()

                shp_file_name = unique_names[0]
                shp_file_path = os.path.join(
                    app.config['UPLOADS_DEFAULT_DEST'], 'unzippedShp', '{}{}'.format(shp_file_name, '.shp'))
                mongo_id = service.shp_to_mongodb(
                    file_path=shp_file_path, user=g.user)
                attribute_list = service.mongodb_get_attributes_for_layer(
                    document_id=mongo_id, user=g.user)
                nbr_of_attributes = service.mongodb_get_nbr_of_attributes(
                    document_id=mongo_id, user=g.user)
                nbr_of_geometries = service.mongodb_get_nbr_of_features(
                    document_id=mongo_id, user=g.user)
                workspaces = service.get_workspace_names(user=g.user)

                return jsonify({
                    "success": True,
                    "msg": "Successful preview upload",
                    "attributes": attribute_list,
                    "nbrOfAttributes": nbr_of_attributes,
                    "nbrOfGeometries": nbr_of_geometries,
                    "fileName": shp_file_name,
                    "workspaces": workspaces,
                    "mongoId": str(mongo_id)
                })

            return jsonify({
                "success": False,
                "msg": "The file contains " + str(
                    len(unique_names)) + " different shapefiles, only 1 allowed or not a valid shapefile."})
        else:
            raise AppException('Method not allowed', status_code=403)
    except UploadNotAllowed as e:
        app.logger.info(e)
        raise AppException('Something went wrong', status_code=404)


@app.route('/vector/zip/createLayer', methods=['POST'])
@login_required
def create_vector_layer_from_shp():
    try:
        '''extract request parameters'''
        submitted_layer_name = str(request.form["layerName"])
        shpFileName = str(request.form["shpfileName"])
        workspaceId = str(request.form["workspaceId"])
        mongoId = str(request.form['mongoId'])
        category = str(request.form['category'])
        description = request.form['description']

        '''Check input'''
        if submitted_layer_name is not None and Validator.validateLayerName(
                submitted_layer_name) and Validator.validateUUID4(workspaceId):

            service = wgrasservice.WgrasService()
            created = service.create_vector_layer_from_shp_file(
                user=g.user, layer_name=submitted_layer_name, workspace_id=workspaceId, mongo_doc_id=mongoId,
                schema='public', category=category, description=description)
            if created:
                return jsonify({"success": True, "msg": "Layer {} created.".format(submitted_layer_name)}), 201
            return jsonify(
                {"success": False, "msg": "Layer {} could not be created.".format(submitted_layer_name)}), 400
        else:
            return jsonify({"success": False, "msg": "Not a valid table name or workspace name"}), 404
    except Exception as e:
        print app.logger.info(e.message)
        return jsonify({"success": False, "msg": 'Could not create layer'}), 404


@app.route('/vector/csv', methods=['POST'])
@login_required
def preview_csv_file():
    try:
        if request.method == 'POST' and 'file' in request.files:
            service = wgrasservice.WgrasService()
            filename = CSV_UPLOAD_SET.save(request.files['file'])
            filePath = os.path.join(
                app.config['UPLOADS_DEFAULT_DEST'], 'csv', filename)
            fields = service.read_csv(file_path=filePath)
            return jsonify({'success': True, 'fileName': filename, 'fields': fields}), 200
        return jsonify({'success': False, 'msg': 'File could not be read'}), 200
    except Exception as e:
        print app.logger.info(e.message)
        return jsonify({"success": False, "msg": e.message}), 404


@app.route('/vector/csv/createLayer', methods=['POST'])
@login_required
def create_vector_layer_from_csv():
    try:
        submitted_table_name = str(request.form["storeName"])
        csv_file_name = str(request.form["fileName"])
        workspace_id = str(request.form["workspaceId"])
        category = str(request.form['category'])
        description = str(request.json['description'])
        file_path = os.path.join(
            app.config['UPLOADS_DEFAULT_DEST'], 'csv', csv_file_name)
        service = wgrasservice.WgrasService()
        created = service.create_vector_layer_from_csv_file(user=g.user, file_path=file_path,
                                                            csv_file_name=csv_file_name,
                                                            table_name=submitted_table_name, workspace_id=workspace_id,
                                                            schema='public', description=description, category=category)
        if created:
            return jsonify({'success': True, 'msg': 'Layer {} was created.'.format(submitted_table_name)}), 200
        return jsonify({'success': False, 'msg': 'Layer could not be created. Try later'}), 400
    except Exception as e:
        app.logger.warning(e.message)
        return jsonify({'success': False, 'msg': 'Layer could not be created. Try later'}), 400


@app.route('/raster/geotiff', methods=['POST'])
@login_required
def preview_geotiff_file():
    try:
        service = wgrasservice.WgrasService()
        if request.method == 'POST' and 'file' in request.files:
            filename = GEOTIFF_UPLOAD_SET.save(request.files['file'])
            file_path = os.path.join(
                app.config['UPLOADS_DEFAULT_DEST'], 'geotiffs', filename)
            statistics = service.get_raster_statistics(
                user=g.user, file_path=file_path)
            statistics['fileName'] = filename
            return jsonify({'msg': statistics, 'success': True}), 200
        return jsonify({'success': False, 'msg': 'Not a valid file or request method'}), 400
    except Exception as e:
        app.logger.warning(e.message)
        return jsonify({'success': False, 'msg': 'Not a valid file or request method'}), 400


@app.route('/raster/geotiff/createLayer', methods=['POST'])
@login_required
def create_vector_layer_from_geotiff():
    try:
        service = wgrasservice.WgrasService()
        workspace_id = request.form['workspaceId']
        layer_name = request.form['layerName']
        file_name = request.form['fileName']
        category = request.form['category']
        description = request.form['description']
        haselevationdata = request.form['hasElevationData']
        if Validator.validateUUID4(workspace_id):
            if Validator.validateLayerName(layer_name):
                if Validator.validateFileName(file_name):
                    file_path = os.path.join(
                        app.config['UPLOADS_DEFAULT_DEST'], 'geotiffs', file_name)
                    created = service.create_raster_layer_from_geotiff(
                        file_path=file_path, workspace_id=workspace_id, layer_name=layer_name, published=True,
                        user=g.user,
                        haselevationdata=haselevationdata, description=description, category=category)
                    if created is not None:
                        return jsonify({'msg': 'Layer ' + layer_name + ' created', 'success': True}), 201

                    return jsonify({'msg': 'Layer ' + layer_name + ' Already exists', 'success': False}), 400

                return jsonify({'sucess': False, 'msg': 'File name {} is not allowed'.format(file_name)})
            return jsonify({'success': False, 'msg': 'Layer name {} is not allowed'.format(layer_name)})
        return jsonify({'success': False, 'msg': 'Workspace name is not allowed'})
    except Exception as e:
        app.logger.warning(e.message)
        return jsonify({'success': False, 'msg': 'Something went wrong, try again later'})


#####################################
#          Resource urls            #
#                                   #
#####################################
@app.route('/resource/attributes/<string:workspace_name>/<string:layer_name>')
@login_required
def get_attributes(workspace_name, layer_name):
    if Validator.validateLayerName(layer_name):
        if Validator.validateWorkspaceName(workspace_name):
            service = wgrasservice.WgrasService()
            result = service.get_attributes_for_layer_by_name(user=g.user, layer_name=str(layer_name),
                                                              workspace_name=str(workspace_name))
            return jsonify(result)
        raise AppException(
            'Workspace name {} is not allowed'.format(workspace_name))
    raise AppException('Layer name {} is not allowed'.format(layer_name))


@app.route('/resource/attributes/<string:layer_id>')
@login_required
def get_attributes_by_layer_id(layer_id):
    if Validator.validateUUID4(layer_id):
        service = wgrasservice.WgrasService()
        result = service.get_attributes_for_layer_by_id(
            user=g.user, layer_id=layer_id)
        return jsonify(result)
    raise AppException('Layer id {} is not allowed'.format(layer_id))


@app.route('/resource/<string:layer_name>/<string:workspace_name>')
@login_required
def get_resource_repr(layer_name, workspace_name):
    service = wgrasservice.WgrasService()
    resource = service.get_resource_repr(
        user=g.user, layer_name=layer_name, workspace_name=workspace_name)
    if resource is not None:
        return jsonify({'resource': resource})
    raise AppException('No resource found')


@app.route('/resources')
@login_required
def get_resources_repr():
    service = wgrasservice.WgrasService()
    res_list = service.get_resources_repr(user=g.user)
    return jsonify({'count': len(res_list), 'resources': res_list})


#####################################
#          Layer urls               #
#                                   #
#####################################
@app.route('/layer/publish', methods=['PUT'])
@login_required
def publish_geoserver_layer():
    try:
        layer_name = request.json['layerName']
        workspace_name = request.json['workspaceName']
        publish = request.json['publish']
        if Validator.validateLayerName(layer_name):
            if Validator.validateWorkspaceName(workspace_name):
                if Validator.validatePublish(publish):

                    service = wgrasservice.WgrasService()
                    published = service.publish_layer(user=g.user, layer_name=layer_name, workspace_name=workspace_name,
                                                      publish=publish)
                    if published is not None:
                        msg = 'published' if published else 'unpublished'
                        return jsonify({'success': True, 'msg': 'Layer {} was {}.'.format(layer_name, msg)})
                    return jsonify({'success': False, 'msg': 'Layer {} could not be updated.'.format(layer_name)})

                raise AppException(
                    'Published must be true or false.', status_code=404)
            raise AppException(
                'Workspace name is not valid.', status_code=404)
        raise AppException('Layer name is not valid.', status_code=404)
    except Exception as e:
        app.logger.info(e.message)
        raise AppException('Something went wrong', status_code=404)


@app.route('/layer/published', methods=['GET'])
@login_required
def get_published_gs_layers():
    try:
        service = wgrasservice.WgrasService()
        return jsonify(service.get_published_layers(user=g.user))
    except Exception as e:
        app.logger.info(e.message)
        raise AppException('Something went wrong', status_code=404)


@app.route('/layer/unpublished')
@login_required
def get_unpublished_gs_layers():
    try:
        service = wgrasservice.WgrasService()
        return jsonify(service.get_unpublished_layers(user=g.user))
    except Exception as e:
        print e
        app.logger.info(e.message)
        raise AppException('Something went wrong', status_code=404)


@app.route('/layer/keywords/<string:workspace_name>/<string:layer_name>')
@login_required
def get_keywords_for_gs_layers(workspace_name, layer_name):
    try:
        if Validator.validateLayerName(layer_name):
            if Validator.validateWorkspaceName(workspace_name):
                service = wgrasservice.WgrasService()
                keywords = service.get_keywords_for_layer(user=g.user, layer_name=layer_name,
                                                          workspace_name=workspace_name)

                if keywords is not None:
                    return jsonify(keywords)
                raise AppException(
                    'No keywords found for layer {}'.format(layer_name))
            raise AppException('Workspace name {} not valid.'.format(
                workspace_name), status_code=400)
        raise AppException('Layer name {} not valid.'.format(
            layer_name), status_code=400)
    except Exception as e:
        app.logger.info(e.message)
        raise AppException('Something went wrong', status_code=404)


@app.route('/layer/properties', methods=['PUT'])
@login_required
def update_gs_layer_properties():
    try:
        layerName = request.json['layerName']
        layerTitle = request.json['layerTitle']
        workspace = request.json['workspaceName']
        layerInfo = request.json['layerInfo']
        keywords = request.json['keywords']
        enabled = request.json['enabled']
        print layerName, layerTitle, workspace, layerInfo, keywords, enabled
        if Validator.validateLayerTitle(layerTitle):
            if Validator.validateLayerName(layerName):
                if Validator.validateWorkspaceName(workspace):
                    if Validator.validateLayerInfo(layerInfo):
                        if Validator.validateLayerKeywords(keywords):
                            if Validator.validatePublish(enabled):
                                service = wgrasservice.WgrasService()
                                updated = service.update_properties_for_layer(user=g.user,
                                                                              layer_name=layerName,
                                                                              workspace_name=workspace,
                                                                              properties={"layerTitle": layerTitle,
                                                                                          "layerInfo": layerInfo,
                                                                                          "keywords": keywords,
                                                                                          "enabled": enabled})
                                if updated:
                                    return jsonify(
                                        {'success': True, 'msg': 'Layer properties changed successfully'}), 200
                                return jsonify({"succes": False, "msg": "Layer could not be updated"}), 400
                            else:
                                raise AppException(
                                    'Enabled must be boolean', status_code=404)
                        else:
                            raise AppException(
                                'Keywords are not valid.', status_code=404)
                    else:
                        raise AppException(
                            'Layer description is not valid.', status_code=404)
                else:
                    raise AppException(
                        'Workspace name is not valid.', status_code=404)
            else:
                raise AppException('Layer name is not valid.', status_code=404)
        else:
            raise AppException('Layer title is not valid.', status_code=404)
    except Exception as e:
        app.logger.info(e.message)
        raise AppException('Something went wrong', status_code=404)


#####################################
#          Process urls             #
#                                   #
#####################################
@app.route('/process/extractPolygon', methods=['POST'])
@login_required
def extract_polygon_from_raster():
    try:
        json_obj = request.json
        layer_id = str(json_obj['layerId'])
        increase = json_obj['increase']
        lon = json_obj['lon']
        lat = json_obj['lat']
        if Validator.validateUUID4(layer_id):
            if Validator.validateInteger(int(increase) and int(increase) > 0):
                if Validator.validateFloat(float(lon)) and Validator.validateFloat(float(lat)):
                    service = wgrasservice.WgrasService()
                    features = service.extract_polygon_from_raster(user=g.user, layer_id=layer_id,
                                                                   coords=[
                                                                       float(lon), float(lat)],
                                                                   increase=int(increase))
                    if features is not None:
                        return jsonify({'features': features})
                    raise AppException('Could not extract polygons')
                raise AppException(
                    'Coordinates are not valid', status_code=404)
            raise AppException(
                'Threshold is not a valid number', status_code=404)
        raise AppException('Layer id not valid,', status_code=400)
    except Exception as e:
        app.logger.info('Could not extract polygon, got exception {}'.format(e.message))
        raise AppException('Could not extract polygon')


@app.route('/process/buffer/layer', methods=['POST'])
@login_required
def buffer_layer():
    try:
        service = wgrasservice.WgrasService()
        layer_id = request.json['layerId']
        distance = request.json['distance']
        features = service.do_buffer_by_layer_id(user=g.user, layer_id=layer_id, distance=distance)
        return jsonify({'features': features})
    except Exception as e:
        app.logger.info(e.message)
        raise AppException('Something went wrong', status_code=404)


@app.route('/process/buffer/feature', methods=['POST'])
@login_required
def buffer_feature():
    try:
        service = wgrasservice.WgrasService()
        feature_id = request.json['featureId']
        layer_id = request.json['layerId']
        distance = request.json['distance']
        feature = service.do_buffer_on_feature(user=g.user, feature_id=feature_id, layer_id=layer_id, distance=distance)
        return jsonify({'feature': feature})
    except Exception as e:
        app.logger.info(e.message)
        raise AppException('Something went wrong', status_code=404)


@app.route('/process/raster/statistics/<layer_name>', methods=['GET'])
@login_required
def get_raster_statistics(layer_name):
    try:
        if Validator.validateLayerName(layer_name):
            service = wgrasservice.WgrasService()
            file_path = service.download_wcs_layer(
                user=g.user, layer_name=layer_name)
            statistics = service.get_raster_statistics(
                user=g.user, file_path=file_path)
            return jsonify({'layerName': layer_name, 'statistics': statistics})
        raise AppException('Layer name {} not valid.'.format(
            layer_name), status_code=400)
    except Exception as e:
        app.logger.info(e.message)
        raise AppException('Something went wrong', status_code=404)


@app.route('/process/externalservice', methods=['POST'])
@login_required
def get_external_service():
    url = request.json['url']
    print url
    service = wgrasservice.WgrasService()
    if Validator.validateUrl(url):
        return jsonify({'layers': service.get_external_wms(user=g.user, url=url)})
    raise AppException('Not a valid url', 400)


@app.route('/process/within', methods=['POST'])
@login_required
def calculate_within():
    point_layer_id = request.json['pointLayerId']
    polygon_layer_id = request.json['polygonLayerId']
    service = wgrasservice.WgrasService()
    features = service.calculate_within(
        user=g.user, point_layer_id=point_layer_id, polygon_layer_id=polygon_layer_id)
    return jsonify({'features': features})


@app.route('/process/querybyattribute', methods=['POST'])
@login_required
def query_by_attribute():
    vector_layer_id = request.json['layerId']
    attribute_name = request.json['attributeName']
    attribute_value = request.json['attributeValue']
    operator = request.json['operator']
    service = wgrasservice.WgrasService()
    features = service.query_by_attribute(layer_id=vector_layer_id, attribute_name=attribute_name,
                                          attribute_value=attribute_value, user=g.user, operator=operator)
    return jsonify({'features': features})


#####################################
#          Layer urls               #
#                                   #
#####################################

@app.route('/layer/point', methods=['POST'])
@login_required
def create_empty_point_layer():
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'Point'
    schema = 'public'
    created = service.create_new_empty_vector_layer(user=g.user, workspace_id=workspace_id, layer_name_dirty=layer_name,
                                                    layer_title=layer_title, geometry_type=geometry_type, schema=schema,
                                                    fields=fields, published=True, description=description,
                                                    category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Layer could not be created')


@app.route('/layer/multipoint', methods=['POST'])
@login_required
def create_empty_multipoint_layer():
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'MultiPoint'
    schema = 'public'
    created = service.create_new_empty_vector_layer(user=g.user, workspace_id=workspace_id, layer_name_dirty=layer_name,
                                                    layer_title=layer_title, geometry_type=geometry_type, schema=schema,
                                                    fields=fields, published=True, description=description,
                                                    category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Layer could not be created')


@app.route('/point', methods=['POST'])
@login_required
def insert_point():
    geojson_point = request.json['geometry']
    layer_id = request.json['layerId']
    layer_service = wgrasservice.WgrasService()
    success = layer_service.insert_geojson_point_in_layer(
        user=g.user, layer_id=str(layer_id), geojson_point=geojson_point)
    if success:
        return jsonify({'success': True})
    raise AppException('Point could not be inserted', 400)


@app.route('/layer/line', methods=['POST'])
@login_required
def create_empty_line_layer():
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'LineString'
    schema = 'public'
    created = service.create_new_empty_vector_layer(user=g.user, workspace_id=workspace_id, layer_name_dirty=layer_name,
                                                    layer_title=layer_title, geometry_type=geometry_type, schema=schema,
                                                    fields=fields, published=True, description=description,
                                                    category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Layer could not be created')


@app.route('/layer/multiline', methods=['POST'])
@login_required
def create_empty_multiline_layer():
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'MultiLineString'
    schema = 'public'
    created = service.create_new_empty_vector_layer(user=g.user, workspace_id=workspace_id, layer_name_dirty=layer_name,
                                                    layer_title=layer_title, geometry_type=geometry_type, schema=schema,
                                                    fields=fields, published=True, description=description,
                                                    category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Layer could not be created')


@app.route('/line', methods=['POST'])
@login_required
def insert_line():
    geojson_line = request.json['geometry']
    layer_id = request.json['layerId']
    service = wgrasservice.WgrasService()
    success = service.insert_geojson_line_in_layer(
        user=g.user, layer_id=str(layer_id), geojson_line=geojson_line)
    if success:
        return jsonify({'success': True})
    raise AppException('Point could not be inserted', 400)


@app.route('/layer/polygon', methods=['POST'])
@login_required
def create_empty_polygon_layer():
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'Polygon'
    schema = 'public'
    created = service.create_new_empty_vector_layer(user=g.user, workspace_id=workspace_id, layer_name_dirty=layer_name,
                                                    layer_title=layer_title, geometry_type=geometry_type, schema=schema,
                                                    fields=fields, published=True, description=description,
                                                    category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Layer could not be created')


@app.route('/layer/multipolygon', methods=['POST'])
@login_required
def create_empty_multipolygon_layer():
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'MultiPolygon'
    schema = 'public'
    created = service.create_new_empty_vector_layer(user=g.user, workspace_id=workspace_id, layer_name_dirty=layer_name,
                                                    layer_title=layer_title, geometry_type=geometry_type, schema=schema,
                                                    fields=fields, published=True, description=description,
                                                    category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Layer could not be created')


@app.route('/polygon', methods=['POST'])
@login_required
def insert_polygon():
    geojson_polygon = request.json['geometry']
    layer_id = request.json['layerId']
    service = wgrasservice.WgrasService()
    success = service.insert_geojson_polygon_in_layer(
        user=g.user, layer_id=str(layer_id), geojson_polygon=geojson_polygon)
    if success:
        return jsonify({'success': True})
    raise AppException('Point could not be inserted', 400)


@app.route('/layer/point/with_geometry', methods=['POST'])
@login_required
def create_point_layer_with_geometry():
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    geojson_points = request.json['geometry']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'Point'
    schema = 'public'
    created = service.create_new_vector_layer_with_geometry(user=g.user, workspace_id=workspace_id,
                                                            layer_name_dirty=layer_name,
                                                            layer_title=layer_title, geometry_type=geometry_type,
                                                            schema=schema,
                                                            fields=fields, geometry=geojson_points, published=True,
                                                            description=description, category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Could not create layer')


@app.route('/layer/multipoint/with_geometry', methods=['POST'])
@login_required
def create_multipoint_layer_with_geometry():
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    geojson_points = request.json['geometry']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'MultiPoint'
    schema = 'public'
    created = service.create_new_vector_layer_with_geometry(user=g.user, workspace_id=workspace_id,
                                                            layer_name_dirty=layer_name,
                                                            layer_title=layer_title, geometry_type=geometry_type,
                                                            schema=schema,
                                                            fields=fields, geometry=geojson_points, published=True,
                                                            description=description, category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Could not create layer')


@app.route('/layer/line/with_geometry', methods=['POST'])
@login_required
def create_line_layer_with_geometry():
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    geojson_lines = request.json['geometry']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'Line'
    schema = 'public'
    created = service.create_new_vector_layer_with_geometry(user=g.user, workspace_id=workspace_id,
                                                            layer_name_dirty=layer_name,
                                                            layer_title=layer_title, geometry_type=geometry_type,
                                                            schema=schema,
                                                            fields=fields, geometry=geojson_lines, published=True,
                                                            description=description, category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Could not create layer')


@app.route('/layer/multiline/with_geometry', methods=['POST'])
@login_required
def create_multiline_layer_with_geometry():
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    geojson_lines = request.json['geometry']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'MultiLine'
    schema = 'public'
    created = service.create_new_vector_layer_with_geometry(user=g.user, workspace_id=workspace_id,
                                                            layer_name_dirty=layer_name,
                                                            layer_title=layer_title, geometry_type=geometry_type,
                                                            schema=schema,
                                                            fields=fields, geometry=geojson_lines, published=True,
                                                            description=description, category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Could not create layer')


@app.route('/layer/polygon/with_geometry', methods=['POST'])
@login_required
def create_polygon_layer_with_geometry():
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    geojson_polygons = request.json['geometry']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'Polygon'
    schema = 'public'
    created = service.create_new_vector_layer_with_geometry(user=g.user, workspace_id=workspace_id,
                                                            layer_name_dirty=layer_name,
                                                            layer_title=layer_title, geometry_type=geometry_type,
                                                            schema=schema,
                                                            fields=fields, geometry=geojson_polygons, published=True,
                                                            description=description, category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Could not create layer')


@app.route('/layer/multipolygon/with_geometry', methods=['POST'])
@login_required
def create_multipolygon_layer_with_geometry():
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    geojson_polygons = request.json['geometry']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'MultiPolygon'
    schema = 'public'
    created = service.create_new_vector_layer_with_geometry(user=g.user, workspace_id=workspace_id,
                                                            layer_name_dirty=layer_name,
                                                            layer_title=layer_title, geometry_type=geometry_type,
                                                            schema=schema,
                                                            fields=fields, geometry=geojson_polygons, published=True,
                                                            description=description, category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Could not create layer')


@app.route('/types/geometry')
@login_required
def get_geom_types():
    service = wgrasservice.WgrasService()
    return jsonify({'geomTypes': service.get_geom_types()})


@app.route('/types/attributes')
@login_required
def get_attribute_types():
    service = wgrasservice.WgrasService()
    return jsonify({'attributeTypes': service.get_attribute_types()})


@app.route('/layer/extent/<string:layer_id>')
@login_required
def get_layer_extent(layer_id):
    if Validator.validateUUID4(layer_id):
        layer = Layer.query.filter_by(layerid=layer_id).first()

        ''' Returns [minx, maxx, miny, maxy] '''
        bbox = PostgisService()._get_bbox(layer_name=layer.name, workspace_name=layer.workspace.name)
        if layer is not None and bbox is not None:
            return jsonify({'extent': {'minx': bbox[0], 'maxx': bbox[1], 'miny': bbox[2], 'maxy': bbox[3]}})
        raise AppException('No layer found')
    raise AppException('Not a valid layer id')


@app.route('/layer/<string:layer_id>')
def get_layer_by_layer_id(layer_id):
    layer = Layer.query.filter_by(layerid=layer_id).first()
    if layer is not None:
        return jsonify({'layer': layer.getObj()})
    raise AppException('No layer found')


@app.route('/layer/exists/<string:workspace_id>/<string:layer_name>')
@login_required
def check_layer(workspace_id, layer_name):
    if Validator.validateLayerName(layer_name):
        if Validator.validateUUID4(workspace_id):
            workspace = Workspace.query.filter_by(wsid=workspace_id).first()
            if workspace is not None:
                for layer in workspace.layers:
                    if layer.name == layer_name:
                        return jsonify({'exists': True})
                return jsonify({'exists': False})
            raise AppException('Workspace not found.')
        raise AppException('Not a valid workspace name')
    raise AppException('Not a valid layer name')


@app.route('/layer/<string:layer_id>/feature/<int:feature_id>', methods=['GET'])
def get_attributes_for_feature(layer_id, feature_id):
    layer = Layer.query.filter_by(layerid=layer_id).first()
    if layer is not None:
        updater = update_layer.UpdateLayer()
        result = updater.get_attribute_list(layer_name=layer.name, feature_id=feature_id)
        return jsonify({'attributes': result})


@app.route('/layer/feature/', methods=['POST'])
def update_feature_attribute_value():
    layer_id = request.json['layerId']
    feature_id = request.json['featureId']
    attr_name = request.json['attrName']
    attr_value = request.json['attr_value']
    layer = Layer.query.filter_by(layerid=layer_id).first()
    if layer is not None and attr_name != 'ogc_fid':
        updater = update_layer.UpdateLayer()
        updated = updater.update_attribute_value(layer_name=layer.name, feature_id=feature_id, attr_name=attr_name,
                                                 attr_value=attr_value)
        if updated:
            return jsonify({'updated': True})
        raise AppException('Not updated')
    raise AppException('Layer does not exist')


########### Temp routes for Mahdis student #################


@app.route('/temp/workspaces/writable', methods=['GET'])
def temp_get_writable_workspaces():
    service = wgrasservice.WgrasService()
    ac = accessControl.AccessControl()
    temp_user = User.query.filter_by(username='nakasiRose').first()
    workspaces = service.get_workspaces(user=temp_user)
    wsList = []
    for workspace in workspaces:
        if ac.canWriteWorkspace(temp_user, workspace.wsid):
            wsList.append(workspace.getObj())
    return jsonify({'workspaces': wsList})


@app.route('/temp/layer/point', methods=['POST'])
def temp_create_empty_point_layer():
    temp_user = User.query.filter_by(username='nakasiRose').first()
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'Point'
    schema = 'public'
    created = service.create_new_empty_vector_layer(user=temp_user, workspace_id=workspace_id,
                                                    layer_name_dirty=layer_name,
                                                    layer_title=layer_title, geometry_type=geometry_type, schema=schema,
                                                    fields=fields, published=True, description=description,
                                                    category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Layer could not be created')


@app.route('/temp/temp/layer/multipoint', methods=['POST'])
def temp_create_empty_multipoint_layer():
    temp_user = User.query.filter_by(username='nakasiRose').first()
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'MultiPoint'
    schema = 'public'
    created = service.create_new_empty_vector_layer(user=temp_user, workspace_id=workspace_id,
                                                    layer_name_dirty=layer_name,
                                                    layer_title=layer_title, geometry_type=geometry_type, schema=schema,
                                                    fields=fields, published=True, description=description,
                                                    category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Layer could not be created')


@app.route('/temp/point', methods=['POST'])
def temp_insert_point():
    temp_user = User.query.filter_by(username='nakasiRose').first()
    geojson_point = request.json['geometry']
    layer_id = request.json['layerId']
    layer_service = wgrasservice.WgrasService()
    success = layer_service.insert_geojson_point_in_layer(
        user=temp_user, layer_id=str(layer_id), geojson_point=geojson_point)
    if success:
        return jsonify({'success': True})
    raise AppException('Point could not be inserted', 400)


@app.route('/temp/layer/line', methods=['POST'])
def temp_create_empty_line_layer():
    temp_user = User.query.filter_by(username='nakasiRose').first()
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'LineString'
    schema = 'public'
    created = service.create_new_empty_vector_layer(user=temp_user, workspace_id=workspace_id,
                                                    layer_name_dirty=layer_name,
                                                    layer_title=layer_title, geometry_type=geometry_type, schema=schema,
                                                    fields=fields, published=True, description=description,
                                                    category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Layer could not be created')


@app.route('/temp/layer/multiline', methods=['POST'])
def temp_create_empty_multiline_layer():
    temp_user = User.query.filter_by(username='nakasiRose').first()
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'MultiLineString'
    schema = 'public'
    created = service.create_new_empty_vector_layer(user=temp_user, workspace_id=workspace_id,
                                                    layer_name_dirty=layer_name,
                                                    layer_title=layer_title, geometry_type=geometry_type, schema=schema,
                                                    fields=fields, published=True, description=description,
                                                    category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Layer could not be created')


@app.route('/temp/line', methods=['POST'])
def temp_insert_line():
    temp_user = User.query.filter_by(username='nakasiRose').first()
    geojson_line = request.json['geometry']
    layer_id = request.json['layerId']
    service = wgrasservice.WgrasService()
    success = service.insert_geojson_line_in_layer(
        user=temp_user, layer_id=str(layer_id), geojson_line=geojson_line)
    if success:
        return jsonify({'success': True})
    raise AppException('Point could not be inserted', 400)


@app.route('/temp/layer/polygon', methods=['POST'])
def temp_create_empty_polygon_layer():
    temp_user = User.query.filter_by(username='nakasiRose').first()
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'Polygon'
    schema = 'public'
    created = service.create_new_empty_vector_layer(user=temp_user, workspace_id=workspace_id,
                                                    layer_name_dirty=layer_name,
                                                    layer_title=layer_title, geometry_type=geometry_type, schema=schema,
                                                    fields=fields, published=True, description=description,
                                                    category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Layer could not be created')


@app.route('/temp/layer/multipolygon', methods=['POST'])
def temp_create_empty_multipolygon_layer():
    temp_user = User.query.filter_by(username='nakasiRose').first()
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'MultiPolygon'
    schema = 'public'
    created = service.create_new_empty_vector_layer(user=temp_user, workspace_id=workspace_id,
                                                    layer_name_dirty=layer_name,
                                                    layer_title=layer_title, geometry_type=geometry_type, schema=schema,
                                                    fields=fields, published=True, description=description,
                                                    category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Layer could not be created')


@app.route('/temp/polygon', methods=['POST'])
def temp_insert_polygon():
    temp_user = User.query.filter_by(username='nakasiRose').first()
    geojson_polygon = request.json['geometry']
    layer_id = request.json['layerId']
    service = wgrasservice.WgrasService()
    success = service.insert_geojson_polygon_in_layer(
        user=temp_user, layer_id=str(layer_id), geojson_polygon=geojson_polygon)
    if success:
        return jsonify({'success': True})
    raise AppException('Point could not be inserted', 400)


@app.route('/temp/layer/point/with_geometry', methods=['POST'])
def temp_create_point_layer_with_geometry():
    temp_user = User.query.filter_by(username='nakasiRose').first()
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    geojson_points = request.json['geometry']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'Point'
    schema = 'public'
    created = service.create_new_vector_layer_with_geometry(user=temp_user, workspace_id=workspace_id,
                                                            layer_name_dirty=layer_name,
                                                            layer_title=layer_title, geometry_type=geometry_type,
                                                            schema=schema,
                                                            fields=fields, geometry=geojson_points, published=True,
                                                            description=description, category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Could not create layer')


@app.route('/temp/layer/multipoint/with_geometry', methods=['POST'])
def temp_create_multipoint_layer_with_geometry():
    temp_user = User.query.filter_by(username='nakasiRose').first()
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    geojson_points = request.json['geometry']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'MultiPoint'
    schema = 'public'
    created = service.create_new_vector_layer_with_geometry(user=temp_user, workspace_id=workspace_id,
                                                            layer_name_dirty=layer_name,
                                                            layer_title=layer_title, geometry_type=geometry_type,
                                                            schema=schema,
                                                            fields=fields, geometry=geojson_points, published=True,
                                                            description=description, category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Could not create layer')


@app.route('/temp/layer/line/with_geometry', methods=['POST'])
def temp_create_line_layer_with_geometry():
    temp_user = User.query.filter_by(username='nakasiRose').first()
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    geojson_lines = request.json['geometry']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'Line'
    schema = 'public'
    created = service.create_new_vector_layer_with_geometry(user=temp_user, workspace_id=workspace_id,
                                                            layer_name_dirty=layer_name,
                                                            layer_title=layer_title, geometry_type=geometry_type,
                                                            schema=schema,
                                                            fields=fields, geometry=geojson_lines, published=True,
                                                            description=description, category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Could not create layer')


@app.route('/temp/layer/multiline/with_geometry', methods=['POST'])
def temp_create_multiline_layer_with_geometry():
    temp_user = User.query.filter_by(username='nakasiRose').first()
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    geojson_lines = request.json['geometry']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'MultiLine'
    schema = 'public'
    created = service.create_new_vector_layer_with_geometry(user=temp_user, workspace_id=workspace_id,
                                                            layer_name_dirty=layer_name,
                                                            layer_title=layer_title, geometry_type=geometry_type,
                                                            schema=schema,
                                                            fields=fields, geometry=geojson_lines, published=True,
                                                            description=description, category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Could not create layer')


@app.route('/temp/layer/polygon/with_geometry', methods=['POST'])
def temp_create_polygon_layer_with_geometry():
    temp_user = User.query.filter_by(username='nakasiRose').first()
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    geojson_polygons = request.json['geometry']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'Polygon'
    schema = 'public'
    created = service.create_new_vector_layer_with_geometry(user=temp_user, workspace_id=workspace_id,
                                                            layer_name_dirty=layer_name,
                                                            layer_title=layer_title, geometry_type=geometry_type,
                                                            schema=schema,
                                                            fields=fields, geometry=geojson_polygons, published=True,
                                                            description=description, category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Could not create layer')


@app.route('/temp/layer/multipolygon/with_geometry', methods=['POST'])
def temp_create_multipolygon_layer_with_geometry():
    temp_user = User.query.filter_by(username='nakasiRose').first()
    layer_name = request.json['layerName']
    layer_title = request.json['layerTitle']
    workspace_id = request.json['workspaceId']
    fields = request.json['fields']
    geojson_polygons = request.json['geometry']
    category = request.json['category']
    description = request.json['description']
    service = wgrasservice.WgrasService()
    geometry_type = 'MultiPolygon'
    schema = 'public'
    created = service.create_new_vector_layer_with_geometry(user=temp_user, workspace_id=workspace_id,
                                                            layer_name_dirty=layer_name,
                                                            layer_title=layer_title, geometry_type=geometry_type,
                                                            schema=schema,
                                                            fields=fields, geometry=geojson_polygons, published=True,
                                                            description=description, category=category)
    if created:
        return jsonify({'success': True, 'rasterMetadata': created['raster_metadata'],
                        'vectorMetadata': created['vector_metadata']})
    raise AppException('Could not create layer')
