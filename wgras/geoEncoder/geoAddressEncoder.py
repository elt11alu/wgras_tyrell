import googlemaps
from geopy.geocoders import Nominatim
from geopy.geocoders import Bing
import geocoder

class GeoAddressEncoder(object):
    '''
    Input address and returns according coordinate
    '''
    def __init__(self, address):
        self.address = address

    def googleAddress2Coord(self):
        gmaps = googlemaps.Client(key='AIzaSyDASd_jGQpbT2qECpbam7uykSuX3KOA3EI')
        result = gmaps.geocode(self.address)
        coord_dict = {}
        coord_dict['lat'] = result[0]['geometry']['location']['lat']
        coord_dict['lng'] = result[0]['geometry']['location']['lng']
        coord_dict['cnt'] = result[0]['address_components'][6]['short_name']
        return coord_dict

    def osmAddress2Coord(self):
        geolocator = Nominatim()
        location = geolocator.geocode(self.address)
        coord_dict = {}
        coord_dict['lat'] = location.latitude
        coord_dict['lng'] = location.longitude
        return coord_dict

    def W3WAddress2Coord(self):
        g = geocoder.w3w(self.address, key='S3OTD6Z4')
        coord_dict = {}
        coord_dict['lat'] = g.lat
        coord_dict['lng'] = g.lng
        return coord_dict

    def bingAddress2Coord(self):
        pass

class GeoCoordEncoder(object):
    '''
    Input address and returns according coordinate
    '''
    def __init__(self, lat, lng):
        self.lat = lat
        self.lng = lng

    def googleCoord2Address(self):
        gmaps = googlemaps.Client(key='AIzaSyDASd_jGQpbT2qECpbam7uykSuX3KOA3EI')
        reverse_geocode_result = gmaps.reverse_geocode((self.lat, self.lng))
        address_components = reverse_geocode_result[0]['address_components']
        for item in address_components:
            shortname = item['short_name']
            gtypes = item['types']
            if 'country' in gtypes:
                country_code = shortname
        return country_code

    def bingCoord2Address(self):
        key = "AokX-S2lieXTaXG8pvEw3i2AKYuStBMK8RsUu6BDJ6hrL5AYv0IfQqM9zc-BAA-v"
        geolocator = Bing(api_key=key)
        country_code = geolocator.reverse((self.lat, self.lng))
        for item in country_code:
            print item
        return 'hello'

    def osmCoord2Address(self):
        geolocator = Nominatim()
        country_code = geolocator.reverse((self.lat, self.lng))
        for item in country_code:
            print item
        return 'hello'
