from wgras.models import *
from wgras import wgrasservice
import csv
import os
from wgras.namefactory import name_factory


def get_insert_countries():
    return [
        {'iso': 'ARM'},
        {'iso': 'AZE'},
        {'iso': 'GEO'},
        {'iso': 'IRN'},
        {'iso': 'TJK'},
        {'iso': 'KGZ'},
        {'iso': 'KAZ'},
        {'iso': 'TKM'},
        {'iso': 'UZB'},
        {'iso': 'UGA'},
        {'iso': 'RWA'},
        {'iso': 'BDI'},
        {'iso': 'MWI'},
        {'iso': 'MOZ'},
        {'iso': 'TZA'},
        {'iso': 'KEN'},
        {'iso': 'ETH'}
    ]


def create_db():
    print 'Dropping database wgrasusers...'
    db.drop_all()
    print 'Creating new database configuration for wgrasusers...'
    db.create_all()
    print 'Done creating new database configuration for wgrasusers!\n'


def insertCountries():
    print 'Inserting countries from csv file...'
    with open(os.path.join('countrydata', 'countries.csv')) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        for row in reader:
            c = Availablecountry(name=row['name'], isocode=row['isocode'])
            db.session.add(c)
        db.session.commit()
        print 'Done inserting countries from csv!\n'


def publishCountries():
    # Armenia, Azerbaijan, Georgia, Iran, Tajikistan, Kyrgyzstan, Kazakhstan, Turkmenistan, Uzbekistan, Uganda, Rwanda, Burundi, Malawi, Mozambique, Tanzania, Kenya, Ethiopia

    print 'Publishing countries: ' + ' '.join(c['iso'] for c in get_insert_countries())
    for c in get_insert_countries():
        k, v = c.popitem()
        country = Availablecountry.query.filter_by(isocode=v).first()
        country.publish()
    print 'Done publishing countries!\n'


def createSuperAdmin():
    admins = [
        {'username': 'antonlundkvist', 'email': 'anton.lundkvist@nateko.lu.se', 'password': 'anton123'},
        {'username': 'alimansourian', 'email': 'ali.mansourian@nateko.lu.se', 'password': 'ali123'},
        {'username': 'petterpilesjo', 'email': 'petter.pilesjo@nateko.lu.se', 'password': 'petter123'},
        {'username': 'rogergroth', 'email': 'roger.groth@nateko.lu.se', 'password': 'roger123'},
        {'username': 'kaivonharbou', 'email': 'kai.fake.mail.who@gmail.com', 'password': 'kaivonharbou123'},
        {'username': 'uteenderlein', 'email': 'ute.fake.mail.who@gmail.com', 'password': 'uteenderlein123'}
    ]

    print 'Creating superadmins...'

    for a in admins:
        pw = bcrypt.hashpw(a['password'], bcrypt.gensalt())
        admin = Superadmin(username=a['username'], email=a['email'], password=pw)
        db.session.add(admin)
    db.session.commit()
    print 'Done creating superadmins!\n'


def createCountryAdmin():
    print 'Creating country admins...'

    # for c in get_insert_countries():
    #     country = Country.query.filter_by(isocode=c['iso']).first()
    #     for nbr in xrange(1, 5):
    #         username = '{}{}{}{}{}'.format(country.name, '_', 'admin', '_', str(nbr))
    #         password = '{}{}'.format(username, '123')
    #         email = '{}{}'.format(username, '@fake.com')
    #         pw = bcrypt.hashpw(password=password, salt=bcrypt.gensalt())
    #         admin = Countryadmin(username=username, email=email, password=pw, country=country)
    #         db.session.add(admin)
    #
    # db.session.commit()
    print 'Done creating countryadmins!\n'


def createCountryUser():
    print 'Creating countryusers...'
    # for c in get_insert_countries():
    #     country = Country.query.filter_by(isocode=c['iso']).first()
    #     for nbr in xrange(1, 6):
    #         username = '{}{}{}{}{}'.format(country.name, '_', 'user', '_', str(nbr))
    #         password = '{}{}'.format(username, '123')
    #         email = '{}{}'.format(username, '@fake.com')
    #         pw = bcrypt.hashpw(password=password, salt=bcrypt.gensalt())
    #         admin = Countryadmin(username=username, email=email, password=pw, country=country)
    #         db.session.add(admin)
    #
    # db.session.commit()
    print 'Done creating countryusers!\n'


def createWorkspace():
    print 'Creating workspaces...'

    service = wgrasservice.WgrasService()
    name_fac = name_factory.NameFactory()

    wspublic = [
        {'name': 'public'}
    ]
    wsadmin = [
        {'name': 'admin'}
    ]

    for wsp in wspublic:
        ws_name = name_fac.create_public_workspace_name(wsp['name'])
        publicws = Publicworkspace(name=ws_name)
        service.create_gs_workspace(workspace_name=ws_name)
        db.session.add(publicws)
    db.session.commit()

    for wsa in wsadmin:
        ws_name = name_fac.create_admin_workspace_name(wsa['name'])
        adminws = Adminworkspace(name=ws_name)
        service.create_gs_workspace(workspace_name=ws_name)
        db.session.add(adminws)
    db.session.commit()

    for c in get_insert_countries():
        country = Country.query.filter_by(isocode=c['iso']).first()
        ws_name = name_fac.create_country_workspace_name(country_name=country.name)
        countryws = Countryworkspace(name=ws_name, country=country)
        service.create_gs_workspace(workspace_name=ws_name)
        db.session.add(countryws)
    db.session.commit()
    print 'Done creating workspaces!\n'


def createOrgAndWorkspace():
    print 'Creating organizations and workspaces...'

    print 'Done creating organizations and workspaces, none created!\n'


def createOrgAdmin():
    print 'Creating orgadmins...'

    print 'Done creating orgadmins, none created!\n'


def createSuperAdminPermission():
    print 'Creating superadmin permissions...'
    # read write all
    superadmins = Superadmin.query.all()
    adminws = Workspace.query.all()

    for s in superadmins:
        for ws in adminws:
            s.grant_fullPermission(workspace=ws)
    print 'Done creating superadim permissions!\n'


def createCountryAdminPermission():
    print 'Creating countryadmin permissions...'

    countryadmins = Countryadmin.query.all()
    adminws = Adminworkspace.query.all()
    countryws = Countryworkspace.query.all()
    orgws = Orgworkspace.query.all()
    for c in countryadmins:
        for aws in adminws:
            c.grant_readPermission(workspace=aws)
        for cws in countryws:
            if c.country.isocode == cws.country.isocode:
                c.grant_fullPermission(workspace=cws)
        for ows in orgws:
            country = ows.organization.country
            if c.country.isocode == country.isocode:
                c.grant_fullPermission(workspace=ows)

    print 'Done creating countryadmin permissions!\n'


def createCountryUserPermission():
    print 'Create countryuser permissions...'
    cusers = Countryuser.query.all()

    for user in cusers:
        workspaces = Countryworkspace.query.filter_by(country=user.country)
        for ws in workspaces:
            user.grant_readPermission(workspace=ws)
    print 'Done creating countryuser permissions!\n'


def createOrgAdminPermission():
    print 'Creating orgadmin permissions...'
    orgadmins = Orgadmin.query.all()
    orgws = Orgworkspace.query.all()

    for admin in orgadmins:
        for ows in orgws:
            if admin.organization.country.isocode == ows.organization.country.isocode:
                admin.grant_fullPermission(workspace=ows)
            else:
                admin.grant_readPermission(workspace=ows)
    print 'Done creating orgadmin permissions!\n'


def createPublicWorkspacePermissions():
    print 'Creating permissions for public workspace...'
    users = User.query.all()
    publicWs = Publicworkspace.query.all()

    for ws in publicWs:
        for u in users:
            u.grant_fullPermission(workspace=ws)
    print 'Done creating public workspace permissions!\n'


def create_private_workspaces_and_permissions():
    print 'Creating private workspaces...'
    users = User.query.all()
    service = wgrasservice.WgrasService()
    name_fac = name_factory.NameFactory()
    for user in users:
        ws_name = name_fac.create_private_workspace_name(user.username)
        gs_workspace = service.create_gs_workspace(ws_name)
        if gs_workspace is not None:
            db_workspace = Privateworkspace(name=ws_name, user=user)
            db.session.add(db_workspace)
            db.session.commit()
            db_ws = Workspace.query.filter_by(name=ws_name).first()
            if db_ws is not None:
                user.grant_fullPermission(db_ws)
    print 'Done creating private workspaces!\n'


def create_private_layers():
    print 'Creating private layers...'
    users = User.query.all()
    service = wgrasservice.WgrasService()
    name_fac = name_factory.NameFactory()
    geom_names = ['Point', 'LineString', 'Polygon']
    category = 'Private layers'
    ii = 0
    for user in users:
        print user.username
        workspace = Privateworkspace.query.filter_by(user=user).first()
        for geom_name in geom_names:
            layer_name = name_fac.create_private_layer_name(user_name=user.username, suffix=geom_name)
            layer_title = name_fac.create_layer_title(title=geom_name)
            schema = 'public'
            service.create_new_empty_vector_layer(workspace_id=workspace.wsid, layer_name_dirty=layer_name,
                                                  layer_title=layer_title, geometry_type=geom_name, schema=schema,
                                                  fields=[], published=True, category=category,
                                                  description='A {} layer created by {}'.format(geom_name,
                                                                                                user.username))
        ii += 1

    print 'Done creating private layers!\n'


def create_geometry_types():
    print 'Creating geometry types...'

    geom_types = ({'type': 'Point'}, {'type': 'MultiPoint'}, {'type': 'MultiLineString'}, {'type': 'MultiPolygon'},
                  {'type': 'LineString'},
                  {'type': 'Polygon'})
    for geom_type in geom_types:
        record = Geomtype(geomtype=geom_type['type'])
        db.session.add(record)
    db.session.commit()

    print 'Done creating geometry types!\n'


def create_attribute_types():
    print 'Creating attribute mapper...'

    attribute_types = ({'attributetype': 'String'}, {'attributetype': 'Integer'})

    for attr_type in attribute_types:
        record = Attributetype(attributetype=attr_type['attributetype'])
        db.session.add(record)
    db.session.commit()

    print 'Done creating attribute types!'


def create():
    create_db()
    insertCountries()
    publishCountries()
    createSuperAdmin()
    createCountryAdmin()
    createCountryUser()
    createWorkspace()
    createOrgAndWorkspace()
    createOrgAdmin()
    createSuperAdminPermission()
    createCountryAdminPermission()
    createCountryUserPermission()
    createOrgAdminPermission()
    createPublicWorkspacePermissions()
    create_private_workspaces_and_permissions()
    create_private_layers()
    create_geometry_types()
    create_attribute_types()
