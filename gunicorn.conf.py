import os

#create log directory
log_dir = os.path.join("logs")
if not os.path.exists(log_dir):
    os.makedirs(log_dir)

accesslog = "logs/access.log"
errorlog = "logs/error.log"
workers = 8
bind = '0.0.0.0:5000'
timeout = 120